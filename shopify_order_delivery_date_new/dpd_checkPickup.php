<?php
include $_SERVER['DOCUMENT_ROOT'] . '/database.php';

// Create connection
$conn = new mysqli($servername, $username, $password, $db);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$result = $conn->query('select * from shopify_order_delivery_date_custom limit 1');
if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
} else {
    $row = array();
}

$pre_first_delivery_date = $row['pre_first_delivery_date'];
$pre_tag = $row['pre_tag'];

$weekend_code = $row['dpd_weekday_code'];
$dpd_saturday_code = $row['dpd_saturday_code'];
$dpd_sunday_code = $row['dpd_sunday_code'];


$delivery_warning_message = $row['delivery_warning_message'];
if($weekend_code){
}else{
	$weekend_code = '2^12';
}
if($dpd_saturday_code){
}else{
	$dpd_saturday_code = '2^72';
}
if($dpd_sunday_code){
}else{
	$dpd_sunday_code = '2^75';
}

$booking_period = $row['booking_period'];
if($booking_period){
}else{
	$booking_period = 45;
}

$disable_dates = $row['disable_dates'];
if($disable_dates){}else{
	$disable_dates = [];
}
?>

$( function() {
		var datearr = disabled_datesArr.split(',');
		var dayarr = cDays.split(',');
		$('.custom_order_delivery_date_cal').datepicker({
			dateFormat: 'dd/mm/yy', 
			multiSelect: 999,
			firstDay: 1,
			minDate: new Date(nextDeliveryDate),
			maxDate: booking_period,
			beforeShowDay: function(date) {
			  var string = jQuery.datepicker.formatDate('dd/mm/yy', date);
			  if (contains(datearr,string)) {
				return [false, '']
			  } else {
				var day = date.getDay();
				if(string == '21/12/2020'){
					return [(day != sunday && day != tuesday && day != wednesday && day != thursday && day != friday && day != saturday), ''];
				}else{
					return [(day != sunday && day != monday && day != tuesday && day != wednesday && day != thursday && day != friday && day != saturday), ''];
				}
				
			  }
			  
			},
			onSelect: function(dates) {
				jQuery('.CDD_date').val(dates);
				localStorage.setItem("DeliveryDateNew", dates);
			}
		});
		
		function contains(a, obj) {
			var i = a.length;
			while (i--) {
			   if (a[i] === obj) {
				   return true;
			   }
			}
			return false;
		}
	});

	