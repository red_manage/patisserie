<?php
include $_SERVER['DOCUMENT_ROOT'] . '/database.php';

// Create connection
$conn = new mysqli($servername, $username, $password, $db);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$result = $conn->query('select * from shopify_order_delivery_date_custom limit 1');
if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
} else {
    $row = array();
}

$weekend_code = $row['dpd_weekday_code'];
$dpd_saturday_code = $row['dpd_saturday_code'];
$dpd_sunday_code = $row['dpd_sunday_code'];

$delivery_warning_message = $row['delivery_warning_message'];
if ($weekend_code) {
} else {
    $weekend_code = '2^12';
}
if ($dpd_saturday_code) {
} else {
    $dpd_saturday_code = '2^72';
}
if ($dpd_sunday_code) {
} else {
    $dpd_sunday_code = '2^75';
}

$booking_period = $row['booking_period'];
if ($booking_period) {
} else {
    $booking_period = 45;
}

$disable_dates = $row['disable_dates'];
if ($disable_dates) {
} else {
    $disable_dates = [];
}

$postalcode = str_replace(' ', '', $_GET['code']);
$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => "https://api.dpdlocal.co.uk/shipping/network/?deliveryDetails.address.countryCode=GB&deliveryDetails.address.postcode=" . $postalcode . "&collectionDetails.address.countryCode=GB&collectionDetails.address.postcode=SE12AT",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
        "Cookie: ak_bmsc=911A9C7FDB89B99A93A6C8EA6867D0710210B5979D5C000054D7A35ED5A19E24~plQ8zNInZXP2b1DN2jKH358zWkcFyGxQ5FBwGFhsFAWSjquB68aXzLFjmwlAvnA48uMOLYjKgMbk+4aVoOnnHHmUwbgquacdUHffnqc2+bT2EQ7dA84fTZrEn/IiaGhPoaFESsY9BAuEAW6kmXQvepRKv9snxq4JxfUXUTW8SRJrTOreW1AP1BmYQLtX/x9zeQIpXjsnsjHCudJuK5Y7WVBgp2AvYYQWxEU3UxIZKNZ0k=; bm_sv=EBE1C33B514758A92D5E92EB472769CD~O6MkmZ+2XpUl38v5RavcwxaJkb2ttK4MFTmwlIWIMU2O6+rUGyQQMA/qcvBEF6AxLgRer7U1VadYelq7STks+eDHhOFZPyTI9rseC72jIcL517uuCwQ/b34J8uqp90r0JEmmDOVGsMv8m08Zq86x/UoJrbKGf2u+cYV3huaMNEE="
    ),
));
$response = curl_exec($curl);
$c_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
curl_close($curl);
$jdata = json_decode($response);

$isServiceNextDay = false;
$isServiceSaturday = false;
$isServiceSunday = false;
if ($jdata->data) {
    foreach ($jdata->data as $data) {
        if ($data->service->serviceCode == $weekend_code) {
            $isServiceNextDay = true;
        }
        if ($data->network->networkCode == $dpd_saturday_code) {
            $isServiceSaturday = true;
        }
        if ($data->network->networkCode == $dpd_sunday_code) {
            $isServiceSunday = true;
        }
    }
}
date_default_timezone_set('Europe/London');
$date = date("d-m-Y H:i:s");

$isMidDay = 0;
if (date('H') >= 12) {
    //$isMidDay = 1;
}

$cut_off = $row['cut_off_time'];
$currenttime = date('H:i');
if (strtotime($currenttime) > strtotime($cut_off)) {
    $isMidDay = 1;
}
$booking_interval = $row['booking_interval'];
if ($booking_interval > 0) {
    $booking_interval = $booking_interval;
}

echo 'var booking_interval = "' . $booking_interval . '"; ';

function isWeekend($date)
{
    return (date('N', strtotime($date)) > 6);
}

echo 'var booking_period = "' . $booking_period . '"; ';
echo 'var disabled_datesArr = "' . $disable_dates . '"; ';

$saveddelivery_days = json_decode($row['delivery_days']);

$sunday = 1;
$monday = 2;
$tuesday = 3;
$wednesday = 4;
$thursday = 5;
$friday = 6;
$saturday = 7;

echo 'var cDays = "";';
$checkeddays = '';
$checkedAppdays = '';
if (!in_array('sunday', $saveddelivery_days)) {
    $checkeddays .= "0,";
    $checkedAppdays .= "1,";
    //$checkedAppdaysStirng .= "Sun,";
}
if (!in_array('monday', $saveddelivery_days)) {
    $checkeddays .= "1,";
    $checkedAppdays .= "1,";
    //$checkedAppdaysStirng .= "Mon,";
}
if (!in_array('tuesday', $saveddelivery_days)) {
    $checkeddays .= "2,";
    $checkedAppdays .= "3,";
    //$checkedAppdaysStirng .= "Tue,";
}
if (!in_array('wednesday', $saveddelivery_days)) {
    $checkeddays .= "3,";
    $checkedAppdays .= "4,";
    //$checkedAppdaysStirng .= "Wed,";
}
if (!in_array('thursday', $saveddelivery_days)) {
    $checkeddays .= "4,";
    $checkedAppdays .= "5,";
    //$checkedAppdaysStirng .= "Thu,";
}
if (!in_array('friday', $saveddelivery_days)) {
    $checkeddays .= "5,";
    $checkedAppdays .= "6,";
    //$checkedAppdaysStirng .= "Fri,";
}
if (!in_array('saturday', $saveddelivery_days)) {
    $checkeddays .= "6,";
    $checkedAppdays .= "7,";
    //$checkedAppdaysStirng .= "Sat,";
}

/*-----------------*/
$checkedAppdaysStirng = '';
if (in_array('sunday', $saveddelivery_days)) {
    $checkedAppdaysStirng .= "Sun,";
}
if (in_array('monday', $saveddelivery_days)) {
    $checkedAppdaysStirng .= "Mon,";
}
if (in_array('tuesday', $saveddelivery_days)) {
    $checkedAppdaysStirng .= "Tue,";
}
if (in_array('wednesday', $saveddelivery_days)) {
    $checkedAppdaysStirng .= "Wed,";
}
if (in_array('thursday', $saveddelivery_days)) {
    $checkedAppdaysStirng .= "Thu,";
}
if (in_array('friday', $saveddelivery_days)) {
    $checkeddays .= "5,";
    $checkedAppdays .= "6,";
    $checkedAppdaysStirng .= "Fri,";
}
if (in_array('saturday', $saveddelivery_days)) {
    $checkedAppdaysStirng .= "Sat,";
}

$appsaved_days = rtrim($checkedAppdays, ',');

$checkedAppdaysStirng = rtrim($checkedAppdaysStirng, ',');

//echo 'var issunday = "'.$isServiceSunday.'"; ';
//echo 'var isServiceSaturday = "'.$isServiceSaturday.'"; ';

$daysinweek = "Sun,Mon,Tue,Wed,Thu,Fri,Sat";

if ($isServiceSunday && $isServiceSaturday) {
    $dpd_delivery_days = "1,2,3,4,5,6,7";
    $dpd_delivery_days_S = "Sun,Mon,Tue,Wed,Thu,Fri,Sat";
    echo 'var delivery_days = "1,2,3,4,5,6,7"; ';
} elseif ($isServiceSunday) {
    $dpd_delivery_days = "1,2,3,4,5,6";
    $dpd_delivery_days_S = "Sun,Mon,Tue,Wed,Thu,Fri";
    echo 'var delivery_days = "1,2,3,4,5,6"; ';
} elseif ($isServiceSaturday) {
    $dpd_delivery_days = "2,3,4,5,6,7";
    $dpd_delivery_days_S = "Mon,Tue,Wed,Thu,Fri,Sat";
    echo 'var delivery_days = "2,3,4,5,6,7"; ';
} else {
    $dpd_delivery_days = "2,3,4,5,6";
    $dpd_delivery_days_S = "Mon,Tue,Wed,Thu,Fri";
    echo 'var delivery_days = "2,3,4,5,6"; ';
}


$dpd_delivery_days_SArr = explode(',', $dpd_delivery_days_S);
$checkedAppdays_SArr = explode(',', $checkedAppdaysStirng);
$daysinweekArr = explode(',', $daysinweek);
$disable_datesArr = explode(',', $disable_dates);

echo 'var test = "' . $dpd_delivery_days_S . '"; ';
echo 'var fsdfds = "' . $checkedAppdaysStirng . '"; ';

if ($isMidDay) {

    $checknxtdate = $booking_interval + 1;
    foreach ($daysinweekArr as $dayval) {
        $nxtdate = date('d/m/Y', strtotime(' +' . $checknxtdate . ' day'));
        if (in_array(date('D', strtotime(' +' . $checknxtdate . ' day')), $dpd_delivery_days_SArr) && in_array(date('D', strtotime(' +' . $checknxtdate . ' day')), $checkedAppdays_SArr) && !in_array($nxtdate, $disable_datesArr)) {
            echo ' var nextDate = "' . date('d/m/Y', strtotime(' +' . $checknxtdate . ' day')) . '"; ';
            echo 'var nextDeliveryDate = "' . date('d F Y', strtotime(' +' . $checknxtdate . ' day')) . '"; ';
            break;
        }
        $checknxtdate++;
    }

} else {

    $checknxtdate = $booking_interval;
    foreach ($daysinweekArr as $dayval) {
        $nxtdate = date('d/m/Y', strtotime(' +' . $checknxtdate . ' day'));
        if (in_array(date('D', strtotime(' +' . $checknxtdate . ' day')), $dpd_delivery_days_SArr) && in_array(date('D', strtotime(' +' . $checknxtdate . ' day')), $checkedAppdays_SArr) && !in_array($nxtdate, $disable_datesArr)) {
            echo ' var nextDate = "' . date('d/m/Y', strtotime(' +' . $checknxtdate . ' day')) . '"; ';
            echo 'var nextDeliveryDate = "' . date('d F Y', strtotime(' +' . $checknxtdate . ' day')) . '"; ';
            break;
        }
        $checknxtdate++;
    }

}


if ($isServiceNextDay || $isServiceSaturday || $isServiceSunday) {
    echo 'var checked = 1;';
} else {
    echo 'var checked = 0;';
}


echo "var sunday = 8;";
echo "var monday = 8;";
echo "var tuesday = 8;";
echo "var wednesday = 8;";
echo "var thursday = 8;";
echo "var friday = 8;";
echo "var saturday = 8;";

if (!in_array('sunday', $saveddelivery_days)) {
    echo "var sunday = 0;";
}
if (!in_array('monday', $saveddelivery_days)) {
    echo "var monday = 1;";
}
if (!in_array('tuesday', $saveddelivery_days)) {
    echo "var tuesday = 2;";
}
if (!in_array('wednesday', $saveddelivery_days)) {
    echo "var wednesday = 3;";
}
if (!in_array('thursday', $saveddelivery_days)) {
    echo "var thursday = 4;";
}
if (!in_array('friday', $saveddelivery_days)) {
    echo "var friday = 5;";
}
if (!in_array('saturday', $saveddelivery_days)) {
    echo "var saturday = 6;";
}

if (!$isServiceSaturday) {
    echo "var saturday = 6;";
}
if (!$isServiceSunday) {
    echo "var sunday = 0;";
}

$days = rtrim($checkeddays, ',');

echo 'cDays = "' . $days . '"';

?>

<?php
$variant_id = $_GET['variant_id'];
$product_id = $_GET['product_id'];

$products = explode(',', $product_id);
$variants = explode(',', $variant_id);

$data = array_combine($products, $variants);

$isUnavailable = false;
$popupHTML = '<div class="out_of_stock_Location_PopUp">';
$popupHTML .= '<p>We\'re sorry but the following item(s) aren\'t currently available via our Premier Delivery Service: </p>';

$remove = '';
if ($data) {
    $removeIds = array();
    foreach ($data as $product_id => $variant_id) {
        if ($product_id && $variant_id) {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com//admin/api/2020-04/products/" . $product_id . ".json",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "Cookie: __cfduid=d8e0f58645e9c4695bc749c2b4b9ac5f21590488150"
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            $results = json_decode($response);

            if (strpos(strtolower($results->product->tags), 'pat val premier') !== false) {

            } else {
                $isUnavailable = true;
                $popupHTML .= '' . $results->product->title . '<br/>';
            }
        }
    }
}

$popupHTML .= '<p></p>';

$popupHTML .= '<div class="cncaddressSec" style="margin-bottom: 30px;"><div class="cncaddressRight"><a href="javascript:void(0)" id="removePVPtags" class="btn btn--small-wide">Remove item(s)</a></div></div>';

?>

var isUnavailable = '<?php echo $isUnavailable; ?>';
jQuery('.cart__submit_custom_cnc').val('Check out');
if(isUnavailable == true){
var popup_html = '<?php echo addslashes($popupHTML); ?>';
jQuery('#cnc_outofstockmessage').html(popup_html);
jQuery('#cnc_outofstockmessage').show();
}

//jQuery('#checkdays').val(delivery_days);
//jQuery('#nextdeliverydate').val(nextDeliveryDate);

var delivery_text = '';

if(checked){
//jQuery('.identixweb-order-delivery').show();
jQuery('#order-delivery_shape_2').hide();
jQuery('.cart__buttons-container').show();
delivery_text = '<?php echo $delivery_warning_message; ?>';
}else{
jQuery('.identixweb-order-delivery').hide();
jQuery('#order-delivery_shape_2').show();
jQuery('.cart__buttons-container').hide();
delivery_text = 'We\'re sorry but we don’t currently delivery to your address. We may deliver to a friend\'s though';
}
jQuery('.template-cart .loading-image').hide();

jQuery('#custom_delivery_app').html('
<div class="app_content">
    <div class="delivery_text">'+delivery_text+'</div>
    <div class="custom_order_delivery_date_cal"></div>
</div>');

if(checked){
var i = 0;
var j = 0;
jQuery('.CDD_date').val(nextDate);
localStorage.setItem("DeliveryDateNew", nextDate);
$( function() {
var datearr = disabled_datesArr.split(',');
var dayarr = cDays.split(',');
$('.custom_order_delivery_date_cal').datepicker({
dateFormat: 'dd/mm/yy',
multiSelect: 999,
firstDay: 1,
minDate: new Date(nextDeliveryDate),
maxDate: booking_period,
beforeShowDay: function(date) {
var string = jQuery.datepicker.formatDate('dd/mm/yy', date);
if (contains(datearr,string)) {
return [false, '']
} else {
var day = date.getDay();
return [(day != sunday && day != monday && day != tuesday && day != wednesday && day != thursday && day != friday && day != saturday), ''];
}

},
onSelect: function(dates) {
jQuery('.CDD_date').val(dates);
localStorage.setItem("DeliveryDateNew", dates);
}
});

function contains(a, obj) {
var i = a.length;
while (i--) {
if (a[i] === obj) {
return true;
}
}
return false;
}
});
}

