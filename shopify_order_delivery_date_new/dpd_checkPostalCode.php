<?php
include $_SERVER['DOCUMENT_ROOT'] . '/database.php';

// Create connection
$conn = new mysqli($servername, $username, $password, $db);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$result = $conn->query('select * from shopify_order_delivery_date_custom limit 1');
if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
} else {
    $row = array();
}

$weekend_code = $row['dpd_weekday_code'];
$dpd_saturday_code = $row['dpd_saturday_code'];
$dpd_sunday_code = $row['dpd_sunday_code'];
if ($weekend_code) {
} else {
    $weekend_code = '2^12';
}
if ($dpd_saturday_code) {
} else {
    $dpd_saturday_code = '2^72';
}
if ($dpd_sunday_code) {
} else {
    $dpd_sunday_code = '2^75';
}

$postalcode = str_replace(' ', '', $_GET['code']);
$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => "https://api.dpdlocal.co.uk/shipping/network/?deliveryDetails.address.countryCode=GB&deliveryDetails.address.postcode=" . $postalcode . "&collectionDetails.address.countryCode=GB&collectionDetails.address.postcode=SE12AT",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
        "Cookie: ak_bmsc=911A9C7FDB89B99A93A6C8EA6867D0710210B5979D5C000054D7A35ED5A19E24~plQ8zNInZXP2b1DN2jKH358zWkcFyGxQ5FBwGFhsFAWSjquB68aXzLFjmwlAvnA48uMOLYjKgMbk+4aVoOnnHHmUwbgquacdUHffnqc2+bT2EQ7dA84fTZrEn/IiaGhPoaFESsY9BAuEAW6kmXQvepRKv9snxq4JxfUXUTW8SRJrTOreW1AP1BmYQLtX/x9zeQIpXjsnsjHCudJuK5Y7WVBgp2AvYYQWxEU3UxIZKNZ0k=; bm_sv=EBE1C33B514758A92D5E92EB472769CD~O6MkmZ+2XpUl38v5RavcwxaJkb2ttK4MFTmwlIWIMU2O6+rUGyQQMA/qcvBEF6AxLgRer7U1VadYelq7STks+eDHhOFZPyTI9rseC72jIcL517uuCwQ/b34J8uqp90r0JEmmDOVGsMv8m08Zq86x/UoJrbKGf2u+cYV3huaMNEE="
    ),
));
$response = curl_exec($curl);
curl_close($curl);
$jdata = json_decode($response);
//file_put_contents('postalcode.json', $jdata);
//echo '<pre>';
//print_r($jdata);
//exit;
$isServiceNextDay = false;
$isServiceSaturday = false;
$isServiceSunday = false;
if ($jdata->data) {
    foreach ($jdata->data as $data) {
        if ($data->service->serviceCode == $weekend_code) {
            $isServiceNextDay = true;
        }
        if ($data->network->networkCode == $dpd_saturday_code) {
            $isServiceSaturday = true;
        }
        if ($data->network->networkCode == $dpd_sunday_code) {
            $isServiceSunday = true;
        }
    }
}
date_default_timezone_set('Europe/London');
$date = date("d-m-Y H:i:s");

$isMidDay = 0;
if (date('H') >= 12) {
    //$isMidDay = 1;
}

$cut_off = $row['cut_off_time'];
$currenttime = date('H:i');
if (strtotime($currenttime) > strtotime($cut_off)) {
    $isMidDay = 1;
}
$booking_interval = $row['booking_interval'];
if ($booking_interval > 0) {
    $booking_interval = $booking_interval;
}

$disable_dates = $row['disable_dates'];
if ($disable_dates) {
} else {
    $disable_dates = [];
}


function isWeekend($date)
{
    return (date('N', strtotime($date)) > 6);
}

$saveddelivery_days = json_decode($row['delivery_days']);

$checkedAppdaysStirng = '';
if (in_array('sunday', $saveddelivery_days)) {
    $checkedAppdaysStirng .= "Sun,";
}
if (in_array('monday', $saveddelivery_days)) {
    $checkedAppdaysStirng .= "Mon,";
}
if (in_array('tuesday', $saveddelivery_days)) {
    $checkedAppdaysStirng .= "Tue,";
}
if (in_array('wednesday', $saveddelivery_days)) {
    $checkedAppdaysStirng .= "Wed,";
}
if (in_array('thursday', $saveddelivery_days)) {
    $checkedAppdaysStirng .= "Thu,";
}
if (in_array('friday', $saveddelivery_days)) {
    $checkeddays .= "5,";
    $checkedAppdays .= "6,";
    $checkedAppdaysStirng .= "Fri,";
}
if (in_array('saturday', $saveddelivery_days)) {
    $checkedAppdaysStirng .= "Sat,";
}

$appsaved_days = rtrim($checkedAppdays, ',');

$checkedAppdaysStirng = rtrim($checkedAppdaysStirng, ',');

//echo 'var issunday = "'.$isServiceSunday.'"; ';
//echo 'var isServiceSaturday = "'.$isServiceSaturday.'"; ';

$daysinweek = "Sun,Mon,Tue,Wed,Thu,Fri,Sat";

if ($isServiceSunday && $isServiceSaturday) {
    $dpd_delivery_days = "1,2,3,4,5,6,7";
    $dpd_delivery_days_S = "Sun,Mon,Tue,Wed,Thu,Fri,Sat";
    echo 'var delivery_days = "1,2,3,4,5,6,7"; ';
} elseif ($isServiceSunday) {
    $dpd_delivery_days = "1,2,3,4,5,6";
    $dpd_delivery_days_S = "Sun,Mon,Tue,Wed,Thu,Fri";
    echo 'var delivery_days = "1,2,3,4,5,6"; ';
} elseif ($isServiceSaturday) {
    $dpd_delivery_days = "2,3,4,5,6,7";
    $dpd_delivery_days_S = "Mon,Tue,Wed,Thu,Fri,Sat";
    echo 'var delivery_days = "2,3,4,5,6,7"; ';
} else {
    $dpd_delivery_days = "2,3,4,5,6";
    $dpd_delivery_days_S = "Mon,Tue,Wed,Thu,Fri";
    echo 'var delivery_days = "2,3,4,5,6"; ';
}

$dpd_delivery_days_SArr = explode(',', $dpd_delivery_days_S);
$checkedAppdays_SArr = explode(',', $checkedAppdaysStirng);
$daysinweekArr = explode(',', $daysinweek);
$disable_datesArr = explode(',', $disable_dates);


echo 'var ukDateTime = "' . $date . '"; ';
echo 'var postalcode = "' . $_GET['code'] . '"; ';

if ($isMidDay) {
    $checknxtdate = $booking_interval + 1;
    foreach ($daysinweekArr as $dayval) {
        $nxtdate = date('d/m/Y', strtotime(' +' . $checknxtdate . ' day'));
        if (in_array(date('D', strtotime(' +' . $checknxtdate . ' day')), $dpd_delivery_days_SArr) && in_array(date('D', strtotime(' +' . $checknxtdate . ' day')), $checkedAppdays_SArr) && !in_array($nxtdate, $disable_datesArr)) {
            echo 'var nextDeliveryDate = "' . date('d F Y', strtotime(' +' . $checknxtdate . ' day')) . '"; ';
            break;
        }
        $checknxtdate++;
    }

} else {

    $checknxtdate = $booking_interval;
    foreach ($daysinweekArr as $dayval) {
        $nxtdate = date('d/m/Y', strtotime(' +' . $checknxtdate . ' day'));
        if (in_array(date('D', strtotime(' +' . $checknxtdate . ' day')), $dpd_delivery_days_SArr) && in_array(date('D', strtotime(' +' . $checknxtdate . ' day')), $checkedAppdays_SArr) && !in_array($nxtdate, $disable_datesArr)) {
            echo 'var nextDeliveryDate = "' . date('d F Y', strtotime(' +' . $checknxtdate . ' day')) . '"; ';
            break;
        }
        $checknxtdate++;
    }

}

//echo 'var issunday = "'.$isServiceSunday.'"; ';
//echo 'var isServiceSaturday = "'.$isServiceSaturday.'"; ';
if ($isServiceSunday && $isServiceSaturday) {
    echo 'var delivery_days = "1,2,3,4,5,6,7"; ';
} elseif ($isServiceSunday) {
    echo 'var delivery_days = "1,2,3,4,5,6"; ';
} elseif ($isServiceSaturday) {
    echo 'var delivery_days = "2,3,4,5,6,7"; ';
} else {
    echo 'var delivery_days = "2,3,4,5,6"; ';
}

if ($isServiceNextDay) {
    echo 'var checked = 1;';
} else {
    echo 'var checked = 0;';
}
//echo 'sdfs';
//echo "var checked;";
//echo "var checked = ".$isService;
?>
jQuery('#checkdays').val(delivery_days);
jQuery('#nextdeliverydate').val(nextDeliveryDate);

