<?php
include $_SERVER['DOCUMENT_ROOT'] . '/database.php';

// Create connection
$conn = new mysqli($servername, $username, $password, $db);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$result = $conn->query('select * from shopify_order_delivery_date_custom limit 1');
if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
} else {
    $row = array();
}

$pre_first_delivery_date = $row['pre_first_delivery_date'];
$pre_tag = $row['pre_tag'];

$weekend_code = $row['dpd_weekday_code'];
$dpd_saturday_code = $row['dpd_saturday_code'];
$dpd_sunday_code = $row['dpd_sunday_code'];



$delivery_warning_message = $row['delivery_warning_message'];
if($weekend_code){
}else{
	$weekend_code = '2^12';
}
if($dpd_saturday_code){
}else{
	$dpd_saturday_code = '2^72';
}
if($dpd_sunday_code){
}else{
	$dpd_sunday_code = '2^75';
}

$booking_period = $row['booking_period'];
if($booking_period){
}else{
	$booking_period = 45;
}

$disable_dates = $row['disable_dates'];
if($disable_dates){}else{
	$disable_dates = [];
}

$postalcode = str_replace(' ', '',$_GET['code']);
$isInCart_christmas = $_GET['isInCart_christmas'];


$notDeliveryPostalcodeArray = array(
	"FK17","FK18","FK19","FK20","FK21",
	"HS1","HS2","HS3","HS4","HS5","HS6","HS7","HS8","HS9",
	"KA27","KA28",
	"KW0","KW1","KW2","KW3","KW4","KW5","KW6","KW7","KW8","KW9","KW10","KW11","KW12","KW13","KW14","KW15","KW16","KW17",
	"ZE1","ZE2","ZE3"
);

$AB31to56 = array();
for($i=31; $i<=56; $i++){
	if($i != 39){
		array_push($notDeliveryPostalcodeArray,"AB".$i);
	}
}

$IV1to63 = array();
for($i=1; $i<=63; $i++){
	array_push($notDeliveryPostalcodeArray,"IV".$i);
}

$PA20to78 = array();
for($i=20; $i<=78; $i++){
	array_push($notDeliveryPostalcodeArray,"PA".$i);
}

$PH15to50 = array();
for($i=15; $i<=50; $i++){
	array_push($notDeliveryPostalcodeArray,"PH".$i);
}
echo 'var notDeliveryPostalcode = "0"; ';
if(in_array($postalcode, $notDeliveryPostalcodeArray)){
	echo 'var notDeliveryPostalcode = "1"; ';
}

if(substr(strtoupper($postalcode), 0, 2 ) === "BT" || substr(strtoupper($postalcode), 0, 2 ) === "JE" || substr(strtoupper($postalcode), 0, 2 ) === "GY"){
	echo 'var notDeliveryPostalcode = "1"; ';
}

if(strtoupper($postalcode) === "BS329BT" || strtoupper($postalcode) === "B762BT" || strtoupper($postalcode) === "BS161BT" || strtoupper($postalcode) === "BS495BT" || strtoupper($postalcode) === "BR28BT" || strtoupper($postalcode) === "B109BT"){
	echo 'var notDeliveryPostalcode = "1"; ';
}

foreach ($notDeliveryPostalcodeArray as $pc) {
	if (strpos(strtoupper($postalcode), strtoupper($pc)) !== false) {
	    echo 'var notDeliveryPostalcode = "1"; ';
	    break;
	}
}

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.dpdlocal.co.uk/shipping/network/?deliveryDetails.address.countryCode=GB&deliveryDetails.address.postcode=".$postalcode."&collectionDetails.address.countryCode=GB&collectionDetails.address.postcode=SE12AT",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "Cookie: ak_bmsc=911A9C7FDB89B99A93A6C8EA6867D0710210B5979D5C000054D7A35ED5A19E24~plQ8zNInZXP2b1DN2jKH358zWkcFyGxQ5FBwGFhsFAWSjquB68aXzLFjmwlAvnA48uMOLYjKgMbk+4aVoOnnHHmUwbgquacdUHffnqc2+bT2EQ7dA84fTZrEn/IiaGhPoaFESsY9BAuEAW6kmXQvepRKv9snxq4JxfUXUTW8SRJrTOreW1AP1BmYQLtX/x9zeQIpXjsnsjHCudJuK5Y7WVBgp2AvYYQWxEU3UxIZKNZ0k=; bm_sv=EBE1C33B514758A92D5E92EB472769CD~O6MkmZ+2XpUl38v5RavcwxaJkb2ttK4MFTmwlIWIMU2O6+rUGyQQMA/qcvBEF6AxLgRer7U1VadYelq7STks+eDHhOFZPyTI9rseC72jIcL517uuCwQ/b34J8uqp90r0JEmmDOVGsMv8m08Zq86x/UoJrbKGf2u+cYV3huaMNEE="
  ),
));
$response = curl_exec($curl);
$c_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
curl_close($curl);
$jdata =  json_decode($response);
//file_put_contents('postalcode.json', $jdata);
//echo '<pre>';
//print_r($jdata);
//exit;
$isServiceNextDay = false;
$isServiceSaturday = false;
$isServiceSunday = false;
if($jdata->data){
	foreach($jdata->data as $data){
		if($data->service->serviceCode == $weekend_code){
			$isServiceNextDay = true;
		}
		if($data->network->networkCode == $dpd_saturday_code){
			$isServiceSaturday = true;
		}
		if($data->network->networkCode == $dpd_sunday_code){
			$isServiceSunday = true;
		}
	}
}
date_default_timezone_set('Europe/London');
$date = date("d-m-Y H:i:s"); 

$isMidDay = 0;
if (date('H') >= 12) {
  //$isMidDay = 1;
}

$cut_off = $row['cut_off_time'];
$currenttime = date('H:i');
if(strtotime($currenttime) > strtotime($cut_off)){
	$isMidDay = 1;
}
$booking_interval = $row['booking_interval'];
if($booking_interval > 0){
	$booking_interval = $booking_interval;
}

echo 'var booking_interval = "'.$booking_interval.'"; ';

function isWeekend($date) {
    return (date('N', strtotime($date)) > 6);
}


//$date = new DateTime("2020-12-31");
//$now = new DateTime();

//$chrismasLastDate = $date->diff($now)->format("%d");

//echo 'var ukDateTime = "'.$date.'"; ';
//echo 'var postalcode = "'.$_GET['code'].'"; ';
echo 'var booking_period = "'.$booking_period.'"; ';
echo 'var disabled_datesArr = "'.$disable_dates.'"; ';

//echo 'var chrismasLastDate = "'.$chrismasLastDate.'"; ';
echo 'var isInCart_christmas = '.$isInCart_christmas.';';

$saveddelivery_days = json_decode($row['delivery_days']);


$sunday = 1;
$monday = 2;
$tuesday = 3;
$wednesday = 4;
$thursday = 5;
$friday = 6;
$saturday = 7;

echo 'var cDays = "";';
$checkeddays = '';
$checkedAppdays = '';
if(!in_array('sunday',$saveddelivery_days)){
	$checkeddays .= "0,";
	$checkedAppdays .= "1,";
	//$checkedAppdaysStirng .= "Sun,";
}
if(!in_array('monday',$saveddelivery_days)){
	$checkeddays .= "1,";
	$checkedAppdays .= "1,";
	//$checkedAppdaysStirng .= "Mon,";
}
if(!in_array('tuesday',$saveddelivery_days)){
	$checkeddays .= "2,";
	$checkedAppdays .= "3,";
	//$checkedAppdaysStirng .= "Tue,";
}
if(!in_array('wednesday',$saveddelivery_days)){
	$checkeddays .= "3,";
	$checkedAppdays .= "4,";
	//$checkedAppdaysStirng .= "Wed,";
}
if(!in_array('thursday',$saveddelivery_days)){
	$checkeddays .= "4,";
	$checkedAppdays .= "5,";
	//$checkedAppdaysStirng .= "Thu,";
}
if(!in_array('friday',$saveddelivery_days)){
	$checkeddays .= "5,";
	$checkedAppdays .= "6,";
	//$checkedAppdaysStirng .= "Fri,";
}
if(!in_array('saturday',$saveddelivery_days)){
	$checkeddays .= "6,";
	$checkedAppdays .= "7,";
	//$checkedAppdaysStirng .= "Sat,";
}

/*-----------------*/
$checkedAppdaysStirng = '';
if(in_array('sunday',$saveddelivery_days)){
	$checkedAppdaysStirng .= "Sun,";
}
if(in_array('monday',$saveddelivery_days)){
	$checkedAppdaysStirng .= "Mon,";
}
if(in_array('tuesday',$saveddelivery_days)){
	$checkedAppdaysStirng .= "Tue,";
}
if(in_array('wednesday',$saveddelivery_days)){
	$checkedAppdaysStirng .= "Wed,";
}
if(in_array('thursday',$saveddelivery_days)){
	$checkedAppdaysStirng .= "Thu,";
}
if(in_array('friday',$saveddelivery_days)){
	$checkeddays .= "5,";
	$checkedAppdays .= "6,";
	$checkedAppdaysStirng .= "Fri,";
}
if(in_array('saturday',$saveddelivery_days)){
	$checkedAppdaysStirng .= "Sat,";
}

$appsaved_days = rtrim($checkedAppdays, ',');

$checkedAppdaysStirng = rtrim($checkedAppdaysStirng, ',');

//echo 'var issunday = "'.$isServiceSunday.'"; ';
//echo 'var isServiceSaturday = "'.$isServiceSaturday.'"; ';

$daysinweek = "Sun,Mon,Tue,Wed,Thu,Fri,Sat";

if($isServiceSunday && $isServiceSaturday){
	$dpd_delivery_days = "1,2,3,4,5,6,7";
	$dpd_delivery_days_S = "Sun,Mon,Tue,Wed,Thu,Fri,Sat";
	echo 'var delivery_days = "1,2,3,4,5,6,7"; ';
}elseif($isServiceSunday){
	$dpd_delivery_days = "1,2,3,4,5,6";
	$dpd_delivery_days_S = "Sun,Mon,Tue,Wed,Thu,Fri";
	echo 'var delivery_days = "1,2,3,4,5,6"; ';
}elseif($isServiceSaturday){
	$dpd_delivery_days = "2,3,4,5,6,7";
	$dpd_delivery_days_S = "Mon,Tue,Wed,Thu,Fri,Sat";
	echo 'var delivery_days = "2,3,4,5,6,7"; ';
}else{
	$dpd_delivery_days = "2,3,4,5,6";
	$dpd_delivery_days_S = "Mon,Tue,Wed,Thu,Fri";
	echo 'var delivery_days = "2,3,4,5,6"; ';
	//echo 'var delivery_days = "1,3,4,5,6,7"; ';
}



$dpd_delivery_days_SArr = explode(',', $dpd_delivery_days_S);
$checkedAppdays_SArr = explode(',', $checkedAppdaysStirng);
$daysinweekArr = explode(',', $daysinweek);
$disable_datesArr = explode(',', $disable_dates);
		
echo 'var test = "'.$dpd_delivery_days_S.'"; ';
echo 'var fsdfds = "'.$checkedAppdaysStirng.'"; ';

if($isMidDay){
	
	$checknxtdate = $booking_interval+1;
	foreach($daysinweekArr as $dayval){
		$nxtdate = date('d/m/Y', strtotime(' +'.$checknxtdate.' day'));
		if(in_array(date('D', strtotime(' +'.$checknxtdate.' day')) , $dpd_delivery_days_SArr) && in_array(date('D', strtotime(' +'.$checknxtdate.' day')) , $checkedAppdays_SArr) && !in_array($nxtdate, $disable_datesArr )){
			echo ' var nextDate = "'.date('d/m/Y', strtotime(' +'.$checknxtdate.' day')).'"; ';
			echo 'var nextDeliveryDate = "'.date('d F Y', strtotime(' +'.$checknxtdate.' day')).'"; ';
			break;
		}
		$checknxtdate++;
	}
		
}else{

	$checknxtdate = $booking_interval;
	foreach($daysinweekArr as $dayval){
		$nxtdate = date('d/m/Y', strtotime(' +'.$checknxtdate.' day'));
		if(in_array(date('D', strtotime(' +'.$checknxtdate.' day')) , $dpd_delivery_days_SArr) && in_array(date('D', strtotime(' +'.$checknxtdate.' day')) , $checkedAppdays_SArr) && !in_array($nxtdate, $disable_datesArr )){
			echo ' var nextDate = "'.date('d/m/Y', strtotime(' +'.$checknxtdate.' day')).'"; ';
			echo 'var nextDeliveryDate = "'.date('d F Y', strtotime(' +'.$checknxtdate.' day')).'"; ';
			break;
		}
		$checknxtdate++;
	}
	
}



if($isServiceNextDay || $isServiceSaturday || $isServiceSunday){
	echo 'var checked = 1;';
}else{
	echo 'var checked = 0;';
}




echo "var sunday = 8;";
echo "var monday = 8;";
echo "var tuesday = 8;";
echo "var wednesday = 8;";
echo "var thursday = 8;";
echo "var friday = 8;";
echo "var saturday = 8;";

if(!in_array('sunday',$saveddelivery_days)){
	echo "var sunday = 0;";
}
if(!in_array('monday',$saveddelivery_days)){
	echo "var monday = 1;";
}
if(!in_array('tuesday',$saveddelivery_days)){
	echo "var tuesday = 2;";
}
if(!in_array('wednesday',$saveddelivery_days)){
	echo "var wednesday = 3;";
}
if(!in_array('thursday',$saveddelivery_days)){
	echo "var thursday = 4;";
}
if(!in_array('friday',$saveddelivery_days)){
	echo "var friday = 5;";
}
if(!in_array('saturday',$saveddelivery_days)){
	echo "var saturday = 6;";
}

if(!$isServiceSaturday){
	echo "var saturday = 6;";
}
if(!$isServiceSunday){
	echo "var sunday = 0;";
}

$days = rtrim($checkeddays, ',');

echo 'cDays = "'.$days.'"; ';


//echo 'sdfs';
//echo "var checked;";
//echo "var checked = ".$isService;
echo 'var lhp = "0"; ';
?>

<?php
$variant_id = $_GET['variant_id'];
$product_id = $_GET['product_id'];

$products = explode(',',$product_id);
$variants = explode(',',$variant_id);

$data = array_combine($products, $variants);

$isUnavailable = false;
$popupHTML = '<div class="out_of_stock_Location_PopUp">';
$popupHTML .= '<p>We\'re sorry but the following item(s) aren\'t currently available via our Premier Delivery Service: </p>';

$remove = '';
if($data){
	$removeIds = array();
	foreach($data as $product_id => $variant_id){
		if($product_id && $variant_id){
			$curl = curl_init();
			curl_setopt_array($curl, array(
			CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com//admin/api/2020-04/products/".$product_id.".json",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"Cookie: __cfduid=d8e0f58645e9c4695bc749c2b4b9ac5f21590488150"
			),
			));

			$response = curl_exec($curl);

			curl_close($curl);
			$results = json_decode($response);
			//echo '<pre>';
			//print_r($results);
			//exit;
			//foreach($results->product as $product){
			
				if($product_id == '4590656749635' || $product_id == '4590662647875' || $product_id == '4590666022979'){
						//echo ' var nextDate = "'.date('d/m/Y', strtotime('2020-12-15')).'"; ';
						//echo 'var nextDeliveryDate = "'.date('d F Y', strtotime('2020-12-15')).'"; ';
                }
                if($product_id == '6545469735093'){
						if(date('Y-m-d') >= date('2021-03-24')){
							$now = time();
							$your_date = strtotime("2021-04-04");
							$datediff =  $your_date - $now;
							$diff = round($datediff / (60 * 60 * 24));
							$diff = $diff+2;
							echo 'var datediff = "asdfsad"; ';

							if(date('Y-m-d', strtotime(' +'.$checknxtdate.' day')) <= date('2021-03-23')){
	                			echo ' var nextDate = "'.date('d/m/Y', strtotime('2021-03-23')).'"; ';
	                			echo 'var nextDeliveryDate = "'.date('d F Y', strtotime('2021-03-23')).'"; ';
	                		}else{
	                			echo ' var nextDate = "'.date('d/m/Y', strtotime(' +'.$checknxtdate.' day')).'"; ';
	                			echo 'var nextDeliveryDate = "'.date('d F Y', strtotime(' +'.$checknxtdate.' day')).'"; ';
	                		}

							echo 'var booking_period = "'.$diff.'"; ';

							if(date('Y-m-d', strtotime(' +'.$checknxtdate.' day')) > date('2021-04-04')){
								echo 'var lhp = "1"; ';
							}

						}else{
							echo ' var nextDate = "'.date('d/m/Y', strtotime(' +'.$checknxtdate.' day')).'"; ';
							echo 'var nextDeliveryDate = "'.date('d F Y', strtotime(' +'.$checknxtdate.' day')).'"; ';
							echo 'var booking_period = "0"; ';
							echo 'var lhp = "1"; ';
						}
						break;
                }
                if($product_id == '6320532226229'){
                	if(date('Y-m-d') < date('2021-03-14')){
						//echo ' var nextDate = "'.date('d/m/Y', strtotime('2021-02-14')).'"; ';

						$now = time();
						$your_date = strtotime("2021-03-14");
						$datediff =  $your_date - $now;
						$diff = round($datediff / (60 * 60 * 24));
						$diff = $diff+1;
						echo 'var datediff = "'.date('Y-m-d').'"; ';

						if(date('Y-m-d', strtotime(' +'.$checknxtdate.' day')) <= date('2021-03-09')){
                			echo ' var nextDate = "'.date('d/m/Y', strtotime('2021-03-09')).'"; ';
                			echo 'var nextDeliveryDate = "'.date('d F Y', strtotime('2021-03-09')).'"; ';
                		}else{
                			echo ' var nextDate = "'.date('d/m/Y', strtotime(' +'.$checknxtdate.' day')).'"; ';
                			echo 'var nextDeliveryDate = "'.date('d F Y', strtotime(' +'.$checknxtdate.' day')).'"; ';
                		}

						echo 'var booking_period = "'.$diff.'"; ';

						if(date('Y-m-d', strtotime(' +'.$checknxtdate.' day')) > date('2021-03-14')){
							echo 'var lhp = "1"; ';
						}
						}else{
							echo ' var nextDate = "'.date('d/m/Y', strtotime(' +'.$checknxtdate.' day')).'"; ';
							echo 'var nextDeliveryDate = "'.date('d F Y', strtotime(' +'.$checknxtdate.' day')).'"; ';
							echo 'var booking_period = "0"; ';
							echo 'var lhp = "1"; ';
						}
					break;
                }
				elseif (strpos(strtolower($results->product->tags), strtolower($pre_tag)) !== false) {
					echo ' var nextDate = "'.date('d/m/Y', strtotime($pre_first_delivery_date)).'"; ';
					echo 'var nextDeliveryDate = "'.date('d F Y', strtotime($pre_first_delivery_date)).'"; ';
				}elseif (strpos(strtolower($results->product->tags), 'pat val premier') !== false) {
					
				}elseif (strpos(strtolower($results->product->tags), 'click and collect') !== false) {
					$isUnavailable = true;
					$popupHTML .= ''.$results->product->title.'<br/>';
				}
		}
	}
}

$popupHTML .= '<p></p>';

$popupHTML .= '<div class="cncaddressSec" style="margin-bottom: 30px;"><div class="cncaddressRight"><a href="javascript:void(0)" id="removePVPtags" class="btn btn--small-wide">Remove item(s)</a></div></div>';

?>

var isUnavailable = '<?php echo $isUnavailable; ?>';
jQuery('.cart__submit_custom_cnc').val('Check out');
if(isUnavailable == true){
	var popup_html = '<?php echo addslashes($popupHTML); ?>';
	jQuery('#cnc_outofstockmessage').html(popup_html);
	jQuery('#cnc_outofstockmessage').show();
}

//jQuery('#checkdays').val(delivery_days);
//jQuery('#nextdeliverydate').val(nextDeliveryDate);

var delivery_text = '';



if(nextDeliveryDate){

	//jQuery('.identixweb-order-delivery').show();
	jQuery('#order-delivery_shape_2').hide();
	//jQuery('.cart__buttons-container').show();
	delivery_text = '<?php echo $delivery_warning_message; ?>';

	if(lhp == '1'){
		jQuery('.cart__buttons-container').hide();
	}
}else{
	jQuery('.identixweb-order-delivery').hide();
	jQuery('#order-delivery_shape_2').show();
	jQuery('.cart__buttons-container').hide();
	delivery_text = 'We\'re sorry but we don’t currently delivery to your address. We may deliver to a friend\'s though';
}
//jQuery('.template-cart .loading-image').hide();

jQuery('#custom_delivery_app').html('<div class="app_content"><div class="delivery_text">'+delivery_text+'</div><div class="custom_order_delivery_date_cal"></div></div>');

if(nextDeliveryDate){
	var i = 0;
	var j = 0;
	jQuery('.CDD_date').val(nextDate);
	localStorage.setItem("DeliveryDateNew", nextDate);

	console.log(isInCart_christmas);
	if(isInCart_christmas === true){
		booking_period = new Date('2020-12-31');
	}
	$( function() {
		var datearr = disabled_datesArr.split(',');
		var dayarr = cDays.split(',');
		$('.custom_order_delivery_date_cal').datepicker({
			dateFormat: 'dd/mm/yy', 
			multiSelect: 999,
			firstDay: 1,
			minDate: new Date(nextDeliveryDate),
			maxDate: booking_period,
			beforeShowDay: function(date) {
			  var string = jQuery.datepicker.formatDate('dd/mm/yy', date);
			  if (contains(datearr,string)) {
				return [false, '']
			  } else {
				var day = date.getDay();
				if(string == '21/12/2020'){
					return [(day != sunday && day != tuesday && day != wednesday && day != thursday && day != friday && day != saturday), ''];
				}else{
					return [(day != sunday && day != monday && day != tuesday && day != wednesday && day != thursday && day != friday && day != saturday), ''];
				}
				
			  }
			  
			},
			onSelect: function(dates) {
				jQuery('.CDD_date').val(dates);
				localStorage.setItem("DeliveryDateNew", dates);
			}
		});
		
		function contains(a, obj) {
			var i = a.length;
			while (i--) {
			   if (a[i] === obj) {
				   return true;
			   }
			}
			return false;
		}
	});
}

if(notDeliveryPostalcode == "1"){
	jQuery('.identixweb-order-delivery').hide();
	jQuery('#order-delivery_shape_2').show();
	jQuery('.cart__buttons-container').hide();
	delivery_text = 'Unfortunately Premier Delivery is not available in your area';
	jQuery('#custom_delivery_app').html('<div class="app_content"><div class="delivery_text">'+delivery_text+'</div><div class="custom_order_delivery_date_cal"></div></div>');
	
}

