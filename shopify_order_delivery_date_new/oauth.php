<?php 

// Set variables for our request
$shop = "pat-val-premier";
$api_key = "7650e9cd3e7bb99a7461fed0949069e8";
$scopes = "read_orders,write_products";
$redirect_uri = "https://matthews42.sg-host.com/shopify_order_delivery_date_new/index.php";

// Build install/approval URL to redirect to
$install_url = "https://" . $shop . ".myshopify.com/admin/oauth/authorize?client_id=" . $api_key . "&scope=" . $scopes . "&redirect_uri=" . urlencode($redirect_uri);

// Redirect
header("Location: " . $install_url);
die();

?>