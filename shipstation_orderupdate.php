<?php

header('Content-Type: application/json');
$data = file_get_contents('php://input');

$result = json_decode($data);

//file_put_contents('shipstation_orderId.php', print_r($result, true));


$getOrderUrl = $result->resource_url;

$getCurl = curl_init();

curl_setopt_array($getCurl, array(
  CURLOPT_URL => $getOrderUrl,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "Host: ssapi.shipstation.com",
    "Authorization: Basic OTc3MmI5NmEyZWQ2NDE2MjhiNGIxMjI2OGI3MjQzYWM6MDJhMTlhNjIwOTA3NDEwNzgzZjllZmM1MTk1ZDE3YzA="
  ),
));

$response = curl_exec($getCurl);

curl_close($getCurl);


$resultData = json_decode($response);

$requestData = json_decode($response);


//file_put_contents('shipstation_orderData.php', print_r($resultData, true));


$resultCount = $resultData->total;
echo "resultCount: " . $resultCount . "\n ";

for ($x = 0; $x <= $resultCount-1; $x++) {

  $customerNotes = $resultData->orders[$x]->customerNotes;

  echo  "\n " . $x . ": " . $customerNotes . "\n";


  if (is_null($customerNotes)) {
    //do nothing
  } else {

    $date = substr($customerNotes, 20, 10);
    $formattedDate = DateTime::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    $dateBefore = date('Y-m-d', (strtotime('-1 day', strtotime($formattedDate))));

    // echo $dateBefore;

    $orderId = $resultData->orders[$x]->orderId;

    $requestData->orders[$x]->shipbydate = $formattedDate;
    $requestData->orders[$x]->holdUntilDate = $dateString;
    $requestData->orders[$x]->advancedOptions->billToParty = null;
    // $requestData->orders[$x]->advancedOptions->billToMyOtherAccount = 'my_other_account';
    $requestDataJson = json_encode($requestData->orders[$x]);

    // echo $requestDataJson;

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://ssapi.shipstation.com/orders/createorder",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $requestDataJson,
      CURLOPT_HTTPHEADER => array(
        "Authorization: Basic OTc3MmI5NmEyZWQ2NDE2MjhiNGIxMjI2OGI3MjQzYWM6MDJhMTlhNjIwOTA3NDEwNzgzZjllZmM1MTk1ZDE3YzA=",
        "Content-Type: application/json"
      ),
    ));

    $updateResponse = curl_exec($curl);

    curl_close($curl);

    // $resultData = json_decode($updateResponse);

    // file_put_contents('shipstation_updatedData.php', print_r($resultData, true));


    $curlToHold = curl_init();

    curl_setopt_array($curlToHold, array(
      CURLOPT_URL => "https://ssapi.shipstation.com/orders/holduntil",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "{\n  \"orderId\": " . $orderId . ",\n  \"holdUntilDate\": \"" . $dateBefore . "\"\n}",
      CURLOPT_HTTPHEADER => array(
        "Authorization: Basic OTc3MmI5NmEyZWQ2NDE2MjhiNGIxMjI2OGI3MjQzYWM6MDJhMTlhNjIwOTA3NDEwNzgzZjllZmM1MTk1ZDE3YzA=",
        "Content-Type: application/json"
      ),
    ));

    $response = curl_exec($curlToHold);

    curl_close($curlToHold);

    echo " \n Processed order: " . $orderId;
  }
}
