<?php 

// Set variables for our request
$shop = "pat-val-premier";
$api_key = "a3e7a5ad68a9c1e02b2145707c5a16ee";
$scopes = "read_orders,write_products";
$redirect_uri = "https://matthews42.sg-host.com/shopify_shipstation_app_new/index.php";

// Build install/approval URL to redirect to
$install_url = "https://" . $shop . ".myshopify.com/admin/oauth/authorize?client_id=" . $api_key . "&scope=" . $scopes . "&redirect_uri=" . urlencode($redirect_uri);

// Redirect
header("Location: " . $install_url);
die();

?>