stdClass Object
(
    [orders] => Array
        (
            [0] => stdClass Object
                (
                    [orderId] => 375321796
                    [orderNumber] => ORD000251753
                    [orderKey] => 4273150197941
                    [orderDate] => 2021-12-15T07:41:25.0000000
                    [createDate] => 2021-12-15T09:36:08.6930000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T07:41:25.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423745
                    [customerUsername] => 5915777269941
                    [customerEmail] => lwrmr@aol.com
                    [billTo] => stdClass Object
                        (
                            [name] => maureen lawrie
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => maureen lawrie
                            [company] => 
                            [street1] => 19 Willow Drive
                            [street2] => 
                            [street3] => 
                            [city] => Milton of Campsie
                            [state] => SCT
                            [postalCode] => G66 8DY
                            [country] => GB
                            [phone] => 07592 887085
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521380
                                    [lineItemKey] => 10814620303541
                                    [sku] => 
                                    [name] => Afternoon Tea In Store Experience Gift Card
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/afternoon-tea-in-store-experience-gift-card-807455.png?v=1637920073
                                    [weight] => stdClass Object
                                        (
                                            [value] => 0
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 25
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => To
                                                    [value] => Joanne
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => From
                                                    [value] => Mum and Dad
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Message
                                                    [value] => Wee extra for Xmas.  Luv xx
                                                )

                                        )

                                    [productId] => 
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:08.63
                                    [modifyDate] => 2021-12-15T09:36:08.63
                                )

                        )

                    [orderTotal] => 27.95
                    [amountPaid] => 27.95
                    [taxAmount] => 0
                    [shippingAmount] => 2.95
                    [customerNotes] => <br/>Delivery Date: null
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Standard
                    [carrierCode] => 
                    [serviceCode] => 
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 0
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [1] => stdClass Object
                (
                    [orderId] => 375321793
                    [orderNumber] => ORD000251754
                    [orderKey] => 4273154031797
                    [orderDate] => 2021-12-15T07:44:25.0000000
                    [createDate] => 2021-12-15T09:36:08.4570000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T07:44:25.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423748
                    [customerUsername] => 5915777368245
                    [customerEmail] => mimimi418@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Michelle Hung
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Michelle Hung
                            [company] => 
                            [street1] => 122a, Bramcote Lane, Bramcote Lane, Bramcote Lane
                            [street2] => Bramcote Lane
                            [street3] => 
                            [city] => Wollaton
                            [state] => ENG
                            [postalCode] => NG8 2QG
                            [country] => GB
                            [phone] => 07594 611492
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521373
                                    [lineItemKey] => 10814629085365
                                    [sku] => P00002S
                                    [name] => Pink Candy Stripe Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/pink-candy-stripe-gateau-857827.jpg?v=1617979646
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 32.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => Inscription (£3.95)
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Inscription_options
                                                    [value] => Happy Birthday Ellie
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [6] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                            [7] => stdClass Object
                                                (
                                                    [name] => _io_order_group
                                                    [value] => 1639582433803
                                                )

                                        )

                                    [productId] => 6552798
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:08.35
                                    [modifyDate] => 2021-12-15T09:36:08.35
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616521374
                                    [lineItemKey] => 10814629118133
                                    [sku] => P00020S
                                    [name] => Inscription
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/inscription-995841.jpg?v=1638365792
                                    [weight] => stdClass Object
                                        (
                                            [value] => 1
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 3.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => _io_parent_order_group
                                                    [value] => 1639582433803
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => _io_field_name
                                                    [value] => Inscription
                                                )

                                        )

                                    [productId] => 6760417
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:08.35
                                    [modifyDate] => 2021-12-15T09:36:08.35
                                )

                        )

                    [orderTotal] => 41.85
                    [amountPaid] => 41.85
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 18/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10001
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [2] => stdClass Object
                (
                    [orderId] => 375321788
                    [orderNumber] => ORD000251755
                    [orderKey] => 4273154556085
                    [orderDate] => 2021-12-15T07:44:53.0000000
                    [createDate] => 2021-12-15T09:36:08.0370000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T07:44:53.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423763
                    [customerUsername] => 5915779563701
                    [customerEmail] => sveta.bodolica@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Svetlana Bodolica
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Svetlana Bodolica
                            [company] => 
                            [street1] => 1 McVicars Lane
                            [street2] => 
                            [street3] => 
                            [city] => Dundee
                            [state] => SCT
                            [postalCode] => DD1 4LH
                            [country] => GB
                            [phone] => 07838 642044
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521362
                                    [lineItemKey] => 10814629839029
                                    [sku] => P00498S
                                    [name] => Classic Fruits of the Forest Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/classic-fruits-of-the-forest-gateau-137144.jpg?v=1617976980
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 32.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 12990279
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:07.957
                                    [modifyDate] => 2021-12-15T09:36:07.957
                                )

                        )

                    [orderTotal] => 37.9
                    [amountPaid] => 37.9
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 23/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [3] => stdClass Object
                (
                    [orderId] => 375321784
                    [orderNumber] => ORD000251756
                    [orderKey] => 4273155145909
                    [orderDate] => 2021-12-15T07:45:25.0000000
                    [createDate] => 2021-12-15T09:36:07.5530000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T07:45:25.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 162265918
                    [customerUsername] => 5385164226741
                    [customerEmail] => nithyahm@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Vinod Kumar Changarangath
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Vinod Kumar Changarangath 
                            [company] => 
                            [street1] => 15, Archers walk,  Trent Vale 
                            [street2] => 
                            [street3] => 
                            [city] => Stoke on Trent, Staffordshire
                            [state] => ENG
                            [postalCode] => ST4 6JT
                            [country] => GB
                            [phone] => +44 7476 198254
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521357
                                    [lineItemKey] => 10814630658229
                                    [sku] => MPAT03
                                    [name] => Madame Valerie's Afternoon Tea
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/madame-valeries-afternoon-tea-179347.jpg?v=1631128275
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 25
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 27539343
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:07.46
                                    [modifyDate] => 2021-12-15T09:36:07.46
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616521358
                                    [lineItemKey] => discount
                                    [sku] => 
                                    [name] => Coupon:  WELCOME01
                                    [imageUrl] => http://images.shipstation.com/resource/1.gif
                                    [weight] => stdClass Object
                                        (
                                            [value] => 0
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => -4.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 
                                    [fulfillmentSku] => 
                                    [adjustment] => 1
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:07.46
                                    [modifyDate] => 2021-12-15T09:36:07.46
                                )

                        )

                    [orderTotal] => 25
                    [amountPaid] => 25
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 18/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [4] => stdClass Object
                (
                    [orderId] => 375321779
                    [orderNumber] => ORD000251758
                    [orderKey] => 4273156489397
                    [orderDate] => 2021-12-15T07:46:21.0000000
                    [createDate] => 2021-12-15T09:36:07.2400000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T07:46:21.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 123690747
                    [customerUsername] => 4663673684149
                    [customerEmail] => somikathapa18@hotmail.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => Somika Thapa
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Sabhyata Pun
                            [company] => 
                            [street1] => 2 Juniper Close, Blunsdon
                            [street2] => Blunsdon
                            [street3] => 
                            [city] => Swindon
                            [state] => ENG
                            [postalCode] => SN26 8AH
                            [country] => GB
                            [phone] => 07918 857873
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521351
                                    [lineItemKey] => 10814633541813
                                    [sku] => P00085S
                                    [name] => Blue Candy Stripe Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/blue-candy-stripe-gateau-110290.jpg?v=1617976990
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 32.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => Inscription (£3.95)
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Inscription_options
                                                    [value] => Happy birthday Sabbu! xxx
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [6] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                            [7] => stdClass Object
                                                (
                                                    [name] => _io_order_group
                                                    [value] => 1639560821711
                                                )

                                        )

                                    [productId] => 8773924
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:06.91
                                    [modifyDate] => 2021-12-15T09:36:06.91
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616521352
                                    [lineItemKey] => 10814633574581
                                    [sku] => P00020S
                                    [name] => Inscription
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/inscription-995841.jpg?v=1638365792
                                    [weight] => stdClass Object
                                        (
                                            [value] => 1
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 3.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => _io_parent_order_group
                                                    [value] => 1639560821711
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => _io_field_name
                                                    [value] => Inscription
                                                )

                                        )

                                    [productId] => 6760417
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:06.91
                                    [modifyDate] => 2021-12-15T09:36:06.91
                                )

                            [2] => stdClass Object
                                (
                                    [orderItemId] => 616521353
                                    [lineItemKey] => discount
                                    [sku] => 
                                    [name] => Coupon:  WELCOME01
                                    [imageUrl] => http://images.shipstation.com/resource/1.gif
                                    [weight] => stdClass Object
                                        (
                                            [value] => 0
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => -4.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 
                                    [fulfillmentSku] => 
                                    [adjustment] => 1
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:06.91
                                    [modifyDate] => 2021-12-15T09:36:06.91
                                )

                        )

                    [orderTotal] => 36.9
                    [amountPaid] => 36.9
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 17/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10001
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [5] => stdClass Object
                (
                    [orderId] => 375321767
                    [orderNumber] => ORD000251760
                    [orderKey] => 4273164878005
                    [orderDate] => 2021-12-15T07:54:06.0000000
                    [createDate] => 2021-12-15T09:36:06.3470000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T07:54:06.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423739
                    [customerUsername] => 5915790442677
                    [customerEmail] => jordan_telles@outlook.com
                    [billTo] => stdClass Object
                        (
                            [name] => Jordan Telles
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Jordan Telles
                            [company] => 
                            [street1] => 31 Forest Road
                            [street2] => 
                            [street3] => 
                            [city] => LONDON
                            [state] => ENG
                            [postalCode] => N9 8RU
                            [country] => GB
                            [phone] => 07932 024495
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521336
                                    [lineItemKey] => 10814650220725
                                    [sku] => P00030S
                                    [name] => Fairytale Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/fairytale-gateau-493921.jpg?v=1617976981
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 32.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => Inscription (£3.95)
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Inscription_options
                                                    [value] => Happy Birthday Natalie
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [6] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                            [7] => stdClass Object
                                                (
                                                    [name] => _io_order_group
                                                    [value] => 1639583310759
                                                )

                                        )

                                    [productId] => 7759241
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:06.207
                                    [modifyDate] => 2021-12-15T09:36:06.207
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616521337
                                    [lineItemKey] => 10814650253493
                                    [sku] => P00020S
                                    [name] => Inscription
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/inscription-995841.jpg?v=1638365792
                                    [weight] => stdClass Object
                                        (
                                            [value] => 1
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 3.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => _io_parent_order_group
                                                    [value] => 1639583310759
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => _io_field_name
                                                    [value] => Inscription
                                                )

                                        )

                                    [productId] => 6760417
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:06.207
                                    [modifyDate] => 2021-12-15T09:36:06.207
                                )

                        )

                    [orderTotal] => 41.85
                    [amountPaid] => 41.85
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 21/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10001
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [6] => stdClass Object
                (
                    [orderId] => 375321762
                    [orderNumber] => ORD000251761
                    [orderKey] => 4273166188725
                    [orderDate] => 2021-12-15T07:55:12.0000000
                    [createDate] => 2021-12-15T09:36:05.9700000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T07:55:12.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423755
                    [customerUsername] => 5915792343221
                    [customerEmail] => Paul@danslowsigns.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => Paul Danslow
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Rosemary l Greenshields 
                            [company] => 
                            [street1] => 53 Ellingham Road 
                            [street2] => 
                            [street3] => 
                            [city] => Hemel Hempstead 
                            [state] => ENG
                            [postalCode] => HP2 5LL
                            [country] => GB
                            [phone] => 07870 676269
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521327
                                    [lineItemKey] => 10814652678325
                                    [sku] => P00501S
                                    [name] => Chocolate Orange Ombre - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/chocolate-orange-ombre-188899.jpg?v=1617976990
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 42.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => Inscription (£3.95)
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Inscription_options
                                                    [value] => Happy 40th Birthday Nikki
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [6] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                            [7] => stdClass Object
                                                (
                                                    [name] => _io_order_group
                                                    [value] => 1639582659169
                                                )

                                        )

                                    [productId] => 15345812
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:05.877
                                    [modifyDate] => 2021-12-15T09:36:05.877
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616521328
                                    [lineItemKey] => 10814652711093
                                    [sku] => P00020S
                                    [name] => Inscription
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/inscription-995841.jpg?v=1638365792
                                    [weight] => stdClass Object
                                        (
                                            [value] => 1
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 3.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => _io_parent_order_group
                                                    [value] => 1639582659169
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => _io_field_name
                                                    [value] => Inscription
                                                )

                                        )

                                    [productId] => 6760417
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:05.877
                                    [modifyDate] => 2021-12-15T09:36:05.877
                                )

                        )

                    [orderTotal] => 51.85
                    [amountPaid] => 51.85
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 17/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10001
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [7] => stdClass Object
                (
                    [orderId] => 375321759
                    [orderNumber] => ORD000251762
                    [orderKey] => 4273170677941
                    [orderDate] => 2021-12-15T07:59:00.0000000
                    [createDate] => 2021-12-15T09:36:05.7200000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T07:59:00.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423752
                    [customerUsername] => 2997547171907
                    [customerEmail] => prayatna@hotmail.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => Prayatna Dewan
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Prayatna Dewan
                            [company] => 
                            [street1] => 53 Romney Road
                            [street2] => Willesborough
                            [street3] => 
                            [city] => Ashford
                            [state] => ENG
                            [postalCode] => TN24 0RR
                            [country] => GB
                            [phone] => 07715 529747
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521324
                                    [lineItemKey] => 10814661820597
                                    [sku] => P00018S
                                    [name] => Black Forest Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/black-forest-gateau-242674.jpg?v=1617976990
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 29.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 6676602
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:05.673
                                    [modifyDate] => 2021-12-15T09:36:05.673
                                )

                        )

                    [orderTotal] => 34.9
                    [amountPaid] => 34.9
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 17/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [8] => stdClass Object
                (
                    [orderId] => 375321756
                    [orderNumber] => ORD000251763
                    [orderKey] => 4273174315189
                    [orderDate] => 2021-12-15T08:01:52.0000000
                    [createDate] => 2021-12-15T09:36:05.4870000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:01:52.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 175757896
                    [customerUsername] => 5533321232565
                    [customerEmail] => elaineallan@allabouttraining.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => Elaine Allan
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Elaine Allan
                            [company] => 
                            [street1] => Graceland
                            [street2] => Kirklinton 
                            [street3] => 
                            [city] => Carlisle
                            [state] => ENG
                            [postalCode] => CA6 6BZ
                            [country] => GB
                            [phone] => 0191 438 4702
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521320
                                    [lineItemKey] => 10814669422773
                                    [sku] => 
                                    [name] => Seasonal Patisserie Box
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/autumn-patisserie-box-179383.jpg?v=1631128275
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 29.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:05.423
                                    [modifyDate] => 2021-12-15T09:36:05.423
                                )

                        )

                    [orderTotal] => 34.9
                    [amountPaid] => 34.9
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 24/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [9] => stdClass Object
                (
                    [orderId] => 375321751
                    [orderNumber] => ORD000251765
                    [orderKey] => 4273178706101
                    [orderDate] => 2021-12-15T08:05:01.0000000
                    [createDate] => 2021-12-15T09:36:05.1500000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:05:01.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423756
                    [customerUsername] => 5915791982773
                    [customerEmail] => rs@whitmanandco.com
                    [billTo] => stdClass Object
                        (
                            [name] => Russell Savage
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Russell Savage 
                            [company] => 
                            [street1] => 17 Fishers Lane 
                            [street2] => 
                            [street3] => 
                            [city] => London 
                            [state] => ENG
                            [postalCode] => W4 1RX
                            [country] => GB
                            [phone] => 07768 843271
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521315
                                    [lineItemKey] => 10814677450933
                                    [sku] => P00019S
                                    [name] => Black Forest Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/black-forest-gateau-242674.jpg?v=1617976990
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 39.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 6816438
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:05.04
                                    [modifyDate] => 2021-12-15T09:36:05.04
                                )

                        )

                    [orderTotal] => 44.9
                    [amountPaid] => 44.9
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 24/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [10] => stdClass Object
                (
                    [orderId] => 375321745
                    [orderNumber] => ORD000251766
                    [orderKey] => 4273183686837
                    [orderDate] => 2021-12-15T08:09:01.0000000
                    [createDate] => 2021-12-15T09:36:04.8370000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:09:01.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423744
                    [customerUsername] => 5915788705973
                    [customerEmail] => leon.natalie@hotmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Natalie Leon Ribas
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Matthew Winger
                            [company] => 
                            [street1] => 6 Tourmaline Drive
                            [street2] => 
                            [street3] => 
                            [city] => Sittingbourne
                            [state] => ENG
                            [postalCode] => ME10 5TA
                            [country] => GB
                            [phone] => 07546 325867
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521305
                                    [lineItemKey] => 10814686953653
                                    [sku] => P00583S
                                    [name] => Peanut Butter Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/peanut-butter-gateau-126813.jpg?v=1631128276
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 32.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => Inscription (£3.95)
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Inscription_options
                                                    [value] => Happy Birthday Matty!! 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [6] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                            [7] => stdClass Object
                                                (
                                                    [name] => _io_order_group
                                                    [value] => 1639511720655
                                                )

                                        )

                                    [productId] => 25859259
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:04.76
                                    [modifyDate] => 2021-12-15T09:36:04.76
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616521306
                                    [lineItemKey] => 10814686986421
                                    [sku] => P00020S
                                    [name] => Inscription
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/inscription-995841.jpg?v=1638365792
                                    [weight] => stdClass Object
                                        (
                                            [value] => 1
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 3.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => _io_parent_order_group
                                                    [value] => 1639511720655
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => _io_field_name
                                                    [value] => Inscription
                                                )

                                        )

                                    [productId] => 6760417
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:04.76
                                    [modifyDate] => 2021-12-15T09:36:04.76
                                )

                        )

                    [orderTotal] => 41.85
                    [amountPaid] => 41.85
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 17/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10001
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [11] => stdClass Object
                (
                    [orderId] => 375321736
                    [orderNumber] => ORD000251767
                    [orderKey] => 4273184342197
                    [orderDate] => 2021-12-15T08:09:28.0000000
                    [createDate] => 2021-12-15T09:36:04.3170000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:09:28.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423736
                    [customerUsername] => 5915810529461
                    [customerEmail] => jasen.cavalli@marklanedesigns.com
                    [billTo] => stdClass Object
                        (
                            [name] => Jasen Cavalli
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Jasen Cavalli
                            [company] => 
                            [street1] => Bramling
                            [street2] => The Orchard
                            [street3] => 
                            [city] => Canterbury
                            [state] => ENG
                            [postalCode] => CT3 1NB
                            [country] => GB
                            [phone] => 07801 357819
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521290
                                    [lineItemKey] => 10814688100533
                                    [sku] => P00549S-1
                                    [name] => Caramel Biscuit Cake - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/caramel-biscuit-cake-327684.jpg?v=1617976981
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 29.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 18670757
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:04.24
                                    [modifyDate] => 2021-12-15T09:36:04.24
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616521291
                                    [lineItemKey] => 10814688133301
                                    [sku] => P00008S
                                    [name] => Tiramisu Cake - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/tiramisu-gateau-183196.jpg?v=1617803921
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 29.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 6820300
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:04.24
                                    [modifyDate] => 2021-12-15T09:36:04.24
                                )

                        )

                    [orderTotal] => 64.85
                    [amountPaid] => 64.85
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 17/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 20000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [12] => stdClass Object
                (
                    [orderId] => 375321731
                    [orderNumber] => ORD000251768
                    [orderKey] => 4273191092405
                    [orderDate] => 2021-12-15T08:13:24.0000000
                    [createDate] => 2021-12-15T09:36:03.9830000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:13:24.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423747
                    [customerUsername] => 5915816067253
                    [customerEmail] => homebabes2020@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Michelle Goulding
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Michelle Goulding
                            [company] => 
                            [street1] => 2 WELGATE CLOSE
                            [street2] => 
                            [street3] => 
                            [city] => Mattishall
                            [state] => ENG
                            [postalCode] => NR20 3NR
                            [country] => GB
                            [phone] => +447842331086
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521284
                                    [lineItemKey] => 10814699012277
                                    [sku] => P00485S
                                    [name] => Classic Strawberry Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/classic-strawberry-gateau-428982.jpg?v=1617979867
                                    [weight] => stdClass Object
                                        (
                                            [value] => 1000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 32.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 11432469
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:03.933
                                    [modifyDate] => 2021-12-15T09:36:03.933
                                )

                        )

                    [orderTotal] => 37.9
                    [amountPaid] => 37.9
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 24/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => dpd_local_package
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 1000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 1
                            [width] => 1
                            [height] => 1
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => 580111
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [13] => stdClass Object
                (
                    [orderId] => 375321727
                    [orderNumber] => ORD000251770
                    [orderKey] => 4273199644853
                    [orderDate] => 2021-12-15T08:20:14.0000000
                    [createDate] => 2021-12-15T09:36:03.7300000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:20:14.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423760
                    [customerUsername] => 5915812167861
                    [customerEmail] => y.sivaprasad@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Siva Yellapantulu
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Siva Yellapantulu
                            [company] => 
                            [street1] => 146 Mayfly Road
                            [street2] => 
                            [street3] => 
                            [city] => Northampton
                            [state] => ENG
                            [postalCode] => NN4 9EQ
                            [country] => GB
                            [phone] => 07584 421826
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521279
                                    [lineItemKey] => 10814715461813
                                    [sku] => P00018S
                                    [name] => Black Forest Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/black-forest-gateau-242674.jpg?v=1617976990
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 29.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => Inscription (£3.95)
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Inscription_options
                                                    [value] => Happy Birthday Divya
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [6] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                            [7] => stdClass Object
                                                (
                                                    [name] => _io_order_group
                                                    [value] => 1639584977061
                                                )

                                        )

                                    [productId] => 6676602
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:03.557
                                    [modifyDate] => 2021-12-15T09:36:03.557
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616521280
                                    [lineItemKey] => 10814715494581
                                    [sku] => P00020S
                                    [name] => Inscription
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/inscription-995841.jpg?v=1638365792
                                    [weight] => stdClass Object
                                        (
                                            [value] => 1
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 3.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => _io_parent_order_group
                                                    [value] => 1639584977061
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => _io_field_name
                                                    [value] => Inscription
                                                )

                                        )

                                    [productId] => 6760417
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:03.557
                                    [modifyDate] => 2021-12-15T09:36:03.557
                                )

                            [2] => stdClass Object
                                (
                                    [orderItemId] => 616521281
                                    [lineItemKey] => discount
                                    [sku] => 
                                    [name] => Coupon:  WELCOME-9C81-4BU3WI
                                    [imageUrl] => http://images.shipstation.com/resource/1.gif
                                    [weight] => stdClass Object
                                        (
                                            [value] => 0
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => -4.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 
                                    [fulfillmentSku] => 
                                    [adjustment] => 1
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:03.557
                                    [modifyDate] => 2021-12-15T09:36:03.557
                                )

                        )

                    [orderTotal] => 33.9
                    [amountPaid] => 33.9
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 17/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10001
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [14] => stdClass Object
                (
                    [orderId] => 375321716
                    [orderNumber] => ORD000251771
                    [orderKey] => 4273203282101
                    [orderDate] => 2021-12-15T08:22:58.0000000
                    [createDate] => 2021-12-15T09:36:03.3170000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:22:58.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 105058153
                    [customerUsername] => 4523301798069
                    [customerEmail] => kolaamarilda@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Amarilda Kola
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Amarilda Kola
                            [company] => 
                            [street1] => 91
                            [street2] => Kempton Avenue
                            [street3] => 
                            [city] => Hornchurch
                            [state] => ENG
                            [postalCode] => RM12 6EB
                            [country] => GB
                            [phone] => 07440 029529
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521260
                                    [lineItemKey] => 10814723424437
                                    [sku] => P00577S
                                    [name] => White Chocolate & Mixed Fruits Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/white-chocolate-mixed-fruits-gateau-919575.jpg?v=1627580968
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 42.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                            [6] => stdClass Object
                                                (
                                                    [name] => Cake Topper_Options
                                                    [value] => 
                                                )

                                            [7] => stdClass Object
                                                (
                                                    [name] => Cake Topper_options
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 24736938
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:03.157
                                    [modifyDate] => 2021-12-15T09:36:03.157
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616521262
                                    [lineItemKey] => 10814723457205
                                    [sku] => P00020S
                                    [name] => Inscription
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/inscription-995841.jpg?v=1638365792
                                    [weight] => stdClass Object
                                        (
                                            [value] => 1
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 3.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6760417
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:03.157
                                    [modifyDate] => 2021-12-15T09:36:03.157
                                )

                            [2] => stdClass Object
                                (
                                    [orderItemId] => 616521263
                                    [lineItemKey] => discount
                                    [sku] => 
                                    [name] => Coupon:  WELCOME01
                                    [imageUrl] => http://images.shipstation.com/resource/1.gif
                                    [weight] => stdClass Object
                                        (
                                            [value] => 0
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => -9.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 
                                    [fulfillmentSku] => 
                                    [adjustment] => 1
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:03.157
                                    [modifyDate] => 2021-12-15T09:36:03.157
                                )

                        )

                    [orderTotal] => 46.9
                    [amountPaid] => 46.9
                    [taxAmount] => 0
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 18/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Pat Val Premier Delivery
                    [carrierCode] => 
                    [serviceCode] => 
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10001
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [15] => stdClass Object
                (
                    [orderId] => 375321703
                    [orderNumber] => ORD000251772
                    [orderKey] => 4273204461749
                    [orderDate] => 2021-12-15T08:23:38.0000000
                    [createDate] => 2021-12-15T09:36:02.5800000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:23:38.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423768
                    [customerUsername] => 5915828289717
                    [customerEmail] => zahramohammed1995@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Zahra Mohammed
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Zahra Mohammed
                            [company] => 
                            [street1] => 26 Westcombe Avenue
                            [street2] => 
                            [street3] => 
                            [city] => Leeds
                            [state] => ENG
                            [postalCode] => LS8 2BS
                            [country] => GB
                            [phone] => 07968 510133
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521248
                                    [lineItemKey] => 10814725292213
                                    [sku] => P00549S-1
                                    [name] => Caramel Biscuit Cake - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/caramel-biscuit-cake-327684.jpg?v=1617976981
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 29.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 18670757
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:02.457
                                    [modifyDate] => 2021-12-15T09:36:02.457
                                )

                        )

                    [orderTotal] => 34.9
                    [amountPaid] => 34.9
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 23/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [16] => stdClass Object
                (
                    [orderId] => 375321695
                    [orderNumber] => ORD000251774
                    [orderKey] => 4273209606325
                    [orderDate] => 2021-12-15T08:27:15.0000000
                    [createDate] => 2021-12-15T09:36:02.2500000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:27:15.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423757
                    [customerUsername] => 5913660358837
                    [customerEmail] => sadafhq@aol.com
                    [billTo] => stdClass Object
                        (
                            [name] => Sadaf Callaghan
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Sadaf Callaghan
                            [company] => 
                            [street1] => 47 Naunton Crescent
                            [street2] => 
                            [street3] => 
                            [city] => Cheltenham
                            [state] => ENG
                            [postalCode] => GL53 7BD
                            [country] => GB
                            [phone] => 07939 046028
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521237
                                    [lineItemKey] => 10814735024309
                                    [sku] => 
                                    [name] => Classic Chocolate Yule Log - 8" - 10"
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/classic-chocolate-yule-log-310390.jpg?v=1637313525
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 42.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                            [6] => stdClass Object
                                                (
                                                    [name] => Christmas Cake Topper
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:01.967
                                    [modifyDate] => 2021-12-15T09:36:01.967
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616521238
                                    [lineItemKey] => 10814735057077
                                    [sku] => P00563S
                                    [name] => Blueberry & Raspberry Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/blueberry-raspberry-gateau-830405.jpg?v=1618236519
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 42.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 21461392
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:02.187
                                    [modifyDate] => 2021-12-15T09:36:02.187
                                )

                            [2] => stdClass Object
                                (
                                    [orderItemId] => 616521239
                                    [lineItemKey] => discount
                                    [sku] => 
                                    [name] => Coupon:  WELCOME01
                                    [imageUrl] => http://images.shipstation.com/resource/1.gif
                                    [weight] => stdClass Object
                                        (
                                            [value] => 0
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => -4.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 
                                    [fulfillmentSku] => 
                                    [adjustment] => 1
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:02.187
                                    [modifyDate] => 2021-12-15T09:36:02.187
                                )

                        )

                    [orderTotal] => 85.9
                    [amountPaid] => 85.9
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 21/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 20000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [17] => stdClass Object
                (
                    [orderId] => 375321689
                    [orderNumber] => ORD000251775
                    [orderKey] => 4273210523829
                    [orderDate] => 2021-12-15T08:27:47.0000000
                    [createDate] => 2021-12-15T09:36:01.7630000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:27:47.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423731
                    [customerUsername] => 5915832025269
                    [customerEmail] => emily.aryee@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => emily abraham
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => emily abraham
                            [company] => 
                            [street1] => 6 Banbury walk , Northolt
                            [street2] => 
                            [street3] => 
                            [city] => Northolt
                            [state] => ENG
                            [postalCode] => UB5 6TJ
                            [country] => GB
                            [phone] => 07961 451325
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521229
                                    [lineItemKey] => 10814736859317
                                    [sku] => P00570S
                                    [name] => Charlotte Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/charlotte-gateau-875224.jpg?v=1620918849
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 42.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 22496505
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:01.687
                                    [modifyDate] => 2021-12-15T09:36:01.687
                                )

                        )

                    [orderTotal] => 47.9
                    [amountPaid] => 47.9
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 17/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [18] => stdClass Object
                (
                    [orderId] => 375321683
                    [orderNumber] => ORD000251776
                    [orderKey] => 4273210818741
                    [orderDate] => 2021-12-15T08:27:59.0000000
                    [createDate] => 2021-12-15T09:36:01.4670000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:27:59.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 186508989
                    [customerUsername] => 5722555547829
                    [customerEmail] => jennifer.tsang67@icloud.com
                    [billTo] => stdClass Object
                        (
                            [name] => Jennifer Tsang
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Jennifer Tsang
                            [company] => 
                            [street1] => 27 valerian garden 
                            [street2] => 
                            [street3] => 
                            [city] => Soham
                            [state] => ENG
                            [postalCode] => CB7 5WR
                            [country] => GB
                            [phone] => 07787 558810
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521224
                                    [lineItemKey] => 10814737318069
                                    [sku] => 
                                    [name] => Classic Chocolate Yule Log - 8" - 10"
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/classic-chocolate-yule-log-310390.jpg?v=1637313525
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 42.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                            [6] => stdClass Object
                                                (
                                                    [name] => Christmas Cake Topper
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:01.357
                                    [modifyDate] => 2021-12-15T09:36:01.357
                                )

                        )

                    [orderTotal] => 47.9
                    [amountPaid] => 47.9
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 24/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [19] => stdClass Object
                (
                    [orderId] => 375321674
                    [orderNumber] => ORD000251777
                    [orderKey] => 4273212588213
                    [orderDate] => 2021-12-15T08:29:28.0000000
                    [createDate] => 2021-12-15T09:36:01.1670000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:29:28.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423750
                    [customerUsername] => 5915825832117
                    [customerEmail] => mondal.partha@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Partha Mondal
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Partha Mondal
                            [company] => 
                            [street1] => 215 London Road
                            [street2] => 
                            [street3] => 
                            [city] => Slough
                            [state] => ENG
                            [postalCode] => SL3 7JN
                            [country] => GB
                            [phone] => 07730 092344
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521213
                                    [lineItemKey] => 10814740660405
                                    [sku] => P00566S
                                    [name] => Chocolate & Hazelnut Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/chocolate-hazelnut-gateau-924972.jpg?v=1618236521
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 32.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => Inscription (£3.95)
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Inscription_options
                                                    [value] => Happy 15 Birthday Aneesh
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [6] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                            [7] => stdClass Object
                                                (
                                                    [name] => _io_order_group
                                                    [value] => 1639585315879
                                                )

                                        )

                                    [productId] => 21444789
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:01.04
                                    [modifyDate] => 2021-12-15T09:36:01.04
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616521214
                                    [lineItemKey] => 10814740693173
                                    [sku] => P00020S
                                    [name] => Inscription
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/inscription-995841.jpg?v=1638365792
                                    [weight] => stdClass Object
                                        (
                                            [value] => 1
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 3.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => _io_parent_order_group
                                                    [value] => 1639585315879
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => _io_field_name
                                                    [value] => Inscription
                                                )

                                        )

                                    [productId] => 6760417
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:01.04
                                    [modifyDate] => 2021-12-15T09:36:01.04
                                )

                            [2] => stdClass Object
                                (
                                    [orderItemId] => 616521215
                                    [lineItemKey] => discount
                                    [sku] => 
                                    [name] => Coupon:  WELCOME-F7C4-4BP5PU
                                    [imageUrl] => http://images.shipstation.com/resource/1.gif
                                    [weight] => stdClass Object
                                        (
                                            [value] => 0
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => -4.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 
                                    [fulfillmentSku] => 
                                    [adjustment] => 1
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:01.04
                                    [modifyDate] => 2021-12-15T09:36:01.04
                                )

                        )

                    [orderTotal] => 36.9
                    [amountPaid] => 36.9
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 19/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10001
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [20] => stdClass Object
                (
                    [orderId] => 375321670
                    [orderNumber] => ORD000251778
                    [orderKey] => 4273212752053
                    [orderDate] => 2021-12-15T08:29:34.0000000
                    [createDate] => 2021-12-15T09:36:00.8330000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:29:34.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423733
                    [customerUsername] => 5915834089653
                    [customerEmail] => zahid_h_@hotmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Henna Zahid
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Henna Zahid
                            [company] => 
                            [street1] => 123 Boundary Road, Walthamstow
                            [street2] => Walthamstow
                            [street3] => 
                            [city] => London
                            [state] => ENG
                            [postalCode] => E17 8NQ
                            [country] => GB
                            [phone] => 07473120029
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521209
                                    [lineItemKey] => 10814741053621
                                    [sku] => P00485S
                                    [name] => Classic Strawberry Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/classic-strawberry-gateau-428982.jpg?v=1617979867
                                    [weight] => stdClass Object
                                        (
                                            [value] => 1000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 32.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => Inscription (£3.95)
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Inscription_options
                                                    [value] => llongyfarchiadau Hamzah!
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [6] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                            [7] => stdClass Object
                                                (
                                                    [name] => _io_order_group
                                                    [value] => 1639585493856
                                                )

                                        )

                                    [productId] => 11432469
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:00.663
                                    [modifyDate] => 2021-12-15T09:36:00.663
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616521210
                                    [lineItemKey] => 10814741086389
                                    [sku] => P00020S
                                    [name] => Inscription
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/inscription-995841.jpg?v=1638365792
                                    [weight] => stdClass Object
                                        (
                                            [value] => 1
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 2
                                    [unitPrice] => 3.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => _io_parent_order_group
                                                    [value] => 1639585493856
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => _io_field_name
                                                    [value] => Inscription
                                                )

                                        )

                                    [productId] => 6760417
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:00.663
                                    [modifyDate] => 2021-12-15T09:36:00.663
                                )

                        )

                    [orderTotal] => 45.8
                    [amountPaid] => 45.8
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 17/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 1002
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [21] => stdClass Object
                (
                    [orderId] => 375321662
                    [orderNumber] => ORD000251779
                    [orderKey] => 4273217142965
                    [orderDate] => 2021-12-15T08:32:50.0000000
                    [createDate] => 2021-12-15T09:36:00.2430000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:32:50.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423765
                    [customerUsername] => 3279733915715
                    [customerEmail] => sweefong_ching@yahoo.com
                    [billTo] => stdClass Object
                        (
                            [name] => Swee Heron
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Trung Suong Nguyen
                            [company] => 
                            [street1] => 35 London Road
                            [street2] => 
                            [street3] => 
                            [city] => Dover
                            [state] => ENG
                            [postalCode] => CT17 0SS
                            [country] => GB
                            [phone] => 07889 377871
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521199
                                    [lineItemKey] => 10814748033205
                                    [sku] => P00486S
                                    [name] => Classic Strawberry Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/classic-strawberry-gateau-428982.jpg?v=1617979867
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 42.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 11432470
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:00.133
                                    [modifyDate] => 2021-12-15T09:36:00.133
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616521200
                                    [lineItemKey] => discount
                                    [sku] => 
                                    [name] => Coupon:  GOOGLEFREE
                                    [imageUrl] => http://images.shipstation.com/resource/1.gif
                                    [weight] => stdClass Object
                                        (
                                            [value] => 0
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => -4.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 
                                    [fulfillmentSku] => 
                                    [adjustment] => 1
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:36:00.133
                                    [modifyDate] => 2021-12-15T09:36:00.133
                                )

                        )

                    [orderTotal] => 42.95
                    [amountPaid] => 42.95
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 18/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => dpd_local_package
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [22] => stdClass Object
                (
                    [orderId] => 375321657
                    [orderNumber] => ORD000251780
                    [orderKey] => 4273220354229
                    [orderDate] => 2021-12-15T08:34:55.0000000
                    [createDate] => 2021-12-15T09:35:59.9630000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:34:55.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423754
                    [customerUsername] => 3019685625923
                    [customerEmail] => ritacherif@yahoo.com
                    [billTo] => stdClass Object
                        (
                            [name] => Rita  Cherif
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Rita Cherif
                            [company] => 
                            [street1] => 115 Applegarth
                            [street2] => 
                            [street3] => 
                            [city] => Croydon
                            [state] => ENG
                            [postalCode] => CR0 9DH
                            [country] => GB
                            [phone] => 07447525527
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521194
                                    [lineItemKey] => 10814753833141
                                    [sku] => 
                                    [name] => Afternoon Tea In Store Experience Gift Card
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/afternoon-tea-in-store-experience-gift-card-807455.png?v=1637920073
                                    [weight] => stdClass Object
                                        (
                                            [value] => 0
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 25
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => To
                                                    [value] => Mummy and Daddy
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => From
                                                    [value] => Les Enfants 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Message
                                                    [value] => May God bless you both and grant you peace and love!
                                                )

                                        )

                                    [productId] => 
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:59.837
                                    [modifyDate] => 2021-12-15T09:35:59.837
                                )

                        )

                    [orderTotal] => 27.95
                    [amountPaid] => 27.95
                    [taxAmount] => 0
                    [shippingAmount] => 2.95
                    [customerNotes] => <br/>Delivery Date: null
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard
                    [carrierCode] => 
                    [serviceCode] => 
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 0
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => 580111
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [23] => stdClass Object
                (
                    [orderId] => 375321650
                    [orderNumber] => ORD000251781
                    [orderKey] => 4273220649141
                    [orderDate] => 2021-12-15T08:35:01.0000000
                    [createDate] => 2021-12-15T09:35:59.5400000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:35:01.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423740
                    [customerUsername] => 5915833893045
                    [customerEmail] => juliachwala@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Julia Chwala
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Julia Chwala
                            [company] => 
                            [street1] => 1 The Downs
                            [street2] => 
                            [street3] => 
                            [city] => Cheadle
                            [state] => ENG
                            [postalCode] => SK8 1JL
                            [country] => GB
                            [phone] => 07400088662
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521185
                                    [lineItemKey] => 10814754324661
                                    [sku] => P00501S
                                    [name] => Chocolate Orange Ombre - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/chocolate-orange-ombre-188899.jpg?v=1617976990
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 42.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => Inscription (£3.95)
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Inscription_options
                                                    [value] => Happy Birthday Kit
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [6] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                            [7] => stdClass Object
                                                (
                                                    [name] => _io_order_group
                                                    [value] => 1639585284824
                                                )

                                        )

                                    [productId] => 15345812
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:59.397
                                    [modifyDate] => 2021-12-15T09:35:59.397
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616521186
                                    [lineItemKey] => 10814754357429
                                    [sku] => P00020S
                                    [name] => Inscription
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/inscription-995841.jpg?v=1638365792
                                    [weight] => stdClass Object
                                        (
                                            [value] => 1
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 3.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => _io_parent_order_group
                                                    [value] => 1639585284824
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => _io_field_name
                                                    [value] => Inscription
                                                )

                                        )

                                    [productId] => 6760417
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:59.397
                                    [modifyDate] => 2021-12-15T09:35:59.397
                                )

                        )

                    [orderTotal] => 51.85
                    [amountPaid] => 51.85
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 30/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10001
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [24] => stdClass Object
                (
                    [orderId] => 375321647
                    [orderNumber] => ORD000251782
                    [orderKey] => 4273232281781
                    [orderDate] => 2021-12-15T08:42:54.0000000
                    [createDate] => 2021-12-15T09:35:59.1630000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:42:54.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423728
                    [customerUsername] => 5911547707573
                    [customerEmail] => maderestauracion@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Ana Ortigosa
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Ana Ortigosa
                            [company] => 
                            [street1] => Managers flat
                            [street2] => The Conningbrook
                            [street3] => 
                            [city] => Ashford 
                            [state] => ENG
                            [postalCode] => ME17 2BQ
                            [country] => GB
                            [phone] => 07871616600
                            [residential] => 
                            [addressVerified] => Address validation failed
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521181
                                    [lineItemKey] => 10814776836277
                                    [sku] => P00566S
                                    [name] => Chocolate & Hazelnut Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/chocolate-hazelnut-gateau-924972.jpg?v=1618236521
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 32.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 21444789
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:59.07
                                    [modifyDate] => 2021-12-15T09:35:59.07
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616521182
                                    [lineItemKey] => discount
                                    [sku] => 
                                    [name] => Coupon:  WELCOME-06D3-4BNUT6
                                    [imageUrl] => http://images.shipstation.com/resource/1.gif
                                    [weight] => stdClass Object
                                        (
                                            [value] => 0
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => -4.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 
                                    [fulfillmentSku] => 
                                    [adjustment] => 1
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:59.07
                                    [modifyDate] => 2021-12-15T09:35:59.07
                                )

                        )

                    [orderTotal] => 32.95
                    [amountPaid] => 32.95
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 19/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [25] => stdClass Object
                (
                    [orderId] => 375321640
                    [orderNumber] => ORD000251783
                    [orderKey] => 4273232838837
                    [orderDate] => 2021-12-15T08:43:25.0000000
                    [createDate] => 2021-12-15T09:35:58.8000000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:43:25.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423753
                    [customerUsername] => 2997326151747
                    [customerEmail] => rekhabagga@rocketmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Rekha Karwal
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Rekha Karwal
                            [company] => 
                            [street1] => 68 Wilkins Road 
                            [street2] => 
                            [street3] => 
                            [city] => Oxford
                            [state] => ENG
                            [postalCode] => OX4 2JB
                            [country] => GB
                            [phone] => 07572 383234
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521163
                                    [lineItemKey] => 10814777950389
                                    [sku] => P00007S
                                    [name] => Double Chocolate Dream Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/double-chocolate-dream-gateau-178356.jpg?v=1617976990
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 39.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => Inscription (£3.95)
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Inscription_options
                                                    [value] => Congratulations Daddy xxx
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [6] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                            [7] => stdClass Object
                                                (
                                                    [name] => _io_order_group
                                                    [value] => 1639568260287
                                                )

                                        )

                                    [productId] => 6760416
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:58.707
                                    [modifyDate] => 2021-12-15T09:35:58.707
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616521164
                                    [lineItemKey] => 10814777983157
                                    [sku] => P00020S
                                    [name] => Inscription
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/inscription-995841.jpg?v=1638365792
                                    [weight] => stdClass Object
                                        (
                                            [value] => 1
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 3.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => _io_parent_order_group
                                                    [value] => 1639568260287
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => _io_field_name
                                                    [value] => Inscription
                                                )

                                        )

                                    [productId] => 6760417
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:58.707
                                    [modifyDate] => 2021-12-15T09:35:58.707
                                )

                        )

                    [orderTotal] => 48.85
                    [amountPaid] => 48.85
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 17/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10001
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [26] => stdClass Object
                (
                    [orderId] => 375321634
                    [orderNumber] => ORD000251784
                    [orderKey] => 4273236050101
                    [orderDate] => 2021-12-15T08:45:43.0000000
                    [createDate] => 2021-12-15T09:35:58.5300000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:45:43.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423732
                    [customerUsername] => 5915855683765
                    [customerEmail] => emma-jane-drury@sky.com
                    [billTo] => stdClass Object
                        (
                            [name] => Emma Drury
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Emma Drury
                            [company] => 
                            [street1] => 2B Moorlands Road, Ridgeway, Ambergate
                            [street2] => 
                            [street3] => 
                            [city] => Derby
                            [state] => ENG
                            [postalCode] => DE56 2JB
                            [country] => GB
                            [phone] => 07468 413315
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521150
                                    [lineItemKey] => 10814783291573
                                    [sku] => P00501S
                                    [name] => Chocolate Orange Ombre - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/chocolate-orange-ombre-188899.jpg?v=1617976990
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 42.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Inscription_options
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [6] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                            [7] => stdClass Object
                                                (
                                                    [name] => _io_order_group
                                                    [value] => 1639586278282
                                                )

                                            [8] => stdClass Object
                                                (
                                                    [name] => Inscription_Options
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 15345812
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:58.437
                                    [modifyDate] => 2021-12-15T09:35:58.437
                                )

                        )

                    [orderTotal] => 47.9
                    [amountPaid] => 47.9
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 17/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [27] => stdClass Object
                (
                    [orderId] => 375321630
                    [orderNumber] => ORD000251785
                    [orderKey] => 4273237983413
                    [orderDate] => 2021-12-15T08:47:08.0000000
                    [createDate] => 2021-12-15T09:35:58.2300000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:47:08.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423741
                    [customerUsername] => 3092330545219
                    [customerEmail] => mark_edward_brown@yahoo.com
                    [billTo] => stdClass Object
                        (
                            [name] => Mark Brown
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Mark Brown
                            [company] => 
                            [street1] => 2 Turnbull Road
                            [street2] => Fradley
                            [street3] => 
                            [city] => Lichfield
                            [state] => ENG
                            [postalCode] => WS13 8TB
                            [country] => GB
                            [phone] => 07584 679075
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521144
                                    [lineItemKey] => 10814786764981
                                    [sku] => P00548S
                                    [name] => Classic Patisserie Box
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/classic-patisserie-box-254461.jpg?v=1636543884
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 29.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 18091504
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:58.167
                                    [modifyDate] => 2021-12-15T09:35:58.167
                                )

                        )

                    [orderTotal] => 34.9
                    [amountPaid] => 34.9
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 23/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => dpd_local_package
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [28] => stdClass Object
                (
                    [orderId] => 375321624
                    [orderNumber] => ORD000251786
                    [orderKey] => 4273238868149
                    [orderDate] => 2021-12-15T08:47:50.0000000
                    [createDate] => 2021-12-15T09:35:57.7470000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:47:50.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423746
                    [customerUsername] => 5915861156021
                    [customerEmail] => Michael.fontenelle82@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Michael Fontenelle
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Michael Fontenelle
                            [company] => 
                            [street1] => 64 Winnington Road
                            [street2] => 
                            [street3] => 
                            [city] => London
                            [state] => ENG
                            [postalCode] => EN3 5RJ
                            [country] => GB
                            [phone] => 07899803163
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521139
                                    [lineItemKey] => 10814788305077
                                    [sku] => P00019S
                                    [name] => Black Forest Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/black-forest-gateau-242674.jpg?v=1617976990
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 39.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 6816438
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:57.683
                                    [modifyDate] => 2021-12-15T09:35:57.683
                                )

                        )

                    [orderTotal] => 44.9
                    [amountPaid] => 44.9
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 23/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => 580111
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [29] => stdClass Object
                (
                    [orderId] => 375321617
                    [orderNumber] => ORD000251787
                    [orderKey] => 4273239523509
                    [orderDate] => 2021-12-15T08:48:11.0000000
                    [createDate] => 2021-12-15T09:35:57.3700000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:48:11.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423758
                    [customerUsername] => 5915858272437
                    [customerEmail] => rachel.luoyu@hotmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Yu Luo
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Sandra Park
                            [company] => 
                            [street1] => 25 Ruscoe Road 
                            [street2] => 
                            [street3] => 
                            [city] => London
                            [state] => ENG
                            [postalCode] => E16 1JA
                            [country] => GB
                            [phone] => +44 7518 987303
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521132
                                    [lineItemKey] => 10814789615797
                                    [sku] => P00571S
                                    [name] => Charlotte Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/charlotte-gateau-875224.jpg?v=1620918849
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 32.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 22496467
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:57.293
                                    [modifyDate] => 2021-12-15T09:35:57.293
                                )

                        )

                    [orderTotal] => 37.9
                    [amountPaid] => 37.9
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 24/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [30] => stdClass Object
                (
                    [orderId] => 375321606
                    [orderNumber] => ORD000251788
                    [orderKey] => 4273244537013
                    [orderDate] => 2021-12-15T08:51:30.0000000
                    [createDate] => 2021-12-15T09:35:56.8530000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:51:30.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 141354651
                    [customerUsername] => 5170078580917
                    [customerEmail] => juliew34@yahoo.com
                    [billTo] => stdClass Object
                        (
                            [name] => Julie Walker
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Jamie Bollan
                            [company] => 
                            [street1] => 5 Cambie Crescent
                            [street2] => 
                            [street3] => 
                            [city] => Colchester Essex
                            [state] => ENG
                            [postalCode] => CO4 5DW
                            [country] => GB
                            [phone] => 07944 161170
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521117
                                    [lineItemKey] => 10814798528693
                                    [sku] => P00007S
                                    [name] => Double Chocolate Dream Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/double-chocolate-dream-gateau-178356.jpg?v=1617976990
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 39.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 6760416
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:56.773
                                    [modifyDate] => 2021-12-15T09:35:56.773
                                )

                        )

                    [orderTotal] => 44.9
                    [amountPaid] => 44.9
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 17/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [31] => stdClass Object
                (
                    [orderId] => 375321600
                    [orderNumber] => ORD000251789
                    [orderKey] => 4273247191221
                    [orderDate] => 2021-12-15T08:53:18.0000000
                    [createDate] => 2021-12-15T09:35:56.5700000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:53:18.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423761
                    [customerUsername] => 5915899592885
                    [customerEmail] => sophieheald@virginmedia.com
                    [billTo] => stdClass Object
                        (
                            [name] => Sophie Heald
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Sophie Heald
                            [company] => 
                            [street1] => West wing, pensax court, pensax
                            [street2] => 
                            [street3] => 
                            [city] => Worcester
                            [state] => ENG
                            [postalCode] => WR6 6XJ
                            [country] => GB
                            [phone] => 07856 776391
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521105
                                    [lineItemKey] => 10814804459701
                                    [sku] => MPAT03
                                    [name] => Madame Valerie's Afternoon Tea
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/madame-valeries-afternoon-tea-179347.jpg?v=1631128275
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 25
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 27539343
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:56.507
                                    [modifyDate] => 2021-12-15T09:35:56.507
                                )

                        )

                    [orderTotal] => 29.95
                    [amountPaid] => 29.95
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 17/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [32] => stdClass Object
                (
                    [orderId] => 375321594
                    [orderNumber] => ORD000251790
                    [orderKey] => 4273247420597
                    [orderDate] => 2021-12-15T08:53:27.0000000
                    [createDate] => 2021-12-15T09:35:56.3830000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:53:27.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 123020575
                    [customerUsername] => 4525322076341
                    [customerEmail] => wevans2809@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Steven  Evans
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Wendy Evans
                            [company] => 
                            [street1] => 83 Reindeer Road
                            [street2] => 
                            [street3] => 
                            [city] => Tamworth
                            [state] => ENG
                            [postalCode] => B78 3SW
                            [country] => GB
                            [phone] => 07595 319135
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521096
                                    [lineItemKey] => 10814804754613
                                    [sku] => P00019S
                                    [name] => Black Forest Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/black-forest-gateau-242674.jpg?v=1617976990
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 39.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 6816438
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:56.337
                                    [modifyDate] => 2021-12-15T09:35:56.337
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616521097
                                    [lineItemKey] => discount
                                    [sku] => 
                                    [name] => Coupon:  WELCOME01
                                    [imageUrl] => http://images.shipstation.com/resource/1.gif
                                    [weight] => stdClass Object
                                        (
                                            [value] => 0
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => -4.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 
                                    [fulfillmentSku] => 
                                    [adjustment] => 1
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:56.337
                                    [modifyDate] => 2021-12-15T09:35:56.337
                                )

                        )

                    [orderTotal] => 39.95
                    [amountPaid] => 39.95
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 24/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [33] => stdClass Object
                (
                    [orderId] => 375321590
                    [orderNumber] => ORD000251792
                    [orderKey] => 4273253154997
                    [orderDate] => 2021-12-15T08:57:13.0000000
                    [createDate] => 2021-12-15T09:35:56.1330000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:57:13.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423764
                    [customerUsername] => 5915905556661
                    [customerEmail] => tommy@gunnersideestate.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => richard murphy
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Tommy Nimmo
                            [company] => 
                            [street1] => GUNNERSIDE LODGE
                            [street2] => 
                            [street3] => 
                            [city] => GUNNERSIDE
                            [state] => ENG
                            [postalCode] => DL11 6JQ
                            [country] => GB
                            [phone] => 01748 886002
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521088
                                    [lineItemKey] => 10814816092341
                                    [sku] => P00523S
                                    [name] => Red Velvet Cake - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/red-velvet-cake-545963.jpg?v=1617979802
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 39.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => Happy Birthday Glitter Cake Topper (£6.95)
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Cake Topper_options
                                                    [value] => Silver Glitter
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [6] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                            [7] => stdClass Object
                                                (
                                                    [name] => _io_order_group
                                                    [value] => 1639410822134
                                                )

                                        )

                                    [productId] => 16391599
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:56.037
                                    [modifyDate] => 2021-12-15T09:35:56.037
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616521089
                                    [lineItemKey] => 10814816125109
                                    [sku] => CTOP003
                                    [name] => Happy Birthday Cake Toppers - Sliver glitter
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/happy-birthday-cake-toppers-783138.jpg?v=1638353383
                                    [weight] => stdClass Object
                                        (
                                            [value] => 0
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 6.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => _io_parent_order_group
                                                    [value] => 1639410822134
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => _io_field_name
                                                    [value] => Cake Topper_options
                                                )

                                        )

                                    [productId] => 8511021
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:56.037
                                    [modifyDate] => 2021-12-15T09:35:56.037
                                )

                        )

                    [orderTotal] => 51.85
                    [amountPaid] => 51.85
                    [taxAmount] => 1.99
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 19/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [34] => stdClass Object
                (
                    [orderId] => 375321586
                    [orderNumber] => ORD000251793
                    [orderKey] => 4273253548213
                    [orderDate] => 2021-12-15T08:57:27.0000000
                    [createDate] => 2021-12-15T09:35:55.8330000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:57:27.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423742
                    [customerUsername] => 5915906932917
                    [customerEmail] => righto@righto.net
                    [billTo] => stdClass Object
                        (
                            [name] => mark stones
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => mark stones
                            [company] => 
                            [street1] => Berwyn Banks
                            [street2] => Botley
                            [street3] => 
                            [city] => Southampton
                            [state] => ENG
                            [postalCode] => SO30 2DL
                            [country] => GB
                            [phone] => 07495 677338
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521084
                                    [lineItemKey] => 10814817009845
                                    [sku] => P00496S
                                    [name] => Classic Habana Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/classic-habana-gateau-390967.jpg?v=1617979743
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 32.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => Inscription (£3.95)
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Inscription_options
                                                    [value] => Happy Birthday Catherine
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [6] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                            [7] => stdClass Object
                                                (
                                                    [name] => _io_order_group
                                                    [value] => 1639587261776
                                                )

                                        )

                                    [productId] => 12990277
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:55.693
                                    [modifyDate] => 2021-12-15T09:35:55.693
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616521085
                                    [lineItemKey] => 10814817042613
                                    [sku] => P00020S
                                    [name] => Inscription
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/inscription-995841.jpg?v=1638365792
                                    [weight] => stdClass Object
                                        (
                                            [value] => 1
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 3.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => _io_parent_order_group
                                                    [value] => 1639587261776
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => _io_field_name
                                                    [value] => Inscription
                                                )

                                        )

                                    [productId] => 6760417
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:55.693
                                    [modifyDate] => 2021-12-15T09:35:55.693
                                )

                        )

                    [orderTotal] => 41.85
                    [amountPaid] => 41.85
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 21/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10001
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [35] => stdClass Object
                (
                    [orderId] => 375321583
                    [orderNumber] => ORD000251794
                    [orderKey] => 4273254400181
                    [orderDate] => 2021-12-15T08:58:02.0000000
                    [createDate] => 2021-12-15T09:35:55.5200000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:58:02.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423766
                    [customerUsername] => 3091595886659
                    [customerEmail] => wayne.r.dawe@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Wayne Dawe
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Wayne Dawe
                            [company] => 
                            [street1] => 49 Russet Avenue
                            [street2] => 
                            [street3] => 
                            [city] => Exeter
                            [state] => ENG
                            [postalCode] => EX1 3QB
                            [country] => GB
                            [phone] => 07748 792187
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521081
                                    [lineItemKey] => 10814818386101
                                    [sku] => P00006S
                                    [name] => Double Chocolate Dream Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/double-chocolate-dream-gateau-178356.jpg?v=1617976990
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 29.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 6759340
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:55.457
                                    [modifyDate] => 2021-12-15T09:35:55.457
                                )

                        )

                    [orderTotal] => 34.9
                    [amountPaid] => 34.9
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 24/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [36] => stdClass Object
                (
                    [orderId] => 375321575
                    [orderNumber] => ORD000251795
                    [orderKey] => 4273255121077
                    [orderDate] => 2021-12-15T08:58:26.0000000
                    [createDate] => 2021-12-15T09:35:55.0370000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T08:58:26.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423767
                    [customerUsername] => 5915908800693
                    [customerEmail] => yvonnecole@blueyonder.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => Yvonne Cole
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Yvonne Cole
                            [company] => 
                            [street1] => 26 Moredun Park Gardens
                            [street2] => 
                            [street3] => 
                            [city] => EDINBURGH
                            [state] => SCT
                            [postalCode] => EH17 7JP
                            [country] => GB
                            [phone] => 07871 502448
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521068
                                    [lineItemKey] => 10814819958965
                                    [sku] => P00005S
                                    [name] => Triple Chocolate Delight Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/triple-chocolate-delight-gateau-185037.jpg?v=1618020666
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 42.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 6587725
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:54.987
                                    [modifyDate] => 2021-12-15T09:35:54.987
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616521069
                                    [lineItemKey] => 10814819991733
                                    [sku] => 
                                    [name] => Classic Chocolate Yule Log - 8" - 10"
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/classic-chocolate-yule-log-310390.jpg?v=1637313525
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 42.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                            [6] => stdClass Object
                                                (
                                                    [name] => Christmas Cake Topper
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:54.987
                                    [modifyDate] => 2021-12-15T09:35:54.987
                                )

                        )

                    [orderTotal] => 90.85
                    [amountPaid] => 90.85
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 23/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 20000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [37] => stdClass Object
                (
                    [orderId] => 375321572
                    [orderNumber] => ORD000251796
                    [orderKey] => 4273257939125
                    [orderDate] => 2021-12-15T09:00:16.0000000
                    [createDate] => 2021-12-15T09:35:54.7700000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T09:00:16.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423751
                    [customerUsername] => 5915908047029
                    [customerEmail] => empeepayne@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Mary Payne
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => philippa  Payne
                            [company] => 
                            [street1] => 16, Farr Street, Edgely
                            [street2] => 
                            [street3] => 
                            [city] => Stockport
                            [state] => ENG
                            [postalCode] => SK3 9LD
                            [country] => GB
                            [phone] => 07930 368805
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521063
                                    [lineItemKey] => 10814824775861
                                    [sku] => P00030S
                                    [name] => Fairytale Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/fairytale-gateau-493921.jpg?v=1617976981
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 32.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => Happy Birthday Glitter Cake Topper (£6.95)
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Cake Topper_options
                                                    [value] => Rose Gold Glitter
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => Metallic Balloons (£3.95) 
                                                )

                                            [6] => stdClass Object
                                                (
                                                    [name] => Balloons_Options
                                                    [value] => Rose Gold Glitter
                                                )

                                            [7] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                            [8] => stdClass Object
                                                (
                                                    [name] => _io_order_group
                                                    [value] => 1639438945192
                                                )

                                        )

                                    [productId] => 7759241
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:54.487
                                    [modifyDate] => 2021-12-15T09:35:54.487
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616521064
                                    [lineItemKey] => 10814824808629
                                    [sku] => CTOP002
                                    [name] => Happy Birthday Cake Toppers - Rose gold glitter
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/happy-birthday-cake-toppers-814064.jpg?v=1638353383
                                    [weight] => stdClass Object
                                        (
                                            [value] => 0
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 6.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => _io_parent_order_group
                                                    [value] => 1639438945192
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => _io_field_name
                                                    [value] => Cake Topper_options
                                                )

                                        )

                                    [productId] => 8499417
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:54.63
                                    [modifyDate] => 2021-12-15T09:35:54.63
                                )

                            [2] => stdClass Object
                                (
                                    [orderItemId] => 616521065
                                    [lineItemKey] => 10814824841397
                                    [sku] => P00270S
                                    [name] => Metallic Balloons - Rose gold glitter
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/metallic-balloons-532347.jpg?v=1593839514
                                    [weight] => stdClass Object
                                        (
                                            [value] => 0
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 3.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => _io_parent_order_group
                                                    [value] => 1639438945192
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => _io_field_name
                                                    [value] => Balloons_Options
                                                )

                                        )

                                    [productId] => 8967910
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:54.63
                                    [modifyDate] => 2021-12-15T09:35:54.63
                                )

                            [3] => stdClass Object
                                (
                                    [orderItemId] => 616521066
                                    [lineItemKey] => discount
                                    [sku] => 
                                    [name] => Coupon:  WELCOME-0428-4BU6PM
                                    [imageUrl] => http://images.shipstation.com/resource/1.gif
                                    [weight] => stdClass Object
                                        (
                                            [value] => 0
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => -4.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 
                                    [fulfillmentSku] => 
                                    [adjustment] => 1
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:54.63
                                    [modifyDate] => 2021-12-15T09:35:54.63
                                )

                        )

                    [orderTotal] => 43.85
                    [amountPaid] => 43.85
                    [taxAmount] => 1.82
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 22/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [38] => stdClass Object
                (
                    [orderId] => 375321567
                    [orderNumber] => ORD000251797
                    [orderKey] => 4273261019317
                    [orderDate] => 2021-12-15T09:02:11.0000000
                    [createDate] => 2021-12-15T09:35:54.2830000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T09:02:11.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 128358083
                    [customerUsername] => 5048904581301
                    [customerEmail] => dscott585@aol.com
                    [billTo] => stdClass Object
                        (
                            [name] => Debra Scott
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Debra Scott
                            [company] => 
                            [street1] => 18 Robinsbay Road
                            [street2] => 
                            [street3] => 
                            [city] => Manchester
                            [state] => ENG
                            [postalCode] => M22 0LT
                            [country] => GB
                            [phone] => 07914 760771
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521045
                                    [lineItemKey] => 10814831034549
                                    [sku] => P00030S
                                    [name] => Fairytale Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/fairytale-gateau-493921.jpg?v=1617976981
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 32.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 7759241
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:54.203
                                    [modifyDate] => 2021-12-15T09:35:54.203
                                )

                        )

                    [orderTotal] => 37.9
                    [amountPaid] => 37.9
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 24/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [39] => stdClass Object
                (
                    [orderId] => 375321564
                    [orderNumber] => ORD000251798
                    [orderKey] => 4273262297269
                    [orderDate] => 2021-12-15T09:02:42.0000000
                    [createDate] => 2021-12-15T09:35:53.7800000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T09:02:42.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 146531598
                    [customerUsername] => 5219040428213
                    [customerEmail] => davibodd@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Lin Bain
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Lin Bain
                            [company] => 
                            [street1] => 19 High Meadow
                            [street2] => Washingborough
                            [street3] => 
                            [city] => Lincoln
                            [state] => ENG
                            [postalCode] => LN4 1SE
                            [country] => GB
                            [phone] => 01522 797015
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521039
                                    [lineItemKey] => 10814832804021
                                    [sku] => ECL821
                                    [name] => Eclair Patisserie Box
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/NEWScreenshot2021-03-10at16.34.36.jpg?v=1615453278
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 24.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 23142910
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:53.733
                                    [modifyDate] => 2021-12-15T09:35:53.733
                                )

                        )

                    [orderTotal] => 29.9
                    [amountPaid] => 29.9
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 24/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [40] => stdClass Object
                (
                    [orderId] => 375321562
                    [orderNumber] => ORD000251799
                    [orderKey] => 4273266000053
                    [orderDate] => 2021-12-15T09:04:56.0000000
                    [createDate] => 2021-12-15T09:35:53.5570000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T09:04:56.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423726
                    [customerUsername] => 5915916173493
                    [customerEmail] => alison@christianalexander.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => Alison McKeon
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Alison McKeon
                            [company] => 
                            [street1] => Yew Tree Cottage
                            [street2] => Westhill
                            [street3] => 
                            [city] => Ledbury
                            [state] => ENG
                            [postalCode] => HR8 1JF
                            [country] => GB
                            [phone] => 07887 546146
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521032
                                    [lineItemKey] => 10814839750837
                                    [sku] => P00549S-1
                                    [name] => Caramel Biscuit Cake - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/caramel-biscuit-cake-327684.jpg?v=1617976981
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 29.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 18670757
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:53.003
                                    [modifyDate] => 2021-12-15T09:35:53.003
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616521033
                                    [lineItemKey] => 10814839783605
                                    [sku] => P00505S
                                    [name] => Madame Valerie's Christmas Cake - 8"
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/madame-valeries-christmas-cake-853716.jpg?v=1604427651
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 49.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                            [6] => stdClass Object
                                                (
                                                    [name] => Christmas Cake Topper
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 15726022
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:53.477
                                    [modifyDate] => 2021-12-15T09:35:53.477
                                )

                        )

                    [orderTotal] => 84.85
                    [amountPaid] => 84.85
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 23/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 20000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [41] => stdClass Object
                (
                    [orderId] => 375321552
                    [orderNumber] => ORD000251800
                    [orderKey] => 4273268523189
                    [orderDate] => 2021-12-15T09:06:35.0000000
                    [createDate] => 2021-12-15T09:35:52.6430000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T09:06:35.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 136330983
                    [customerUsername] => 5112701452469
                    [customerEmail] => debeemcbee@yahoo.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => Deborah McBride
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Deborah McBride
                            [company] => 
                            [street1] => 14 Gagiebank 
                            [street2] => Wellbank
                            [street3] => 
                            [city] => By Dundee
                            [state] => SCT
                            [postalCode] => DD5 3PT
                            [country] => GB
                            [phone] => 07825 786213
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521015
                                    [lineItemKey] => 10814844829877
                                    [sku] => P00599S
                                    [name] => Christmas Coconut Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/christmas-coconut-gateau-382981.jpg?v=1638915060
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 44.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                            [6] => stdClass Object
                                                (
                                                    [name] => Christmas Cake Topper
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 28806554
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:52.58
                                    [modifyDate] => 2021-12-15T09:35:52.58
                                )

                        )

                    [orderTotal] => 49.9
                    [amountPaid] => 49.9
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 23/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [42] => stdClass Object
                (
                    [orderId] => 375321548
                    [orderNumber] => ORD000251801
                    [orderKey] => 4273272914101
                    [orderDate] => 2021-12-15T09:09:21.0000000
                    [createDate] => 2021-12-15T09:35:52.4100000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T09:09:21.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 84939693
                    [customerUsername] => 3291139768387
                    [customerEmail] => elizabeth.taylor002@outlook.com
                    [billTo] => stdClass Object
                        (
                            [name] => Elizabeth Taylor
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Elizabeth Taylor
                            [company] => 
                            [street1] => 15 Tingley Hall Rise
                            [street2] => 
                            [street3] => 
                            [city] => Wakefield
                            [state] => ENG
                            [postalCode] => WF3 1QZ
                            [country] => GB
                            [phone] => 07415 272330
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616521006
                                    [lineItemKey] => 10814852825269
                                    [sku] => P00504S
                                    [name] => Champagne & Raspberry Yule Log - 8" - 10"
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/champagne-raspberry-yule-log-815294.jpg?v=1604427657
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 44.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                            [6] => stdClass Object
                                                (
                                                    [name] => Christmas Cake Topper
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 15726021
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:52.33
                                    [modifyDate] => 2021-12-15T09:35:52.33
                                )

                        )

                    [orderTotal] => 49.9
                    [amountPaid] => 49.9
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 24/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [43] => stdClass Object
                (
                    [orderId] => 375321540
                    [orderNumber] => ORD000251802
                    [orderKey] => 4273275109557
                    [orderDate] => 2021-12-15T09:10:53.0000000
                    [createDate] => 2021-12-15T09:35:51.6870000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T09:10:53.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423734
                    [customerUsername] => 5915925381301
                    [customerEmail] => ingridcizaite@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Ingrid Cizaite
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Ingrid Cizaite
                            [company] => 
                            [street1] => 73 High Street
                            [street2] => Linton
                            [street3] => 
                            [city] => Cambridg
                            [state] => ENG
                            [postalCode] => CB21 4HS
                            [country] => GB
                            [phone] => 07591 403528
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616520988
                                    [lineItemKey] => 10814856560821
                                    [sku] => P00485S
                                    [name] => Classic Strawberry Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/classic-strawberry-gateau-428982.jpg?v=1617979867
                                    [weight] => stdClass Object
                                        (
                                            [value] => 1000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 32.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 11432469
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:51.563
                                    [modifyDate] => 2021-12-15T09:35:51.563
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616520989
                                    [lineItemKey] => 10814856495285
                                    [sku] => ECL821
                                    [name] => Eclair Patisserie Box
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/NEWScreenshot2021-03-10at16.34.36.jpg?v=1615453278
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 24.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 23142910
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:51.203
                                    [modifyDate] => 2021-12-15T09:35:51.203
                                )

                            [2] => stdClass Object
                                (
                                    [orderItemId] => 616520990
                                    [lineItemKey] => 10814856528053
                                    [sku] => (Coffee 1)
                                    [name] => Patisserie Valerie House Blend Coffee - House Blend Whole Bean
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/house-blend-coffee-481055.jpg?v=1622116645
                                    [weight] => stdClass Object
                                        (
                                            [value] => 0
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 5.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 17954371
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:51.407
                                    [modifyDate] => 2021-12-15T09:35:51.407
                                )

                        )

                    [orderTotal] => 68.8
                    [amountPaid] => 68.8
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 24/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 11000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [44] => stdClass Object
                (
                    [orderId] => 375321530
                    [orderNumber] => ORD000251803
                    [orderKey] => 4273281040565
                    [orderDate] => 2021-12-15T09:13:27.0000000
                    [createDate] => 2021-12-15T09:35:50.7800000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T09:13:27.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423738
                    [customerUsername] => 5915927609525
                    [customerEmail] => jonajonabob@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Jonathan Lee
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Jonathan Lee
                            [company] => 
                            [street1] => 76 Bath Street, Abingdon, 
                            [street2] => 
                            [street3] => 
                            [city] => Oxford
                            [state] => ENG
                            [postalCode] => OX14 1EB
                            [country] => GB
                            [phone] => 07544321478
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616520965
                                    [lineItemKey] => 10814865703093
                                    [sku] => P00009S
                                    [name] => Tiramisu Cake - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/tiramisu-gateau-183196.jpg?v=1617803921
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 39.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 6816437
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:50.703
                                    [modifyDate] => 2021-12-15T09:35:50.703
                                )

                        )

                    [orderTotal] => 44.9
                    [amountPaid] => 44.9
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 23/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [45] => stdClass Object
                (
                    [orderId] => 375321525
                    [orderNumber] => ORD000251804
                    [orderKey] => 4273284612277
                    [orderDate] => 2021-12-15T09:15:39.0000000
                    [createDate] => 2021-12-15T09:35:50.1400000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T09:15:39.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423749
                    [customerUsername] => 5915930394805
                    [customerEmail] => lloydjonesn@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Naomi Lloyd-Jones
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Naomi Lloyd-Jones
                            [company] => 
                            [street1] => 1 wick house cottages
                            [street2] => Mesh Pond
                            [street3] => 
                            [city] => Downton
                            [state] => ENG
                            [postalCode] => SP5 3NQ
                            [country] => GB
                            [phone] => 07503 449710
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616520958
                                    [lineItemKey] => 10814871339189
                                    [sku] => P00496S
                                    [name] => Classic Habana Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/classic-habana-gateau-390967.jpg?v=1617979743
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 32.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => Inscription (£3.95)
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Inscription_options
                                                    [value] => Happy Birthday Titus xxx
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [6] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                            [7] => stdClass Object
                                                (
                                                    [name] => _io_order_group
                                                    [value] => 1639588195426
                                                )

                                        )

                                    [productId] => 12990277
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:50.03
                                    [modifyDate] => 2021-12-15T09:35:50.03
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616520959
                                    [lineItemKey] => 10814871371957
                                    [sku] => P00020S
                                    [name] => Inscription
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/inscription-995841.jpg?v=1638365792
                                    [weight] => stdClass Object
                                        (
                                            [value] => 1
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 3.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => _io_parent_order_group
                                                    [value] => 1639588195426
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => _io_field_name
                                                    [value] => Inscription
                                                )

                                        )

                                    [productId] => 6760417
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:50.03
                                    [modifyDate] => 2021-12-15T09:35:50.03
                                )

                        )

                    [orderTotal] => 41.85
                    [amountPaid] => 41.85
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 17/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10001
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [46] => stdClass Object
                (
                    [orderId] => 375321521
                    [orderNumber] => ORD000251805
                    [orderKey] => 4273285660853
                    [orderDate] => 2021-12-15T09:16:26.0000000
                    [createDate] => 2021-12-15T09:35:49.8430000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T09:16:26.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423759
                    [customerUsername] => 5915929936053
                    [customerEmail] => yeechin88@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Yee Chin
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Sen Oxland
                            [company] => 
                            [street1] => 4 Greenwich Place
                            [street2] => 
                            [street3] => 
                            [city] => Canterbury
                            [state] => ENG
                            [postalCode] => CT1 3YL
                            [country] => GB
                            [phone] => 7814274641
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616520952
                                    [lineItemKey] => 10814872977589
                                    [sku] => MPAT03
                                    [name] => Madame Valerie's Afternoon Tea
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/madame-valeries-afternoon-tea-179347.jpg?v=1631128275
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 25
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 27539343
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:49.767
                                    [modifyDate] => 2021-12-15T09:35:49.767
                                )

                        )

                    [orderTotal] => 29.95
                    [amountPaid] => 29.95
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 17/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [47] => stdClass Object
                (
                    [orderId] => 375321514
                    [orderNumber] => ORD000251806
                    [orderKey] => 4273289429173
                    [orderDate] => 2021-12-15T09:19:00.0000000
                    [createDate] => 2021-12-15T09:35:49.2000000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T09:19:00.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423735
                    [customerUsername] => 3020339118147
                    [customerEmail] => janeeosullivan@hotmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Jane O’Sullivan
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Jane O’Sullivan
                            [company] => 
                            [street1] => Wentloog 
                            [street2] => 
                            [street3] => 
                            [city] => Cardiff
                            [state] => WLS
                            [postalCode] => CF3 5TX
                            [country] => GB
                            [phone] => 07572 857185
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616520942
                                    [lineItemKey] => 10814879367349
                                    [sku] => 
                                    [name] => Classic Chocolate Yule Log - 8" - 10"
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/classic-chocolate-yule-log-310390.jpg?v=1637313525
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 42.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                            [6] => stdClass Object
                                                (
                                                    [name] => Christmas Cake Topper
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:48.763
                                    [modifyDate] => 2021-12-15T09:35:48.763
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616520943
                                    [lineItemKey] => 10814879432885
                                    [sku] => P00019S
                                    [name] => Black Forest Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/black-forest-gateau-242674.jpg?v=1617976990
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 39.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 6816438
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:49.137
                                    [modifyDate] => 2021-12-15T09:35:49.137
                                )

                            [2] => stdClass Object
                                (
                                    [orderItemId] => 616520944
                                    [lineItemKey] => 10814879400117
                                    [sku] => P00507S
                                    [name] => Mince Pie Gift Box of 6 - 6 Mince Pie Gift Box
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/6-x-add-on-mince-pie-gift-box-197418.jpg?v=1636059926
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 11.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 28592695
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:48.967
                                    [modifyDate] => 2021-12-15T09:35:48.967
                                )

                        )

                    [orderTotal] => 99.8
                    [amountPaid] => 99.8
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 24/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 30000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [48] => stdClass Object
                (
                    [orderId] => 375321508
                    [orderNumber] => ORD000251807
                    [orderKey] => 4273294540981
                    [orderDate] => 2021-12-15T09:23:04.0000000
                    [createDate] => 2021-12-15T09:35:48.6370000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T09:23:04.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423727
                    [customerUsername] => 5915913781429
                    [customerEmail] => altonsiu@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Alton Siu
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Alton Siu
                            [company] => 
                            [street1] => 3 More Place, St. Matthews Road, Winchester, Hampshire SO22 6BX
                            [street2] => 
                            [street3] => 
                            [city] => Winchester 
                            [state] => ENG
                            [postalCode] => SO22 6BX
                            [country] => GB
                            [phone] => 07522 508578
                            [residential] => 
                            [addressVerified] => Address validation failed
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616520931
                                    [lineItemKey] => 10814888738997
                                    [sku] => P00005S
                                    [name] => Triple Chocolate Delight Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/triple-chocolate-delight-gateau-185037.jpg?v=1618020666
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 42.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => Inscription (£3.95)
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Inscription_options
                                                    [value] => Happy Birthday Larry! 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [6] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                            [7] => stdClass Object
                                                (
                                                    [name] => _io_order_group
                                                    [value] => 1639587288966
                                                )

                                        )

                                    [productId] => 6587725
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:48.073
                                    [modifyDate] => 2021-12-15T09:35:48.073
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616520932
                                    [lineItemKey] => 10814888771765
                                    [sku] => P00020S
                                    [name] => Inscription
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/inscription-995841.jpg?v=1638365792
                                    [weight] => stdClass Object
                                        (
                                            [value] => 1
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 3.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => _io_parent_order_group
                                                    [value] => 1639587288966
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => _io_field_name
                                                    [value] => Inscription
                                                )

                                        )

                                    [productId] => 6760417
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:48.527
                                    [modifyDate] => 2021-12-15T09:35:48.527
                                )

                            [2] => stdClass Object
                                (
                                    [orderItemId] => 616520933
                                    [lineItemKey] => discount
                                    [sku] => 
                                    [name] => Coupon:  WELCOME-2B04-4BP822
                                    [imageUrl] => http://images.shipstation.com/resource/1.gif
                                    [weight] => stdClass Object
                                        (
                                            [value] => 0
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => -4.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 
                                    [fulfillmentSku] => 
                                    [adjustment] => 1
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:48.527
                                    [modifyDate] => 2021-12-15T09:35:48.527
                                )

                        )

                    [orderTotal] => 46.9
                    [amountPaid] => 46.9
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 23/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10001
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [49] => stdClass Object
                (
                    [orderId] => 375321495
                    [orderNumber] => ORD000251808
                    [orderKey] => 4273299751093
                    [orderDate] => 2021-12-15T09:26:55.0000000
                    [createDate] => 2021-12-15T09:35:47.2270000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T09:26:55.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 197365104
                    [customerUsername] => 5869238386869
                    [customerEmail] => alex.ngkc@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Alex Ng
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Alex Ng
                            [company] => 
                            [street1] => 41 St. Cuthberts House,7 Upper King Street
                            [street2] => 
                            [street3] => 
                            [city] => Norwich
                            [state] => ENG
                            [postalCode] => NR3 1FA
                            [country] => GB
                            [phone] => 07735 771647
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616520905
                                    [lineItemKey] => 10814898176181
                                    [sku] => P00012S
                                    [name] => Naked Carrot Cake - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/naked-carrot-cake-584654.jpg?v=1617976981
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 29.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 6489824
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:47.163
                                    [modifyDate] => 2021-12-15T09:35:47.163
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616520906
                                    [lineItemKey] => discount
                                    [sku] => 
                                    [name] => Coupon:  WELCOME-31B9-4AJNIW
                                    [imageUrl] => http://images.shipstation.com/resource/1.gif
                                    [weight] => stdClass Object
                                        (
                                            [value] => 0
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => -4.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 
                                    [fulfillmentSku] => 
                                    [adjustment] => 1
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:47.163
                                    [modifyDate] => 2021-12-15T09:35:47.163
                                )

                        )

                    [orderTotal] => 29.95
                    [amountPaid] => 29.95
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 23/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [50] => stdClass Object
                (
                    [orderId] => 375321490
                    [orderNumber] => ORD000251809
                    [orderKey] => 4273302306997
                    [orderDate] => 2021-12-15T09:28:43.0000000
                    [createDate] => 2021-12-15T09:35:46.7230000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T09:28:43.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423762
                    [customerUsername] => 5915949858997
                    [customerEmail] => stellagujjar@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Stella Remedios
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Stella Remedios
                            [company] => 
                            [street1] => 7 welland road, Welland Road, Spalding
                            [street2] => Welland Road
                            [street3] => 
                            [city] => Spalding
                            [state] => ENG
                            [postalCode] => PE11 2DJ
                            [country] => GB
                            [phone] => 07404790292
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616520896
                                    [lineItemKey] => 10814902927541
                                    [sku] => P00523S
                                    [name] => Red Velvet Cake - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/red-velvet-cake-545963.jpg?v=1617979802
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 39.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 16391599
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:46.66
                                    [modifyDate] => 2021-12-15T09:35:46.66
                                )

                        )

                    [orderTotal] => 44.9
                    [amountPaid] => 44.9
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 08/01/2022
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [51] => stdClass Object
                (
                    [orderId] => 375321483
                    [orderNumber] => ORD000251810
                    [orderKey] => 4273302896821
                    [orderDate] => 2021-12-15T09:29:10.0000000
                    [createDate] => 2021-12-15T09:35:46.2870000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T09:29:10.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423737
                    [customerUsername] => 5915944157365
                    [customerEmail] => sparkyjc1@yahoo.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => John Coombs
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => John Coombs
                            [company] => 
                            [street1] => 6 butlers cottages woolwich road 
                            [street2] => 
                            [street3] => 
                            [city] => Bexleyheath 
                            [state] => ENG
                            [postalCode] => DA7 4LL
                            [country] => GB
                            [phone] => 07444 965351
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616520885
                                    [lineItemKey] => 10814903713973
                                    [sku] => P00500S
                                    [name] => Chocolate Orange Ombre - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/chocolate-orange-ombre-188899.jpg?v=1617976990
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 32.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Candles_Options
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => Metallic Balloons (£3.95) 
                                                )

                                            [6] => stdClass Object
                                                (
                                                    [name] => Balloons_Options
                                                    [value] => Silver Glitter
                                                )

                                            [7] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                            [8] => stdClass Object
                                                (
                                                    [name] => _io_order_group
                                                    [value] => 1639589172694
                                                )

                                            [9] => stdClass Object
                                                (
                                                    [name] => Candles_options
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 15345811
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:45.293
                                    [modifyDate] => 2021-12-15T09:35:45.293
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616520886
                                    [lineItemKey] => 10814903746741
                                    [sku] => P00269S
                                    [name] => Metallic Balloons - Silver glitter
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/metallic-balloons-319928.jpg?v=1593839514
                                    [weight] => stdClass Object
                                        (
                                            [value] => 0
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 3.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => _io_parent_order_group
                                                    [value] => 1639589172694
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => _io_field_name
                                                    [value] => Balloons_Options
                                                )

                                        )

                                    [productId] => 8962402
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:45.497
                                    [modifyDate] => 2021-12-15T09:35:45.497
                                )

                            [2] => stdClass Object
                                (
                                    [orderItemId] => 616520887
                                    [lineItemKey] => 10814903779509
                                    [sku] => P00522S
                                    [name] => Skinny Metallic Candles - Gold
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/skinny-metallic-candles-838779.jpg?v=1605206328
                                    [weight] => stdClass Object
                                        (
                                            [value] => 0
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 3.5
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => _io_parent_order_group
                                                    [value] => 1639588791945
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => _io_field_name
                                                    [value] => Candles_Options
                                                )

                                        )

                                    [productId] => 16049581
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:45.693
                                    [modifyDate] => 2021-12-15T09:35:45.693
                                )

                        )

                    [orderTotal] => 45.35
                    [amountPaid] => 45.35
                    [taxAmount] => 2.07
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 18/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [52] => stdClass Object
                (
                    [orderId] => 375321467
                    [orderNumber] => ORD000251811
                    [orderKey] => 4273304010933
                    [orderDate] => 2021-12-15T09:29:55.0000000
                    [createDate] => 2021-12-15T09:35:44.9330000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T09:29:55.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423730
                    [customerUsername] => 5899189747893
                    [customerEmail] => lizbevan99@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Elizabeth Bevan
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Elizabeth Bevan
                            [company] => 
                            [street1] => CYGNET HOUSE, SWAN YARD, 
                            [street2] => East st , Coggeshall
                            [street3] => 
                            [city] => Colchester
                            [state] => ENG
                            [postalCode] => CO6 1SJ
                            [country] => GB
                            [phone] => 01376 561828
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616520869
                                    [lineItemKey] => 10814905581749
                                    [sku] => 
                                    [name] => Classic Chocolate Yule Log - 8" - 10"
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/classic-chocolate-yule-log-310390.jpg?v=1637313525
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 42.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                            [6] => stdClass Object
                                                (
                                                    [name] => Christmas Cake Topper
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:44.857
                                    [modifyDate] => 2021-12-15T09:35:44.857
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616520870
                                    [lineItemKey] => discount
                                    [sku] => 
                                    [name] => Coupon:  WELCOME-586D-4B8HAO
                                    [imageUrl] => http://images.shipstation.com/resource/1.gif
                                    [weight] => stdClass Object
                                        (
                                            [value] => 0
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => -4.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 
                                    [fulfillmentSku] => 
                                    [adjustment] => 1
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:44.857
                                    [modifyDate] => 2021-12-15T09:35:44.857
                                )

                        )

                    [orderTotal] => 42.95
                    [amountPaid] => 42.95
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 21/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [53] => stdClass Object
                (
                    [orderId] => 375321464
                    [orderNumber] => ORD000251813
                    [orderKey] => 4273311613109
                    [orderDate] => 2021-12-15T09:35:02.0000000
                    [createDate] => 2021-12-15T09:35:44.6070000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T09:35:02.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423743
                    [customerUsername] => 5915957067957
                    [customerEmail] => marlenebeglin@hotmail.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => Marlene Beglin
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Marlene Beglin
                            [company] => 
                            [street1] => 4 Rosewood Avenue
                            [street2] => 
                            [street3] => 
                            [city] => Bellshill
                            [state] => SCT
                            [postalCode] => ML4 1NR
                            [country] => GB
                            [phone] => 07841 901614
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616520864
                                    [lineItemKey] => 10814919835829
                                    [sku] => P00599S
                                    [name] => Christmas Coconut Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/christmas-coconut-gateau-382981.jpg?v=1638915060
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 44.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                            [6] => stdClass Object
                                                (
                                                    [name] => Christmas Cake Topper
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 28806554
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:44.217
                                    [modifyDate] => 2021-12-15T09:35:44.217
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 616520865
                                    [lineItemKey] => 10814919868597
                                    [sku] => 
                                    [name] => Classic Chocolate Yule Log - 8" - 10"
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/classic-chocolate-yule-log-310390.jpg?v=1637313525
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 42.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                            [6] => stdClass Object
                                                (
                                                    [name] => Christmas Cake Topper
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:44.497
                                    [modifyDate] => 2021-12-15T09:35:44.497
                                )

                            [2] => stdClass Object
                                (
                                    [orderItemId] => 616520866
                                    [lineItemKey] => discount
                                    [sku] => 
                                    [name] => Coupon:  WELCOME-3402-4BU7Z7
                                    [imageUrl] => http://images.shipstation.com/resource/1.gif
                                    [weight] => stdClass Object
                                        (
                                            [value] => 0
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => -4.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 
                                    [fulfillmentSku] => 
                                    [adjustment] => 1
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:44.497
                                    [modifyDate] => 2021-12-15T09:35:44.497
                                )

                        )

                    [orderTotal] => 87.9
                    [amountPaid] => 87.9
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 17/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 20000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [54] => stdClass Object
                (
                    [orderId] => 375321451
                    [orderNumber] => ORD000251814
                    [orderKey] => 4273311940789
                    [orderDate] => 2021-12-15T09:35:19.0000000
                    [createDate] => 2021-12-15T09:35:43.7130000
                    [modifyDate] => 2021-12-15T09:36:13.5530000
                    [paymentDate] => 2021-12-15T09:35:19.0000000
                    [shipByDate] => 
                    [orderStatus] => awaiting_shipment
                    [customerId] => 204423729
                    [customerUsername] => 5915953135797
                    [customerEmail] => marcoleung310@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Chui Chi Leung
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Chui Chi Leung
                            [company] => 
                            [street1] => Flat 4, 16 Botley Road, Park Gate
                            [street2] => 
                            [street3] => 
                            [city] => Southampton
                            [state] => ENG
                            [postalCode] => SO31 1AJ
                            [country] => GB
                            [phone] => 07759966074
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 616520848
                                    [lineItemKey] => 10814920229045
                                    [sku] => P00496S
                                    [name] => Classic Habana Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/classic-habana-gateau-390967.jpg?v=1617979743
                                    [weight] => stdClass Object
                                        (
                                            [value] => 10000
                                            [units] => grams
                                            [WeightUnits] => 2
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 32.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                            [0] => stdClass Object
                                                (
                                                    [name] => Inscription
                                                    [value] => 
                                                )

                                            [1] => stdClass Object
                                                (
                                                    [name] => Mince Pies
                                                    [value] => 
                                                )

                                            [2] => stdClass Object
                                                (
                                                    [name] => Cake Topper
                                                    [value] => 
                                                )

                                            [3] => stdClass Object
                                                (
                                                    [name] => Candles
                                                    [value] => 
                                                )

                                            [4] => stdClass Object
                                                (
                                                    [name] => Balloons
                                                    [value] => 
                                                )

                                            [5] => stdClass Object
                                                (
                                                    [name] => 250gs_coffee
                                                    [value] => 
                                                )

                                        )

                                    [productId] => 12990277
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2021-12-15T09:35:43.637
                                    [modifyDate] => 2021-12-15T09:35:43.637
                                )

                        )

                    [orderTotal] => 37.9
                    [amountPaid] => 37.9
                    [taxAmount] => 0
                    [shippingAmount] => 4.95
                    [customerNotes] => <br/>Delivery Date: 18/12/2021
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Standard Delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_sunday
                    [packageCode] => 
                    [confirmation] => delivery
                    [shipDate] => 
                    [holdUntilDate] => 
                    [weight] => stdClass Object
                        (
                            [value] => 10000
                            [units] => grams
                            [WeightUnits] => 2
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => merchandise
                            [customsItems] => 
                            [nonDelivery] => return_to_sender
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

        )

    [total] => 55
    [page] => 1
    [pages] => 1
)
