<?php
date_default_timezone_set('Europe/London');
$tomorrow_date = date('Y-m-d', strtotime('+1 day'));

$curl = curl_init();
curl_setopt_array($curl, array(
CURLOPT_URL => "https://ssapi11.shipstation.com/orders?storeId=22533&sortDir=desc&orderStatus=awaiting_shipment",
CURLOPT_RETURNTRANSFER => true,
CURLOPT_ENCODING => "",
CURLOPT_MAXREDIRS => 10,
CURLOPT_TIMEOUT => 0,
CURLOPT_FOLLOWLOCATION => true,
CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
CURLOPT_CUSTOMREQUEST => "GET",
CURLOPT_HTTPHEADER => array(
	"Authorization: Basic MDgzYTI5Y2JjNTI2NDc1ZmIzNWQwNGI5MDg3NTBiMjI6M2ZjNWE0OGRlZmZkNDc0OWE1Nzk0OTIyMzg3MjJhNzQ="
),
));
$response = curl_exec($curl);

curl_close($curl);
//echo $response;
$resultData = json_decode($response);
$requestData = json_decode($response);
//file_put_contents('/home/u1476-rtcdbxtxadnb/www/matthews42.sg-host.com/public_html/cron/orderstatusdata.php', print_r($resultData, true));
if($resultData){
	//update order on shipstation
	$resultCount = $resultData->total;
	$Pages = $resultData->pages;

	for ($x = 1; $x <= $Pages; $x++) {
		
		$page = $x;
		
		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://ssapi11.shipstation.com/orders?storeId=22533&sortDir=desc&orderStatus=awaiting_shipment&page=".$page,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
			"Authorization: Basic MDgzYTI5Y2JjNTI2NDc1ZmIzNWQwNGI5MDg3NTBiMjI6M2ZjNWE0OGRlZmZkNDc0OWE1Nzk0OTIyMzg3MjJhNzQ="
		),
		));
		$response = curl_exec($curl);

		curl_close($curl);
		//echo $response;
		$resultDataN = json_decode($response);
		
		//echo '<pre>';
		//print_r($resultDataN);
		//echo '</pre>';
		
		
		
		foreach($resultDataN->orders as $key => $orderdata){
			$customerNotes = $orderdata->customerNotes;
			if (is_null($customerNotes)) {
			//do nothing
			} else {
				$orderId = $orderdata->orderId;
				//$delivrydate = substr($customerNotes, 20, 10);

				if(preg_match("(\d{1,4}([.\-/])\d{1,2}([.\-/])\d{1,4})", $customerNotes, $match))
			    {
			    	$delivrydate = $match[0];
			    	$orderdate = str_replace('/','-',$delivrydate);
					$formattedDate = $orderdate;
					$dateBefore = date('Y-m-d', (strtotime('-1 day', strtotime($formattedDate))));
					
					if(strtotime($formattedDate) > strtotime($tomorrow_date)){
						if($dateBefore){
							//file_put_contents('/home/u1476-rtcdbxtxadnb/www/matthews42.sg-host.com/public_html/cron/orderstatusdata.php', print_r($resultData, true));
							$curlToHold = curl_init();
							curl_setopt_array($curlToHold, array(
							CURLOPT_URL => "https://ssapi.shipstation.com/orders/holduntil",
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_ENCODING => "",
							CURLOPT_MAXREDIRS => 10,
							CURLOPT_TIMEOUT => 0,
							CURLOPT_FOLLOWLOCATION => true,
							CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
							CURLOPT_CUSTOMREQUEST => "POST",
							CURLOPT_POSTFIELDS => "{\n  \"orderId\": " . $orderId . ",\n  \"holdUntilDate\": \"" . $dateBefore . "\"\n}",
							CURLOPT_HTTPHEADER => array(
								"Authorization: Basic MDgzYTI5Y2JjNTI2NDc1ZmIzNWQwNGI5MDg3NTBiMjI6M2ZjNWE0OGRlZmZkNDc0OWE1Nzk0OTIyMzg3MjJhNzQ=",
								"Content-Type: application/json"
							  ),
							));

							$response = curl_exec($curlToHold);
							$c_status = curl_getinfo($curlToHold, CURLINFO_HTTP_CODE);
							curl_close($curlToHold);
						}
					}
			    }

				
				
			}
		}

		
	}
echo "done";
}

