<?php
require dirname(dirname(__FILE__)).'/config/db.php';

$orders = $conn->query('select * from shopify_orders where ss_updated = 0 and ss_created = 1  limit 1'); //and DATE(c_date) = CURDATE()


if ($orders->num_rows > 0) {
    $orders = $orders->fetch_assoc();
} else {
    $orders = array();
}

date_default_timezone_set('Europe/London');
?>


<?php
if($orders){
	$orderNumber = $orders['order_number']; //ordernumber
	
	$curl = curl_init();
	curl_setopt_array($curl, array(
	CURLOPT_URL => "https://ssapi.shipstation.com/orders?orderNumber=".$orderNumber,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 0,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => array(
	"Authorization: Basic MDgzYTI5Y2JjNTI2NDc1ZmIzNWQwNGI5MDg3NTBiMjI6M2ZjNWE0OGRlZmZkNDc0OWE1Nzk0OTIyMzg3MjJhNzQ="
	),
	));
	$response = curl_exec($curl);

	curl_close($curl);
	//echo $response;
	$resultData = json_decode($response);
	$requestData = json_decode($response);
	
	if($resultData){
		//update order on shipstation
		$resultCount = $resultData->total;
		for ($x = 0; $x <= $resultCount-1; $x++) {
		//echo 'ship date : '.$requestData->orders[$x]->shipByDate;
		//if(is_null($requestData->orders[$x]->shipByDate) || $requestData->orders[$x]->shipByDate == '' || empty($requestData->orders[$x]->shipByDate)){
		//	echo 'fdsaf';
		//}
		//exit;
		if(is_null($requestData->orders[$x]->shipByDate) || $requestData->orders[$x]->shipByDate == '' || empty($requestData->orders[$x]->shipByDate)){
			
			//$date = substr($customerNotes, 20, 10);
			
			//$formattedDate = DateTime::createFromFormat('d/m/Y', $orders['delivery_date'])->format('Y-m-d');
			$orderdate = str_replace('/','-',$orders['delivery_date']);
			$formattedDate = $orderdate;
			$dateBefore = date('Y-m-d', (strtotime('-1 day', strtotime($formattedDate))));



		    if($dateBefore == date('Y-m-d')){
				//$dateBefore = null;
		    }

			// echo $dateBefore;

			$orderId = $resultData->orders[$x]->orderId;
			//$requestData->orders[$x]->orderStatus = 'awaiting_shipment';
			$requestData->orders[$x]->shipByDate = $formattedDate;			
			$requestData->orders[$x]->shipDate = $formattedDate;			
			$requestData->orders[$x]->holdUntilDate = $dateBefore;
			$requestData->orders[$x]->advancedOptions->billToParty = null;
			// $requestData->orders[$x]->advancedOptions->billToMyOtherAccount = 'my_other_account';
			
			$requestDataJson = json_encode($requestData->orders[$x]);
			

			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://ssapi.shipstation.com/orders/createorder",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => $requestDataJson,
			  CURLOPT_HTTPHEADER => array(
				"Authorization: Basic MDgzYTI5Y2JjNTI2NDc1ZmIzNWQwNGI5MDg3NTBiMjI6M2ZjNWE0OGRlZmZkNDc0OWE1Nzk0OTIyMzg3MjJhNzQ=",
				"Content-Type: application/json"
			  ),
			));

			$updateResponse = curl_exec($curl);
			
			$c_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	
			if($c_status == 200 || $c_status == '200' || $c_status == 201 || $c_status == '201'){
				$conn->query('update shopify_orders set ss_updated = 1 where order_number = "'.$resultData->orders[$x]->orderNumber.'" ');
			}

			curl_close($curl);

			// $resultData = json_decode($updateResponse);

			// file_put_contents('shipstation_updatedData.php', print_r($resultData, true));

		if($dateBefore){
			
			$curlToHold = curl_init();

			curl_setopt_array($curlToHold, array(
			  CURLOPT_URL => "https://ssapi.shipstation.com/orders/holduntil",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => "{\n  \"orderId\": " . $orderId . ",\n  \"holdUntilDate\": \"" . $dateBefore . "\"\n}",
			  CURLOPT_HTTPHEADER => array(
				"Authorization: Basic MDgzYTI5Y2JjNTI2NDc1ZmIzNWQwNGI5MDg3NTBiMjI6M2ZjNWE0OGRlZmZkNDc0OWE1Nzk0OTIyMzg3MjJhNzQ=",
				"Content-Type: application/json"
			  ),
			));

			$response = curl_exec($curlToHold);
			$c_status = curl_getinfo($curlToHold, CURLINFO_HTTP_CODE);
	
			if($c_status == 200 || $c_status == '200' || $c_status == 201 || $c_status == '201'){
				$conn->query('update shopify_orders set ss_updated = 1 where order_number = "'.$resultData->orders[$x]->orderNumber.'" ');
			}

			curl_close($curlToHold);

			//file_put_contents('orderupdatebycron.php', $orderId);
		}

		}else{
			$conn->query('update shopify_orders set ss_updated = 1 where order_number = "'.$resultData->orders[$x]->orderNumber.'" ');
		}
		}
	
	}

	
}

