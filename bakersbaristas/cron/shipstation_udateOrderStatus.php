<?php
date_default_timezone_set('Europe/London');
$tomorrow_date = date('d/m/Y', strtotime('+1 day'));

$curl = curl_init();
curl_setopt_array($curl, array(
CURLOPT_URL => "https://ssapi11.shipstation.com/orders?storeId=22533&sortDir=ASC&orderStatus=on_hold",
CURLOPT_RETURNTRANSFER => true,
CURLOPT_ENCODING => "",
CURLOPT_MAXREDIRS => 10,
CURLOPT_TIMEOUT => 0,
CURLOPT_FOLLOWLOCATION => true,
CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
CURLOPT_CUSTOMREQUEST => "GET",
CURLOPT_HTTPHEADER => array(
    "Authorization: Basic MDgzYTI5Y2JjNTI2NDc1ZmIzNWQwNGI5MDg3NTBiMjI6M2ZjNWE0OGRlZmZkNDc0OWE1Nzk0OTIyMzg3MjJhNzQ=",
    "cache-control: no-cache",
    "postman-token: f1a22631-b757-4a67-227a-ac44a6f226f7"
  ),
));
$response = curl_exec($curl);

curl_close($curl);
//echo $response;
$resultData = json_decode($response);
$requestData = json_decode($response);
//file_put_contents('/home/u1476-rtcdbxtxadnb/www/matthews42.sg-host.com/public_html/cron/orderstatusdata.php', print_r($resultData, true));
//file_put_contents('orderstatusbycron.php', print_r($resultData, true));
//echo '<pre>';
//print_r($resultData);
//echo '</pre>';
//exit;
if($resultData){
	//update order on shipstation
	
	$resultCount = $resultData->total;
	$Pages = $resultData->pages;

	for ($x = 1; $x <= $Pages; $x++) {
		
		$page = $x;
		
		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://ssapi11.shipstation.com/orders?storeId=22533&sortDir=ASC&orderStatus=on_hold&page=".$page,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
		    "Authorization: Basic MDgzYTI5Y2JjNTI2NDc1ZmIzNWQwNGI5MDg3NTBiMjI6M2ZjNWE0OGRlZmZkNDc0OWE1Nzk0OTIyMzg3MjJhNzQ=",
		    "cache-control: no-cache",
		    "postman-token: f1a22631-b757-4a67-227a-ac44a6f226f7"
		  ),
		));
		$response = curl_exec($curl);

		curl_close($curl);
		//echo $response;
		$resultDataN = json_decode($response);
		
		//echo '<pre>';
		//print_r($resultDataN);
		//echo '</pre>';
		
		
		
		foreach($resultDataN->orders as $key => $orderdata){
			$customerNotes = $orderdata->customerNotes;
			if (is_null($customerNotes)) {
			//do nothing
			} else {
				//$delivrydate = substr($customerNotes, 20, 10);

				if(preg_match("(\d{1,4}([.\-/])\d{1,2}([.\-/])\d{1,4})", $customerNotes, $match))
			    {
			    	$delivrydate = $match[0];

			    	if($delivrydate == $tomorrow_date){
					    $order_num = $orderdata->orderNumber;
						$orderId = $orderdata->orderId;
						$curlToRemoveHold = curl_init();

						curl_setopt_array($curlToRemoveHold, array(
						CURLOPT_URL => "https://ssapi.shipstation.com/orders/restorefromhold",
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_ENCODING => "",
						CURLOPT_MAXREDIRS => 10,
						CURLOPT_TIMEOUT => 0,
						CURLOPT_FOLLOWLOCATION => true,
						CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						CURLOPT_CUSTOMREQUEST => "POST",
						CURLOPT_POSTFIELDS => "{\n  \"orderId\": " . $orderId . "\n}",
						CURLOPT_HTTPHEADER => array(
						    "Authorization: Basic MDgzYTI5Y2JjNTI2NDc1ZmIzNWQwNGI5MDg3NTBiMjI6M2ZjNWE0OGRlZmZkNDc0OWE1Nzk0OTIyMzg3MjJhNzQ=",
						    "cache-control: no-cache",
						    "postman-token: f1a22631-b757-4a67-227a-ac44a6f226f7"
						  ),
						));

						$response = curl_exec($curlToRemoveHold);
						$c_status = curl_getinfo($curlToRemoveHold, CURLINFO_HTTP_CODE);

						if($c_status == 200 || $c_status == '200' || $c_status == 201 || $c_status == '201'){
							//$conn->query('update shopify_orders set ss_updated = 1 where order_number = "'.$resultData->orders[$x]->orderNumber.'" ');
						}
						curl_close($curlToRemoveHold);

						//file_put_contents('/home/u1476-rtcdbxtxadnb/www/matthews42.sg-host.com/public_html/cron/orderstatusupdatebycron.php', $order_num);
					}
			    }
				
			}
		}

		
	}
echo "done";
}

