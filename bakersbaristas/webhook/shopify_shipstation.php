<?php
define('SHOPIFY_APP_SECRET', '777d6e6e0dd5497cafcce10ceb8acb6f35b306ce851e26e5d4bc0e052496b4f6');
function verify_webhook($data, $hmac_header)
{
  $calculated_hmac = base64_encode(hash_hmac('sha256', $data, SHOPIFY_APP_SECRET, true));
  return hash_equals($hmac_header, $calculated_hmac);
}
$hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
$data = file_get_contents('php://input');
$verified = verify_webhook($data, $hmac_header);
//error_log('Webhook verified: '.var_export($verified, true)); //check error.log to see the result
$result = json_decode($data);
//$fname = 'test_'.rand().'.txt';
//file_put_contents('test_123.txt', print_r($result, true));

$paBaseUrl = 'https://f5c5b8bd35c26cf665c5e726dacabef3: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@bakersbaristas.myshopify.com/admin/api/2021-01';

$note_attributes = $result->note_attributes;
$orderid = $result->id;
$delivery_date = '';
foreach($note_attributes as $attributs){
	if($attributs->name == 'Delivery Date'){
		$delivery_date = $attributs->value;
	}
}

$incriptionsData = array();
$line_items = $result->line_items;

$isCNC = false;

foreach($line_items as $key => $item){
	$incriptionsData[$key]['id'] = $item->id;
	$incriptionsData[$key]['variant_id'] = $item->variant_id;
	$incriptionsData[$key]['sku'] = $item->sku;
	
	if (strpos($item->sku, 'XXXXXX-') !== false) {
		$isCNC = true;
	}

	/*foreach($item->properties as $properties){
		if($properties->name == 'inscription_text'){
			$incriptionsData[$key]['incription_text'] = $properties->value;
		}
	}*/
}

	$shipping_lines = $result->shipping_lines;
	$shipping_title = $shipping_lines[0]->title;
	
	$orderupdate = array();
	$orderupdate['order']['id'] = $orderid;

	if($delivery_date){
		
		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => $paBaseUrl."/locations.json",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
		    "cache-control: no-cache",
		    "postman-token: 3ec881d5-0ae2-5bd3-6413-76d6fd5073cb"
		  ),
		));

		$response = curl_exec($curl);
		curl_close($curl);
		
		$locations = json_decode($response);
		
		$locArray = array();
		foreach($locations->locations as $location){
			$locArray[] = $location->name;
		}
		
		if(in_array($shipping_title, $locArray)){
			$note = "online order, customer requested collection on ".$delivery_date;
			$orderupdate['order']['note'] = $note;
		}
	}

	
	
	if($shipping_title == 'Pat Val Premier Delivery'){
		if($result->tags){
			$tags = $result->tags.', Premier';
		}else{
			$tags = 'Premier';
		}
		$orderupdate['order']['tags'] = $tags;
	}elseif($isCNC == true){
		if($result->tags){
			$tags = $result->tags.', Pronto';
		}else{
			$tags = 'Pronto';
		}
		$orderupdate['order']['tags'] = $tags;
	}elseif($shipping_title != ''){
		if($result->tags){
			$tags = $result->tags.', Next Day';
		}else{
			$tags = 'Next Day';
		}
		$orderupdate['order']['tags'] = $tags;
	}

	if($result->tags){
		$tags = $result->tags.', Next Day';
	}else{
		$tags = 'Next Day';
	}
	$orderupdate['order']['tags'] = $tags;
	
	$orderdata =  json_encode($orderupdate);
			
	$curl = curl_init();

	curl_setopt_array($curl, array(
	CURLOPT_URL => $paBaseUrl."/orders/".$orderid.".json",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 0,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "PUT",
	CURLOPT_POSTFIELDS => $orderdata,
	CURLOPT_HTTPHEADER => array(
		"Content-Type: application/json",
		"Cookie: __cfduid=db9ea86a18592f2c26102a550468d4f331587629534"
		),
	));
	curl_exec($curl);
	curl_close($curl);




//file_put_contents('incriptionsData.php', print_r($incriptionsData, true));

$orderNumber = $result->name; //ordernumber
$orderData = array();
$orderData = json_encode($incriptionsData);


require dirname(dirname(__FILE__)).'/config/db.php';


//$query = $conn->query('select * from shopify_inscription_settings');
//$query = "insert into shopify_orders (`order_id`, `order_number`, `order_data`, `delivery_date`, `c_date`, `ss_updated`, `ss_created`) values ('".$orderid."','".$orderNumber."','".addslashes($orderData)."', '".addslashes($delivery_date)."', '".date('Y-m-d H:i:s')."', '0','0') ";
//file_put_contents('query.txt', $query);
$conn->query("insert into shopify_orders (`order_id`, `order_number`, `order_data`, `delivery_date`, `c_date`, `ss_updated`, `ss_created`) values ('".$orderid."','".$orderNumber."','".addslashes($orderData)."', '".addslashes($delivery_date)."', '".date('Y-m-d H:i:s')."', '0','0') ");