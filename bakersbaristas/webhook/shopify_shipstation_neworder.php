<?php

header('Content-Type: application/json');
$data = file_get_contents('php://input');

$result = json_decode($data);

require dirname(dirname(__FILE__)).'/config/db.php';

/*$shopify_settings = $conn->query('select * from shopify_inscription_settings limit 1');
//$result = $conn->query($sql);

if ($shopify_settings->num_rows > 0) {
    $settings = $shopify_settings->fetch_assoc();
} else {
    $settings = array();
}*/
date_default_timezone_set('Europe/London');
function searchForId($id, $array) {
   foreach ($array as $key => $val) {
       if ($val['sku'] === $id) {
           return $key;
       }
   }
   return null;
}

$getOrderUrl = $result->resource_url;
//file_put_contents('shipstation_OrderUrl.php', print_r($getOrderUrl, true));
$getCurl = curl_init();

curl_setopt_array($getCurl, array(
  CURLOPT_URL => $getOrderUrl,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "Host: ssapi.shipstation.com",
    "Authorization: Basic MDgzYTI5Y2JjNTI2NDc1ZmIzNWQwNGI5MDg3NTBiMjI6M2ZjNWE0OGRlZmZkNDc0OWE1Nzk0OTIyMzg3MjJhNzQ="
  ),
));

$response = curl_exec($getCurl);

curl_close($getCurl);


$resultData = json_decode($response);

$requestData = json_decode($response);

//file_put_contents('shipstation_orderData.php', print_r($resultData, true));


$resultCount = $resultData->total;
//echo "resultCount: " . $resultCount . "\n ";

foreach($resultData->orders as $key => $orderdata){
	$conn->query('update shopify_orders set ss_created = 1 where order_number = "'.$orderdata->orderNumber.'" ');
	$customerNotes = $orderdata->customerNotes;
	if (is_null($customerNotes)) {
    //do nothing
    } else {

    if(preg_match("(\d{1,4}([.\-/])\d{1,2}([.\-/])\d{1,4})", $customerNotes, $match))
    {
        $date = $match[0];
        //$date = substr($customerNotes, 20, 10);
        //file_put_contents('customernote.php', $date);
        $formattedDate = DateTime::createFromFormat('d/m/Y', $date)->format('Y-m-d');
        $dateBefore = date('Y-m-d', (strtotime('-1 day', strtotime($formattedDate))));
        
      
        $orderId = $resultData->orders[$key]->orderId;
      
        if(strtotime($dateBefore) <= strtotime(date('Y-m-d'))){
          $dateBefore = '';
        }else{
          $requestData->orders[$key]->orderStatus = 'on_hold';
          $requestData->orders[$key]->holdUntilDate = $dateBefore;
          $requestData->orders[$key]->shipDate = $dateBefore;
        }
      //$requestData->orders[$key]->holdUntilDate = $dateBefore;
        $requestData->orders[$key]->shipByDate = $formattedDate;
      //$requestData->orders[$key]->shipDate = $dateBefore;
        $requestData->orders[$key]->advancedOptions->billToParty = null;
        
        $requestDataJson = json_encode($requestData->orders[$key]);

        //file_put_contents('shipstation_requestDataJson.php', print_r($requestDataJson, true));


        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://ssapi.shipstation.com/orders/createorder",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $requestDataJson,
          CURLOPT_HTTPHEADER => array(
            "Authorization: Basic MDgzYTI5Y2JjNTI2NDc1ZmIzNWQwNGI5MDg3NTBiMjI6M2ZjNWE0OGRlZmZkNDc0OWE1Nzk0OTIyMzg3MjJhNzQ=",
            "Content-Type: application/json"
          ),
        ));

        $updateResponse = curl_exec($curl);
      
        $c_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
          
        if($c_status == 200 || $c_status == '200' || $c_status == 201 || $c_status == '201'){
          $conn->query('update shopify_orders set ss_updated = 1 where order_number = "'.$resultData->orders[$key]->orderNumber.'" ');
        }

        curl_close($curl);


      if($dateBefore){
      
        $curlToHold = curl_init();

        curl_setopt_array($curlToHold, array(
          CURLOPT_URL => "https://ssapi.shipstation.com/orders/holduntil",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "{\n  \"orderId\": " . $orderId . ",\n  \"holdUntilDate\": \"" . $dateBefore . "\"\n}",
          CURLOPT_HTTPHEADER => array(
          "Authorization: Basic MDgzYTI5Y2JjNTI2NDc1ZmIzNWQwNGI5MDg3NTBiMjI6M2ZjNWE0OGRlZmZkNDc0OWE1Nzk0OTIyMzg3MjJhNzQ=",
          "Content-Type: application/json"
          ),
        ));

        $response = curl_exec($curlToHold);
        $c_status = curl_getinfo($curlToHold, CURLINFO_HTTP_CODE);
        
        if($c_status == 200 || $c_status == '200' || $c_status == 201 || $c_status == '201'){
          $conn->query('update shopify_orders set ss_updated = 1 where order_number = "'.$resultData->orders[$key]->orderNumber.'" ');
        }

        curl_close($curlToHold);
      }

    }  

    
  }
}

/*

for ($x = 0; $x <= $resultCount-1; $x++) {
	
  $conn->query('update shopify_orders set ss_created = 1 where order_number = "'.$resultData->orders[$x]->orderNumber.'" ');
  $customerNotes = $resultData->orders[$x]->customerNotes;

 
  if (is_null($customerNotes)) {
    //do nothing
  } else {

    $date = substr($customerNotes, 20, 10);
    $formattedDate = DateTime::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    $dateBefore = date('Y-m-d', (strtotime('-1 day', strtotime($formattedDate))));

    

    // echo $dateBefore;

    $result = $conn->query('select * from shopify_orders where order_number = "'.$resultData->orders[$x]->orderNumber.'" ');
    $data = $result->fetch_assoc();
    if($data){
      $order_Data = json_decode($data['order_data']);

      if($settings['integration_1_active'] == "1"){
        if($data['holduntill']){
          $holduntill = date('Y-m-d', (strtotime('-'.$data['holduntill'].' day', strtotime($formattedDate))));
          $dateBefore = $holduntill;
        }
      }
      
    }else{
      $order_Data = array();
    }

    if($dateBefore == date('Y-m-d')){
      $dateBefore = '';
    }

    $orderId = $resultData->orders[$x]->orderId;
	
	$requestData->orders[$x]->orderStatus = 'on_hold';
    $requestData->orders[$x]->shipByDate = $formattedDate;
    $requestData->orders[$x]->holdUntilDate = $dateBefore;
	$requestData->orders[$x]->shipDate = $dateBefore;
    $requestData->orders[$x]->advancedOptions->billToParty = null;
    // $requestData->orders[$x]->advancedOptions->billToMyOtherAccount = 'my_other_account';

    if($settings['integration_2_active'] == "1"){
      /*foreach($resultData->orders[$x]->items as $key => $orderitem){
          $itemArrkey = searchForId($orderitem->sku, $order_Data);
          if($itemArrkey){
            $requestData->orders[$x]->items[$itemArrkey]->options[$key]['name'] = 'incription_text';
            $requestData->orders[$x]->items[$itemArrkey]->options[$key]['value'] = $order_Data[$x]['incription_text'];
          }
        }
    }

    $requestDataJson = json_encode($requestData->orders[$x]);

    file_put_contents('shipstation_requestDataJson.php', print_r($requestDataJson, true));

    // echo $requestDataJson;

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://ssapi.shipstation.com/orders/createorder",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $requestDataJson,
      CURLOPT_HTTPHEADER => array(
        "Authorization: Basic OTc3MmI5NmEyZWQ2NDE2MjhiNGIxMjI2OGI3MjQzYWM6MDJhMTlhNjIwOTA3NDEwNzgzZjllZmM1MTk1ZDE3YzA=",
        "Content-Type: application/json"
      ),
    ));

    $updateResponse = curl_exec($curl);

    curl_close($curl);

    // $resultData = json_decode($updateResponse);

    // file_put_contents('shipstation_updatedData.php', print_r($resultData, true));


    $curlToHold = curl_init();

    curl_setopt_array($curlToHold, array(
      CURLOPT_URL => "https://ssapi.shipstation.com/orders/holduntil",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "{\n  \"orderId\": " . $orderId . ",\n  \"holdUntilDate\": \"" . $dateBefore . "\"\n}",
      CURLOPT_HTTPHEADER => array(
        "Authorization: Basic OTc3MmI5NmEyZWQ2NDE2MjhiNGIxMjI2OGI3MjQzYWM6MDJhMTlhNjIwOTA3NDEwNzgzZjllZmM1MTk1ZDE3YzA=",
        "Content-Type: application/json"
      ),
    ));

    $response = curl_exec($curlToHold);
	$c_status = curl_getinfo($curlToHold, CURLINFO_HTTP_CODE);
	
	if($c_status == 200 || $c_status == '200' || $c_status == 201 || $c_status == '201'){
		$conn->query('update shopify_orders set ss_updated = 1 where order_number = "'.$resultData->orders[$x]->orderNumber.'" ');
	}

    curl_close($curlToHold);

    echo " \n Processed order: " . $orderId;
  }
}
*/