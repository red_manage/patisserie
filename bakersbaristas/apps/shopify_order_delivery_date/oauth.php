<?php 

// Set variables for our request
$shop = "bakersbaristas";
$api_key = "e471e6b093076c14e9ad09a987b93304";
$scopes = "read_orders,write_products";
$redirect_uri = "https://matthews42.sg-host.com/bakersbaristas/apps/shopify_order_delivery_date/index.php";

// Build install/approval URL to redirect to
$install_url = "https://" . $shop . ".myshopify.com/admin/oauth/authorize?client_id=" . $api_key . "&scope=" . $scopes . "&redirect_uri=" . urlencode($redirect_uri);

// Redirect
header("Location: " . $install_url);
die();

?>