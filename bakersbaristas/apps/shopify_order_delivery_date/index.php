<?php
// Set variables for our request
$query = array(
  "client_id" => 'e471e6b093076c14e9ad09a987b93304', // Your API key
  "client_secret" => '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', // Your app credentials (secret key)
  "code" => $params['code'] // Grab the access key from the URL
);

// Generate access token URL
$access_token_url = "https://" . $params['shop'] . "/admin/oauth/access_token";

// Configure curl client and execute request
$ch = curl_init();
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_URL, $access_token_url);
curl_setopt($ch, CURLOPT_POST, count($query));
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($query));
$result = curl_exec($ch);
curl_close($ch);

// Store the access token
$result = json_decode($result, true);
$access_token = $result['access_token'];

?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>
<?php
require dirname(dirname(dirname(__FILE__))).'/config/db.php';
//echo "Connected successfully";
//exit;

/*$result = $conn->query('select * from shopify_orders where order_number = "ORD0001036" ');
$data = $result->fetch_assoc();

$order_Data = json_decode($data['order_data']);
echo '<pre>';
print_r($order_Data);
echo '</pre>';
*/
if(isset($_POST['submit'])){
	//echo '<pre>';
	//print_r($_POST);
	//echo '</pre>';
	
  $delivery_days = json_encode($_POST['delivery_days']);
  $delivery_warning_message = $_POST['delivery_warning_message'];
  $disable_dates = $_POST['disable_dates'];
  $booking_period = $_POST['booking_period'];
  $booking_interval = $_POST['booking_interval'];
  $cut_off_time = $_POST['cut_off_time'];
  $dpd_weekday_code = $_POST['dpd_weekday_code'];
  $dpd_saturday_code = $_POST['dpd_saturday_code'];
  $dpd_sunday_code = $_POST['dpd_sunday_code'];
  
  $pre_first_delivery_date = $_POST['pre_first_delivery_date'];
  $pre_tag = $_POST['pre_tag'];

  $result = $conn->query('select * from shopify_order_delivery_date_custom');
  if ($result->num_rows > 0) {
      $row = $result->fetch_assoc();
      $conn->query('update shopify_order_delivery_date_custom set delivery_warning_message = "'.addslashes($delivery_warning_message).'", disable_dates = "'.$disable_dates.'", delivery_days = "'.addslashes($delivery_days).'", booking_period = "'.$booking_period.'", booking_interval = "'.$booking_interval.'", cut_off_time = "'.$cut_off_time.'", dpd_weekday_code = "'.$dpd_weekday_code.'", dpd_saturday_code = "'.$dpd_saturday_code.'", dpd_sunday_code = "'.$dpd_sunday_code.'", pre_first_delivery_date = "'.$pre_first_delivery_date.'", pre_tag = "'.$pre_tag.'" where id = "'.$row['id'].'" ');
  } else {
	  //echo 'insert into shopify_order_delivery_date_custom (`delivery_warning_message`, `delivery_days`, `booking_period`, `booking_interval`, `cut_off_time`, `dpd_weekday_code`, `dpd_saturday_code`, `dpd_sunday_code`) values ("'.$delivery_warning_message.'", "'.$delivery_days.'", "'.$booking_period.'", "'.$booking_interval.'", "'.$cut_off_time.'", "'.$dpd_weekday_code.'", "'.$dpd_saturday_code.'", "'.$dpd_sunday_code.'") ';
      $conn->query('insert into shopify_order_delivery_date_custom (`delivery_warning_message`, `disable_dates`, `delivery_days`, `booking_period`, `booking_interval`, `cut_off_time`, `dpd_weekday_code`, `dpd_saturday_code`, `dpd_sunday_code`) values ("'.addslashes($delivery_warning_message).'", "'.$disable_dates.'", "'.addslashes($delivery_days).'", "'.$booking_period.'", "'.$booking_interval.'", "'.$cut_off_time.'", "'.$dpd_weekday_code.'", "'.$dpd_saturday_code.'", "'.$dpd_sunday_code.'") ');
  }

}

$result = $conn->query('select * from shopify_order_delivery_date_custom limit 1');
//$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
} else {
    $row = array();
}
//echo '<pre>';
//print_r($row);
//echo '</pre>';
$saveddelivery_days = json_decode($row['delivery_days']);

$booking_period = $row['booking_period'];
if($booking_period){
}else{
	$booking_period = 45;
}

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Shipping Date Selector App</title>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
.ciappsectiontwo{
	width: 100%;
	background: #d9d9d9;
	font-family: verdana;
	float: left;
    padding: 50px 0px;
}
.mcontainerapptwo{
	width: 1100px;
	margin: auto;
	margin: 0 30px;
}
.tpheaddingm{
	font-size: 18px;
	color: #000;
}
.shipdateselc{
	width: 100%;
	float: left;
}
.firstsdateselc{
	width: 50%;
	float: left;
}
.secondsdateselc{
	width: 50%;
	float: left;
}
.firstsdateselc h3, .secondsdateselc h3 {
    font-size: 14px;
    color: #000;
    font-weight: normal;
    margin-bottom: 0px;
    text-decoration: underline;
}
.inforntbx{
	width: 100%;
	display: flex;
    align-items: center;
    margin-top: 5px;
}
.inforntbx span{
	font-size: 12px;
    color: #000;
    min-width: 150px;
}
.subbuttonsm input[type="checkbox"]{
	background: #FFF;
	color: #333;
	border:solid 1px #1e3c84;
	padding: 0px;
	display: block;
	width: 20px;
    height: 20px;
    margin: 0px;
}
.subbuttonsm input[type="text"]{
	background: #FFF;
	color: #333;
	border:solid 1px #1e3c84;
	padding: 5px 10px;
	display: block;
}
.subbuttonsm input[type="submit"], .btn{
	background: #1e3c84;
	color: #FFF;
	border:solid 1px #1e3c84;
	padding: 5px 10px;
	display: block;
	width: 100%;
}
.inforntbxtwo{
	width: 100%;
    margin-top: 5px;
}
.inforntbxtwo span{
	font-size: 12px;
    color: #000;
    min-width: 150px;
    padding-bottom: 5px;
    display: block;
}
.inforntbxtwo textarea{
	width: 50%;
	height: 100px;
	border:solid 1px #1e3c84;
	padding: 5px 10px;
}

</style>
</head>

<body>
<form method="post">
<div class="ciappsectiontwo">
	<div class="mcontainerapptwo">
		<h2 class="tpheaddingm">Shipping Date Selector App</h2>
		<div class="shipdateselc">
			<div class="firstsdateselc">
				<h3>Test Query</h3>
				<div class="inforntbx">
					<span>Test Postcode</span>
					<div class="subbuttonsm">
						<input type="text" name="" id="postalcode">
						<input type="button" class="btn" value="Submit" name="" id="checkpostalcode">
					</div>
				</div>
				<div class="inforntbx">
					<span>DPD Delivery Days</span>
					<div class="subbuttonsm">
						<input type="text" name="" id="checkdays" readonly>
					</div>
				</div>
				<div class="inforntbx">
					<span>Next Available Date</span>
					<div class="subbuttonsm">
						<input type="text" name="" id="nextdeliverydate" readonly>
					</div>
				</div>
				<h3>Blocked Days</h3>
				<div class="inforntbxtwo">
					<span>(dd/mm/yyyy separated by a comma) </span>
					<div class="subbuttonsm">
					<input id="blocked_days" type="text" class="datepicker" autocomplete="off">
					<br/>
					<textarea id="disable_dates" name="disable_dates"><?php echo $row['disable_dates']; ?></textarea>
					</div>
				</div>
				<h3>Delivery warning message</h3>
				<div class="inforntbxtwo">
					<span>(appears below calendar if completed) </span>
					<div class="subbuttonsm">
						<textarea name="delivery_warning_message"><?php echo $row['delivery_warning_message']; ?></textarea>
					</div>
				</div>
			</div>
			<div class="secondsdateselc">
				<h3>Delivery Days</h3>
				<div class="inforntbx">
					<span>Sunday</span>
					<div class="subbuttonsm">
						<input type="checkbox" name="delivery_days[]" value="sunday" <?php if(in_array('sunday',$saveddelivery_days)){ echo 'checked'; } ?>>
					</div>
				</div>
				<div class="inforntbx">
					<span>Monday</span>
					<div class="subbuttonsm">
						<input type="checkbox" name="delivery_days[]" value="monday" <?php if(in_array('monday',$saveddelivery_days)){ echo 'checked'; } ?>>
					</div>
				</div>
				<div class="inforntbx">
					<span>Tuesday</span>
					<div class="subbuttonsm">
						<input type="checkbox" name="delivery_days[]" value="tuesday" <?php if(in_array('tuesday',$saveddelivery_days)){ echo 'checked'; } ?>>
					</div>
				</div>
				<div class="inforntbx">
					<span>Wednesday</span>
					<div class="subbuttonsm">
						<input type="checkbox" name="delivery_days[]" value="wednesday" <?php if(in_array('wednesday',$saveddelivery_days)){ echo 'checked'; } ?>>
					</div>
				</div>
				<div class="inforntbx">
					<span>Thursday</span>
					<div class="subbuttonsm">
						<input type="checkbox" name="delivery_days[]" value="thursday" <?php if(in_array('thursday',$saveddelivery_days)){ echo 'checked'; } ?>>
					</div>
				</div>
				<div class="inforntbx">
					<span>Friday</span>
					<div class="subbuttonsm">
						<input type="checkbox" name="delivery_days[]" value="friday" <?php if(in_array('friday',$saveddelivery_days)){ echo 'checked'; } ?>>
					</div>
				</div>
				<div class="inforntbx">
					<span>Saturday</span>
					<div class="subbuttonsm">
						<input type="checkbox" name="delivery_days[]" value="saturday" <?php if(in_array('saturday',$saveddelivery_days)){ echo 'checked'; } ?>>
					</div>
				</div>
				<h3>Settings</h3>
				<div class="inforntbx">
					<span>Booking period (days)</span>
					<div class="subbuttonsm">
						<input type="text" name="booking_period" value="<?php echo $row['booking_period']; ?>">
					</div>
				</div>
				<div class="inforntbx">
					<span>Interval (1 = next day)</span>
					<div class="subbuttonsm">
						<input type="text" name="booking_interval" value="<?php echo $row['booking_interval']; ?>">
					</div>
				</div>
				<div class="inforntbx">
					<span>Cut off time</span>
					<div class="subbuttonsm">
						<input type="time" name="cut_off_time" value="<?php echo $row['cut_off_time']; ?>">
					</div>
				</div>
				<h3>DPD delivery inputs</h3>
				<div class="inforntbx">
					<span>Weekday code</span>
					<div class="subbuttonsm">
						<input type="text" name="dpd_weekday_code" value="<?php echo $row['dpd_weekday_code']; ?>">
					</div>
				</div>
				<div class="inforntbx">
					<span>Saturday code</span>
					<div class="subbuttonsm">
						<input type="text" name="dpd_saturday_code" value="<?php echo $row['dpd_saturday_code']; ?>">
					</div>
				</div>
				<div class="inforntbx">
					<span>Sunday code</span>
					<div class="subbuttonsm">
						<input type="text" name="dpd_sunday_code" value="<?php echo $row['dpd_sunday_code']; ?>">
					</div>
				</div>
				
				<h3>Prelaunch products</h3>
				<div class="inforntbx">
					<span>First delivery date</span>
					<div class="subbuttonsm">
						<input type="text" name="pre_first_delivery_date" id="pre_first_delivery_date" value="<?php echo $row['pre_first_delivery_date']; ?>">
					</div>
				</div>
				<div class="inforntbx">
					<span>Tag</span>
					<div class="subbuttonsm">
						<input type="text" name="pre_tag" value="<?php echo $row['pre_tag']; ?>">
					</div>
				</div>
				
			</div>
		</div>
		
		<div class="inforntbx">
		<div class="subbuttonsm">
			<br/>
			<input type="submit" value="Save" name="submit">
		</div>
		</div>
	</div>
</div>
</form>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  $( function() {
	  var datearr = [];
	  var APP_PATH = '<?php echo APP_PATH; ?>';
	$('#pre_first_delivery_date').datepicker();
	$('#blocked_days').datepicker({
		dateFormat: 'dd/mm/yy', 
		multiSelect: 999,
		//minDate: 0,
		//maxDate: <?php echo $booking_period; ?>,
		onSelect: function(dates) {
			if($('#disable_dates').val()){
				var datearr = $('#disable_dates').val().split(',');
			}else{
				var datearr = [];	
			}
			datearr.push(dates);
			console.log(datearr);
		    $('#disable_dates').val(jQuery.unique(datearr));
		}
	});
	
	jQuery('body').on('click', '#checkpostalcode', function(){
		var postalcode = jQuery('#postalcode').val();
        var script = document.createElement("script");
        script.src = APP_PATH+"/dpd_checkPostalCode.php?code="+postalcode;
        document.head.appendChild(script);
	});
  } );
  </script>
</body>
</html>