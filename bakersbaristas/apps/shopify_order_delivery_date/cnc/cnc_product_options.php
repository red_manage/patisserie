<?php
$product_id = $_GET['product_id'];
$variant_id = $_GET['variant_id'];
$location_id = '34789523523';
/*if($location_id){
	$curl = curl_init();
	curl_setopt_array($curl, array(
	CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com/admin/api/2020-04/inventory_levels.json?location_ids=".$location_id."&limit=250",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 0,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => array(
	"Cookie: __cfduid=db9ea86a18592f2c26102a550468d4f331587629534"
	),
	));

	$response = curl_exec($curl);

	curl_close($curl);
	$html = '';
	if($response){
		$results = json_decode($response);
		foreach($results->inventory_levels as $key => $inventory_levels){
			if($inventory_levels->available > 0){
				$inventory_itemArray[$key]['inventory_item_id'] = $inventory_levels->inventory_item_id;
				$inventory_itemArray[$key]['available'] = $inventory_levels->available;
			}
			
		}
	}
	
}*/

function searchForItemInventory($id, $array) {
   foreach ($array as $key => $val) {
       if ($val['inventory_item_id'] === $id) {
           if($key == 0){
			   return 'a';
		   }else{
			   return $key;
		   }
       }
   }
   return null;
}

if($product_id && $variant_id){
	$curl = curl_init();
	curl_setopt_array($curl, array(
	CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com//admin/api/2020-04/products/".$product_id.".json",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 0,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => array(
		"Cookie: __cfduid=d8e0f58645e9c4695bc749c2b4b9ac5f21590488150"
	),
	));

	$response = curl_exec($curl);

	curl_close($curl);
	$results = json_decode($response);
	
	$isPatValPremiertag = false;
	$isCNCtag = false;
	if (strpos(strtolower($results->product->tags), 'pat val premier') !== false) {
		$isPatValPremiertag = true;
	}
	if (strpos(strtolower($results->product->tags), 'click and collect') !== false) {
		$isCNCtag = true;
	}
		
	if($variant_id){
		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com//admin/api/2020-04/variants/".$variant_id.".json",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
			"Cookie: __cfduid=d8e0f58645e9c4695bc749c2b4b9ac5f21590488150"
		),
		));

		$variants = curl_exec($curl);

		curl_close($curl);
		
		echo 'jQuery("#check_premier_service").show();';
		echo 'jQuery("#cnc_options_available").show();';
		if($variants){
			$variants = json_decode($variants);
			//echo '<pre>';
			//print_r($variants);
			//exit;
			//foreach($variants->variant as $key => $variant){
				
				if($_GET['isCollectionPage'] == 1){
					if($variants->variant->inventory_quantity > 0){
						if($isPatValPremiertag == false && $isCNCtag == true){
							echo 'var cllss = "cnc-'.$product_id.'"; '; 
							echo 'jQuery("."+cllss).html("(Click & Collect only)");';
						}
					}
				}else{
					
					$inventory_item_id = $variants->variant->inventory_item_id;

					if($location_id){
						$curl = curl_init();
						curl_setopt_array($curl, array(
						CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com/admin/api/2020-07/inventory_levels.json?inventory_item_ids=".$inventory_item_id,
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_ENCODING => "",
						CURLOPT_MAXREDIRS => 10,
						CURLOPT_TIMEOUT => 0,
						CURLOPT_FOLLOWLOCATION => true,
						CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						CURLOPT_CUSTOMREQUEST => "GET",
						CURLOPT_HTTPHEADER => array(
						"Cookie: __cfduid=db9ea86a18592f2c26102a550468d4f331587629534"
						),
						));

						$response = curl_exec($curl);

						curl_close($curl);
						$html = '';
						if($response){
							$results = json_decode($response);
							
							foreach($results->inventory_levels as $key => $inventory_levels){
								if($inventory_levels->available > 0 && $inventory_levels->location_id == $location_id){
									$inventory_itemArray[$key]['inventory_item_id'] = $inventory_levels->inventory_item_id;
									$inventory_itemArray[$key]['available'] = $inventory_levels->available;
									$inventory_itemArray[$key]['location_id'] = $inventory_levels->location_id;
								}
								
							}
						}
						
					}
					//echo '<pre>';
					//print_r($inventory_itemArray);
					//$searchKey = searchForItemInventory($inventory_item_id,$inventory_itemArray);
					if($inventory_itemArray){
						//if($searchKey == 'a'){
						//	$searchKey = 0;
						//}
						if($isPatValPremiertag == false){
							echo 'jQuery("#check_premier_service").removeClass("active");';
							echo 'jQuery("#check_premier_service span").html("Premier Delivery <br/> not available");';
							echo 'jQuery("#check_premier_service .over-text").html("This item is not currently available via our Premier Delivery Service.");';
							echo 'jQuery("#check_premier_service").addClass("inactive");'; //echo 'Grey van';
						}elseif($isPatValPremiertag == true){
							echo 'jQuery("#check_premier_service").removeClass("inactive");';
							echo 'jQuery("#check_premier_service .over-text").html("This item is available via our Premier Delivery Service. Please select this option at checkout.");';
							echo 'jQuery("#check_premier_service span").html("Premier Delivery <br/> available");';
							echo 'jQuery("#check_premier_service").addClass("active");'; //echo 'blue van';
						}else{
							echo 'jQuery("#check_premier_service").removeClass("active");';
							echo 'jQuery("#check_premier_service .over-text").html("This item is not currently available via our Premier Delivery Service.");';
							echo 'jQuery("#check_premier_service span").html("Premier Delivery <br/> not available");';
							echo 'jQuery("#check_premier_service").addClass("inactive");'; //echo 'Grey van';
						}
						
						//if($isCNCtag == true && $isPatValPremiertag == false && $inventory_itemArray[$searchKey]['available'] > 0){
						//	echo 'jQuery("#check_premier_service").hide();';
						//}
					
					}else{
							echo 'jQuery("#check_premier_service").removeClass("active");';
							echo 'jQuery("#check_premier_service .over-text").html("This item is not currently available via our Premier Delivery Service.");';
							echo 'jQuery("#check_premier_service span").html("Premier Delivery <br/> not available");';
							echo 'jQuery("#check_premier_service").addClass("inactive");'; //echo 'Grey van';
					}
					
					if($isCNCtag == true){
						echo 'jQuery("#cnc_options_available").removeClass("inactive");';
						echo 'jQuery("#cnc_options_available .over-text").removeClass("disable");';
						echo 'jQuery("#cnc_options_available .over-text").html("This item may be ordered online and picked up in store (subject to availability). Please select this option at checkout to confirm local availability.");';
						echo 'jQuery("#cnc_options_available span").html("Click & Collect <br/> options available");';
						echo 'jQuery("#cnc_options_available").addClass("active");';
						//echo 'cnc available';
					}else{
						echo 'jQuery("#cnc_options_available").removeClass("active");';
						echo 'jQuery("#cnc_options_available .over-text").addClass("disable");';
						echo 'jQuery("#cnc_options_available span").html("Click & Collect <br/> not available");';
						echo 'jQuery("#cnc_options_available").addClass("inactive");';
						echo 'jQuery("#cnc_options_available").hide();';
						//echo 'cnc not available';
					}
					
					if($variants->variant->inventory_quantity <= 0){
						echo 'jQuery("#check_premier_service").addClass("inactive");';
						echo 'jQuery("#check_premier_service .over-text").addClass("disable");';
						echo 'jQuery("#cnc_options_available .over-text").addClass("disable");';
						echo 'jQuery("#cnc_options_available").addClass("inactive");';
						echo 'jQuery("#cnc_options_available").hide();';
						echo 'jQuery("#check_premier_service").hide();';
					}	
				}
				
				
			//}
		}

		
	}
		
	
	
}

?>