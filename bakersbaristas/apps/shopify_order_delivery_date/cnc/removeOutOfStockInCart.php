<?php
$location_id = $_GET['location_id'];
$inventory_itemArray = array();
if($location_id){
	$curl = curl_init();
	curl_setopt_array($curl, array(
	CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com/admin/api/2020-04/inventory_levels.json?location_ids=".$location_id."&limit=250",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 0,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => array(
	"Cookie: __cfduid=db9ea86a18592f2c26102a550468d4f331587629534"
	),
	));

	$response = curl_exec($curl);

	curl_close($curl);
	$html = '';
	if($response){
		$results = json_decode($response);
		foreach($results->inventory_levels as $key => $inventory_levels){
			if($inventory_levels->available > 0){
				$inventory_itemArray[$key]['inventory_item_id'] = $inventory_levels->inventory_item_id;
				$inventory_itemArray[$key]['available'] = $inventory_levels->available;
			}
			
		}
	}
	
}

function searchForItemInventory($id, $array) {
   foreach ($array as $key => $val) {
       if ($val['inventory_item_id'] === $id) {
           if($key == 0){
			   return 'a';
		   }else{
			   return $key;
		   }
       }
   }
   return null;
}
//echo '<pre>';
//print_r($inventory_itemArray);

$productArray = array();



$variant_id = $_GET['variant_id'];
$product_id = $_GET['product_id'];

$products = explode(',',$product_id);
$variants = explode(',',$variant_id);

$data = array_combine($products, $variants);
$remove = '';
if($data){
	$removeIds = array();
	foreach($data as $product_id => $variant_id){
		if($product_id && $variant_id){
			$curl = curl_init();
			curl_setopt_array($curl, array(
			CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com//admin/api/2020-04/products/".$product_id.".json",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"Cookie: __cfduid=d8e0f58645e9c4695bc749c2b4b9ac5f21590488150"
			),
			));

			$response = curl_exec($curl);

			curl_close($curl);
			$results = json_decode($response);
			if($variant_id){
				$curl = curl_init();
				curl_setopt_array($curl, array(
				CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com//admin/api/2020-04/variants/".$variant_id.".json",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
				CURLOPT_HTTPHEADER => array(
					"Cookie: __cfduid=d8e0f58645e9c4695bc749c2b4b9ac5f21590488150"
				),
				));

				$variants = curl_exec($curl);

				curl_close($curl);
				
				
				if($variants){
					$variants = json_decode($variants);
					//echo '<pre>';
					//print_r($variants);
					//exit;
					//foreach($variants->variant as $key => $variant){
						$inventory_item_id = $variants->variant->inventory_item_id;
						$searchKey = searchForItemInventory($inventory_item_id,$inventory_itemArray);
						if($searchKey != null || $searchKey == 'a' || $searchKey != '' || !empty($searchKey)){
							//do nothing
						}else{ 
							$remove .= "updates[".$variant_id."]=0&";
							
						}
					//}
				}

				
			}
			
			
		}
	}
}
$ids = rtrim($remove,"&");
?>
jQuery.post('/cart/update.js',
  "<?php echo $ids;  ?>"
);
setTimeout(function(){
  jQuery('#removeOutOfStock').html('Remove item(s)');
  location.reload();
}, 2000);
<?php
exit;