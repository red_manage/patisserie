<?php 

// Set variables for our request
$shop = "bakersbaristas";
$api_key = "1246c47557dc7b2c62b0594e8e5391e1";
$scopes = "read_orders,write_products";
$redirect_uri = "https://matthews42.sg-host.com/bakersbaristas/apps/shopify_shipstation_app/index.php";

// Build install/approval URL to redirect to
$install_url = "https://" . $shop . ".myshopify.com/admin/oauth/authorize?client_id=" . $api_key . "&scope=" . $scopes . "&redirect_uri=" . urlencode($redirect_uri);

// Redirect
header("Location: " . $install_url);
die();

?>