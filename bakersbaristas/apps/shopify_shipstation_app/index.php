<?php
// Set variables for our request
$query = array(
  "client_id" => '1246c47557dc7b2c62b0594e8e5391e1', // Your API key
  "client_secret" => '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', // Your app credentials (secret key)
  "code" => $params['code'] // Grab the access key from the URL
);

// Generate access token URL
$access_token_url = "https://" . $params['shop'] . "/admin/oauth/access_token";

// Configure curl client and execute request
$ch = curl_init();
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_URL, $access_token_url);
curl_setopt($ch, CURLOPT_POST, count($query));
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($query));
$result = curl_exec($ch);
curl_close($ch);

// Store the access token
$result = json_decode($result, true);
$access_token = $result['access_token'];

?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>
<?php


require dirname(dirname(dirname(__FILE__))).'/config/db.php';
//echo "Connected successfully";
//exit;

/*$result = $conn->query('select * from shopify_orders where order_number = "ORD0001036" ');
$data = $result->fetch_assoc();

$order_Data = json_decode($data['order_data']);
echo '<pre>';
print_r($order_Data);
echo '</pre>';
*/
if(isset($_POST['submit'])){
  $integration_1_active = $_POST['integration_1_active'];
  $integration_2_active = $_POST['integration_2_active'];
  $holduntill = $_POST['holduntill'];
  $SS_store_name = $_POST['SS_store_name'];
  $SS_apiKey = $_POST['SS_apiKey'];
  $SS_secretKey = $_POST['SS_secretKey'];

  $result = $conn->query('select * from shopify_inscription_settings');
  if ($result->num_rows > 0) {
      $row = $result->fetch_assoc();
      $conn->query('update shopify_inscription_settings set integration_1_active = "'.$integration_1_active.'", integration_2_active = "'.$integration_2_active.'", holduntill = "'.$holduntill.'", SS_store_name = "'.$SS_store_name.'", SS_apiKey = "'.$SS_apiKey.'", SS_secretKey = "'.$SS_secretKey.'" where id = "'.$row['id'].'" ');
  } else {
      $conn->query('insert into shopify_inscription_settings (`integration_1_active`, `integration_2_active`, `holduntill`, `SS_store_name`, `SS_apiKey`, `SS_secretKey`) values ("'.$integration_1_active.'", "'.$integration_2_active.'", "'.$holduntill.'", "'.$SS_store_name.'", "'.$SS_apiKey.'", "'.$SS_secretKey.'") ');
  }

}

$result = $conn->query('select * from shopify_inscription_settings limit 1');
//$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
} else {
    $row = array();
}

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Custom Integration App</title>
<style>
.ciappsection{
	width: 100%;
	background: #d9d9d9;
	font-family: verdana;
	    float: left;
    padding: 50px 30px;
}
.mcontainerapp{
	width: 1100px;
	margin: auto;
}

.onefieldscol{

	width: 100%;

	float: left;

	margin-bottom: 10px;

}

.onefieldscol h2{
	font-size: 18px;
	color:#000;
}

.onefieldscol h3{
	font-size: 14px;
	color:#000;
	font-weight: normal;
	margin-bottom: 0px;
	text-decoration: underline;
}

.mfieidsapp{

	width: 100%;

	margin-top: 10px;

}

.mfieidsapp span{

	font-size: 12px;

	color:#000;

}

.mfieidsapp span:first-child{

	    width: 100px;

    display: inline-block;

}
.mfieidsapp input.short{
	width: 50px;
  margin: 0px 30px 0px 0px;
  color:#000;
  font-size: 12px;
  padding: 3px 10px;
}
.mfieidsapp input.mid{
	width: 150px;
  margin:  0px 30px 0px 0px;
  color:#000;
  font-size: 12px;
  padding: 3px 10px;
}
.mfieidsapp input.biggers{
	width: 250px;
  margin:  0px 30px 0px 0px;
  color:#000;
  font-size: 12px;
  padding: 3px 10px;
}
.mfieidsapptext{
	width: 100%;
	padding-top: 5px;
}
.mfieidsapptext p{
	font-size: 12px;
	color:#000;
	margin: 0px;
	margin-top: 5px;
}
button#submit {
    background: linear-gradient(to bottom, #6371c7, #5563c1);
    border-color: #3f4eae;
    -webkit-box-shadow: inset 0 1px 0 0 #6f7bcb;
    box-shadow: inset 0 1px 0 0 #6f7bcb;
    color: white;
    padding: 8px 16px;
    border: none;
}
</style>
</head>
<body>
<form method="post">
<div class="ciappsection">
	<div class="mcontainerapp">
		<div class="onefieldscol">
			<h2>Custom Integration App</h2>
			<h3>Integration 1</h3>
			<div class="mfieidsapp">
				<span>Active?</span> 
				<input type="checkbox" name="integration_1_active" value="1" <?php if($row['integration_1_active'] == "1"){ echo 'checked'; } ?>>
			</div>
			<div class="mfieidsapp">
				<span>Take off hold</span> 
				<input class="short" type="number" name="holduntill" value="<?php echo $row['holduntill']; ?>" min="0">
				<span>day(s) before requested delivery</span>
			</div>
			<div class="mfieidsapptext">
				<p>Purpose: Update Shipstation with the delivery date requested by the customer</p> 
				<p>Action: Update ShipBy and HoldUntil fields in Shipstation with data from customer notes in Shopify</p>
			</div>
		</div>
		<div class="onefieldscol">
			<h3>Integration 2</h3>
			<div class="mfieidsapp">
				<span>Active?</span> 
				<input type="checkbox" name="integration_2_active" value="1" <?php if($row['integration_2_active'] == "1"){ echo 'checked'; } ?>>
			</div>
			<div class="mfieidsapptext">
				<p>Purpose: Update Shipstation so Bakery team can inscription text for orders</p> 
				<p>Action: Transfer inscription text from product line to inscription line name in Shipstation</p>
			</div>
		</div>
		<div class="onefieldscol">
			<h3>Shipstation Settings</h3>
			<div class="mfieidsapp">
				<span>Store name</span> 
				<input class="mid" type="text" name="SS_store_name" value="<?php echo $row['SS_store_name']; ?>">
			</div>
			<div class="mfieidsapp">
				<span>API Key</span> 
				<input class="biggers" type="text" name="SS_apiKey" value="<?php echo $row['SS_apiKey']; ?>">
			</div>
			<div class="mfieidsapp">
				<span>API Secret</span> 
				<input class="biggers" type="text" name="SS_secretKey" value="<?php echo $row['SS_secretKey']; ?>">
			</div>
			<div class="mfieidsapptext">
				<p>If it needs username and P/W these should be here too</p>
			</div>
		</div>
    <div class="onefieldscol">
      <div class="mfieidsapp">
          <button id="submit" name="submit" class="btn">Save</button>
      </div>
    </div>
	</div>
</div>
</form>
</body>
</html>