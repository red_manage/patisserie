<?php
$order_id = $_GET['order_id'];
$delivery_date = '';
if($order_id){
	$curl = curl_init();
	curl_setopt_array($curl, array(
	CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com//admin/api/2020-04/orders/".$order_id.".json",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 0,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => array(
	"Cookie: __cfduid=db9ea86a18592f2c26102a550468d4f331587629534"
	),
	));

	$response = curl_exec($curl);

	curl_close($curl);
	
	$delivery_date = '';
	if($response){
		$result = json_decode($response);
		$note_attributes = $result->order->note_attributes;
		foreach($note_attributes as $attributs){
			if($attributs->name == 'Delivery Date'){
				$delivery_date = $attributs->value;
			}

		}
	}
	if($delivery_date){ 
		echo 'var deliver_date = "'.$delivery_date.'"; ';
	}else{
		$delivery_date = $_GET['delivery_date'];
		//file_put_contents('dynamicupdateorder.php', print_r($delivery_date,true));
		if($order_id && $delivery_date){
			$curl = curl_init();
			  curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com//admin/api/2020-04/orders/".$order_id.".json",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "PUT",
			  CURLOPT_POSTFIELDS =>"{\r\n  \"order\": {\r\n    \"id\": ".$order_id.",\r\n    \"note_attributes\": [\r\n{\r\n\"name\": \"Delivery Date\",\r\n \"value\": \"".$delivery_date."\"\r\n }\r\n ]\r\n  }\r\n}",
			  CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"Cookie: __cfduid=db9ea86a18592f2c26102a550468d4f331587629534"
			  ),
			));

			$response = curl_exec($curl);
			curl_close($curl);
			
			echo 'var deliver_date = "'.$delivery_date.'"; ';
		}
	}
}
?>
var html = '<div class="content-box"><div class="content-box__row text-container"><h2>Selected Delivery Date: '+deliver_date+'</h2><div class="os-step__special-description"><p class="os-step__description">Our delivery partner will be in touch with you the day before to confirm your delivery slot.</p></div></div></div>';
jQuery('.section__content > div.content-box:first-child').after(html);

var fbbutton = '<div style="margin-top: 20px;"><div class="fb-like" data-href="https://www.facebook.com/patisserievalerie/" data-width="" data-layout="standard" data-action="like" data-size="large" data-share="false"></div></div>';
jQuery('.section__content > div.content-box:last-child').after(fbbutton);
<?php
exit;


