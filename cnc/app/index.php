<?php
// Set variables for our request
$query = array(
  "client_id" => '4117b7381022ceac6752a7616eb941f0', // Your API key
  "client_secret" => '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', // Your app credentials (secret key)
  "code" => $params['code'] // Grab the access key from the URL
);

// Generate access token URL
$access_token_url = "https://" . $params['shop'] . "/admin/oauth/access_token";

// Configure curl client and execute request
$ch = curl_init();
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_URL, $access_token_url);
curl_setopt($ch, CURLOPT_POST, count($query));
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($query));
$result = curl_exec($ch);
curl_close($ch);

// Store the access token
$result = json_decode($result, true);
$access_token = $result['access_token'];

?>

<?php
include $_SERVER['DOCUMENT_ROOT'] . '/database.php';

echo __DIR__;

// Create connection
$conn = new mysqli($servername, $username, $password, $db);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if(isset($_POST['submit'])){
	
  $page_description = $_POST['page_description'];

  $result = $conn->query('select * from shopify_click_and_collect_page');
  if ($result->num_rows > 0) {
      $row = $result->fetch_assoc();
      $conn->query('update shopify_click_and_collect_page set page_description = "'.addslashes($page_description).'" where id = "'.$row['id'].'" ');
  } else {
	  //echo 'insert into shopify_order_delivery_date_custom (`delivery_warning_message`, `delivery_days`, `booking_period`, `booking_interval`, `cut_off_time`, `dpd_weekday_code`, `dpd_saturday_code`, `dpd_sunday_code`) values ("'.$delivery_warning_message.'", "'.$delivery_days.'", "'.$booking_period.'", "'.$booking_interval.'", "'.$cut_off_time.'", "'.$dpd_weekday_code.'", "'.$dpd_saturday_code.'", "'.$dpd_sunday_code.'") ';
      $conn->query('insert into shopify_click_and_collect_page (`page_description`) values ("'.addslashes($page_description).'") ');
  }
  
  
    $location_id = $_POST['location_id'];
	if($location_id){
		$location_instruction = $_POST['location_instruction'];
		$enable_pickup = $_POST['enable_pickup'];
		$variant_id = $_POST['product_variant'];
		$radius = $_POST['radius'];
		$postal_code = $_POST['postal_code'];
		
		if($enable_pickup){
		}else{
			$enable_pickup = 0;
		}
		$result = $conn->query('select * from shopify_locations_instruction where location_id = "'.$location_id.'" ');
	  if ($result->num_rows > 0) {
		  $row = $result->fetch_assoc();
		  $conn->query('update shopify_locations_instruction set location_instruction = "'.addslashes($location_instruction).'" , enable_pickup = "'.$enable_pickup.'", variant_id = "'.$variant_id.'", radius = "'.$radius.'", postal_code = "'.$postal_code.'" where id = "'.$row['id'].'" ');
	  } else {
		  //echo 'insert into shopify_order_delivery_date_custom (`delivery_warning_message`, `delivery_days`, `booking_period`, `booking_interval`, `cut_off_time`, `dpd_weekday_code`, `dpd_saturday_code`, `dpd_sunday_code`) values ("'.$delivery_warning_message.'", "'.$delivery_days.'", "'.$booking_period.'", "'.$booking_interval.'", "'.$cut_off_time.'", "'.$dpd_weekday_code.'", "'.$dpd_saturday_code.'", "'.$dpd_sunday_code.'") ';
		  $conn->query('insert into shopify_locations_instruction (`location_id`, `location_instruction`, `enable_pickup`, `variant_id`, `radius`, `postal_code`) values ("'.$location_id.'","'.addslashes($location_instruction).'", "'.$enable_pickup.'","'.$variant_id.'", "'.$radius.'", "'.$postal_code.'") ');
	  }
	}
  

}

if(isset($_POST['action']) && $_POST['action'] == 'get_location_ins'){
	$location_id = $_POST['location_id'];
	$result = $conn->query('select * from shopify_locations_instruction where location_id = "'.$location_id.'"');
	
	if ($result->num_rows > 0) {
		$row = $result->fetch_assoc();
	} else {
		$row = array();
	}
	$data = array();
	$location_instruction = $row['location_instruction'];
	$data['location_instruction'] = trim($location_instruction);
	$data['enable_pickup'] = $row['enable_pickup'];
	$data['variant_id'] = $row['variant_id'];
	$data['radius'] = $row['radius'];
	echo json_encode($data);
	die;
}

$result = $conn->query('select * from shopify_click_and_collect_page limit 1');
//$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
} else {
    $row = array();
}
$page_description = $row['page_description'];


$curl = curl_init();
curl_setopt_array($curl, array(
CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com/admin/api/2020-04/locations.json",
CURLOPT_RETURNTRANSFER => true,
CURLOPT_ENCODING => "",
CURLOPT_MAXREDIRS => 10,
CURLOPT_TIMEOUT => 0,
CURLOPT_FOLLOWLOCATION => true,
CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
CURLOPT_CUSTOMREQUEST => "GET",
CURLOPT_HTTPHEADER => array(
"Cookie: __cfduid=db9ea86a18592f2c26102a550468d4f331587629534"
),
));

$response = curl_exec($curl);

curl_close($curl);
$locationMenu = '<select class="dropdown-location" id="shopifyLocations" name="location_id"><option value="">Choose your Location</option>';
if($response){
	$results = json_decode($response);
	foreach($results->locations as $location){
		$locationMenu .= '<option value="'.$location->id.'" data-postal-code="'.$location->zip.'">'.$location->name.'</option>';
	}
}
$locationMenu .= '</select>';

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Shipping Date Selector App</title>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
.ciappsectiontwo{
	width: 100%;
	background: #d9d9d9;
	font-family: verdana;
	float: left;
    padding: 50px 0px;
}
.mcontainerapptwo{
	width: 1100px;
	margin: auto;
}
.tpheaddingm{
	font-size: 18px;
	color: #000;
}
.shipdateselc{
	width: 100%;
	float: left;
}
.firstsdateselc{
	width: 50%;
	float: left;
}
.secondsdateselc{
	width: 50%;
	float: left;
}
.firstsdateselc h3, .secondsdateselc h3 {
    font-size: 14px;
    color: #000;
    font-weight: normal;
    margin-bottom: 0px;
    text-decoration: underline;
}
.inforntbx{
	width: 100%;
	display: flex;
    align-items: center;
    margin-top: 5px;
}
.inforntbx span{
	font-size: 12px;
    color: #000;
    min-width: 150px;
}
.subbuttonsm input[type="checkbox"]{
	background: #FFF;
	color: #333;
	border:solid 1px #1e3c84;
	padding: 0px;
	display: block;
	width: 20px;
    height: 20px;
    margin: 0px;
}
.subbuttonsm input[type="text"]{
	background: #FFF;
	color: #333;
	border:solid 1px #1e3c84;
	padding: 5px 10px;
	display: block;
}
.subbuttonsm input[type="submit"], .btn{
	background: #1e3c84;
	color: #FFF;
	border:solid 1px #1e3c84;
	padding: 5px 10px;
	display: block;
	width: 100%;
}
.inforntbxtwo{
	width: 100%;
    margin-top: 5px;
}
.inforntbxtwo span{
	font-size: 12px;
    color: #000;
    min-width: 150px;
    padding-bottom: 5px;
    display: block;
}
.inforntbxtwo textarea{
	width: 100%;
	height: 150px;
	border:solid 1px #1e3c84;
	padding: 5px 10px;
}

</style>
</head>

<body>
<form method="post" id="cncForm">
<div class="ciappsectiontwo">
	<div class="mcontainerapptwo">
		<h2 class="tpheaddingm">Click and collect page settings</h2>
		<div class="shipdateselc">
			<div class="firstsdateselc">
				
				<h3>Page Description</h3>
				<div class="inforntbxtwo">
					<div class="subbuttonsm">
					<br/>
					<textarea id="page_description" name="page_description"><?php echo $row['page_description']; ?></textarea>
					</div>
				</div>
				
				<h3>Location Instructions</h3>
				<div class="inforntbxtwo">
					<div class="subbuttonsm">
					<input type="hidden" id="postalCode" name="postal_code">
					<br/>
					<?php echo $locationMenu; ?>
					<p></p>
					<textarea id="location_instruction" name="location_instruction" style="display:none;"></textarea>
					<p></p>
					<div id="pickLcation" style="display:none;">
					<p>Enable Location</p>
					<input type="checkbox" value="1" name="enable_pickup" id="enable_pickup">
					</div>
					
					<div id="productVariant" style="display:none;">
					<p>Location Click and Collect Item Code</p>
					<?php
					$curl = curl_init();
					curl_setopt_array($curl, array(
					  CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com/admin/api/2020-04/products/4479898058819/variants.json",
					  CURLOPT_RETURNTRANSFER => true,
					  CURLOPT_ENCODING => "",
					  CURLOPT_MAXREDIRS => 10,
					  CURLOPT_TIMEOUT => 0,
					  CURLOPT_FOLLOWLOCATION => true,
					  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					  CURLOPT_CUSTOMREQUEST => "GET",
					  CURLOPT_HTTPHEADER => array(
						"Cookie: _master_udr=eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaEpJaWt4TldVeE1HUXlPUzB3Tm1JM0xUUXdZVFF0T0dJeU1DMWlNalV5WVRGbVpEUTRNbVlHT2daRlJnPT0iLCJleHAiOiIyMDIyLTA1LTMwVDA3OjIxOjI0LjE0NVoiLCJwdXIiOiJjb29raWUuX21hc3Rlcl91ZHIifX0%3D--6dc36557d8802e6fc4477eda2d2d4e9cafe2707d; _secure_admin_session_id_csrf=1bdc262f9441f84a05178767f7671da0; _secure_admin_session_id=1bdc262f9441f84a05178767f7671da0; __cfduid=d8e0f58645e9c4695bc749c2b4b9ac5f21590488150; _orig_referrer=https%3A%2F%2Fde1311fca0a2fb17bb1f52828ae8daeb%3A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%40pat-val-premier.myshopify.com%2Fadmin%2Fapi%2F2020-04%2Fcarrier_services.jso; _landing_page=%2Fadmin%2Fauth%2Flogin; _y=8bd56dde-cbc5-4330-a752-e9fa91d565a4; _shopify_y=8bd56dde-cbc5-4330-a752-e9fa91d565a4"
					  ),
					));

					$response = curl_exec($curl);
					curl_close($curl);
					
					//echo '<pre>';
					//print_r($response);
					$variants = json_decode($response);
					
					?>
					<select id="product_variant" name="product_variant">
					<?php if($variants){
						echo '<option value="">Select Product Variant</option>';
						foreach($variants->variants as $variant){
							echo '<option value="'.$variant->id.'">'.$variant->title.' ('.$variant->sku.')</option>';
						}
						
					} ?>
					</select>
					</div>
					<p></p>
					<div id="locationRadius" style="display:none;">
					<p>Click and collect radius (in miles)</p>
					<input type="text" name="radius" id="location_radius" placeholder="Radius in miles">
					</div>
					</div>
				</div>
				
				
			</div>
			
		</div>
		<div class="inforntbx">
		<div class="subbuttonsm">
			<br/>
			<input type="submit" value="Save" name="" id="submitcnc">
			<input type="submit" value="Save" name="submit" id="submitform" style="visibility:hidden;">
		</div>
		</div>
	</div>
</div>
</form>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>
<script>
jQuery(document).ready(function(){
	
	jQuery('body').on('click', '#submitcnc', function(e){
		e.preventDefault();
		var variant_id = jQuery('#product_variant').val();
		if (jQuery('#enable_pickup').is(':checked')) {
			/*if(variant_id){
				//jQuery('#cncForm').submit();
				jQuery('#submitform').trigger('click');
			}else{
				alert('Please select location variant');
			}*/
			jQuery('#submitform').trigger('click');
		}else{
			jQuery('#submitform').trigger('click');
		}
	});
	jQuery('body').on('change', '#shopifyLocations', function(e){
    var location_id = jQuery(this).val();
	var postal_code = jQuery('#shopifyLocations option[value="'+location_id+'"]').data('postal-code');
	//var postal_code = jQuery(this).attr('data-postal-code');
	//console.log(postal_code);
	jQuery('#postalCode').val(postal_code);
    $.ajax({
		type: "POST",
		data: 'action=get_location_ins&location_id='+location_id,
		dataType: 'json',
		success: function(response)
		{
			var data = response;
			var des = data.location_instruction;
			var enable_pickup = data.enable_pickup;
			var radius = data.radius;
			
			jQuery('#location_radius').val(radius);
			
			jQuery('#location_instruction').val(jQuery.trim(des));
			if(enable_pickup == 1){
				jQuery('#enable_pickup').prop('checked', true);
			}else{
				jQuery('#enable_pickup').prop('checked', false);
			}
			var variant_id = data.variant_id;
			if(variant_id){
				jQuery('#productVariant option[value="'+variant_id+'"]').prop('selected', true);
			}else{
				jQuery('#productVariant option:eq(0)').prop('selected', true);
			}
			
	    }
   });
    
    if(jQuery(this).val() == ''){
        jQuery('#location_instruction').hide();
		jQuery('#pickLcation').hide();
		jQuery('#productVariant').hide();
		jQuery('#locationRadius').hide();
    }else{
        jQuery('#location_instruction').show();
		jQuery('#pickLcation').show();
		jQuery('#productVariant').show();
		jQuery('#locationRadius').show();
    }
  });
});
</script>
</body>
</html>