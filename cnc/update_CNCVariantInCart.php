<?php
include $_SERVER['DOCUMENT_ROOT'] . '/database.php';

$variant_id = (int)$_GET['variant_id'];

if($variant_id){

	// Create connection
	$conn = new mysqli($servername, $username, $password, $db);

	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}

    $sql = $conn->prepare("SELECT * FROM shopify_locations_instruction WHERE variant_id = ? AND enable_pickup = 1");
    $sql->bind_param('i', $variant_id);
	$sql->execute();
    $result = $sql->get_result();

	  if ($result->num_rows > 0) {
		  $row = $result->fetch_assoc();
		  $CNCLoactionID = $row['location_id'];
		  $CNCLoactionDescription = $row['location_instruction'];
	  } else {
		  $CNCLoactionID = '';
		  $CNCLoactionDescription = 'Sorry this service is not available at the moment';
	  }
}
?>
var locCNCLoactionID = '<?php echo $CNCLoactionID; ?>';
localStorage.setItem("CNCLoactionID", '<?php echo $CNCLoactionID; ?>');

var variant_Id = '<?php echo $variant_id; ?>';
localStorage.setItem("CNCLoactionVariantID", '<?php echo $variant_id; ?>');

var locDescription = '<?php echo addslashes($CNCLoactionDescription); ?>';
localStorage.setItem("CNCLoactionDescription", '<?php echo addslashes($CNCLoactionDescription); ?>');

localStorage.setItem("CNCincart", true);

