<?php
$referrer = $_GET['referrer'];

$uri_path = parse_url($referrer, PHP_URL_PATH);
$uri_segments = explode('/', $uri_path);
echo 'var page = "'.$uri_segments[2].'"; ';
?>
console.log(page);
if(page == "click-and-collect"){
	var CNCLoactionID = localStorage.getItem("CNCLoactionID");
	var CNCLoactionAddress = localStorage.getItem("CNCLoactionAddress");
	var CNCLoactionDescription = localStorage.getItem("CNCLoactionDescription");
	var CNCLoactionVariantID = localStorage.getItem("CNCLoactionVariantID");
	jQuery('body').on('click', '.product-form__cart-submit', function(){
		$.ajax({
		  type: 'GET', 
		  url: '/cart.js',
		  dataType: 'json', 
		  success: function(response){
			  var isInCart = false;
			  console.log(response.items);
			  if(response.items.length > 0){
				  $.each(response.items, function(k, v) {
					 if(response.items[k].id == CNCLoactionVariantID){
						 isInCart = true;
					 }
				});
			  }
			  if(isInCart == false){
					jQuery.post('/cart/add.js', {
					  items: [
						{
						  quantity: 1,
						  id: CNCLoactionVariantID,
						  properties: {
							'Location': CNCLoactionAddress
						  }
						}
					  ]
					});
				}
		  }
		});
	});
}
