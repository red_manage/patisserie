<?php
$collection_id = $_GET['collection_id'];
$location_id = $_GET['location_id'];
$inventory_itemArray = array();
if($location_id){
	$curl = curl_init();
	curl_setopt_array($curl, array(
	CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com/admin/api/2020-04/inventory_levels.json?location_ids=".$location_id."&limit=250",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 0,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => array(
	"Cookie: __cfduid=db9ea86a18592f2c26102a550468d4f331587629534"
	),
	));

	$response = curl_exec($curl);

	curl_close($curl);
	$html = '';
	if($response){
		$results = json_decode($response);
		foreach($results->inventory_levels as $key => $inventory_levels){
			$inventory_itemArray[$key]['inventory_item_id'] = $inventory_levels->inventory_item_id;
			$inventory_itemArray[$key]['available'] = $inventory_levels->available;
		}
	}
	
}

function searchForItemInventory($id, $array) {
   foreach ($array as $key => $val) {
       if ($val['inventory_item_id'] === $id) {
           if($key == 0){
			   return 'a';
		   }else{
			   return $key;
		   }
       }
   }
   return null;
}


$productArray = array();
if($collection_id){
	$curl = curl_init();
	curl_setopt_array($curl, array(
	CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com//admin/api/2020-04/collections/".$collection_id."/products.json",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 0,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => array(
	"Cookie: __cfduid=db9ea86a18592f2c26102a550468d4f331587629534"
	),
	));

	$response = curl_exec($curl);

	curl_close($curl);
	$html = '';
	if($response){
		$results = json_decode($response);
		foreach($results->products as $product){
			//if (strpos(strtolower($product->tags), 'click and collect') !== false) {
				$curl = curl_init();
				curl_setopt_array($curl, array(
				CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com/admin/api/2020-04/products/".$product->id."/variants.json",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
				CURLOPT_HTTPHEADER => array(
					"Cookie: __cfduid=d8e0f58645e9c4695bc749c2b4b9ac5f21590488150"
				),
				));

				$variants = curl_exec($curl);

				curl_close($curl);
				
				if($variants){
					$variants = json_decode($variants);
					$price = $variants->variants[0]->price;
					foreach($variants->variants as $key => $variant){
						$inventory_item_id = $variant->inventory_item_id;
						$searchKey = searchForItemInventory($inventory_item_id,$inventory_itemArray);
						if($searchKey != null || $searchKey == 'a' || $searchKey != '' || !empty($searchKey)){
							if($searchKey == 'a'){
								$searchKey = 0;
							}
							$productArray[$product->id]['quantity'] += $inventory_itemArray[$searchKey]['available'];
							if($variant->price <= $price){	
								$price = $variant->price;
								$productArray[$product->id]['product_id'] = $product->id;
								$productArray[$product->id]['product_url'] = $product->handle;
								$productArray[$product->id]['product_title'] = $product->title;
								$productArray[$product->id]['product_image'] = $product->image->src;
								$productArray[$product->id]['variant_id'] = $variant->id;
								$productArray[$product->id]['variant_title'] = $variant->title;
								$productArray[$product->id]['variant_price'] = $variant->price;
								//$productArray[$product->id]['quantity'] = $variant->inventory_quantity;
								$productArray[$product->id]['inventory_quantity'] = $inventory_itemArray[$searchKey]['available'];
							}
							
						}
					}
				}

			//}
		}
	}
	
}
//echo '<pre>';
//print_r($productArray);
//exit;
if($productArray){
	foreach($productArray as $product){
		if($product['product_id']){
				if($product['quantity'] > 0){
					$soldout = '';
					if($product['quantity'] <= 0){
						$soldout = '<div class="price__badges price__badges--listing"><span class="price__badge price__badge--sold-out"><span>Sold out</span></span></div>';
					}
					$pathinfo = pathinfo($product['product_image']);
					$imge = str_replace($pathinfo['filename'].'.'.$pathinfo['extension'],$pathinfo['filename'].'_450x450.'.$pathinfo['extension'],$product['product_image']);
					$producthtml .= '<li class="grid__item grid__item--collection-template small--one-half medium-up--one-quarter"><div class="grid-view-item product-card"><a href="/products/'.$product['product_url'].'"><img src="'.$imge.'"><div class="h4 grid-view-item__title product-card__title" aria-hidden="true">'.$product['product_title'].'</div><div class="price__regular"><span class="price-item price-item--regular">From : £'.$product['variant_price'].'</span></div>'.$soldout.'</a></div></li>';
			
				}
			}
		}
}

?>

var html = '<?php echo $producthtml; ?>';
jQuery('.loading-image').hide();
jQuery('#productList').html(html);

<?php
exit;


