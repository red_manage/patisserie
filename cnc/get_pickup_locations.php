
var CNCLoactionID = localStorage.getItem("CNCLoactionID");
<?php
include $_SERVER['DOCUMENT_ROOT'] . '/database.php';

$shopify = $_GET['shopify'];

$htmldata = '<select class="dropdown-location" id="shopifyLocations"><option value="">Choose location</option>';
if($shopify){
	$postalcode = str_replace(' ', '',$_GET['code']);

	$variantids = explode(",", $_GET['vids']);

	$varint_inventory_ids = array();
	if($variantids){
		foreach($variantids as $vids){
			$curl = curl_init();
			curl_setopt_array($curl, array(
			CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com/admin/api/2021-04/variants/".$vids.".json",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"Cookie: __cfduid=d8e0f58645e9c4695bc749c2b4b9ac5f21590488150"
			),
			));

			$response = curl_exec($curl);

			curl_close($curl);

			$variants = json_decode($response);
			
			$varint_inventory_ids[] = $variants->variant->inventory_item_id;
		}
	}

	// get miles $i distance
	// @retun miles in number
	function getMiles($i) {
     return number_format($i*0.000621371192,2);
	}
	function getMeters($i) {
	     return number_format($i*1609.344,2);
	}

	// Create connection
	$conn = new mysqli($servername, $username, $password, $db);

	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	}


	$notDeliveryPostalcodeArray = array(
		"FK17","FK18","FK19","FK20","FK21",
		"HS1","HS2","HS3","HS4","HS5","HS6","HS7","HS8","HS9",
		"KA27","KA28",
		"KW0","KW1","KW2","KW3","KW4","KW5","KW6","KW7","KW8","KW9","KW10","KW11","KW12","KW13","KW14","KW15","KW16","KW17",
		"ZE1","ZE2","ZE3"
	);

	$AB31to56 = array();
	for($i=31; $i<=56; $i++){
		if($i != 39){
			array_push($notDeliveryPostalcodeArray,"AB".$i);
		}
	}

	$IV1to63 = array();
	for($i=1; $i<=63; $i++){
		array_push($notDeliveryPostalcodeArray,"IV".$i);
	}

	$PA20to78 = array();
	for($i=20; $i<=78; $i++){
		array_push($notDeliveryPostalcodeArray,"PA".$i);
	}

	$PH15to50 = array();
	for($i=15; $i<=50; $i++){
		array_push($notDeliveryPostalcodeArray,"PH".$i);
	}

	function strposarr($haystack, $needles=array(), $offset=0) {
	        $chr = array();
	        foreach($needles as $needle) {
	                $res = strpos($haystack, $needle, $offset);
	                if ($res !== false) $chr[$needle] = $res;
	        }
	        if(empty($chr)) return false;
	        return min($chr);
	}


	$result = $conn->query('select * from shopify_locations_instruction where enable_pickup = 1 ');
	echo 'jQuery("#cnc_message").html("");';
	echo 'var isProntoShow = "0"; ';
	echo 'localStorage.setItem("userloaction", false);';
	echo 'var country = ""; ';
	echo 'var notDeliveryPostalcode = ""; ';

	if(substr(strtoupper($postalcode), 0, 2 ) === "BT" || substr(strtoupper($postalcode), 0, 2 ) === "JE" || substr(strtoupper($postalcode), 0, 2 ) === "GY"){
		echo 'var country = "uk"; ';
		echo 'var notDeliveryPostalcode = "1"; ';
	}

	if(in_array($postalcode, $notDeliveryPostalcodeArray)){
		echo 'var country = "uk"; ';
		echo 'var notDeliveryPostalcode = "1"; ';
	}else{
		if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()){
			if($row['postal_code']){

				// Google distance matrix API to fetch data from postal code
				$curl = curl_init();
				curl_setopt_array($curl, array(
					CURLOPT_URL => "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".str_replace(' ', '%20', $row['postal_code']).",UK&destinations=".str_replace(' ','%20', $_GET['code']).",UK&key=AIzaSyCcM4xtOxPKZ05a0tPhT8zEtqrNCHOUdNc",
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "GET",
				));

				$response = curl_exec($curl);

				curl_close($curl);

				$distance_arr = json_decode($response);
				//echo '<pre>';
				//print_r($distance_arr);
				if ($distance_arr->status=='OK') {
					$destination_addresses = $distance_arr->destination_addresses[0];
					$address = explode(", ", $destination_addresses);
					$country = end($address);
					
					if(strtolower($country) == 'uk'){
						echo 'var country = "uk"; ';
						$elements = $distance_arr->rows[0]->elements;
					    if($elements[0]->status=='OK'){
						   $distance = $elements[0]->distance->value;
						   $radius = getMiles($distance); //str_replace(" mi", "", $distance);
						   //$result = $conn->query('select * from shopify_locations_instruction where enable_pickup = 1 and radius >= '.$radius.' ');
						   
						   if ($row['radius'] >= $radius) {
							  //$row = $result->fetch_assoc();
							  //echo $row['radius'].' == '.$radius;
								$curl = curl_init();
								curl_setopt_array($curl, array(
								CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com/admin/api/2020-04/inventory_levels.json?location_ids=".$row['location_id']."&limit=250",
								CURLOPT_RETURNTRANSFER => true,
								CURLOPT_ENCODING => "",
								CURLOPT_MAXREDIRS => 10,
								CURLOPT_TIMEOUT => 0,
								CURLOPT_FOLLOWLOCATION => true,
								CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
								CURLOPT_CUSTOMREQUEST => "GET",
								CURLOPT_HTTPHEADER => array(
								"Cookie: __cfduid=db9ea86a18592f2c26102a550468d4f331587629534"
								),
								));

								$response = curl_exec($curl);

								curl_close($curl);
								$inventory_itemArray = array();
								if($response){
									$results = json_decode($response);
									foreach($results->inventory_levels as $key => $inventory_levels){
										if($inventory_levels->available > 0 || $inventory_levels->available == null){
											$inventory_itemArray[] = $inventory_levels->inventory_item_id;
										}
									}
								}


								$foundinventroy = true;
								foreach($varint_inventory_ids as $v_ids){
									if(!in_array($v_ids, $inventory_itemArray)){
										$foundinventroy = false;
									}
								}
								// Matched location display in dropdown

								if($foundinventroy){
									$curl = curl_init();
									curl_setopt_array($curl, array(
									CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com/admin/api/2020-04/locations/".$row['location_id'].".json",
									CURLOPT_RETURNTRANSFER => true,
									CURLOPT_ENCODING => "",
									CURLOPT_MAXREDIRS => 10,
									CURLOPT_TIMEOUT => 0,
									CURLOPT_FOLLOWLOCATION => true,
									CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
									CURLOPT_CUSTOMREQUEST => "GET",
									CURLOPT_HTTPHEADER => array(
									"Cookie: __cfduid=db9ea86a18592f2c26102a550468d4f331587629534"
									),
									));

									$response = curl_exec($curl);

									curl_close($curl);
									
									$results = json_decode($response);
									$htmldata .= '<option value="'.$results->location->id.'">'.$results->location->name.'</option>';
								}
							  
						   }
					    }
					}else{
						echo 'var country = "'.$country.'"; ';
					}
				   
				}
			}
			
		}
	}
	}
}

$htmldata .= '</select>';

echo "var html = '".$htmldata."'; ";
?>
jQuery('.template-cart .loading-image').hide();
jQuery('#locations_box').html(html);
jQuery('#locations_box').show();
<?php
exit;


