<?php
include $_SERVER['DOCUMENT_ROOT'] . '/database.php';
define('SHOPIFY_APP_SECRET', 'cb174284db60b4f172acb35bf0011e85118ec96479a7943e45d4f6278e1d9010');
function verify_webhook($data, $hmac_header)
{
  $calculated_hmac = base64_encode(hash_hmac('sha256', $data, SHOPIFY_APP_SECRET, true));
  return hash_equals($hmac_header, $calculated_hmac);
}
$hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
$data = file_get_contents('php://input');
$verified = verify_webhook($data, $hmac_header);
//error_log('Webhook verified: '.var_export($verified, true)); //check error.log to see the result
$result = json_decode($data);

// Create connection
$conn = new mysqli($servername, $username, $password, $db);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$shopify_settings = $conn->query('select * from shopify_inscription_settings limit 1');
//$result = $conn->query($sql);

if ($shopify_settings->num_rows > 0) {
    $settings = $shopify_settings->fetch_assoc();
} else {
    $settings = array();
}


function searchForId($id, $array) {
   foreach ($array as $key => $val) {
       if ($val['sku'] === $id) {
           return $key;
       }
   }
   return null;
}

$note_attributes = $result->note_attributes;
$orderid = $result->id;
$delivery_date = '';
foreach($note_attributes as $attributs){
	if($attributs->name == 'Delivery Date'){
		$delivery_date = $attributs->value;
	}
}

$incriptionsData = array();
$line_items = $result->line_items;
foreach($line_items as $key => $item){
	
	$incriptionsData[$key]['sku'] = $item->sku;
	
	foreach($item->properties as $properties){
		if($properties->name == 'inscription_text'){
			$incriptionsData[$key]['incription_text'] = $properties->value;
		}
	}
	
}


$orderNumber = $result->name; //ordernumber
?>


<?php
if($orderNumber){
	$curl = curl_init();
	curl_setopt_array($curl, array(
	CURLOPT_URL => "https://ssapi.shipstation.com/orders?orderNumber=".$orderNumber,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 0,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => array(
	"Authorization: Basic OTc3MmI5NmEyZWQ2NDE2MjhiNGIxMjI2OGI3MjQzYWM6MDJhMTlhNjIwOTA3NDEwNzgzZjllZmM1MTk1ZDE3YzA="
	),
	));
	$response = curl_exec($curl);

	curl_close($curl);
	//echo $response;
	$resultData = json_decode($response);
	$requestData = json_decode($response);
	
	if($resultData){
	
		//$orderKey = $resultData->orders[0]->orderKey;
		
		//file_put_contents('shipstation.php', print_r($resultData, true));
		
		
		//update order on shipstation
		
		$resultCount = $resultData->total;
		echo "resultCount: " . $resultCount . "\n ";

		for ($x = 0; $x <= $resultCount-1; $x++) {

		  $customerNotes = $resultData->orders[$x]->customerNotes;

		  echo  "\n " . $x . ": " . $customerNotes . "\n";


		  if (is_null($customerNotes)) {
			//do nothing
		  } else {

			//$date = substr($customerNotes, 20, 10);
			
			$formattedDate = DateTime::createFromFormat('d/m/Y', $delivery_date)->format('Y-m-d');
			$dateBefore = date('Y-m-d', (strtotime('-1 day', strtotime($formattedDate))));
			file_put_contents('deliverydate.txt', $formattedDate);


	
			if(strtotime($dateBefore) <= strtotime(date('Y-m-d'))){
				$dateBefore = '';
				$requestData->orders[$x]->orderStatus = 'awaiting_shipment';
			}else{
				$requestData->orders[$x]->orderStatus = 'on_hold';
			}

			$requestData->orders[$x]->customerNotes = '<br/>Delivery Date: '.$delivery_date;
			$orderId = $resultData->orders[$x]->orderId;
			//$requestData->orders[$x]->orderStatus = 'awaiting_shipment';
			$requestData->orders[$x]->shipByDate = $formattedDate;			
			$requestData->orders[$x]->shipDate = $formattedDate;			
			$requestData->orders[$x]->holdUntilDate = $dateBefore;
			$requestData->orders[$x]->advancedOptions->billToParty = null;
			// $requestData->orders[$x]->advancedOptions->billToMyOtherAccount = 'my_other_account';
			
			
			$requestDataJson = json_encode($requestData->orders[$x]);
			
			file_put_contents('shipstationupdated.php', print_r($requestDataJson, true));

			// echo $requestDataJson;

			$curl = curl_init();

			curl_setopt_array($curl, array(
			CURLOPT_URL => "https://ssapi.shipstation.com/orders/createorder",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $requestDataJson,
			CURLOPT_HTTPHEADER => array(
				"Authorization: Basic OTc3MmI5NmEyZWQ2NDE2MjhiNGIxMjI2OGI3MjQzYWM6MDJhMTlhNjIwOTA3NDEwNzgzZjllZmM1MTk1ZDE3YzA=",
				"Content-Type: application/json"
			  ),
			));

			$updateResponse = curl_exec($curl);

			curl_close($curl);

			// $resultData = json_decode($updateResponse);

			// file_put_contents('shipstation_updatedData.php', print_r($resultData, true));
		
	
			if($dateBefore){
				$curlToHold = curl_init();

				curl_setopt_array($curlToHold, array(
				CURLOPT_URL => "https://ssapi.shipstation.com/orders/holduntil",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => "{\n  \"orderId\": " . $orderId . ",\n  \"holdUntilDate\": \"" . $dateBefore . "\"\n}",
				CURLOPT_HTTPHEADER => array(
					"Authorization: Basic OTc3MmI5NmEyZWQ2NDE2MjhiNGIxMjI2OGI3MjQzYWM6MDJhMTlhNjIwOTA3NDEwNzgzZjllZmM1MTk1ZDE3YzA=",
					"Content-Type: application/json"
				  ),
				));

				$response = curl_exec($curlToHold);

				curl_close($curlToHold);		
			}

			echo " \n Processed order: " . $orderId;
		  }
		}
	
	}

	
}

