<?php
include $_SERVER['DOCUMENT_ROOT'] . '/database.php';

// verify shopify webhook

define('SHOPIFY_APP_SECRET', 'cb174284db60b4f172acb35bf0011e85118ec96479a7943e45d4f6278e1d9010');
function verify_webhook($data, $hmac_header)
{
  $calculated_hmac = base64_encode(hash_hmac('sha256', $data, SHOPIFY_APP_SECRET, true));
  return hash_equals($hmac_header, $calculated_hmac);
}
$hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
$data = file_get_contents('php://input');
$verified = verify_webhook($data, $hmac_header);
//error_log('Webhook verified: '.var_export($verified, true)); //check error.log to see the result
$result = json_decode($data);
//$fname = 'test_'.rand().'.txt';
//file_put_contents('test_123.txt', print_r($result, true));

// Extract order data and check Delivery Date attribute

$note_attributes = $result->note_attributes;
$orderid = $result->id;
$delivery_date = '';
foreach($note_attributes as $attributs){
	if($attributs->name == 'Delivery Date'){
		$delivery_date = $attributs->value;
	}
}

$incriptionsData = array();
$line_items = $result->line_items;

$isCNC = false;

foreach($line_items as $key => $item){
	$incriptionsData[$key]['id'] = $item->id;
	$incriptionsData[$key]['variant_id'] = $item->variant_id;
	$incriptionsData[$key]['sku'] = $item->sku;
	
	if (strpos($item->sku, 'XXXXXX-') !== false) {
		$isCNC = true;
	}

	foreach($item->properties as $properties){
		if($properties->name == 'inscription_text'){
			$incriptionsData[$key]['incription_text'] = $properties->value;
		}
	}
}

	$shipping_lines = $result->shipping_lines;
	$shipping_title = $shipping_lines[0]->title;
	
	$orderupdate = array();
	$orderupdate['order']['id'] = $orderid;

	if($delivery_date){
		// Fetch all locations
		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com/admin/api/2020-04/locations.json",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
			"Cookie: _master_udr=eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaEpJaWt4TldVeE1HUXlPUzB3Tm1JM0xUUXdZVFF0T0dJeU1DMWlNalV5WVRGbVpEUTRNbVlHT2daRlJnPT0iLCJleHAiOiIyMDIyLTA1LTMwVDA3OjIxOjI0LjE0NVoiLCJwdXIiOiJjb29raWUuX21hc3Rlcl91ZHIifX0%3D--6dc36557d8802e6fc4477eda2d2d4e9cafe2707d; _secure_admin_session_id_csrf=1bdc262f9441f84a05178767f7671da0; _secure_admin_session_id=1bdc262f9441f84a05178767f7671da0; __cfduid=d8e0f58645e9c4695bc749c2b4b9ac5f21590488150; _orig_referrer=https%3A%2F%2Fde1311fca0a2fb17bb1f52828ae8daeb%3A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%40pat-val-premier.myshopify.com%2Fadmin%2Fapi%2F2020-04%2Fcarrier_services.jso; _landing_page=%2Fadmin%2Fauth%2Flogin; _y=8bd56dde-cbc5-4330-a752-e9fa91d565a4; _shopify_y=8bd56dde-cbc5-4330-a752-e9fa91d565a4"
		  ),
		));

		$response = curl_exec($curl);
		curl_close($curl);
		
		$locations = json_decode($response);
		
		$locArray = array();

		// create location array
		foreach($locations->locations as $location){
			$locArray[] = $location->name;
		}
		
		// check if shipping title is matched with location then update order note
		if(in_array($shipping_title, $locArray)){
			$note = "online order, customer requested collection on ".$delivery_date;
			$orderupdate['order']['note'] = $note;
		}
	}

	// Update order tag according shipping title
	
	if($shipping_title == 'Pat Val Premier Delivery'){
		if($result->tags){
			$tags = $result->tags.', Premier';
		}else{
			$tags = 'Premier';
		}
		$orderupdate['order']['tags'] = $tags;
	}elseif($isCNC == true){
		if($result->tags){
			$tags = $result->tags.', Pronto';
		}else{
			$tags = 'Pronto';
		}
		$orderupdate['order']['tags'] = $tags;
	}elseif($shipping_title != ''){
		if($result->tags){
			$tags = $result->tags.', Standard';
		}else{
			$tags = 'Standard';
		}
		$orderupdate['order']['tags'] = $tags;
	}
	
	$orderdata =  json_encode($orderupdate);
	
	// Update order tag		
	$curl = curl_init();

	curl_setopt_array($curl, array(
	CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com/admin/api/2020-04/orders/".$orderid.".json",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 0,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "PUT",
	CURLOPT_POSTFIELDS => $orderdata,
	CURLOPT_HTTPHEADER => array(
		"Content-Type: application/json",
		"Cookie: __cfduid=db9ea86a18592f2c26102a550468d4f331587629534"
		),
	));
	curl_exec($curl);
	curl_close($curl);

$orderNumber = $result->name; //ordernumber
$orderData = array();
$orderData = json_encode($incriptionsData);

// Create connection
$conn = new mysqli($servername, $username, $password, $db);

// Save order data into database
$conn->query("insert into shopify_orders (`order_id`, `order_number`, `order_data`, `delivery_date`, `c_date`, `ss_updated`, `ss_created`) values ('".$orderid."','".$orderNumber."','".addslashes($orderData)."', '".addslashes($delivery_date)."', '".date('Y-m-d H:i:s')."', '0','0') ");