<?php
include $_SERVER['DOCUMENT_ROOT'] . '/database.php';

$postalcode = str_replace(' ', '',$_GET['code']);
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.dpdlocal.co.uk/shipping/network/?deliveryDetails.address.countryCode=GB&deliveryDetails.address.postcode=".$postalcode."&collectionDetails.address.countryCode=GB&collectionDetails.address.postcode=SE12AT",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "Cookie: ak_bmsc=911A9C7FDB89B99A93A6C8EA6867D0710210B5979D5C000054D7A35ED5A19E24~plQ8zNInZXP2b1DN2jKH358zWkcFyGxQ5FBwGFhsFAWSjquB68aXzLFjmwlAvnA48uMOLYjKgMbk+4aVoOnnHHmUwbgquacdUHffnqc2+bT2EQ7dA84fTZrEn/IiaGhPoaFESsY9BAuEAW6kmXQvepRKv9snxq4JxfUXUTW8SRJrTOreW1AP1BmYQLtX/x9zeQIpXjsnsjHCudJuK5Y7WVBgp2AvYYQWxEU3UxIZKNZ0k=; bm_sv=EBE1C33B514758A92D5E92EB472769CD~O6MkmZ+2XpUl38v5RavcwxaJkb2ttK4MFTmwlIWIMU2O6+rUGyQQMA/qcvBEF6AxLgRer7U1VadYelq7STks+eDHhOFZPyTI9rseC72jIcL517uuCwQ/b34J8uqp90r0JEmmDOVGsMv8m08Zq86x/UoJrbKGf2u+cYV3huaMNEE="
  ),
));
$response = curl_exec($curl);
curl_close($curl);
$jdata =  json_decode($response);
//file_put_contents('postalcode.json', $jdata);
//echo '<pre>';
//print_r($jdata);
$isService = false;
if($jdata->data){
	foreach($jdata->data as $data){
		if($data->service->serviceCode == '2^12'){
			$isService = true;
		}
	}
}
date_default_timezone_set('Europe/London');
$date = date("d-m-Y H:i:s"); 

$isMidDay = 0;
if (date('H') >= 12) {
  $isMidDay = 1;
}

function isWeekend($date) {
    return (date('N', strtotime($date)) > 6);
}

echo 'var ukDateTime = "'.$date.'"; ';
echo 'var postalcode = "'.$_GET['code'].'"; ';

if($isMidDay){
	if(isWeekend(date('d-m-Y', strtotime(' +2 day')))){
		echo 'var nextDeliveryDate = "'.date('d F Y', strtotime(' +3 day')).'"; ';
	}else{
		echo 'var nextDeliveryDate = "'.date('d F Y', strtotime(' +2 day')).'"; ';
	}
}else{
	if(isWeekend(date('d-m-Y', strtotime(' +1 day')))){
		echo 'var nextDeliveryDate = "'.date('d F Y', strtotime(' +2 day')).'"; ';
	}else{
		echo 'var nextDeliveryDate = "'.date('d F Y', strtotime(' +1 day')).'"; ';
	}
	
}

if($isService){
	echo 'var checked = 1;';
}else{
	echo 'var checked = 0;';
}
//echo 'sdfs';
//echo "var checked;";
//echo "var checked = ".$isService;

function getMiles($i) {
     return number_format($i*0.000621371192,2);
}
function getMeters($i) {
     return number_format($i*1609.344,2);
}

// Create connection
$conn = new mysqli($servername, $username, $password, $db);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$result = $conn->query('select * from shopify_locations_instruction where enable_pickup = 1 ');
echo 'jQuery("#cnc_message").html("");';
echo 'var isProntoShow = "0"; ';
if ($result->num_rows > 0) {
	while($row = $result->fetch_assoc()){
		if($row['postal_code']){
			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL => "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".str_replace(' ', '%20', $row['postal_code'])."&destinations=".str_replace(' ','%20', $_GET['code'])."&key=AIzaSyCcM4xtOxPKZ05a0tPhT8zEtqrNCHOUdNc",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
			));

			$response = curl_exec($curl);

			curl_close($curl);

			$distance_arr = json_decode($response);

			if ($distance_arr->status=='OK') {
			   $elements = $distance_arr->rows[0]->elements;
			   if($elements[0]->status=='OK'){
				   $distance = $elements[0]->distance->value;
				   $radius = getMiles($distance); //str_replace(" mi", "", $distance);
				   //$result = $conn->query('select * from shopify_locations_instruction where enable_pickup = 1 and radius >= '.$radius.' ');
				   if ($row['radius'] >= $radius) {
					  //$row = $result->fetch_assoc();
					  echo 'var isProntoShow = "1"; ';
					  break;
				   }
			   }
			}
		}
		
	}
}

?>
if(isProntoShow == "1"){
	jQuery(".cnc_pronto").addClass("locShow");
	var prohtmml = 'Same day Click & Collect now available';
	jQuery("#cnc_message").html(prohtmml);
}else{
	jQuery(".cnc_pronto").removeClass("locShow");
	jQuery("#cnc_message").html('');
}

if(checked){
	localStorage.setItem("dpdPostalCode", postalcode);
	jQuery('#dpd_form_section #msg_top').html('Great news, we deliver here ');
	jQuery('#dpd_form_section #msg_bottom').html('');
	//jQuery('#dpd_form_section #msg_bottom').html('Next available delivery is '+nextDeliveryDate);
}else{
	localStorage.setItem("dpdPostalCode", '');
	jQuery('#dpd_form_section #msg_top').html('We’re sorry but we don’t deliver here yet');
	jQuery('#dpd_form_section #msg_bottom').html('We may be able to deliver to a friend instead');
	jQuery(".cnc_pronto").removeClass("locShow");
	jQuery("#cnc_message").html('');
}
jQuery('#dpd_form_section').addClass('dpd_checked');
//console.log(isProntoShow);

localStorage.setItem("isProntoShow", isProntoShow);
