<?php
include $_SERVER['DOCUMENT_ROOT'] . '/database.php';

/*
 Get user's entered postal code
 @param $postalcode
*/
$postalcode = str_replace(' ', '',$_GET['code']);


function getMiles($i) {
     return number_format($i*0.000621371192,2);
}

/*
	Get meters 
	@return meters in numbers
*/
function getMeters($i) {
     return number_format($i*1609.344,2);
}

// Create connection
$conn = new mysqli($servername, $username, $password, $db);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


//Exclude postal code to not delivery here

$notDeliveryPostalcodeArray = array(
	"FK17","FK18","FK19","FK20","FK21",
	"HS1","HS2","HS3","HS4","HS5","HS6","HS7","HS8","HS9",
	"KA27","KA28",
	"KW0","KW1","KW2","KW3","KW4","KW5","KW6","KW7","KW8","KW9","KW10","KW11","KW12","KW13","KW14","KW15","KW16","KW17",
	"ZE1","ZE2","ZE3"
);

$AB31to56 = array();
for($i=31; $i<=56; $i++){
	if($i != 39){
		array_push($notDeliveryPostalcodeArray,"AB".$i);
	}
}

$IV1to63 = array();
for($i=1; $i<=63; $i++){
	array_push($notDeliveryPostalcodeArray,"IV".$i);
}

$PA20to78 = array();
for($i=20; $i<=78; $i++){
	array_push($notDeliveryPostalcodeArray,"PA".$i);
}

$PH15to50 = array();
for($i=15; $i<=50; $i++){
	array_push($notDeliveryPostalcodeArray,"PH".$i);
}

function strposarr($haystack, $needles=array(), $offset=0) {
        $chr = array();
        foreach($needles as $needle) {
                $res = strpos($haystack, $needle, $offset);
                if ($res !== false) $chr[$needle] = $res;
        }
        if(empty($chr)) return false;
        return min($chr);
}

// Get all enabled locations from database

$result = $conn->query('select * from shopify_locations_instruction where enable_pickup = 1 ');
echo 'jQuery("#cnc_message").html("");';
echo 'var isProntoShow = "0"; ';
echo 'localStorage.setItem("userloaction", false);';
echo 'var country = ""; ';
echo 'var notDeliveryPostalcode = ""; ';

if(substr(strtoupper($postalcode), 0, 2 ) === "BT" || substr(strtoupper($postalcode), 0, 2 ) === "JE" || substr(strtoupper($postalcode), 0, 2 ) === "GY"){
	echo 'var country = "uk"; ';
	echo 'var notDeliveryPostalcode = "1"; ';
}

if(in_array($postalcode, $notDeliveryPostalcodeArray)){
	echo 'var country = "uk"; ';
	echo 'var notDeliveryPostalcode = "1"; ';
}else{
	if ($result->num_rows > 0) {
	while($row = $result->fetch_assoc()){
		if($row['postal_code']){

			/* Google distance matrix API search postal with in radius range  */

			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL => "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".str_replace(' ', '%20', $row['postal_code'])."&destinations=".str_replace(' ','%20', $_GET['code']).",UK&key=AIzaSyCcM4xtOxPKZ05a0tPhT8zEtqrNCHOUdNc",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
			));

			$response = curl_exec($curl);

			curl_close($curl);

			$distance_arr = json_decode($response);
			//echo '<pre>';
			//print_r($distance_arr);
			if ($distance_arr->status=='OK') {
				$destination_addresses = $distance_arr->destination_addresses[0];
				$address = explode(", ", $destination_addresses);
				$country = end($address);
				
				if(strtolower($country) == 'uk'){
					echo 'var country = "uk"; ';
					$elements = $distance_arr->rows[0]->elements;
				    if($elements[0]->status=='OK'){
					   $distance = $elements[0]->distance->value;
					   $radius = getMiles($distance); //str_replace(" mi", "", $distance);
					   //$result = $conn->query('select * from shopify_locations_instruction where enable_pickup = 1 and radius >= '.$radius.' ');
					   if ($row['radius'] >= $radius) {
						  //$row = $result->fetch_assoc();
						  
						  // Fetch matched location data 

						  $curl = curl_init();
							curl_setopt_array($curl, array(
							CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com/admin/api/2020-04/locations.json",
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_ENCODING => "",
							CURLOPT_MAXREDIRS => 10,
							CURLOPT_TIMEOUT => 0,
							CURLOPT_FOLLOWLOCATION => true,
							CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
							CURLOPT_CUSTOMREQUEST => "GET",
							CURLOPT_HTTPHEADER => array(
							"Cookie: __cfduid=db9ea86a18592f2c26102a550468d4f331587629534"
							),
							));

							$response = curl_exec($curl);

							curl_close($curl);
							
							$results = json_decode($response);
							foreach($results->locations as $location){
								$result = $conn->query('select * from shopify_locations_instruction  where location_id = "'.$location->id.'" and enable_pickup = 1 ');
								if ($result->num_rows > 0) {
									$row = $result->fetch_assoc();
									$googlelink = $location->name;
									
									if($location->address1){
										$googlelink .= ','.$location->address1;
									}
									if($location->address2){
										$googlelink .= ','.$location->address2;
									}
									if($location->city){
										$googlelink .= ','.$location->city;
									}
									if($location->province){
										$googlelink .= ','.$location->province;
									}
									if($location->country){
										$googlelink .= ','.$location->country;
									}
									if($location->zip){
										$googlelink .= ','.$location->zip;
									}
									
									$destination = $_GET['code'];
									
									if (strpos(strtolower($googlelink), strtolower($destination)) !== false) {
										echo 'var isProntoShow = "1"; ';
										echo 'localStorage.setItem("CNCLoactionID", "'.$location->id.'");';
										echo 'localStorage.setItem("CNCLoactionVariantID", "'.$row['variant_id'].'");';
										echo 'localStorage.setItem("userloaction", true);';
										break;
									}
								}
							}
	
	
						  echo 'var isProntoShow = "1"; ';
						  break;
					   }
				    }
				}else{
					echo 'var country = "'.$country.'"; ';
				}
			   
			}
		}
		
	}
}
}

?>
jQuery('.new-serch-box').addClass('show_results');
jQuery('.notDeliveryPostalcode').hide();
if(notDeliveryPostalcode == "1"){
	jQuery('#search_response .first-search').addClass('not_found');
	jQuery('#search_response .second-pronoto').addClass('not_found');
	jQuery('.error_msg').hide();
	jQuery('.notDeliveryPostalcode').show();
}else{
	if(country == "uk"){
		if(isProntoShow == "1"){
			jQuery(".cnc_pronto").addClass("locShow");
			//var prohtmml = 'Same day Click & Collect now available';
			//jQuery("#cnc_message").html(prohtmml);
			jQuery('#search_response .first-search #cncicon').show();
			jQuery('#search_response .first-search').removeClass('not_found');
			jQuery('#search_response .second-pronoto').removeClass('not_found');
			
			jQuery('.new-serch-box').addClass('showcontent');
			
			jQuery('#search_response .first-search .rel-content').html('<a href="https://www.patisserie-valerie.co.uk/collections/pat-val-premier">Premier Delivery and Click & Collect available</a>');
		}else{
			jQuery(".cnc_pronto").removeClass("locShow");
			//jQuery("#cnc_message").html('');
			jQuery('.new-serch-box').removeClass('showcontent');
			jQuery('#search_response .first-search #cncicon').hide();
			jQuery('#search_response .first-search').removeClass('not_found');
			jQuery('#search_response .second-pronoto').addClass('not_found');
			jQuery('#search_response .first-search .rel-content').html('<a href="https://www.patisserie-valerie.co.uk/collections/pat-val-premier">Premier Delivery available in your area</a>');
		}
		jQuery('.error_msg').hide();
	}else{
		jQuery('#search_response .first-search').addClass('not_found');
		jQuery('#search_response .second-pronoto').addClass('not_found');
		jQuery('.error_msg').show();
		//alert('your have entered country '+country);
	}
}


/*
if(checked){
	localStorage.setItem("dpdPostalCode", postalcode);
	jQuery('#dpd_form_section #msg_top').html('Great news, we deliver here ');
	jQuery('#dpd_form_section #msg_bottom').html('');
	//jQuery('#dpd_form_section #msg_bottom').html('Next available delivery is '+nextDeliveryDate);
}else{
	localStorage.setItem("dpdPostalCode", '');
	jQuery('#dpd_form_section #msg_top').html('We’re sorry but we don’t deliver here yet');
	jQuery('#dpd_form_section #msg_bottom').html('We may be able to deliver to a friend instead');
	jQuery(".cnc_pronto").removeClass("locShow");
	jQuery("#cnc_message").html('');
}
jQuery('#dpd_form_section').addClass('dpd_checked');
*/
//console.log(isProntoShow);

localStorage.setItem("isProntoShow", isProntoShow);
