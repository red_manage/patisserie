<?php
date_default_timezone_set('Europe/London');

$last_char = $_GET['last_char'];
$isAfternoonTea = $_GET['isAfternoonTea'];

if($last_char){
    $curl = curl_init();
    curl_setopt_array($curl, array(
    CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com/admin/api/2021-10/gift_cards/search.json?query=last_characters:".$last_char,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
      "Cookie: __cfduid=d8e0f58645e9c4695bc749c2b4b9ac5f21590488150"
    ),
    ));

    $gift_card = curl_exec($curl);
    curl_close($curl);

    $gift_card_arr = json_decode($gift_card);
    //echo '<pre>';
    //print_r($gift_card_arr);
    echo 'var AfternoonGCID = 0;';



    foreach($gift_card_arr->gift_cards as $gc){

        if($gc->order_id){
            $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com/admin/api/2021-10/orders/".$gc->order_id.".json",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
              "Cookie: __cfduid=d8e0f58645e9c4695bc749c2b4b9ac5f21590488150"
            ),
            ));

            $gift_card_order = curl_exec($curl);
            curl_close($curl);

            $orderdata = json_decode($gift_card_order);
            //print_r($orderdata->order->line_items);

            foreach($orderdata->order->line_items as $linedata){
                if($linedata->product_id == '4512694304835' && $isAfternoonTea == 'true'){
                    echo 'var AfternoonGCID = '.$linedata->product_id.';';
                }
            }
        }else{
            echo 'var AfternoonGCID = 1;';
        }

        
    }
}
?>

if(AfternoonGCID){
    //console.log(AfternoonGCID);
}else{
    jQuery('.tag__button').trigger('click');
    setTimeout(function(){
        alert("This afternoon Gift card is available for Madame Valerie's Afternoon Tea product only");
        location.reload();
    }, 500);
}