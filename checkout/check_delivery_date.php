<?php
date_default_timezone_set('Europe/London');
//$date = date("d-m-Y H:i:s");

// Get variables shipping title and current step of checkout page
$nextDayDate = date('d/m/Y', strtotime(' +1 day'));
echo 'var nextDaydate = "'.$nextDayDate.'"; ';

$shipping_title = $_GET['shipping_title'];
echo 'var shipping_title = "'.$shipping_title.'"; ';

$step = $_GET['step'];
echo 'var step = "'.$step.'"; ';

// If delivery date is tomorrow and current step is payment and shipping title is Standard Delivery so display aleart message to update delivery method and redirect to shipping step

// If delivery date is not tomorrow and current step is payment and shipping title is Premier Delivery so display aleart message to update delivery method and redirect to shipping step 
?>
var delivery_date = localStorage.getItem("DeliveryDateNew");
//console.log(nextDaydate);
//console.log(delivery_date);
var trow = jQuery('.section--shipping-method .content-box__row').length;
if(delivery_date == nextDaydate){
	
	if(step == 'payment_method'){
		if(shipping_title == 'Standard Delivery'){
			alert('Shipping method updated. Please select another shipping method.');
			window.location.href = '/checkout?step=shipping_method';
		}
	}
	
	jQuery('.section--shipping-method .content-box__row').each(function(){
		var shiptitle = jQuery(this).find('.radio__label__primary').attr('data-shipping-method-label-title');
		console.log(shiptitle);
		if(shiptitle == 'Pat Val Premier Delivery'){
			jQuery(this).find('.radio__label__primary').trigger('click');
		}else if(shiptitle != 'Free Shipping'){
		    if(trow > 1){
		    	jQuery(this).remove();
			}
		}
	});
}else{
	if(step == 'payment_method'){
		if(shipping_title == 'Pat Val Premier Delivery'){
		    alert('Shipping method updated. Please select another shipping method.');
			window.location.href = '/checkout?step=shipping_method';
		}
	}
	
	jQuery('.section--shipping-method .content-box__row').each(function(){
		var shiptitle = jQuery(this).find('.radio__label__primary').attr('data-shipping-method-label-title');
		console.log(shiptitle);
		if(shiptitle == 'Standard Delivery'){
			jQuery(this).find('.radio__label__primary').trigger('click');
		}else if(shiptitle != 'Free Shipping'){
			if(trow > 1){
		    	jQuery(this).remove();
			}
		}
	});
}