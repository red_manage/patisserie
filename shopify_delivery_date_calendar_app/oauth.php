<?php 

// Set variables for our request
$shop = "pat-val-premier";
$api_key = "117c61c7c322ff72e45f9ab3dc5eed11";
$scopes = "read_orders,write_products";
$redirect_uri = "https://matthews42.sg-host.com/shopify_delivery_date_calendar_app/index.php";

// Build install/approval URL to redirect to
$install_url = "https://" . $shop . ".myshopify.com/admin/oauth/authorize?client_id=" . $api_key . "&scope=" . $scopes . "&redirect_uri=" . urlencode($redirect_uri);

// Redirect
header("Location: " . $install_url);
die();

?>