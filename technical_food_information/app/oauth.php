<?php 

// Set variables for our request
$shop = "pat-val-premier";
$api_key = "8b6d65bb41b69d38b7060a7efbd75e0c";
$scopes = "read_orders,write_products";
$redirect_uri = "https://matthews42.sg-host.com/technical_food_information/app/index.php";

// Build install/approval URL to redirect to
$install_url = "https://" . $shop . ".myshopify.com/admin/oauth/authorize?client_id=" . $api_key . "&scope=" . $scopes . "&redirect_uri=" . urlencode($redirect_uri);

// Redirect
header("Location: " . $install_url);
die();

?>