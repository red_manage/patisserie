<?php
// Set variables for our request
$query = array(
  "client_id" => '8b6d65bb41b69d38b7060a7efbd75e0c', // Your API key
  "client_secret" => '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', // Your app credentials (secret key)
  "code" => $params['code'] // Grab the access key from the URL
);

// Generate access token URL
$access_token_url = "https://" . $params['shop'] . "/admin/oauth/access_token";

// Configure curl client and execute request
$ch = curl_init();
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_URL, $access_token_url);
curl_setopt($ch, CURLOPT_POST, count($query));
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($query));
$result = curl_exec($ch);
curl_close($ch);

// Store the access token
$result = json_decode($result, true);
$access_token = $result['access_token'];

?>

<?php
include $_SERVER['DOCUMENT_ROOT'] . '/database.php';

// Create connection
$conn = new mysqli($servername, $username, $password, $db);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Import data from CSV file
if(isset($_POST['importCSv']) && $_POST['action'] == 'Import_data'){
	
	$fileName = $_FILES["file"]["tmp_name"];
	if ($_FILES["file"]["size"] > 0) {
		
		$file = fopen($fileName, "r");
		$i=0;
		while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
			//echo '<pre>';
			//print_r($column);
			
			if($i != 0){
				$product_id = str_replace("'",'',$column[0]);
				$result = $conn->query('select * from technical_food_information where product_id = "'.$product_id.'"');
				 if ($result->num_rows > 0) {
					$row = $result->fetch_assoc();
					$conn->query('update technical_food_information set suitable_for_vegetarians = "'.$column[1].'" , suitable_for_vegans = "'.$column[2].'", ingredients = "'.addslashes($column[3]).'", contains = "'.addslashes($column[4]).'", may_contain = "'.addslashes($column[5]).'", serving_advice = "'.addslashes($column[6]).'", storage_advice = "'.addslashes($column[7]).'", allergen_warning_enable = "'.$column[8].'", allergen_warning = "'.addslashes($column[9]).'", hand_finished_warning_enable = "'.$column[10].'", hand_finished_warning = "'.addslashes($column[11]).'" where product_id = "'.$row['product_id'].'" ');
				} else {
					  //echo 'insert into shopify_locations_instruction (`product_id`, `suitable_for_vegetarians`, `suitable_for_vegans`, `ingredients`, `contains`, `may_contain`, `serving_advice`, `storage_advice`, `allergen_warning_enable`, `allergen_warning`, `hand_finished_warning_enable`, `hand_finished_warning`) values ("'.$product_id.'","'.$suitable_for_vegetarians.'", "'.$suitable_for_vegans.'","'.addslashes($ingredients).'","'.addslashes($contains).'","'.addslashes($may_contain).'","'.addslashes($serving_advice).'","'.addslashes($storage_advice).'","'.$allergen_warning_enable.'","'.addslashes($allergen_warning).'","'.$hand_finished_warning_enable.'","'.addslashes($hand_finished_warning).'") ';
					$conn->query('insert into technical_food_information (`product_id`, `suitable_for_vegetarians`, `suitable_for_vegans`, `ingredients`, `contains`, `may_contain`, `serving_advice`, `storage_advice`, `allergen_warning_enable`, `allergen_warning`, `hand_finished_warning_enable`, `hand_finished_warning`) values ("'.$product_id.'","'.$column[1].'", "'.$column[2].'","'.addslashes($column[3]).'","'.addslashes($column[4]).'","'.addslashes($column[5]).'","'.addslashes($column[6]).'","'.addslashes($column[7]).'","'.$column[8].'","'.addslashes($column[9]).'","'.$column[10].'","'.addslashes($column[11]).'") ');
				}
			}
			$i++;
		}
	}
	
}

// Export data into CSV file products.csv

if($_POST['action'] == 'export_data'){
	

	$productCSV = 'product_id, suitable_for_vegetarians, suitable_for_vegans, ingredients, contains,may_contain, serving_advice,storage_advice,allergen_warning_enable,allergen_warning,hand_finished_warning_enable,hand_finished_warning'.PHP_EOL;
	
	if($_POST['product_id'] != ''){
		$result = $conn->query('select * from technical_food_information where product_id = "'.$_POST['product_id'].'"');
		if ($result->num_rows > 0) {
			$row = $result->fetch_assoc();
			$prodid = "'".$row['product_id']."'";
			$productCSV .= $prodid.','.$row['suitable_for_vegetarians'].','.$row['suitable_for_vegans'].','.$row['ingredients'].','.$row['contains'].','.$row['may_contain'].','.$row['serving_advice'].','.$row['storage_advice'].','.$row['allergen_warning_enable'].','.$row['allergen_warning'].','.$row['hand_finished_warning_enable'].','.$row['hand_finished_warning'].PHP_EOL;
		} 
	}else{
		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com/admin/api/2020-04/products.json",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
		"Cookie: __cfduid=db9ea86a18592f2c26102a550468d4f331587629534"
		),
		));

		$response = curl_exec($curl);

		curl_close($curl);
	
		$tag = $_POST['tag'];
		if($response){
			$results = json_decode($response);
			foreach($results->products as $product){
				$all_tags = strtolower($product->tags);
				$tags = explode(', ',$all_tags);
				if (in_array(strtolower($tag),$tags)) {
					$result = $conn->query('select * from technical_food_information where product_id = "'.$product->id.'"');
					if ($result->num_rows > 0) {
						$row = $result->fetch_assoc();
						$prodid = "'".$row['product_id']."'";
						$productCSV .= $prodid.','.$row['suitable_for_vegetarians'].','.$row['suitable_for_vegans'].','.$row['ingredients'].','.$row['contains'].','.$row['may_contain'].','.$row['serving_advice'].','.$row['storage_advice'].','.$row['allergen_warning_enable'].','.$row['allergen_warning'].','.$row['hand_finished_warning_enable'].','.$row['hand_finished_warning'].PHP_EOL;
					} 
				}
				
			}
		}
	}
	//echo $productCSV;
	file_put_contents('products.csv',$productCSV);
}


// Save product data into database table technical_food_information
if(isset($_POST['submit'])){
	
	$product_id = $_POST['product_id'];
	
 
	if($product_id){
		$suitable_for_vegetarians = $_POST['suitable_for_vegetarians'];
		$suitable_for_vegans = $_POST['suitable_for_vegans'];
		$ingredients = $_POST['ingredients'];
		$contains = $_POST['contains'];
		$may_contain = $_POST['may_contain'];
		$serving_advice = $_POST['serving_advice'];
		$storage_advice = $_POST['storage_advice'];
		$allergen_warning_enable = $_POST['allergen_warning_enable'];
		$allergen_warning = $_POST['allergen_warning'];
		$hand_finished_warning_enable = $_POST['hand_finished_warning_enable'];
		$hand_finished_warning = $_POST['hand_finished_warning'];

		
		if($suitable_for_vegetarians){
		}else{
			$suitable_for_vegetarians = 0;
		}
		if($suitable_for_vegans){
		}else{
			$suitable_for_vegans = 0;
		}
		if($allergen_warning_enable){
		}else{
			$allergen_warning_enable = 0;
		}
		if($hand_finished_warning_enable){
		}else{
			$hand_finished_warning_enable = 0;
		}
		
		$result = $conn->query('select * from technical_food_information where product_id = "'.$product_id.'"');
		 if ($result->num_rows > 0) {
			$row = $result->fetch_assoc();
			$conn->query('update technical_food_information set suitable_for_vegetarians = "'.$suitable_for_vegetarians.'" , suitable_for_vegans = "'.$suitable_for_vegans.'", ingredients = "'.addslashes($ingredients).'", contains = "'.addslashes($contains).'", may_contain = "'.addslashes($may_contain).'", serving_advice = "'.addslashes($serving_advice).'", storage_advice = "'.addslashes($storage_advice).'", allergen_warning_enable = "'.$allergen_warning_enable.'", allergen_warning = "'.addslashes($allergen_warning).'", hand_finished_warning_enable = "'.$hand_finished_warning_enable.'", hand_finished_warning = "'.addslashes($hand_finished_warning).'" where product_id = "'.$row['product_id'].'" ');
		} else {
			  //echo 'insert into shopify_locations_instruction (`product_id`, `suitable_for_vegetarians`, `suitable_for_vegans`, `ingredients`, `contains`, `may_contain`, `serving_advice`, `storage_advice`, `allergen_warning_enable`, `allergen_warning`, `hand_finished_warning_enable`, `hand_finished_warning`) values ("'.$product_id.'","'.$suitable_for_vegetarians.'", "'.$suitable_for_vegans.'","'.addslashes($ingredients).'","'.addslashes($contains).'","'.addslashes($may_contain).'","'.addslashes($serving_advice).'","'.addslashes($storage_advice).'","'.$allergen_warning_enable.'","'.addslashes($allergen_warning).'","'.$hand_finished_warning_enable.'","'.addslashes($hand_finished_warning).'") ';
			$conn->query('insert into technical_food_information (`product_id`, `suitable_for_vegetarians`, `suitable_for_vegans`, `ingredients`, `contains`, `may_contain`, `serving_advice`, `storage_advice`, `allergen_warning_enable`, `allergen_warning`, `hand_finished_warning_enable`, `hand_finished_warning`) values ("'.$product_id.'","'.$suitable_for_vegetarians.'", "'.$suitable_for_vegans.'","'.addslashes($ingredients).'","'.addslashes($contains).'","'.addslashes($may_contain).'","'.addslashes($serving_advice).'","'.addslashes($storage_advice).'","'.$allergen_warning_enable.'","'.addslashes($allergen_warning).'","'.$hand_finished_warning_enable.'","'.addslashes($hand_finished_warning).'") ');
		}
	}
  

}

// Fetch product data from database by product id
if(isset($_POST['action']) && $_POST['action'] == 'get_product_data'){
	$product_id = $_POST['product_id'];
	$result = $conn->query('select * from technical_food_information where product_id = "'.$product_id.'"');
	
	if ($result->num_rows > 0) {
		$row = $result->fetch_assoc();
	} else {
		$row = array();
	}
	$data = $row;
	echo json_encode($data);
	die;
}

$result = $conn->query('select * from shopify_click_and_collect_page limit 1');
//$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
} else {
    $row = array();
}
$page_description = $row['page_description'];


// Fiter products by tags and display in dropdown
if($_POST['action'] == 'filter_by_tag'){
	
	$curl = curl_init();
	curl_setopt_array($curl, array(
	CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com/admin/api/2020-04/products.json",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 0,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => array(
	"Cookie: __cfduid=db9ea86a18592f2c26102a550468d4f331587629534"
	),
	));

	$response = curl_exec($curl);

	curl_close($curl);

	$tag = $_POST['tag'];
	$productDD = '<select class="dropdown-location" id="shopifyProducts" name="product_id"><option value="">Select Product</option>';
	if($response){
		$results = json_decode($response);
		foreach($results->products as $product){
			$all_tags = strtolower($product->tags);
			$tags = explode(', ',$all_tags);
			if (in_array(strtolower($tag),$tags)) {
				$productDD .= '<option value="'.$product->id.'">'.$product->title.'</option>';
			}
			
		}
	}
	$productDD .= '</select>';
	echo $productDD;
	die;
}

$curl = curl_init();
curl_setopt_array($curl, array(
CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com/admin/api/2020-04/products.json",
CURLOPT_RETURNTRANSFER => true,
CURLOPT_ENCODING => "",
CURLOPT_MAXREDIRS => 10,
CURLOPT_TIMEOUT => 0,
CURLOPT_FOLLOWLOCATION => true,
CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
CURLOPT_CUSTOMREQUEST => "GET",
CURLOPT_HTTPHEADER => array(
"Cookie: __cfduid=db9ea86a18592f2c26102a550468d4f331587629534"
),
));

$response = curl_exec($curl);

curl_close($curl);

$productDD = '<select class="dropdown-location" id="shopifyProducts" name="product_id"><option value="">Select Product</option>';
if($response){
	$results = json_decode($response);
	foreach($results->products as $product){
		$all_tags = strtolower($product->tags);
		$tags = explode(', ',$all_tags);
		if (in_array(strtolower('food'),$tags)) {
			$productDD .= '<option value="'.$product->id.'">'.$product->title.'</option>';
		}
		
	}
}
$productDD .= '</select>';

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Technical Food Information App</title>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>

.ciappsectiontwo{
	width: 100%;
	font-family: verdana;
	float: left;
	padding-top: 20px;
	padding-left: 40px;
	box-sizing: border-box;
	color: #1e3c84;
}
.mcontainerapptwo {
    width: 45%;
    float: left;
    box-sizing: border-box;
}
.mcontainerappthree {
    width: 55%;
    float: left;
    box-sizing: border-box;
    padding-left: 20px;
    margin-top: 104px;
}
.tpheaddingm{
	font-size: 18px;
	color: #000;
}
.shipdateselc{
	width: 100%;
	float: left;
}
.firstsdateselc{
	width: 100%;
	float: left;
}
.secondsdateselc{
	width: 50%;
	float: left;
}
.firstsdateselc h3, .secondsdateselc h3 {
    font-size: 14px;
    font-weight: normal;
    margin-bottom: 0px;
   
}
.inforntbx{
	width: 100%;
	display: flex;
    align-items: center;
    margin-top: 5px;
}
.inforntbx span{
	font-size: 12px;
    color: #000;
    min-width: 150px;
}
.subbuttonsm input[type="checkbox"]{
	background: #FFF;
	color: #333;
	border:solid 1px #1e3c84;
	padding: 0px;
	display: block;
	width: 20px;
    height: 20px;
    margin: 0px;
}
.subbuttonsm input[type="text"]{
	background: #FFF;
	color: #333;
	border:solid 1px #1e3c84;
	padding: 8px;
	display: block;
	margin-top: 8px;
}
.subbuttonsm input[type="submit"], .btn{
	background: #1e3c84;
	color: #FFF;
	border:solid 1px #1e3c84;
	padding: 10px 30px;
	display: block;
	width: 100%;
}
.inforntbxtwo{
	width: 100%;
    margin-top: 8px;
}
.inforntbxtwo span{
	font-size: 12px;
    color: #000;
    min-width: 150px;
    padding-bottom: 5px;
    display: block;
}
.inforntbxtwo textarea{
	width: 100%;
	height: 150px;
	border:solid 1px #1e3c84;
	padding: 5px 10px;
}
form#cncForm {
    width: 100%;
    float: left;
    box-sizing: border-box;
    overflow: hidden;
}
select#shopifyProducts {
    padding: 7px;
    border-radius: 0;
    border-color: #1e3c84;
    margin-top: 8px;
    width: 276px;
}
body {
    background-color: #f2f2f2;
}
.firstsdateselc {
    display: flex;
    align-items: center;
}
.ingredients_one {
    width: 100%;
    float: left;
    margin-top: 20px;
    color:#1e3c84;
}
.shipdateselc .inforntbxtwo:nth-child(2) {
    margin-left: 10px;
}
.ingredients_two {
    border: 1px solid #1e3c84;
    padding: 10px;
    width: 100%;
    float: left;
    box-sizing: border-box;
}
.ingredients_two h2 {
    font-size: 18;
    margin-top: 0;
}
.ingredients_two p {
    font-size: 13px;
    line-height: 1.4em;
    letter-spacing: 0.5px;
}
.pre {
    width: 100%;
    float: left;
}
.pre span:first-child {
    width: 47%;
}
.pre span {
    font-size: 15px;
    font-weight: bold;
    margin: 7px;
    width: auto;
    float: left;
}
.suitiable_vag span {
    display: inline-flex;
    align-items: center;
}
.suitiable_vag span h6{
	font-weight: normal;
	margin: 0;
	font-size: 13px;
}
.suitiable_vag span:nth-child(1) h6 {
    width: 180px;
}
.suitiable_vag span:nth-child(1) {
    margin-right: 58px;
}
.mcontainerappthree h6 {
    font-weight: normal;
}
.mcontainerappthree textarea{
    text-align: left;
    font-size: 13px;
    border-radius: 0;
    border-color: #1e3c84;
    padding: 7px;
    width: 100%;
}
.content_four {
    display: inline-flex;
    align-items: baseline;
}
.mcontainerappthree h6 {
    font-weight: normal;
    width: 180px;
    font-size: 14px;
}

.mcontainerappthree .ingredients h6 {
    font-weight: normal;
    width: 275px;
    font-size: 13px;
    margin: 0;
}
.content_four {
    display: inline-flex;
    width: 100%;
    margin-top: 15px;
}
.content_four p {
    font-size: 13px;
}
.content_four h6 span {
    width: 100%;
    float: left;
    margin-left: 54px;
}
.modal-overlay {
    position: fixed;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0,0,0,0.75);
    display: none;
    z-index: 2;
}
.inner-import-modal{
	position: absolute;
    left: 50%;
    top: 50%;
    z-index: 9;
    background: #fff;
    padding: 50px 20px 10px;
    border-radius: 5px;
    transform: translate(-50%, -50%);
    display: none;
}
body.form-modal-open{
	overflow: hidden;
}
.form-modal-open .modal-overlay,
.form-modal-open .inner-import-modal{
	display: block;
}
.inner-import-modal .modal-close{
	position: absolute;
    right: 15px;
    top: 8px;
    width: 30px;
    height: 30px;
}
.inner-import-modal .modal-close span{
	position: relative;
}
.inner-import-modal .modal-close span:after,
.inner-import-modal .modal-close span:before{
	content: '';
	position: absolute;
	left: 3px;
	top: 13px;
	width: 25px;
	height: 3px;
	background-color: #333;
	transform: rotate(-45deg);
}
.inner-import-modal .modal-close span:before{
	transform: rotate(45deg);
}
.inner-import-modal .input-row input[type="submit"]{
	background-color: #1e3c84;
    color: #fff;
    border: 1px solid transparent;
    padding: 6px 20px 8px;
    cursor: pointer;
}
</style>
</head>

<body>
<div class="inner-import-modal">
	<a href="javascript:void(0)" class="modal-close"><span></span></a>
	<form method="post" name="frmCSVImport" id="frmCSVImport" enctype="multipart/form-data">
		<div class="input-row">
			<label class="col-md-4 control-label">Choose CSV
				File</label> <input type="file" name="file" id="file" accept=".csv">
				<input type="hidden" name="action" value="Import_data">
			<input type="submit" name="importCSv" value="import">
			<br />
		</div>
	</form>
</div>
<form method="post" id="cncForm">
<div class="ciappsectiontwo">
	<div class="mcontainerapptwo">
		<!-- <h2 class="tpheaddingm">Technical Food Information App</h2> -->
		<div class="shipdateselc">
			<div class="firstsdateselc">
				
				
				<div class="inforntbxtwo">
				<h3>Product</h3>
					<div class="subbuttonsm">
					<div id="productDropDown"><?php echo $productDD; ?></div>
					</div>
				</div>
				
				<div class="inforntbxtwo">
				<h3>Filter by tag</h3>
					<div class="subbuttonsm">
					<input type="text" name="tag" id="tags" value="food">
					</div>
				</div>
			</div>
			
		</div>



		<div class="ingredients_one">
			<div class="pre">
				<span>Preview</span>
				<span><a href="javascript:void(0)" id="exportData">Export</a></span>
				<span><a href="javascript:void(0)" class="import_form">Import</a></span>
			</div>
			<div class="ingredients_two">
				<div class="content_one">
					<h2>Ingredients</h2>
					<p id="prev_suitable_for"><p>
					<p id="prev_ingredients"></p>
				</div>
				<div class="content_two">
					<h2>Allergens</h2>
					<p><strong>Contains:</strong> <span id="prev_contains"></span><p>
					<p><strong>May Contains: </strong><span id="prev_may_contain"></span></p>
					<p id="prev_allergen_warning"></p>
				</div>
				<div class="content_three">
					<h2>Storage and Serving Advice</h2>
					<p id="prev_serving_advice"><p>
					<p id="prev_storage_advice"></p>
					<p id="prev_hand_finished_warning"></p>
				</div>

			</div>
		</div>


		<div class="inforntbx">
		<div class="subbuttonsm">
			<br/>
			<input type="submit" value="Save" name="" id="submitcnc">
			<input type="submit" value="Save" name="submit" id="submitform" style="visibility:hidden;">
		</div>
		</div>
	</div>

	<div class="mcontainerappthree">
		<div class="suitiable_vag">
			<span><h6>Suitable for vegetarians:</h6> <input type="checkbox" name="suitable_for_vegetarians" id="suitable_for_vegetarians" value="1"></span>
			<span><h6>Suitiable for vegans:</h6> <input type="checkbox" name="suitable_for_vegans" id="suitable_for_vegans" value="1"></span>
		</div>
		<div class="ingredients">
			<div class="content_four">
					<h6>Ingredients:</h6> 
					<textarea name="ingredients" id="ingredients" rows="6"></textarea>
			</div>
			<div class="content_four">
					<h6>Contains:</h6> 
					<textarea name="contains" id="contains" rows="1"></textarea>
			</div>
			<div class="content_four">
					<h6>May contain:</h6> 
					<textarea name="may_contain" id="may_contain" rows="1"></textarea>
			</div>
			<div class="content_four">
					<h6>Serving advice:</h6> 
					<textarea name="serving_advice" id="serving_advice" rows="3"></textarea>
			</div>
			<div class="content_four">
					<h6>Storage advice:</h6> 
					<textarea name="storage_advice" id="storage_advice" rows="2"></textarea>
			</div>
			<div class="content_four"><p>Company wide general product warnings <i>(if below text is amended it will affect multiple products)</i></p></div>
			<div class="content_four">
					<h6>Allergen Warning: <span><input type="checkbox" name="allergen_warning_enable" id="allergen_warning_enable" value="1"></span></h6> 
					<textarea name="allergen_warning" id="allergen_warning" rows="4" style="display:none;"></textarea>
			</div>
			<div class="content_four">
					<h6>Hand Finished Warning: <span><input type="checkbox" name="hand_finished_warning_enable" id="hand_finished_warning_enable" value="1"></span></h6> 
					<textarea name="hand_finished_warning" id="hand_finished_warning" rows="2" style="display:none;"></textarea>
			</div>
		</div>
	</div>
</div>
</form>

<form id="exportcsv" method="post" style="display:none;">
<input type="hidden" name="action" id="export_action" value="export_data">
<input type="hidden" name="product_id" id="export_product_id" value="">
<input type="hidden" name="tag" id="export_tag" value="">
<input type="submit" id="exportdatasubmit">
</form>
<div class="modal-overlay"></div>

				
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>

<!-- Javascript for update values on click or change -->
<script>
jQuery(document).ready(function(){
	function delay(callback, ms) {
	  var timer = 0;
	  return function() {
		var context = this, args = arguments;
		clearTimeout(timer);
		timer = setTimeout(function () {
		  callback.apply(context, args);
		}, ms || 0);
	  };
	}
	
	
	
	$('#ingredients').keyup(function (e) {
		var ingredients = jQuery('#ingredients').val();
		jQuery('#prev_ingredients').html(ingredients);
	});
	
	$('#contains').keyup(function (e) {
		var contains = jQuery('#contains').val();
		jQuery('#prev_contains').html(contains);
	});
	
	$('#may_contain').keyup(function (e) {
		var may_contain = jQuery('#may_contain').val();
		jQuery('#prev_may_contain').html(may_contain);
	});
	
	$('#serving_advice').keyup(function (e) {
		var serving_advice = jQuery('#serving_advice').val();
		jQuery('#prev_serving_advice').html(serving_advice);
	});
	
	$('#storage_advice').keyup(function (e) {
		var storage_advice = jQuery('#storage_advice').val();
		jQuery('#prev_storage_advice').html(storage_advice);
	});
	
	$('#allergen_warning').keyup(function (e) {
		var allergen_warning = jQuery('#allergen_warning').val();
		jQuery('#prev_allergen_warning').html(allergen_warning);
	});
	
	$('#hand_finished_warning').keyup(function (e) {
		var hand_finished_warning = jQuery('#hand_finished_warning').val();
		jQuery('#prev_hand_finished_warning').html(hand_finished_warning);
	});
	
	$('#tags').keyup(delay(function (e) {
		var tag = jQuery('#tags').val();
		$.ajax({
			type: "POST",
			data: 'action=filter_by_tag&tag='+tag,
			success: function(response){
				jQuery('#productDropDown').html(response);
			}
	   });
	}, 500));

	jQuery('body').on('click', '#submitcnc', function(e){
		e.preventDefault();
		var product_id = jQuery('#shopifyProducts').val();
		
		if(product_id){
			jQuery('#submitform').trigger('click');
		}else{
			alert('Please select product');
		}
	});
	

	
	jQuery('body').on('click', '#suitable_for_vegans , #suitable_for_vegetarians', function(e){
		var suitable_for = '';
		if(jQuery('#suitable_for_vegetarians').prop('checked') == true && jQuery('#suitable_for_vegans').prop('checked') == true){
			suitable_for = 'Suitable for vegans and vegetarians';
		}else if(jQuery('#suitable_for_vegetarians').prop('checked') == true){
			suitable_for = 'Suitable for vegetarians';
		}else if(jQuery('#suitable_for_vegans').prop('checked') == true){
			suitable_for = 'Suitable for vegans';
		}else{
			suitable_for = 'Not suitable for vegans and vegetarians';
		}
		
		jQuery('#prev_suitable_for').html(suitable_for);
		
	});
	
	
	
	jQuery('body').on('click', '#allergen_warning_enable', function(e){
		if(jQuery('#allergen_warning_enable').prop('checked') == true){
			jQuery('#allergen_warning').show();
			jQuery('#prev_allergen_warning').show();
		}else{
			jQuery('#allergen_warning').hide();
			jQuery('#prev_allergen_warning').hide();
		}
	});
	jQuery('body').on('click', '#hand_finished_warning_enable', function(e){
		if(jQuery('#hand_finished_warning_enable').prop('checked') == true){
			jQuery('#hand_finished_warning').show();
			jQuery('#prev_hand_finished_warning').show();
		}else{
			jQuery('#hand_finished_warning').hide();
			jQuery('#prev_hand_finished_warning').hide();
		}
	});
	jQuery('body').on('change', '#shopifyProducts', function(e){
		var product_id = jQuery(this).val();
		showProductData(product_id);
	});
	
	jQuery('body').on('click', '#exportData', function(e){
		var product_id = jQuery('#shopifyProducts').val();
		var tag = jQuery('#tags').val();
		
		if(product_id || tag){
			jQuery('#export_product_id').val(product_id);
			jQuery('#export_tag').val(tag);
			jQuery('#exportdatasubmit').trigger('click');
		}else{
			alert('Please enter tag');
		}
		
		
	});
	
	
	
});

function showProductData(product_id){
	$.ajax({
		type: "POST",
		data: 'action=get_product_data&product_id='+product_id,
		dataType: 'json',
		success: function(response)
		{
			var data = response;
			console.log(data);
			
			jQuery('#ingredients').val(jQuery.trim(data.ingredients));
			jQuery('#contains').val(jQuery.trim(data.contains));
			jQuery('#may_contain').val(jQuery.trim(data.may_contain));
			jQuery('#allergen_warning').val(jQuery.trim(data.allergen_warning));
			jQuery('#serving_advice').val(jQuery.trim(data.serving_advice));
			jQuery('#storage_advice').val(jQuery.trim(data.storage_advice));
			jQuery('#hand_finished_warning').val(jQuery.trim(data.hand_finished_warning));
			
			
			jQuery('#prev_ingredients').html(jQuery.trim(data.ingredients));
			jQuery('#prev_contains').html(jQuery.trim(data.contains));
			jQuery('#prev_may_contain').html(jQuery.trim(data.may_contain));
			jQuery('#prev_allergen_warning').html(jQuery.trim(data.allergen_warning));
			jQuery('#prev_serving_advice').html(jQuery.trim(data.serving_advice));
			jQuery('#prev_storage_advice').html(jQuery.trim(data.storage_advice));
			jQuery('#prev_hand_finished_warning').html(jQuery.trim(data.hand_finished_warning));
			
			
			
			var suitable_for_vegetarians = data.suitable_for_vegetarians;
			if(suitable_for_vegetarians == 1){
				jQuery('#suitable_for_vegetarians').prop('checked', true);
			}else{
				jQuery('#suitable_for_vegetarians').prop('checked', false);
			}
			
			var suitable_for_vegans = data.suitable_for_vegans;
			if(suitable_for_vegans == 1){
				jQuery('#suitable_for_vegans').prop('checked', true);
			}else{
				jQuery('#suitable_for_vegans').prop('checked', false);
			}
			
			var suitable_for = '';
			if(suitable_for_vegetarians == 1 && suitable_for_vegans == 1){
				suitable_for = 'Suitable for vegans and vegetarians';
			}else if(suitable_for_vegetarians == 1){
				suitable_for = 'Suitable for vegetarians';
			}else if(suitable_for_vegans == 1){
				suitable_for = 'Suitable for vegans';
			}else{
				suitable_for = 'Not suitable for vegans and vegetarians';
			}
			
			jQuery('#prev_suitable_for').html(suitable_for);
			
			var allergen_warning_enable = data.allergen_warning_enable;
			if(allergen_warning_enable == 1){
				jQuery('#allergen_warning_enable').prop('checked', true);
				jQuery('#allergen_warning').show();
				jQuery('#prev_allergen_warning').show();
			}else{
				jQuery('#allergen_warning').hide();
				jQuery('#prev_allergen_warning').hide();
				jQuery('#allergen_warning_enable').prop('checked', false);
			}
			
			var hand_finished_warning_enable = data.hand_finished_warning_enable;
			if(hand_finished_warning_enable == 1){
				jQuery('#hand_finished_warning').show();
				jQuery('#prev_hand_finished_warning').show();
				jQuery('#hand_finished_warning_enable').prop('checked', true);
			}else{
				jQuery('#hand_finished_warning').hide();
				jQuery('#prev_hand_finished_warning').hide();
				jQuery('#hand_finished_warning_enable').prop('checked', false);
			}
			
		}
   });
}

jQuery(document).ready(function(){
	jQuery(".import_form").click(function(){
		jQuery("body").addClass("form-modal-open");
	});
	jQuery(".modal-close").click(function(){
		jQuery("body").removeClass("form-modal-open");
	});
});
</script>

<?php if(isset($_POST['submit'])){
	$product_id = $_POST['product_id'];
	$tag = $_POST['tag'];
	?>
	<script>
	jQuery(document).ready(function(){
		var product_id = '<?php echo $product_id; ?>';
		var tag = '<?php echo $tag; ?>';
		jQuery('#shopifyProducts').val(product_id);
		jQuery('#tags').val(tag);
		showProductData(product_id);
	});
	</script>
<?php } ?>

<?php if($_POST['action'] == 'export_data'){
	$product_id = $_POST['product_id'];
	$tag = $_POST['tag'];
	?>
	<script>
	jQuery(document).ready(function(){
		var product_id = '<?php echo $product_id; ?>';
		var tag = '<?php echo $tag; ?>';
		jQuery('#shopifyProducts').val(product_id);
		jQuery('#tags').val(tag);
		showProductData(product_id);
		setTimeout(function () {
		  window.location.href = "https://matthews42.sg-host.com/technical_food_information/app/products.csv";
		}, 2000);
		
	});
	</script>
<?php } ?>

</body>
</html>