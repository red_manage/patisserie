<?php
include $_SERVER['DOCUMENT_ROOT'] . '/database.php';

// Create connection
$conn = new mysqli($servername, $username, $password, $db);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Get product information on selection of product in dropdown in app.

if($_GET['product_id']){
	$product_id = (int)$_GET['product_id'];

    $sql = $conn->prepare("SELECT * FROM technical_food_information WHERE product_id = ?");
    $sql->bind_param('i', $product_id);
    $sql->execute();
    $result = $sql->get_result();

	if ($result->num_rows > 0) {
		$row = $result->fetch_assoc();
	}else{
		$row = array();
	}
	echo 'var jdata = "'.addslashes(json_encode($row)).'"; ';
}
?>
var data = JSON.parse(jdata);

jQuery('#prev_ingredients').html(jQuery.trim(data.ingredients));
jQuery('#prev_contains').html(jQuery.trim(data.contains));
jQuery('#prev_may_contain').html(jQuery.trim(data.may_contain));
jQuery('#prev_allergen_warning').html(jQuery.trim(data.allergen_warning));
jQuery('#prev_serving_advice').html(jQuery.trim(data.serving_advice));
jQuery('#prev_storage_advice').html(jQuery.trim(data.storage_advice));
jQuery('#prev_hand_finished_warning').html(jQuery.trim(data.hand_finished_warning));

var suitable_for_vegetarians = data.suitable_for_vegetarians;
var suitable_for_vegans = data.suitable_for_vegans;

var suitable_for = '';
if(suitable_for_vegetarians == 1 && suitable_for_vegans == 1){
	suitable_for = 'Suitable for vegans and vegetarians';
}else if(suitable_for_vegetarians == 1){
	suitable_for = 'Suitable for vegetarians';
}else if(suitable_for_vegans == 1){
	suitable_for = 'Suitable for vegans';
}else{
	suitable_for = 'Not suitable for vegans and vegetarians';
}

jQuery('#prev_suitable_for').html(suitable_for);

var allergen_warning_enable = data.allergen_warning_enable;
if(allergen_warning_enable == 1){
	jQuery('#prev_allergen_warning').show();
}else{
	jQuery('#prev_allergen_warning').hide();
}

var hand_finished_warning_enable = data.hand_finished_warning_enable;
if(hand_finished_warning_enable == 1){
	jQuery('#prev_hand_finished_warning').show();
}else{
	jQuery('#prev_hand_finished_warning').hide();
}
			