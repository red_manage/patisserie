<?php 

// Set variables for our request
$shop = "pat-val-premier";
$api_key = "66a7ac3817e01df2274558fb5e925c9d";
$scopes = "read_orders,write_products";
$redirect_uri = "https://matthews42.sg-host.com/shopify_shipstation_app/index.php";

// Build install/approval URL to redirect to
$install_url = "https://" . $shop . ".myshopify.com/admin/oauth/authorize?client_id=" . $api_key . "&scope=" . $scopes . "&redirect_uri=" . urlencode($redirect_uri);

// Redirect
header("Location: " . $install_url);
die();

?>