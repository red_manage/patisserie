<?php
include $_SERVER['DOCUMENT_ROOT'] . '/database.php';

// Set variables for our request
$query = array(
  "client_id" => '79a26b6a5e5c211a0b40a35f104622fa', // Your API key
  "client_secret" => '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', // Your app credentials (secret key)
  "code" => $params['code'] // Grab the access key from the URL
);

// Generate access token URL
$access_token_url = "https://" . $params['shop'] . "/admin/oauth/access_token";

// Configure curl client and execute request
$ch = curl_init();
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_URL, $access_token_url);
curl_setopt($ch, CURLOPT_POST, count($query));
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($query));
$result = curl_exec($ch);
curl_close($ch);

// Store the access token
$result = json_decode($result, true);
$access_token = $result['access_token'];


date_default_timezone_set('Europe/London');

// Create connection
$conn = new mysqli($servername, $username, $password, $db);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Get Printer id from database when choose location

if(isset($_POST['action']) && $_POST['action'] == 'get_location_ins'){
	$location_id = $_POST['location_id'];
	$result = $conn->query('select * from print_to_kitchen_settings where location_id = "'.$location_id.'"');
	
	if ($result->num_rows > 0) {
		$row = $result->fetch_assoc();
	} else {
		$row = array();
	}
	$data = array();
	$data['printer_id'] = $row['printer_id'];
	echo json_encode($data);
	die;
}

// Connect to google cloud printer with Printer id
// Using mpdf library to create PDF file

if($_POST['action'] == 'SubmitPrint'){
	$locationid = $_POST['locationid'];
	$result = $conn->query('select * from print_to_kitchen_settings where location_id = "'.$locationid.'"');
	$data = '';
	if ($result->num_rows > 0) {
		$row = $result->fetch_assoc();
		$printer_id = $row['printer_id'];

		require_once dirname(__FILE__).'/google-api-php-client/google-api-php-client/vendor/autoload.php';

		$client = new Google_Client();

		function getClient()
		{
			$client = new Google_Client();
		    $client->setAuthConfig(dirname(__FILE__).'/credentials.json');
		    $client->setAccessType('offline');
		    $client->setPrompt('select_account consent');
			$client->setApplicationName("Kitchen Printer");
			$client->setScopes(array('https://www.googleapis.com/auth/cloudprint'));

		    // Load previously authorized token from a file, if it exists.
		    // The file token.json stores the user's access and refresh tokens, and is
		    // created automatically when the authorization flow completes for the first
		    // time.
		    $tokenPath = dirname(__FILE__).'/token.json';
		    if (file_exists($tokenPath)) {
		        $accessToken = json_decode(file_get_contents($tokenPath), true);
		        $client->setAccessToken($accessToken);
		    }

		    // If there is no previous token or it's expired.
		    if ($client->isAccessTokenExpired()) {
		        // Refresh the token if possible, else fetch a new one.
		        if ($client->getRefreshToken()) {
		            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
		        } else {
		            // Request authorization from the user.
		            $authUrl = $client->createAuthUrl();
		            printf("Open the following link in your browser:\n%s\n", $authUrl);
		            print 'Enter verification code: ';
		            $authCode = $_GET['code'];
		            if($authCode){
		            	// Exchange authorization code for an access token.
			            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
			            $client->setAccessToken($accessToken);

			            // Check to see if there was an error.
			            if (array_key_exists('error', $accessToken)) {
			                throw new Exception(join(', ', $accessToken));
			            }
		            }


		            
		        }
		        // Save the token to a file.
		        if (!file_exists(dirname($tokenPath))) {
		            mkdir(dirname($tokenPath), 0700, true);
		        }
		        file_put_contents($tokenPath, json_encode($client->getAccessToken()));
		    }
		    return $client;
		}


		$html = $_POST['html'];
		require_once dirname(__FILE__).'/mpdf/mpdf/vendor/autoload.php';
		//$mpdf = new \Mpdf\Mpdf();
		$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => [76,160]]);

		$mpdf->WriteHTML($html);

		$filename = dirname(__FILE__).'/uploads/'.$locationid.'-'.$printer_id.'.pdf';
		$mpdf->Output($filename,'F');

		// Get the API client and construct the service object.
		$client = getClient();
		$httpClient = $client->authorize();

		$params = array(
		    'printerid' => $printer_id,
		    'title' => 'Print to kitchen',
		    'ticket' => '{"version":"1.0","print":{}}',
		    'content' => 'data:application/pdf;base64,' . base64_encode(file_get_contents($filename)),
		    'contentType' => 'dataUrl'
		);

		/* Use form_params instead of body for version 6 of Guzzle Http */
		$response = $httpClient->post('https://www.google.com/cloudprint/submit', array('form_params'=>$params));
		$res = json_decode($response->getBody());
		if($res->success == 1){
			$data = 'success';
		}
	} else {
		$row = array();

	}
	
	echo $data;
	die;
	

}

?>
<style type="text/css">
	*{
		box-sizing: border-box;
	}
	body{
		padding: 0;
		margin: 0;
		font-family: 'Arial';
		background-color: #f7f7f7;
	}
	.main-box .content-box{
		width: 500px;
		padding: 30px;
	}
	.content-box h2{
		margin-top: 0;
		margin-bottom: 30px;
		font-size: 18px;
		color: #000;
	}
	.content-box h4{
		font-size: 16px;
		font-weight: bold;
		margin-bottom: 14px;
	}
	.content-box form label{
		font-size: 14px;
		font-weight: normal;
		padding-bottom: 10px;
		display: inline-block;
		text-decoration: underline;
	}
	.content-box form input{
		margin-bottom: 12px;
	}
	.content-box form input,
	.content-box form select{
		width: 100%;
		border: 1px solid #c3c3c3;
		font-size: 13px;
		color: #000;
		background-color: #fff;
	}
	.content-box form input:focus,
	.content-box form select:focus{
		outline: none;
	}
	.content-box form button{
		background: #1e3c84;
	    color: #FFF;
	    border: solid 1px #1e3c84;
	    padding: 5px 10px;
	    margin-top: 50px;
	    cursor: pointer;
	}
	.form-control {
    margin-bottom: 10px;
}
input, select {
    padding: 5px;
}
.main-box{
	    width: 100%;
    background: #d9d9d9;
    font-family: verdana;
    float: left;
    padding: 50px 0px;
}
#submitPrinter{
	background: #1e3c84;
    color: #FFF;
    border: solid 1px #1e3c84;
    padding: 5px 10px;
    display: block;
    width: auto;
	margin-top:10px;
}
#CloudPrint{
	background: #1e3c84;
    color: #FFF;
    border: solid 1px #1e3c84;
    padding: 5px 10px;
    margin-top: 10px;
    width: 60px;
    text-align: center;
}
</style>
<style>

.loading {
  position: fixed;
  z-index: 999;
  height: 2em;
  width: 2em;
  overflow: visible;
  margin: auto;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
}


.loading:before {
  content: '';
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0,0,0,0.6);
}


.loading:not(:required) {
  font: 0/0 a;
  color: transparent;
  text-shadow: none;
  background-color: transparent;
  border: 0;
}

.loading:not(:required):after {
  content: '';
  display: block;
  font-size: 10px;
  width: 1em;
  height: 1em;
  margin-top: -0.5em;
  -webkit-animation: spinner 1500ms infinite linear;
  -moz-animation: spinner 1500ms infinite linear;
  -ms-animation: spinner 1500ms infinite linear;
  -o-animation: spinner 1500ms infinite linear;
  animation: spinner 1500ms infinite linear;
  border-radius: 0.5em;
  -webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
  box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
}



@-webkit-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-moz-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-o-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
</style>
<?php

/*
if(isset($_POST['product_ids'])){
	$product_ids = rtrim($_POST['product_ids'],',');
	$variant_ids = rtrim($_POST['variant_ids'],',');
	$customer_id = $_POST['customer_id'];
	$location = $_POST['location'];
	$res = $conn->query('insert into print_to_kitchen (`product_ids`, `variant_ids`, `location`, `customer_id`, `orderdate`) VALUES ("'.$product_ids.'", "'.$variant_ids.'", "'.$location.'", "'.$customer_id.'", "'.date('Y-m-d').'"  ) ');
	if($res){
		echo 'insterted';
	}
	die;
}
*/

// Save app values to database

if(isset($_POST['submit'])){
	$location_id = $_POST['location_id'];
	$printer_id = $_POST['printer_id'];
	$tags = $_POST['tags'];
	
	if($location_id and $printer_id){
		$result = $conn->query('select * from print_to_kitchen_settings where location_id = "'.$location_id.'" ');
		//$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			$res = $conn->query('update print_to_kitchen_settings set printer_id = "'.$printer_id.'" where location_id = "'.$location_id.'" ');
		}else{
			//echo 'insert into print_to_kitchen_settings (`location_id`, `printer_id`, `tags`) VALUES ("'.$location_id.'", "'.$printer_id.'", "'.$tags.'") ';
			$res = $conn->query('insert into print_to_kitchen_settings (`location_id`, `printer_id`) VALUES ("'.$location_id.'", "'.$printer_id.'") ');
		}
	}
	
	$result = $conn->query('select * from print_to_kitchen_tag');
	if ($result->num_rows > 0) {
		$res = $conn->query('update print_to_kitchen_tag set tag = "'.$tags.'" ');
	}else{
		$res = $conn->query('insert into print_to_kitchen_tag (`tag`) VALUES ("'.$tags.'") ');
	}
}


// Update product tags

if(isset($_POST['product_ids'])){
	$productides = explode(',',$_POST['product_ids']);

	$quantity = explode(',', $_POST['quantity']);
	
	$result = $conn->query('select * from print_to_kitchen_tag');
	$tags = array();
	if ($result->num_rows > 0) {
		$tags = $result->fetch_assoc();
	}
	$data = '';
	$data .= '<div id="responseData"><ul style="margin-top:20px; margin-bottom:20px; list-style:none;  padding-left:0px; line-height:2em;">';
	foreach($productides as $key => $pid){
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com/admin/api/2020-07/products/".$pid.".json",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"Cookie: _secure_admin_session_id_csrf=1bdc262f9441f84a05178767f7671da0; _secure_admin_session_id=1bdc262f9441f84a05178767f7671da0; _master_udr=eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaEpJaWt4TldVeE1HUXlPUzB3Tm1JM0xUUXdZVFF0T0dJeU1DMWlNalV5WVRGbVpEUTRNbVlHT2daRlJnPT0iLCJleHAiOiIyMDIyLTA2LTI2VDExOjEzOjAyLjExOFoiLCJwdXIiOiJjb29raWUuX21hc3Rlcl91ZHIifX0%3D--934e63c79b583c8364029a20482af8aba1172eac; _y=8bd56dde-cbc5-4330-a752-e9fa91d565a4; _shopify_y=8bd56dde-cbc5-4330-a752-e9fa91d565a4; __cfduid=d822e91ce72a52ecc2c57501abad650a01593167308"
			),
		));

		$response = curl_exec($curl);
	
		curl_close($curl);
		//echo '<pre>';
		if($tags){
			$tag = $tags['tag'];
		}else{
			$tag = 'kitchen'; //default tag
		}
		
		$product = json_decode($response);
		
		$all_tags = strtolower($product->product->tags);
		$Ptags = explode(', ',$all_tags);
		
		if (in_array(strtolower($tag),$Ptags)) {
			$data .= '<li>'.$quantity[$key].'x '.$product->product->title.'</li>';
		}
		
	}
	$data .= '</ul></div>';

	echo $data;
	die;
}

// Fetch product tags from database
$result = $conn->query('select * from print_to_kitchen_tag');
$tags = array();
if ($result->num_rows > 0) {
	$tags = $result->fetch_assoc();
}


?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Print to kitchen App</title>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

</head>

<body>

<div style=" width:287px; font-family: Arial, Helvetica, sans-serif; text-align:left; font-weight:normal; margin:0px; display: none;" id="cartDetails">
	<h2 style="font-size:25px;" class="customerName"></h2>
	<h4 class="datetime"><?php echo date('h:i d/m'); ?></h4>
	<h4 class="userName"></h4>
	<div class="itmeDetails"></div>
	<h4>Notes:</h4>
	<h4 class="note"></h4>
	<h2 style="font-size:25px; padding:0px; margin:0px;" class="customerName"></h2>
</div>

<div id="current_location_id" style="display:none;"></div>
<div id="print_button_container"><a href="javascript:void(0);" id="CloudPrint" style="display:none;">Print</a></div>


<div class="main-box">
		<div class="content-box">
		<form method="post" id="printSettings">
<?php 

// Fetch all locations
$curl = curl_init();
	curl_setopt_array($curl, array(
	CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com/admin/api/2020-04/locations.json",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 0,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => array(
	"Cookie: __cfduid=db9ea86a18592f2c26102a550468d4f331587629534"
	),
	));

	$response = curl_exec($curl);
	
	curl_close($curl);
	$html = '<div class="form-control"><label>Location</label><select name="location_id" id="location_id"><option value="">Choose your Location</option>';
	$loc_description = '';
	if($response){
		$results = json_decode($response);
		foreach($results->locations as $location){
			$result = $conn->query('select * from print_to_kitchen_settings  where location_id = "'.$location->id.'" ');
			//$result = $conn->query($sql);
			if ($result->num_rows > 0) {
				if($location->id == $row['location_id']){
					
				}
			}
				
			$html .= '<option value="'.$location->id.'">'.$location->name.'</option>';
		}
		$html .= '</select></div>';
	}
	echo $html;
?>
<h4>Kitchen Printer</h4>
<div class="form-control">
<label>Kitchen Printer ID</label>
<input type="text" name="printer_id" id="printer_id" placeholder="Printer Id" value="">
</div>

<div class="form-control">
	<label>Product tags</label>
	<input type="text" name="tags" placeholder="Please enter tag" value="<?php echo $tags['tag']; ?>">
</div>
<input type="submit" value="Save" id="submitPrinter">
<input type="submit" value="Save" name="submit" id="submitform" style="display:none;">
</form>
</div>
</div>
<div id="container1"></div>	
<div id="container2"></div>		
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>
<script src="https://www.google.com/cloudprint/client/cpgadget.js"></script>
<script src="https://unpkg.com/@shopify/app-bridge"></script>
<script>
jQuery('body').on('change', '#location_id', function(e){
    var location_id = jQuery(this).val();
    $.ajax({
		type: "POST",
		data: 'action=get_location_ins&location_id='+location_id,
		dataType: 'json',
		success: function(response)
		{
			var data = response;
			var printer_id = data.printer_id;

			$('#printer_id').val(printer_id);
	    }
   });
    
    if(jQuery(this).val() == ''){
        jQuery('#location_instruction').hide();
		jQuery('#pickLcation').hide();
		jQuery('#productVariant').hide();
		jQuery('#locationRadius').hide();
    }else{
        jQuery('#location_instruction').show();
		jQuery('#pickLcation').show();
		jQuery('#productVariant').show();
		jQuery('#locationRadius').show();
    }
  });
  
$('body').on('click', '#submitPrinter', function(e){
	e.preventDefault();
	$('#submitform').trigger('click');
});

var shopOrigin = 'pat-val-premier.myshopify.com';

//shopify app bridge to connect POS app to shopify
// Set POS app in partner account of shopify 
const AppBridge = window['app-bridge'];
const actions = window['app-bridge'].actions;
const createApp = AppBridge.createApp;
const Cart = actions.Cart;
const Pos = actions.Pos;

const app = createApp({
  apiKey: '79a26b6a5e5c211a0b40a35f104622fa',
  shopOrigin: shopOrigin,
});

var pos_location_id = '';
var useName = '';
app.getState().then((state) => {
	 if(state.pos.location){
	 	$('.main-box').hide();
		 pos_location_id = state.pos.location.id;
		 $('#current_location_id').html(pos_location_id);
		 useName = state.pos.user.firstName+' '+state.pos.user.lastName;
		 $('.userName').html(useName);
	 }
	 
}).catch(error => {
    alert(error);
});

var cart = Cart.create(app);
cart.subscribe(Cart.Action.UPDATE, function (payload) {

});

var unsubscriber = cart.subscribe(Cart.Action.UPDATE, function (payload) {
  var jsonData = JSON.stringify(payload);
  $('.main-box').hide();
  if(payload.data.customer){
  	$('body').addClass('loading');
	var customer_id = payload.data.customer.id;
	var itmes = payload.data.lineItems;
	var note = payload.data.note;
	var prducts = '';
	var productids = '';
	var quantity = '';
	var variantids = '';
	for (var i = 0; i < itmes.length; i++) {
		productids +=   itmes[i].productId+',';
		variantids +=   itmes[i].variantId+',';
		quantity +=   itmes[i].quantity+',';
	}

	$("#cartDetails .customerName").html(payload.data.customer.firstName+' '+payload.data.customer.lastName);

	$('.note').html(note);
		productids = productids.replace(/,\s*$/, "");
		quantity = quantity.replace(/,\s*$/, "");
		$.ajax({
			type: "POST",
			data: "product_ids="+productids+'&quantity='+quantity,
			success : function(response){
				var products =  $(response).filter('#responseData').html();
				$("#cartDetails .itmeDetails").html(products);
				var printdata = $('#cartDetails').html();

				var locid = $('#current_location_id').html();
				//alert(locid);
				var printdata = $('#cartDetails').html();
				$.ajax({
					type: "POST",
					data: "action=SubmitPrint&locationid="+locid+"&html="+printdata,
					success : function(response){
						if(response == 'success'){
							//alert('File send to printer');
							const pos = Pos.create(app);
							pos.dispatch(Pos.ActionType.CLOSE);
						}
					}
				});
			}
		});
  }else{	
  	$('.main-box').hide();
	  alert('Add customer');
	  $('#CloudPrint').hide();
  }
  
  unsubscriber();
});
cart.dispatch(Cart.Action.FETCH);


$('body').on('click', '#CloudPrint', function(){
	var locid = $('#current_location_id').html();
	//alert(locid);
	var printdata = $('#cartDetails').html();
	$.ajax({
		type: "POST",
		data: "action=SubmitPrint&locationid="+locid+"&html="+printdata,
		success : function(response){
			if(response == 'success'){
				alert('File send to printer');
				const pos = Pos.create(app);
				pos.dispatch(Pos.ActionType.CLOSE);
			}
			//$('#container2').html(response);
		}
	});
});
</script>

</body>
</html>