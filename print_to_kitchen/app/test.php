<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GPC (Google Cloud Print) Web Element</title>
    <link rel="stylesheet" href="">
    <script src="https://www.google.com/cloudprint/client/cpgadget.js"></script>
</head>

<?php
include $_SERVER['DOCUMENT_ROOT'] . '/database.php';

date_default_timezone_set('Europe/London');

// Create connection
$conn = new mysqli($servername, $username, $password, $db);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if(isset($_POST['product_ids'])){
	$productides = explode(',',$_POST['product_ids']);

	$quantity = explode(',', $_POST['quantity']);
	
	$result = $conn->query('select * from print_to_kitchen_tag');
	$tags = array();
	if ($result->num_rows > 0) {
		$tags = $result->fetch_assoc();
	}
	$data = '';
	$data .= '<div id="responseData"><ul style="margin-top:20px; margin-bottom:20px; list-style:none;  padding-left:0px; line-height:2em;">';
	foreach($productides as $key => $pid){
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com/admin/api/2020-07/products/".$pid.".json",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"Cookie: _secure_admin_session_id_csrf=1bdc262f9441f84a05178767f7671da0; _secure_admin_session_id=1bdc262f9441f84a05178767f7671da0; _master_udr=eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaEpJaWt4TldVeE1HUXlPUzB3Tm1JM0xUUXdZVFF0T0dJeU1DMWlNalV5WVRGbVpEUTRNbVlHT2daRlJnPT0iLCJleHAiOiIyMDIyLTA2LTI2VDExOjEzOjAyLjExOFoiLCJwdXIiOiJjb29raWUuX21hc3Rlcl91ZHIifX0%3D--934e63c79b583c8364029a20482af8aba1172eac; _y=8bd56dde-cbc5-4330-a752-e9fa91d565a4; _shopify_y=8bd56dde-cbc5-4330-a752-e9fa91d565a4; __cfduid=d822e91ce72a52ecc2c57501abad650a01593167308"
			),
		));

		$response = curl_exec($curl);
	
		curl_close($curl);
		//echo '<pre>';
		if($tags){
			$tag = trim(strtolower($tags['tag']));
		}else{
			$tag = 'kitchen'; //default tag
		}
		
		$product = json_decode($response);
		
		$all_tags = trim(strtolower($product->product->tags));
		$ptags = explode(', ',$all_tags);
		if (in_array($tag,$ptags)) {
			$data .= '<li>'.$quantity[$key].'x '.$product->product->title.'</li>';
		}
		
	}
	$data .= '</ul></div>';

	echo $data;
	die;
}

?>

<body>
    <h1>GPC (Google Cloud Print) Web Element</h1>
    <h2>Documentation</h2>
    <p><a target="_blank" href="https://developers.google.com/cloud-print/docs/gadget">https://developers.google.com/cloud-print/docs/gadget</a></p>
    <h2>Example Buttons</h2>
    <p><a href="" id="manual-button">🖨&nbsp;&nbsp;Print</a></p>
    <p><div id="default-button"></div></p>
	
	<div class="itmeDetails"></div>
<?php 

?>	
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>
<script src="https://apis.google.com/js/api.js"></script>


<script>
var productids = '4507473248323,4507473805379';
var quantity = '2,1';
var customer_id = '23423432423423423';
$.ajax({
		type: "POST",
		data: "product_ids="+productids+'&quantity='+quantity,
		success : function(response){
			var products =  $(response).filter('#responseData').html();
			$(".itmeDetails").html(products);
	});
</script>	
</body>
</html>