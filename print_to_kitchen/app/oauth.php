<?php 

// Set variables for our request
$shop = "pat-val-premier";
$api_key = "79a26b6a5e5c211a0b40a35f104622fa";
$scopes = "read_orders,write_orders";
$redirect_uri = "https://matthews42.sg-host.com/print_to_kitchen/app/index.php";

// Build install/approval URL to redirect to
$install_url = "https://" . $shop . ".myshopify.com/admin/oauth/authorize?client_id=" . $api_key . "&scope=" . $scopes . "&redirect_uri=" . urlencode($redirect_uri);

// Redirect
header("Location: " . $install_url);
die();

?>