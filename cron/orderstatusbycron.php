stdClass Object
(
    [orders] => Array
        (
            [0] => stdClass Object
                (
                    [orderId] => 46461024
                    [orderNumber] => ORD0001026
                    [orderKey] => 2083497672771
                    [orderDate] => 2020-04-27T13:36:38.0000000
                    [createDate] => 2020-04-27T15:11:20.2100000
                    [modifyDate] => 2020-04-30T10:27:53.5030000
                    [paymentDate] => 2020-04-27T13:36:38.0000000
                    [shipByDate] => 2020-05-20T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 27410033
                    [customerUsername] => 2947434184771
                    [customerEmail] => racheljeffery@hotmail.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => Viv Jeffery
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Viv Jeffery
                            [company] => 
                            [street1] => 9 Stephensons Close
                            [street2] => 
                            [street3] => 
                            [city] => Alford
                            [state] => Lincolnshire
                            [postalCode] => LN13 0AJ
                            [country] => GB
                            [phone] => 01507 466823
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 75222032
                                    [lineItemKey] => 4551676788803
                                    [sku] => P00018S
                                    [name] => Black Forest Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/blackforest2020_1_33f87db8-b249-4cf8-bb16-c3b8fb85a4b8.jpg?v=1587287211
                                    [weight] => stdClass Object
                                        (
                                            [value] => 353
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6676602
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-04-27T15:11:20.15
                                    [modifyDate] => 2020-04-27T15:11:20.15
                                )

                        )

                    [orderTotal] => 37.6
                    [amountPaid] => 35.95
                    [taxAmount] => 1.65
                    [shippingAmount] => 9.94
                    [customerNotes] => <br/>Delivery Date: 20/05/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Week day delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 2020-04-30
                    [holdUntilDate] => 2020-05-19
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => my_other_account
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 209131
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [1] => stdClass Object
                (
                    [orderId] => 47529807
                    [orderNumber] => ORD0001043
                    [orderKey] => 2087290339395
                    [orderDate] => 2020-04-29T09:45:28.0000000
                    [createDate] => 2020-04-29T09:54:04.1300000
                    [modifyDate] => 2020-04-30T07:15:30.4670000
                    [paymentDate] => 2020-04-29T09:45:28.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 28126713
                    [customerUsername] => 2952109097027
                    [customerEmail] => lilianabreu10@hotmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Liliana  Abreu
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Liliana  Abreu
                            [company] => 
                            [street1] => Flat 606
                            [street2] => Pinnacle Tower
                            [street3] => 23 Fulton Road
                            [city] => Wembley
                            [state] => Middlesex
                            [postalCode] => HA9 0GB
                            [country] => GB
                            [phone] => 07479357760
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 76736439
                                    [lineItemKey] => 4558594801731
                                    [sku] => P00012S
                                    [name] => Carrot Cake - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Carrot-Cake.jpg?v=1588175920
                                    [weight] => stdClass Object
                                        (
                                            [value] => 353
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6489824
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-04-29T09:54:04.05
                                    [modifyDate] => 2020-04-29T09:54:04.05
                                )

                        )

                    [orderTotal] => 35.95
                    [amountPaid] => 35.95
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 20/05/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Week day delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-18
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => 580111
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [2] => stdClass Object
                (
                    [orderId] => 47711871
                    [orderNumber] => ORD0001070
                    [orderKey] => 2087527841859
                    [orderDate] => 2020-04-29T12:00:24.0000000
                    [createDate] => 2020-04-29T13:33:07.4200000
                    [modifyDate] => 2020-05-15T05:20:16.7100000
                    [paymentDate] => 2020-04-29T12:00:24.0000000
                    [shipByDate] => 2020-05-23T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 28211181
                    [customerUsername] => 2952380579907
                    [customerEmail] => meriam.gordon@yahoo.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => Maria Gordon
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Meriam Gordon
                            [company] => 
                            [street1] => 2 Long Close
                            [street2] => 
                            [street3] => 
                            [city] => Bristol
                            [state] => Gloucestershire
                            [postalCode] => BS16 2UE
                            [country] => GB
                            [phone] => +44 0771  539 9079
                            [residential] => 
                            [addressVerified] => Address not yet validated
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 91571097
                                    [lineItemKey] => 4559072165955
                                    [sku] => P00001S
                                    [name] => Spring Surprise Cake - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Pinata-Cake-NO-EGGS.png?v=1588924495
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 29.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6559585
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-15T05:20:16.71
                                    [modifyDate] => 2020-05-15T05:20:16.71
                                )

                        )

                    [orderTotal] => 42.9
                    [amountPaid] => 42.9
                    [taxAmount] => 2.16
                    [shippingAmount] => 12.95
                    [customerNotes] => <br/>Delivery Date: 23/05/2020<br/>Delivery Day: Saturday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Prem Sat
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_saturday
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 2020-05-23
                    [holdUntilDate] => 2020-05-22
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => shopify_draft_order
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [3] => stdClass Object
                (
                    [orderId] => 47788835
                    [orderNumber] => ORD0001102
                    [orderKey] => 2087819378755
                    [orderDate] => 2020-04-29T14:51:28.0000000
                    [createDate] => 2020-04-29T15:33:15.4270000
                    [modifyDate] => 2020-04-30T08:13:13.7600000
                    [paymentDate] => 2020-04-29T14:51:28.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 28253307
                    [customerUsername] => 2952731230275
                    [customerEmail] => carmelaborrotzu@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Carmela Borrotzu-Evans
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Carmela Borrotzu-Evans
                            [company] => 
                            [street1] => 37 Brighton Road
                            [street2] => 
                            [street3] => 
                            [city] => Horley
                            [state] => Surrey
                            [postalCode] => RH6 7HH
                            [country] => GB
                            [phone] => 07833911534
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 77115160
                                    [lineItemKey] => 4559643050051
                                    [sku] => P00011S
                                    [name] => Raspberry Fault Line Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Fault-Line-Cake.jpg?v=1588176011
                                    [weight] => stdClass Object
                                        (
                                            [value] => 353
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 38.5
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6682160
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-04-29T15:33:15.363
                                    [modifyDate] => 2020-04-29T15:33:15.363
                                )

                        )

                    [orderTotal] => 48.45
                    [amountPaid] => 48.45
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 29/05/2020<br/>Delivery Day: Friday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-27
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => shopify_draft_order
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [4] => stdClass Object
                (
                    [orderId] => 48039688
                    [orderNumber] => ORD0001126
                    [orderKey] => 2088497250371
                    [orderDate] => 2020-04-29T23:57:24.0000000
                    [createDate] => 2020-04-30T01:33:40.2200000
                    [modifyDate] => 2020-05-11T00:18:38.4830000
                    [paymentDate] => 2020-04-29T23:57:24.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 28425702
                    [customerUsername] => 2953456713795
                    [customerEmail] => bexy253@hotmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Rebecca Benson
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Rebecca Benson
                            [company] => 
                            [street1] => 9 Herdwick Close
                            [street2] => 
                            [street3] => 
                            [city] => Ashford
                            [state] => Kent
                            [postalCode] => TN25 7FH
                            [country] => GB
                            [phone] => 07949 783268
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 77504785
                                    [lineItemKey] => 4560960880707
                                    [sku] => P00003S
                                    [name] => Candy Stripe Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Pink-Candy-Stripe-Cake.jpg?v=1588175864
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 38.5
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6816436
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-04-30T01:33:40.157
                                    [modifyDate] => 2020-04-30T01:33:40.157
                                )

                        )

                    [orderTotal] => 51.45
                    [amountPaid] => 51.45
                    [taxAmount] => 2.16
                    [shippingAmount] => 12.95
                    [customerNotes] => <br/>Delivery Date: 30/05/2020<br/>Delivery Day: Saturday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Saturday delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_saturday
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 2020-05-11
                    [holdUntilDate] => 2020-05-28
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => 
                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => my_other_account
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 209131
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [5] => stdClass Object
                (
                    [orderId] => 48039685
                    [orderNumber] => ORD0001129
                    [orderKey] => 2088528511043
                    [orderDate] => 2020-04-30T00:42:49.0000000
                    [createDate] => 2020-04-30T01:33:38.9570000
                    [modifyDate] => 2020-04-30T07:15:30.4670000
                    [paymentDate] => 2020-04-30T00:42:49.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 28425695
                    [customerUsername] => 2953475391555
                    [customerEmail] => cokapple@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Jie Gu
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Jie Gu
                            [company] => 
                            [street1] => 23 Bankfield Drive
                            [street2] => 
                            [street3] => 
                            [city] => Nottingham
                            [state] => Nottinghamshire
                            [postalCode] => NG9 3EH
                            [country] => GB
                            [phone] => 07830 052229
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 77504781
                                    [lineItemKey] => 4561022255171
                                    [sku] => P00010S
                                    [name] => Raspberry Fault Line Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Fault-Line-Cake.jpg?v=1588176011
                                    [weight] => stdClass Object
                                        (
                                            [value] => 353
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6649612
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-04-30T01:33:38.893
                                    [modifyDate] => 2020-04-30T01:33:38.893
                                )

                        )

                    [orderTotal] => 41.9
                    [amountPaid] => 41.9
                    [taxAmount] => 2.16
                    [shippingAmount] => 12.95
                    [customerNotes] => <br/>Delivery Date: 23/05/2020<br/>Delivery Day: Saturday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => DPD - Pat Val Prem Sat
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_saturday
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-21
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [6] => stdClass Object
                (
                    [orderId] => 48054153
                    [orderNumber] => ORD0001145
                    [orderKey] => 2088602107971
                    [orderDate] => 2020-04-30T02:18:34.0000000
                    [createDate] => 2020-04-30T03:17:56.8900000
                    [modifyDate] => 2020-04-30T07:15:30.4670000
                    [paymentDate] => 2020-04-30T02:18:34.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 28436021
                    [customerUsername] => 2953581396035
                    [customerEmail] => gyin_80@yahoo.com
                    [billTo] => stdClass Object
                        (
                            [name] => Miss A Tan
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Miss A Tan
                            [company] => 
                            [street1] => 5 Linden Avenue
                            [street2] => 
                            [street3] => 
                            [city] => Ruislip
                            [state] => Middlesex
                            [postalCode] => HA4 8TW
                            [country] => GB
                            [phone] => 07925 711229
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 77534420
                                    [lineItemKey] => 4561168531523
                                    [sku] => P00019S
                                    [name] => Black Forest Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Black-Forest-Gateau.jpg?v=1588175780
                                    [weight] => stdClass Object
                                        (
                                            [value] => 353
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 35.5
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6816438
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-04-30T03:17:56.83
                                    [modifyDate] => 2020-04-30T03:17:56.83
                                )

                        )

                    [orderTotal] => 45.45
                    [amountPaid] => 45.45
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 22/05/2020<br/>Delivery Day: Friday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-20
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [7] => stdClass Object
                (
                    [orderId] => 48061977
                    [orderNumber] => ORD0001168
                    [orderKey] => 2088667906115
                    [orderDate] => 2020-04-30T03:36:06.0000000
                    [createDate] => 2020-04-30T04:05:02.8070000
                    [modifyDate] => 2020-04-30T07:15:30.4670000
                    [paymentDate] => 2020-04-30T03:36:06.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 28439546
                    [customerUsername] => 2953728491587
                    [customerEmail] => sandy.lovett13@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Sandy Lovett
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Sandy Lovett
                            [company] => 
                            [street1] => 22 Meadow Way
                            [street2] => 
                            [street3] => 
                            [city] => Durham
                            [state] => County Durham
                            [postalCode] => DH7 0QB
                            [country] => GB
                            [phone] => 07833195478
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 77544217
                                    [lineItemKey] => 4561297080387
                                    [sku] => P00006S
                                    [name] => Double Chocolate Dream Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Double-Choc-Dream-Cake.jpg?v=1588175958
                                    [weight] => stdClass Object
                                        (
                                            [value] => 353
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6759340
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-04-30T04:05:02.743
                                    [modifyDate] => 2020-04-30T04:05:02.743
                                )

                        )

                    [orderTotal] => 35.95
                    [amountPaid] => 35.95
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 20/05/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-18
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [8] => stdClass Object
                (
                    [orderId] => 48061955
                    [orderNumber] => ORD0001174
                    [orderKey] => 2088685240387
                    [orderDate] => 2020-04-30T03:55:19.0000000
                    [createDate] => 2020-04-30T04:04:59.9070000
                    [modifyDate] => 2020-04-30T08:14:47.2630000
                    [paymentDate] => 2020-04-30T03:55:19.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 28439544
                    [customerUsername] => 2953747234883
                    [customerEmail] => opengates@tiscali.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => Maria Morris
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Maria Morris
                            [company] => 
                            [street1] => 18 Friary Road
                            [street2] => 
                            [street3] => 
                            [city] => Staines-Upon-Thames
                            [state] => Windsor and Maidenhead
                            [postalCode] => TW19 5JP
                            [country] => GB
                            [phone] => +44 7814 574124
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 77544172
                                    [lineItemKey] => 4561328832579
                                    [sku] => P00005S
                                    [name] => Triple Chocolate Delight Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Triple-Choc-Gateau-1.jpg?v=1588176197
                                    [weight] => stdClass Object
                                        (
                                            [value] => 353
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 38.5
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6587725
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-04-30T04:04:59.843
                                    [modifyDate] => 2020-04-30T04:04:59.843
                                )

                        )

                    [orderTotal] => 48.45
                    [amountPaid] => 48.45
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 29/05/2020<br/>Delivery Day: Friday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Week day delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-27
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => d2fac49f-5009-4715-9132-528b8815a141
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [9] => stdClass Object
                (
                    [orderId] => 48074415
                    [orderNumber] => ORD0001176
                    [orderKey] => 2088703328323
                    [orderDate] => 2020-04-30T04:10:33.0000000
                    [createDate] => 2020-04-30T04:46:35.8470000
                    [modifyDate] => 2020-04-30T07:15:30.4670000
                    [paymentDate] => 2020-04-30T04:10:33.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 28443384
                    [customerUsername] => 2953763586115
                    [customerEmail] => jenni.chandler93@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Jenni Chandler
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Jenni Chandler
                            [company] => 
                            [street1] => 11 Vine Street
                            [street2] => 
                            [street3] => 
                            [city] => Galashiels
                            [state] => Scottish Borders
                            [postalCode] => TD1 3LU
                            [country] => GB
                            [phone] => +447792838261
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 77564779
                                    [lineItemKey] => 4561361272899
                                    [sku] => P00019S
                                    [name] => Black Forest Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Black-Forest-Gateau.jpg?v=1588175780
                                    [weight] => stdClass Object
                                        (
                                            [value] => 353
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 35.5
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6816438
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-04-30T04:46:35.8
                                    [modifyDate] => 2020-04-30T04:46:35.8
                                )

                        )

                    [orderTotal] => 45.45
                    [amountPaid] => 45.45
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 30/05/2020<br/>Delivery Day: Saturday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Week day delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-28
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => 580111
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [10] => stdClass Object
                (
                    [orderId] => 48077362
                    [orderNumber] => ORD0001186
                    [orderKey] => 2088750252099
                    [orderDate] => 2020-04-30T04:50:38.0000000
                    [createDate] => 2020-04-30T05:00:39.8800000
                    [modifyDate] => 2020-04-30T08:06:21.4200000
                    [paymentDate] => 2020-04-30T04:50:38.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 28444398
                    [customerUsername] => 2953806446659
                    [customerEmail] => tracey.dodds@hotmail.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => tracey Dodds
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Kathryn Laverick
                            [company] => 
                            [street1] => 37 Mill Lane
                            [street2] => 
                            [street3] => 
                            [city] => Durham
                            [state] => County Durham
                            [postalCode] => DH6 1EQ
                            [country] => GB
                            [phone] => 07979 357817
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 77569636
                                    [lineItemKey] => 4561451941955
                                    [sku] => P00002S
                                    [name] => Candy Stripe Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Pink-Candy-Stripe-Cake.jpg?v=1588175864
                                    [weight] => stdClass Object
                                        (
                                            [value] => 353
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6552798
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-04-30T05:00:39.833
                                    [modifyDate] => 2020-04-30T05:00:39.833
                                )

                        )

                    [orderTotal] => 38.9
                    [amountPaid] => 38.9
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 21/05/2020<br/>Delivery Day: Thursday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-19
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [11] => stdClass Object
                (
                    [orderId] => 48086023
                    [orderNumber] => ORD0001192
                    [orderKey] => 2088797831235
                    [orderDate] => 2020-04-30T05:30:12.0000000
                    [createDate] => 2020-04-30T05:33:19.5800000
                    [modifyDate] => 2020-04-30T07:15:30.4670000
                    [paymentDate] => 2020-04-30T05:30:12.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 28449529
                    [customerUsername] => 2953855631427
                    [customerEmail] => crllnc@netscape.net
                    [billTo] => stdClass Object
                        (
                            [name] => Carol Lancaster
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Jack Lancaster
                            [company] => 
                            [street1] => 6 The Macies
                            [street2] => 
                            [street3] => 
                            [city] => Bath
                            [state] => Somerset
                            [postalCode] => BA1 4HS
                            [country] => GB
                            [phone] => 07910 699608
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 77581237
                                    [lineItemKey] => 4561543200835
                                    [sku] => P00004S
                                    [name] => Triple Chocolate Delight Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Triple-Choc-Gateau-1.jpg?v=1588176197
                                    [weight] => stdClass Object
                                        (
                                            [value] => 353
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6677587
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-04-30T05:33:19.517
                                    [modifyDate] => 2020-04-30T05:33:19.517
                                )

                        )

                    [orderTotal] => 38.9
                    [amountPaid] => 38.9
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 22/05/2020<br/>Delivery Day: Friday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-20
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [12] => stdClass Object
                (
                    [orderId] => 48189955
                    [orderNumber] => ORD0001221
                    [orderKey] => 2089048047683
                    [orderDate] => 2020-04-30T08:06:42.0000000
                    [createDate] => 2020-04-30T09:12:52.7630000
                    [modifyDate] => 2020-04-30T09:12:59.7730000
                    [paymentDate] => 2020-04-30T08:06:42.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 28511428
                    [customerUsername] => 2954110828611
                    [customerEmail] => ttxqj1@nottingham.ac.uk
                    [billTo] => stdClass Object
                        (
                            [name] => Qiaona Jiang
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Xiaoxiang Sun
                            [company] => 
                            [street1] => Flat1, Room2, Hemlocks, Turnpike Lane Broadgate Park, Beeston
                            [street2] => Flat1, Room2, Hemlocks, Turnpike Lane Broadgate Park, Beeston
                            [street3] => 
                            [city] => Nottingham
                            [state] => 
                            [postalCode] => NG92US
                            [country] => GB
                            [phone] => 07724 259226
                            [residential] => 
                            [addressVerified] => Address validation failed
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 77744799
                                    [lineItemKey] => 4562033279043
                                    [sku] => P00004S
                                    [name] => Triple Chocolate Delight Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Triple-Choc-Gateau-1.jpg?v=1588176197
                                    [weight] => stdClass Object
                                        (
                                            [value] => 353
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6677587
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-04-30T09:12:52.657
                                    [modifyDate] => 2020-04-30T09:12:52.657
                                )

                        )

                    [orderTotal] => 38.9
                    [amountPaid] => 38.9
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 20/05/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-18
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [13] => stdClass Object
                (
                    [orderId] => 48189936
                    [orderNumber] => ORD0001226
                    [orderKey] => 2089110700099
                    [orderDate] => 2020-04-30T08:36:29.0000000
                    [createDate] => 2020-04-30T09:12:51.2030000
                    [modifyDate] => 2020-04-30T09:58:01.9430000
                    [paymentDate] => 2020-04-30T08:36:29.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 28511410
                    [customerUsername] => 2954180362307
                    [customerEmail] => christine-matthews@live.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => Chistine Matthews
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Jennifer Middleton
                            [company] => 
                            [street1] => 19 Pratt Road
                            [street2] => 
                            [street3] => 
                            [city] => Rushden
                            [state] => Northamptonshire
                            [postalCode] => NN10 0EQ
                            [country] => GB
                            [phone] => 07878 141575
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 77744768
                                    [lineItemKey] => 4562160975939
                                    [sku] => P00004S
                                    [name] => Triple Chocolate Delight Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Triple-Choc-Gateau-1.jpg?v=1588176197
                                    [weight] => stdClass Object
                                        (
                                            [value] => 353
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6677587
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-04-30T09:12:51.143
                                    [modifyDate] => 2020-04-30T09:12:51.143
                                )

                        )

                    [orderTotal] => 38.9
                    [amountPaid] => 38.9
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 29/05/2020<br/>Delivery Day: Friday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-27
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [14] => stdClass Object
                (
                    [orderId] => 48214120
                    [orderNumber] => ORD0001238
                    [orderKey] => 2089203531843
                    [orderDate] => 2020-04-30T09:14:08.0000000
                    [createDate] => 2020-04-30T09:50:47.2900000
                    [modifyDate] => 2020-04-30T13:50:52.0770000
                    [paymentDate] => 2020-04-30T09:14:08.0000000
                    [shipByDate] => 2020-05-27T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 28525737
                    [customerUsername] => 2954277060675
                    [customerEmail] => helen@glasshouseltd.com
                    [billTo] => stdClass Object
                        (
                            [name] => Sofia Creedon
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Sofia Creedon
                            [company] => 
                            [street1] => 65 Highwalls Avenue
                            [street2] => 
                            [street3] => 
                            [city] => Dinas Powys
                            [state] => Glamorgan
                            [postalCode] => CF64 4AQ
                            [country] => GB
                            [phone] => 07811 208208
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 77781924
                                    [lineItemKey] => 4562339201091
                                    [sku] => P00003S
                                    [name] => Candy Stripe Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Pink-Candy-Stripe-Cake.jpg?v=1588175864
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 38.5
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6816436
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-04-30T09:50:47.227
                                    [modifyDate] => 2020-04-30T09:50:47.227
                                )

                        )

                    [orderTotal] => 50.06
                    [amountPaid] => 48.45
                    [taxAmount] => 1.63
                    [shippingAmount] => 9.92
                    [customerNotes] => <br/>Delivery Date: 27/05/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-26
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [15] => stdClass Object
                (
                    [orderId] => 48231078
                    [orderNumber] => ORD0001248
                    [orderKey] => 2089311469635
                    [orderDate] => 2020-04-30T10:12:00.0000000
                    [createDate] => 2020-04-30T10:17:30.9500000
                    [modifyDate] => 2020-04-30T10:17:39.4400000
                    [paymentDate] => 2020-04-30T10:12:00.0000000
                    [shipByDate] => 2020-05-30T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 28535279
                    [customerUsername] => 2954389520451
                    [customerEmail] => lizzieshiers@msn.com
                    [billTo] => stdClass Object
                        (
                            [name] => Elizabeth Shiers
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Elizabeth Shiers
                            [company] => 
                            [street1] => 15 Mill Road
                            [street2] => 
                            [street3] => 
                            [city] => Chelmsford
                            [state] => Essex
                            [postalCode] => CM3 6EQ
                            [country] => GB
                            [phone] => 07914 095637
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 77805103
                                    [lineItemKey] => 4562554454083
                                    [sku] => P00008S
                                    [name] => Tiramisu Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Tiramisu-Cake.jpg?v=1588176158
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6820300
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-04-30T10:17:30.887
                                    [modifyDate] => 2020-04-30T10:17:30.887
                                )

                        )

                    [orderTotal] => 41.1
                    [amountPaid] => 38.95
                    [taxAmount] => 2.16
                    [shippingAmount] => 12.94
                    [customerNotes] => <br/>Delivery Date: 30/05/2020<br/>Delivery Day: Saturday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => DPD - Pat Val Prem Sat
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_saturday
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-29
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [16] => stdClass Object
                (
                    [orderId] => 48231068
                    [orderNumber] => ORD0001249
                    [orderKey] => 2089319759939
                    [orderDate] => 2020-04-30T10:16:30.0000000
                    [createDate] => 2020-04-30T10:17:30.4970000
                    [modifyDate] => 2020-04-30T10:21:14.1500000
                    [paymentDate] => 2020-04-30T10:16:30.0000000
                    [shipByDate] => 2020-05-21T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 28535281
                    [customerUsername] => 2954404233283
                    [customerEmail] => kapelanr@yahoo.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => Renata  Kapelan
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Renata  Kapelan 
                            [company] => 
                            [street1] => 55A Cardigan Lane
                            [street2] => 
                            [street3] => 
                            [city] => Leeds
                            [state] => Yorkshire
                            [postalCode] => LS4 2LE
                            [country] => GB
                            [phone] => 07850 479841
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 77805089
                                    [lineItemKey] => 4562571886659
                                    [sku] => P00004S
                                    [name] => Triple Chocolate Delight Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Triple-Choc-Gateau-1.jpg?v=1588176197
                                    [weight] => stdClass Object
                                        (
                                            [value] => 353
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.94
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6677587
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-04-30T10:17:30.123
                                    [modifyDate] => 2020-04-30T10:17:30.123
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 77805090
                                    [lineItemKey] => 4562571919427
                                    [sku] => P00020S
                                    [name] => Inscription
                                    [imageUrl] => 
                                    [weight] => stdClass Object
                                        (
                                            [value] => 0
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 3
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6760417
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-04-30T10:17:30.31
                                    [modifyDate] => 2020-04-30T10:17:30.31
                                )

                        )

                    [orderTotal] => 43.55
                    [amountPaid] => 41.89
                    [taxAmount] => 1.65
                    [shippingAmount] => 9.94
                    [customerNotes] => <br/>Delivery Date: 21/05/2020<br/>Delivery Day: Thursday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-19
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [17] => stdClass Object
                (
                    [orderId] => 48309217
                    [orderNumber] => ORD0001255
                    [orderKey] => 2089361014851
                    [orderDate] => 2020-04-30T10:38:11.0000000
                    [createDate] => 2020-04-30T12:18:26.5200000
                    [modifyDate] => 2020-04-30T16:18:44.6670000
                    [paymentDate] => 2020-04-30T10:38:11.0000000
                    [shipByDate] => 2020-05-29T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 28585638
                    [customerUsername] => 2954437296195
                    [customerEmail] => mo@moshouse.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => MRS M S A Wilson
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => MRS M S A Wilson
                            [company] => 
                            [street1] => 28 Chobham Road
                            [street2] => 
                            [street3] => 
                            [city] => Camberley
                            [state] => Surrey
                            [postalCode] => GU16 8PF
                            [country] => GB
                            [phone] => 07710 663551
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 77931615
                                    [lineItemKey] => 4562659082307
                                    [sku] => P00006S
                                    [name] => Double Chocolate Dream Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Double-Choc-Dream-Cake.jpg?v=1588175958
                                    [weight] => stdClass Object
                                        (
                                            [value] => 353
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6759340
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-04-30T12:18:26.39
                                    [modifyDate] => 2020-04-30T12:18:26.39
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 77931616
                                    [lineItemKey] => 4562659115075
                                    [sku] => P00010S
                                    [name] => Raspberry Fault Line Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Fault-Line-Cake.jpg?v=1588176011
                                    [weight] => stdClass Object
                                        (
                                            [value] => 353
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.94
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6649612
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-04-30T12:18:26.39
                                    [modifyDate] => 2020-04-30T12:18:26.39
                                )

                        )

                    [orderTotal] => 66.51
                    [amountPaid] => 64.9
                    [taxAmount] => 1.63
                    [shippingAmount] => 9.92
                    [customerNotes] => <br/>Delivery Date: 29/05/2020<br/>Delivery Day: Friday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-28
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [18] => stdClass Object
                (
                    [orderId] => 48309207
                    [orderNumber] => ORD0001257
                    [orderKey] => 2089364652099
                    [orderDate] => 2020-04-30T10:39:50.0000000
                    [createDate] => 2020-04-30T12:18:25.7030000
                    [modifyDate] => 2020-04-30T14:18:46.3270000
                    [paymentDate] => 2020-04-30T10:39:50.0000000
                    [shipByDate] => 2020-05-23T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 28585617
                    [customerUsername] => 2954447552579
                    [customerEmail] => kaunert_christian@hotmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Christian Kaunert
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Christian Kaunert
                            [company] => 
                            [street1] => 16 The Parade
                            [street2] => 
                            [street3] => 
                            [city] => Barry
                            [state] => Glamorgan
                            [postalCode] => CF62 6SE
                            [country] => GB
                            [phone] => 07467 485684
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 77931604
                                    [lineItemKey] => 4562666618947
                                    [sku] => P00008S
                                    [name] => Tiramisu Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Tiramisu-Cake.jpg?v=1588176158
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6820300
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-04-30T12:18:25.61
                                    [modifyDate] => 2020-04-30T12:18:25.61
                                )

                        )

                    [orderTotal] => 41.09
                    [amountPaid] => 38.95
                    [taxAmount] => 2.16
                    [shippingAmount] => 12.93
                    [customerNotes] => <br/>Delivery Date: 23/05/2020<br/>Delivery Day: Saturday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Prem Sat
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_saturday
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-22
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [19] => stdClass Object
                (
                    [orderId] => 48309140
                    [orderNumber] => ORD0001271
                    [orderKey] => 2089415737411
                    [orderDate] => 2020-04-30T11:06:21.0000000
                    [createDate] => 2020-04-30T12:18:21.3200000
                    [modifyDate] => 2020-05-01T00:02:13.8870000
                    [paymentDate] => 2020-04-30T11:06:21.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 28585648
                    [customerUsername] => 2954503520323
                    [customerEmail] => rute.figueira.lis@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Rute Figueira
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Rute Figueira
                            [company] => 
                            [street1] => 9 Rye Grass Way
                            [street2] => 
                            [street3] => 
                            [city] => Braintree
                            [state] => Essex
                            [postalCode] => CM7 1GL
                            [country] => GB
                            [phone] => 07983 008228
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 77931515
                                    [lineItemKey] => 4562775736387
                                    [sku] => P00015S
                                    [name] => Salted Caramel Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Salted-Caramel-Cake.jpg?v=1588176072
                                    [weight] => stdClass Object
                                        (
                                            [value] => 353
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 35.5
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6762096
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-04-30T12:18:21.243
                                    [modifyDate] => 2020-04-30T12:18:21.247
                                )

                        )

                    [orderTotal] => 45.45
                    [amountPaid] => 45.45
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 27/05/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-25
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => shopify_draft_order
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [20] => stdClass Object
                (
                    [orderId] => 48309130
                    [orderNumber] => ORD0001273
                    [orderKey] => 2089426485315
                    [orderDate] => 2020-04-30T11:11:41.0000000
                    [createDate] => 2020-04-30T12:18:20.6800000
                    [modifyDate] => 2020-05-01T00:01:44.9200000
                    [paymentDate] => 2020-04-30T11:11:41.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 28585618
                    [customerUsername] => 2954513743939
                    [customerEmail] => lisajmundy@yahoo.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => Clare Henry
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Clare Henry
                            [company] => 
                            [street1] => 33 Balfour Road
                            [street2] => 
                            [street3] => 
                            [city] => Gloucester
                            [state] => Gloucestershire
                            [postalCode] => GL1 5QH
                            [country] => GB
                            [phone] => 07857 692989
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 77931506
                                    [lineItemKey] => 4562796576835
                                    [sku] => P00002S
                                    [name] => Candy Stripe Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Pink-Candy-Stripe-Cake.jpg?v=1588175864
                                    [weight] => stdClass Object
                                        (
                                            [value] => 353
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6552798
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-04-30T12:18:20.573
                                    [modifyDate] => 2020-04-30T12:18:20.573
                                )

                        )

                    [orderTotal] => 38.9
                    [amountPaid] => 38.9
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 11/06/2020<br/>Delivery Day: Thursday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-06-09
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [21] => stdClass Object
                (
                    [orderId] => 48309100
                    [orderNumber] => ORD0001278
                    [orderKey] => 2089442836547
                    [orderDate] => 2020-04-30T11:19:58.0000000
                    [createDate] => 2020-04-30T12:18:19.0900000
                    [modifyDate] => 2020-05-01T00:01:14.6800000
                    [paymentDate] => 2020-04-30T11:19:58.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 28585623
                    [customerUsername] => 2954529243203
                    [customerEmail] => fatimah.afzalfab@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Fatimah Afzal
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Fatimah Afzal
                            [company] => 
                            [street1] => Apartment 127
                            [street2] => Daisy Spring Works
                            [street3] => 1 Dun Street
                            [city] => Sheffield
                            [state] => Yorkshire
                            [postalCode] => S3 8DR
                            [country] => GB
                            [phone] => 07495 161531
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 77931464
                                    [lineItemKey] => 4562830655555
                                    [sku] => P00018S
                                    [name] => Black Forest Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Black-Forest-Gateau.jpg?v=1588175780
                                    [weight] => stdClass Object
                                        (
                                            [value] => 353
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6676602
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-04-30T12:18:19.027
                                    [modifyDate] => 2020-04-30T12:18:19.027
                                )

                        )

                    [orderTotal] => 35.95
                    [amountPaid] => 35.95
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 21/05/2020<br/>Delivery Day: Thursday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-19
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => shopify_draft_order
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [22] => stdClass Object
                (
                    [orderId] => 48384503
                    [orderNumber] => ORD0001299
                    [orderKey] => 2089566175299
                    [orderDate] => 2020-04-30T12:23:32.0000000
                    [createDate] => 2020-04-30T14:18:46.8930000
                    [modifyDate] => 2020-04-30T18:18:57.7670000
                    [paymentDate] => 2020-04-30T12:23:32.0000000
                    [shipByDate] => 2020-05-20T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 28633694
                    [customerUsername] => 2954676174915
                    [customerEmail] => alisonvictoriarichardson@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Alison Richardson
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Angela Richardson
                            [company] => 
                            [street1] => Flat 5
                            [street2] => Beaulieu House
                            [street3] => Greyfriars Close
                            [city] => Salisbury
                            [state] => Wiltshire
                            [postalCode] => SP1 2HJ
                            [country] => GB
                            [phone] => 07456 050097
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 78041628
                                    [lineItemKey] => 4563079266371
                                    [sku] => P00003S
                                    [name] => Candy Stripe Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Pink-Candy-Stripe-Cake.jpg?v=1588175864
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 38.5
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6816436
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-04-30T14:18:46.813
                                    [modifyDate] => 2020-04-30T14:18:46.813
                                )

                        )

                    [orderTotal] => 50.06
                    [amountPaid] => 48.45
                    [taxAmount] => 1.63
                    [shippingAmount] => 9.92
                    [customerNotes] => <br/>Delivery Date: 20/05/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-19
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [23] => stdClass Object
                (
                    [orderId] => 48384474
                    [orderNumber] => ORD0001307
                    [orderKey] => 2089643769923
                    [orderDate] => 2020-04-30T12:50:23.0000000
                    [createDate] => 2020-04-30T14:18:44.4430000
                    [modifyDate] => 2020-05-01T00:04:03.1400000
                    [paymentDate] => 2020-04-30T12:50:23.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 28633718
                    [customerUsername] => 2954734698563
                    [customerEmail] => me@melissaadvani.com
                    [billTo] => stdClass Object
                        (
                            [name] => Melissa Advani
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Melissa Advani
                            [company] => 
                            [street1] => 8 Woodbank Drive
                            [street2] => 
                            [street3] => 
                            [city] => Chalfont Saint Giles
                            [state] => Buckinghamshire
                            [postalCode] => HP8 4RP
                            [country] => GB
                            [phone] => 07515429715
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 78041596
                                    [lineItemKey] => 4563217776707
                                    [sku] => P00001S
                                    [name] => Spring Surprise Cake - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Pinata-Cake-NO-EGGS.jpg?v=1588176645
                                    [weight] => stdClass Object
                                        (
                                            [value] => 353
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 29.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6559585
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-04-30T14:18:44.363
                                    [modifyDate] => 2020-04-30T14:18:44.363
                                )

                        )

                    [orderTotal] => 39.9
                    [amountPaid] => 39.9
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 04/06/2020<br/>Delivery Day: Thursday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Week day delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-06-02
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => 580111
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [24] => stdClass Object
                (
                    [orderId] => 48384448
                    [orderNumber] => ORD0001315
                    [orderKey] => 2089677750339
                    [orderDate] => 2020-04-30T13:08:09.0000000
                    [createDate] => 2020-04-30T14:18:41.7770000
                    [modifyDate] => 2020-05-01T00:03:27.3730000
                    [paymentDate] => 2020-04-30T13:08:09.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 28633723
                    [customerUsername] => 2954768023619
                    [customerEmail] => sagi_13@hotmail.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => Sagipa Vigneswaran
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Sagipa Vigneswaran
                            [company] => 
                            [street1] => 172 Malvern Avenue
                            [street2] => 
                            [street3] => 
                            [city] => Harrow
                            [state] => Middlesex
                            [postalCode] => HA2 9HD
                            [country] => GB
                            [phone] => 07947 794114
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 78041570
                                    [lineItemKey] => 4563286229059
                                    [sku] => P00006S
                                    [name] => Double Chocolate Dream Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Double-Choc-Dream-Cake.jpg?v=1588175958
                                    [weight] => stdClass Object
                                        (
                                            [value] => 353
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6759340
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-04-30T14:18:41.713
                                    [modifyDate] => 2020-04-30T14:18:41.713
                                )

                        )

                    [orderTotal] => 35.95
                    [amountPaid] => 35.95
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 02/06/2020<br/>Delivery Day: Tuesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-06-01
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [25] => stdClass Object
                (
                    [orderId] => 48453500
                    [orderNumber] => ORD0001351
                    [orderKey] => 2089989570627
                    [orderDate] => 2020-04-30T16:02:53.0000000
                    [createDate] => 2020-04-30T16:18:52.8770000
                    [modifyDate] => 2020-05-01T00:04:50.0570000
                    [paymentDate] => 2020-04-30T16:02:53.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 28678791
                    [customerUsername] => 2955097636931
                    [customerEmail] => dotcomhall@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Emily Francis
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Emily Francis
                            [company] => 
                            [street1] => 40 Beacon Drive
                            [street2] => 
                            [street3] => 
                            [city] => Newton Abbot
                            [state] => Devon
                            [postalCode] => TQ12 1GG
                            [country] => GB
                            [phone] => 07747 458627
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 78152968
                                    [lineItemKey] => 4563913113667
                                    [sku] => P00006S
                                    [name] => Double Chocolate Dream Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Double-Choc-Dream-Cake.jpg?v=1588175958
                                    [weight] => stdClass Object
                                        (
                                            [value] => 353
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6759340
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-04-30T16:18:52.813
                                    [modifyDate] => 2020-04-30T16:18:52.813
                                )

                        )

                    [orderTotal] => 38.95
                    [amountPaid] => 38.95
                    [taxAmount] => 2.16
                    [shippingAmount] => 12.95
                    [customerNotes] => <br/>Delivery Date: 23/05/2020<br/>Delivery Day: Saturday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Saturday delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_saturday
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-21
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [26] => stdClass Object
                (
                    [orderId] => 48685588
                    [orderNumber] => ORD0001371
                    [orderKey] => 2090835279939
                    [orderDate] => 2020-05-01T00:46:15.0000000
                    [createDate] => 2020-05-01T01:07:44.8470000
                    [modifyDate] => 2020-05-01T03:48:22.6400000
                    [paymentDate] => 2020-05-01T00:46:15.0000000
                    [shipByDate] => 2020-05-23T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 28848337
                    [customerUsername] => 2955911331907
                    [customerEmail] => jack.conway93@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Jack Conway
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Jack Tissington-Conway
                            [company] => 
                            [street1] => Old Rectory Mews House
                            [street2] => Scampton
                            [street3] => 
                            [city] => Lincoln
                            [state] => 
                            [postalCode] => LN1 2SE
                            [country] => GB
                            [phone] => 07525749317
                            [residential] => 
                            [addressVerified] => Address validation failed
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 78573272
                                    [lineItemKey] => 4565432631363
                                    [sku] => P00010S
                                    [name] => Raspberry Fault Line Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Fault-Line-Cake.jpg?v=1588176011
                                    [weight] => stdClass Object
                                        (
                                            [value] => 353
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6649612
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-01T03:48:22.64
                                    [modifyDate] => 2020-05-01T03:48:22.64
                                )

                        )

                    [orderTotal] => 38.9
                    [amountPaid] => 38.9
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 23/05/2020<br/>Delivery Day: Saturday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Week day delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-22
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => 580111
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [27] => stdClass Object
                (
                    [orderId] => 48722143
                    [orderNumber] => ORD0001377
                    [orderKey] => 2090913890371
                    [orderDate] => 2020-05-01T01:40:43.0000000
                    [createDate] => 2020-05-01T03:17:58.0330000
                    [modifyDate] => 2020-05-01T07:18:07.0530000
                    [paymentDate] => 2020-05-01T01:40:43.0000000
                    [shipByDate] => 2020-05-21T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 28858099
                    [customerUsername] => 2955992203331
                    [customerEmail] => charlotteshield@hotmail.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => Charlotte Shield
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Charlotte Shield
                            [company] => 
                            [street1] => 30 Edmonton Way
                            [street2] => 
                            [street3] => 
                            [city] => Liphook
                            [state] => Hampshire
                            [postalCode] => GU30 7TG
                            [country] => GB
                            [phone] => 07914 843752
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 78566323
                                    [lineItemKey] => 4565556396099
                                    [sku] => P00006S
                                    [name] => Double Chocolate Dream Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Double-Choc-Dream-Cake.jpg?v=1588175958
                                    [weight] => stdClass Object
                                        (
                                            [value] => 353
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6759340
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-01T03:17:57.97
                                    [modifyDate] => 2020-05-01T03:17:57.97
                                )

                        )

                    [orderTotal] => 37.56
                    [amountPaid] => 35.95
                    [taxAmount] => 1.63
                    [shippingAmount] => 9.92
                    [customerNotes] => <br/>Delivery Date: 21/05/2020<br/>Delivery Day: Thursday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-20
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [28] => stdClass Object
                (
                    [orderId] => 48722138
                    [orderNumber] => ORD0001381
                    [orderKey] => 2090941907011
                    [orderDate] => 2020-05-01T01:56:59.0000000
                    [createDate] => 2020-05-01T03:17:56.6770000
                    [modifyDate] => 2020-05-01T07:18:12.6070000
                    [paymentDate] => 2020-05-01T01:56:59.0000000
                    [shipByDate] => 2020-05-23T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 28858110
                    [customerUsername] => 2956009373763
                    [customerEmail] => madel.dinoia@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Maria Cruz
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Maria Cruz
                            [company] => 
                            [street1] => 20 Wylam Street
                            [street2] => 
                            [street3] => 
                            [city] => Durham
                            [state] => County Durham
                            [postalCode] => DH6 5BD
                            [country] => GB
                            [phone] => 07914 206269
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 78566318
                                    [lineItemKey] => 4565599092803
                                    [sku] => P00003S
                                    [name] => Candy Stripe Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Pink-Candy-Stripe-Cake.jpg?v=1588175864
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 38.5
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6816436
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-01T03:17:56.63
                                    [modifyDate] => 2020-05-01T03:17:56.63
                                )

                        )

                    [orderTotal] => 53.59
                    [amountPaid] => 51.45
                    [taxAmount] => 2.16
                    [shippingAmount] => 12.93
                    [customerNotes] => <br/>Delivery Date: 23/05/2020<br/>Delivery Day: Saturday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Prem Sat
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_saturday
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-22
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [29] => stdClass Object
                (
                    [orderId] => 48727728
                    [orderNumber] => ORD0001397
                    [orderKey] => 2091090673731
                    [orderDate] => 2020-05-01T03:31:14.0000000
                    [createDate] => 2020-05-01T03:48:21.3930000
                    [modifyDate] => 2020-05-01T07:48:28.8330000
                    [paymentDate] => 2020-05-01T03:31:14.0000000
                    [shipByDate] => 2020-05-22T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 28861321
                    [customerUsername] => 2956116262979
                    [customerEmail] => fustina.mirza@hotmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Fustina Mirza
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Ateek  Mirza
                            [company] => 
                            [street1] => 210 Green Lane
                            [street2] => 
                            [street3] => 
                            [city] => London
                            [state] => Surrey
                            [postalCode] => SW16 3BL
                            [country] => GB
                            [phone] => 07837 197229
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 78573265
                                    [lineItemKey] => 4565849309251
                                    [sku] => P00010S
                                    [name] => Raspberry Fault Line Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Fault-Line-Cake.jpg?v=1588176011
                                    [weight] => stdClass Object
                                        (
                                            [value] => 353
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.94
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6649612
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-01T03:48:21.33
                                    [modifyDate] => 2020-05-01T03:48:21.33
                                )

                        )

                    [orderTotal] => 40.51
                    [amountPaid] => 38.89
                    [taxAmount] => 1.63
                    [shippingAmount] => 9.92
                    [customerNotes] => <br/>Delivery Date: 22/05/2020<br/>Delivery Day: Friday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-21
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [30] => stdClass Object
                (
                    [orderId] => 48727720
                    [orderNumber] => ORD0001401
                    [orderKey] => 2091100504131
                    [orderDate] => 2020-05-01T03:38:51.0000000
                    [createDate] => 2020-05-01T03:48:20.3470000
                    [modifyDate] => 2020-05-01T07:48:33.8900000
                    [paymentDate] => 2020-05-01T03:38:51.0000000
                    [shipByDate] => 2020-05-30T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 28861323
                    [customerUsername] => 2956128157763
                    [customerEmail] => debbie.cozens@btinternet.com
                    [billTo] => stdClass Object
                        (
                            [name] => Debbie Cozens
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Debbie Cozens
                            [company] => 
                            [street1] => Flat 2
                            [street2] => 8 Prittlewell Square
                            [street3] => 
                            [city] => Southend-On-Sea
                            [state] => Essex
                            [postalCode] => SS1 1DW
                            [country] => GB
                            [phone] => 07815 153617
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 78573254
                                    [lineItemKey] => 4565867036739
                                    [sku] => P00004S
                                    [name] => Triple Chocolate Delight Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Triple-Choc-Gateau-1.jpg?v=1588176197
                                    [weight] => stdClass Object
                                        (
                                            [value] => 353
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.94
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6677587
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-01T03:48:20.287
                                    [modifyDate] => 2020-05-01T03:48:20.287
                                )

                        )

                    [orderTotal] => 44.03
                    [amountPaid] => 41.89
                    [taxAmount] => 2.16
                    [shippingAmount] => 12.92
                    [customerNotes] => <br/>Delivery Date: 30/05/2020<br/>Delivery Day: Saturday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Prem Sat
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_saturday
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-29
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [31] => stdClass Object
                (
                    [orderId] => 48732326
                    [orderNumber] => ORD0001407
                    [orderKey] => 2091118067779
                    [orderDate] => 2020-05-01T03:51:41.0000000
                    [createDate] => 2020-05-01T04:19:29.5330000
                    [modifyDate] => 2020-05-01T06:19:34.4430000
                    [paymentDate] => 2020-05-01T03:51:41.0000000
                    [shipByDate] => 2020-05-20T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 28863794
                    [customerUsername] => 2956141985859
                    [customerEmail] => verityjohnson68@yahoo.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => Verity Johnson
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Verity Johnson
                            [company] => 
                            [street1] => 72 Bransby Way
                            [street2] => 
                            [street3] => 
                            [city] => Weston-Super-Mare
                            [state] => Somerset
                            [postalCode] => BS24 7BW
                            [country] => GB
                            [phone] => 07833394663
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 78581149
                                    [lineItemKey] => 4565897609283
                                    [sku] => P00002S
                                    [name] => Candy Stripe Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Pink-Candy-Stripe-Cake.jpg?v=1588175864
                                    [weight] => stdClass Object
                                        (
                                            [value] => 353
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.94
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6552798
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-01T04:19:29.47
                                    [modifyDate] => 2020-05-01T04:19:29.47
                                )

                        )

                    [orderTotal] => 40.53
                    [amountPaid] => 38.89
                    [taxAmount] => 1.64
                    [shippingAmount] => 9.93
                    [customerNotes] => <br/>Delivery Date: 20/05/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Week day delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-19
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [32] => stdClass Object
                (
                    [orderId] => 48760862
                    [orderNumber] => ORD0001417
                    [orderKey] => 2091166269507
                    [orderDate] => 2020-05-01T04:32:40.0000000
                    [createDate] => 2020-05-01T06:07:47.5670000
                    [modifyDate] => 2020-05-01T10:07:52.8700000
                    [paymentDate] => 2020-05-01T04:32:40.0000000
                    [shipByDate] => 2020-05-23T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 28881211
                    [customerUsername] => 2956184977475
                    [customerEmail] => jadegough4@msn.com
                    [billTo] => stdClass Object
                        (
                            [name] => Jade Gough
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Jade Gough
                            [company] => 
                            [street1] => 85 Longfield Road
                            [street2] => 
                            [street3] => 
                            [city] => Dover
                            [state] => Kent
                            [postalCode] => CT17 9QR
                            [country] => GB
                            [phone] => 07523 365666
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 78636297
                                    [lineItemKey] => 4565984510019
                                    [sku] => P00002S
                                    [name] => Candy Stripe Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Pink-Candy-Stripe-Cake.jpg?v=1588175864
                                    [weight] => stdClass Object
                                        (
                                            [value] => 353
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.94
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6552798
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-01T06:07:47.487
                                    [modifyDate] => 2020-05-01T06:07:47.487
                                )

                        )

                    [orderTotal] => 44.03
                    [amountPaid] => 41.89
                    [taxAmount] => 2.16
                    [shippingAmount] => 12.92
                    [customerNotes] => <br/>Delivery Date: 23/05/2020<br/>Delivery Day: Saturday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Saturday delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_saturday
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-22
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [33] => stdClass Object
                (
                    [orderId] => 48760842
                    [orderNumber] => ORD0001426
                    [orderKey] => 2091227021379
                    [orderDate] => 2020-05-01T05:08:15.0000000
                    [createDate] => 2020-05-01T06:07:44.7100000
                    [modifyDate] => 2020-05-01T06:15:44.4170000
                    [paymentDate] => 2020-05-01T05:08:15.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 28881206
                    [customerUsername] => 2956242812995
                    [customerEmail] => rebeccacrago@hotmail.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => Becky Crago
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Becky Crago
                            [company] => 
                            [street1] => 16 Lindby Road
                            [street2] => 
                            [street3] => 
                            [city] => Liverpool
                            [state] => Lancashire
                            [postalCode] => L32 6SJ
                            [country] => GB
                            [phone] => 07740 264375
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 78636275
                                    [lineItemKey] => 4566089072707
                                    [sku] => P00003S
                                    [name] => Candy Stripe Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Pink-Candy-Stripe-Cake.jpg?v=1588175864
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 38.5
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6816436
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-01T06:07:44.65
                                    [modifyDate] => 2020-05-01T06:07:44.65
                                )

                        )

                    [orderTotal] => 48.45
                    [amountPaid] => 48.45
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 28/05/2020<br/>Delivery Day: Thursday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Week day delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-26
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [34] => stdClass Object
                (
                    [orderId] => 48811222
                    [orderNumber] => ORD0001442
                    [orderKey] => 2091424284739
                    [orderDate] => 2020-05-01T07:00:44.0000000
                    [createDate] => 2020-05-01T07:44:19.7130000
                    [modifyDate] => 2020-05-01T11:44:36.5300000
                    [paymentDate] => 2020-05-01T07:00:44.0000000
                    [shipByDate] => 2020-05-23T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 28905733
                    [customerUsername] => 2956430606403
                    [customerEmail] => traceycross1@aol.com
                    [billTo] => stdClass Object
                        (
                            [name] => Tracey Cross
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Tracey Cross
                            [company] => 
                            [street1] => 2 Walled Garden
                            [street2] => Woodfold Park
                            [street3] => 
                            [city] => Blackburn
                            [state] => Lancashire
                            [postalCode] => BB2 7QA
                            [country] => GB
                            [phone] => 07973240244
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 78709463
                                    [lineItemKey] => 4566471344195
                                    [sku] => P00005S
                                    [name] => Triple Chocolate Delight Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Triple-Choc-Gateau-1.jpg?v=1588176197
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 38.5
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6587725
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-01T07:44:19.65
                                    [modifyDate] => 2020-05-01T07:44:19.65
                                )

                        )

                    [orderTotal] => 50.06
                    [amountPaid] => 48.45
                    [taxAmount] => 1.63
                    [shippingAmount] => 9.92
                    [customerNotes] => <br/>Delivery Date: 23/05/2020<br/>Delivery Day: Saturday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Week day delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-22
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => 580111
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [35] => stdClass Object
                (
                    [orderId] => 48811218
                    [orderNumber] => ORD0001444
                    [orderKey] => 2091454365763
                    [orderDate] => 2020-05-01T07:16:42.0000000
                    [createDate] => 2020-05-01T07:44:19.1970000
                    [modifyDate] => 2020-05-04T00:46:41.1430000
                    [paymentDate] => 2020-05-01T07:16:42.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 28905726
                    [customerUsername] => 2956459999299
                    [customerEmail] => monicafioresicoach@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Monica  Fioresi
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Monica  Fioresi
                            [company] => 
                            [street1] => Flat 710
                            [street2] => Cradford House North
                            [street3] => 45 Palmers Road
                            [city] => London
                            [state] => Middlesex
                            [postalCode] => E2 0DF
                            [country] => GB
                            [phone] => 07563 377554
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 78709457
                                    [lineItemKey] => 4566533931075
                                    [sku] => P00010S
                                    [name] => Raspberry Fault Line Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Fault-Line-Cake.jpg?v=1588176011
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6649612
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-01T07:44:19.15
                                    [modifyDate] => 2020-05-01T07:44:19.15
                                )

                        )

                    [orderTotal] => 38.9
                    [amountPaid] => 38.9
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 27/05/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-25
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [36] => stdClass Object
                (
                    [orderId] => 48811213
                    [orderNumber] => ORD0001446
                    [orderKey] => 2091472027715
                    [orderDate] => 2020-05-01T07:26:52.0000000
                    [createDate] => 2020-05-01T07:44:18.6530000
                    [modifyDate] => 2020-05-01T07:44:24.2800000
                    [paymentDate] => 2020-05-01T07:26:52.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 28905722
                    [customerUsername] => 2956478218307
                    [customerEmail] => tattiekat@hotmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Katy Wood
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Katy Wood
                            [company] => 
                            [street1] => 20 Framfield Road
                            [street2] => 
                            [street3] => 
                            [city] => Uckfield
                            [state] => Sussex
                            [postalCode] => TN22 5AG
                            [country] => GB
                            [phone] => 07305 188239
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 78709442
                                    [lineItemKey] => 4566567125059
                                    [sku] => P00005S
                                    [name] => Triple Chocolate Delight Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Triple-Choc-Gateau-1.jpg?v=1588176197
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 38.5
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6587725
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-01T07:44:18.573
                                    [modifyDate] => 2020-05-01T07:44:18.573
                                )

                        )

                    [orderTotal] => 48.45
                    [amountPaid] => 48.45
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 22/05/2020<br/>Delivery Day: Friday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Week day delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-20
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [37] => stdClass Object
                (
                    [orderId] => 48889698
                    [orderNumber] => ORD0001455
                    [orderKey] => 2091606933571
                    [orderDate] => 2020-05-01T08:33:17.0000000
                    [createDate] => 2020-05-01T09:44:31.6030000
                    [modifyDate] => 2020-05-17T10:15:03.0000000
                    [paymentDate] => 2020-05-01T08:33:17.0000000
                    [shipByDate] => 2020-05-22T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 28952687
                    [customerUsername] => 2954549231683
                    [customerEmail] => lzvaginyte@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Lina  Zvaginyte
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Lina  Zvaginyte
                            [company] => 
                            [street1] => 88 Thornsbeach Road
                            [street2] => 
                            [street3] => 
                            [city] => London
                            [state] => Kent
                            [postalCode] => SE6 1HD
                            [country] => GB
                            [phone] => 07872311989
                            [residential] => 
                            [addressVerified] => Address not yet validated
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 93195024
                                    [lineItemKey] => 4566838575171
                                    [sku] => P00001S
                                    [name] => Spring Surprise Cake - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Pinata-Cake-NO-EGGS.png?v=1588924495
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 29.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6559585
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-17T10:15:03
                                    [modifyDate] => 2020-05-17T10:15:03
                                )

                        )

                    [orderTotal] => 39.9
                    [amountPaid] => 39.9
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 22/05/2020<br/>Delivery Day: Friday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Week day delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 2020-05-22
                    [holdUntilDate] => 2020-05-21
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => 580111
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [38] => stdClass Object
                (
                    [orderId] => 48889648
                    [orderNumber] => ORD0001465
                    [orderKey] => 2091746099267
                    [orderDate] => 2020-05-01T09:26:26.0000000
                    [createDate] => 2020-05-01T09:44:28.1400000
                    [modifyDate] => 2020-05-01T09:44:35.3100000
                    [paymentDate] => 2020-05-01T09:26:26.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 28952692
                    [customerUsername] => 2956723126339
                    [customerEmail] => 09raquelsvieira@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => S R VIEIRA DE ABREU
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => S R VIEIRA DE ABREU
                            [company] => 
                            [street1] => Dryden Way
                            [street2] => 
                            [street3] => 
                            [city] => Corby
                            [state] => 
                            [postalCode] => NN17 2NX
                            [country] => GB
                            [phone] => +44 7736 812875
                            [residential] => 
                            [addressVerified] => Address validation failed
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 78833530
                                    [lineItemKey] => 4567111925827
                                    [sku] => P00009S
                                    [name] => Tiramisu Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Tiramisu-Cake.jpg?v=1588176158
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 35.5
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6816437
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-01T09:44:28.08
                                    [modifyDate] => 2020-05-01T09:44:28.08
                                )

                        )

                    [orderTotal] => 45.45
                    [amountPaid] => 45.45
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 27/05/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-25
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [39] => stdClass Object
                (
                    [orderId] => 48969652
                    [orderNumber] => ORD0001474
                    [orderKey] => 2091839914051
                    [orderDate] => 2020-05-01T10:02:04.0000000
                    [createDate] => 2020-05-01T11:44:42.9500000
                    [modifyDate] => 2020-05-01T15:44:52.4400000
                    [paymentDate] => 2020-05-01T10:02:04.0000000
                    [shipByDate] => 2020-06-12T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 28998479
                    [customerUsername] => 2956801835075
                    [customerEmail] => vita.davies@sky.com
                    [billTo] => stdClass Object
                        (
                            [name] => Philip Davies
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Philip Davies
                            [company] => 
                            [street1] => 9 Sandon Close
                            [street2] => 
                            [street3] => 
                            [city] => Esher
                            [state] => Surrey
                            [postalCode] => KT10 8JE
                            [country] => GB
                            [phone] => 07803 407571
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 78957078
                                    [lineItemKey] => 4567292870723
                                    [sku] => P00002S
                                    [name] => Candy Stripe Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Pink-Candy-Stripe-Cake.jpg?v=1588175864
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.94
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6552798
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-01T11:44:42.86
                                    [modifyDate] => 2020-05-01T11:44:42.86
                                )

                        )

                    [orderTotal] => 40.53
                    [amountPaid] => 38.89
                    [taxAmount] => 1.64
                    [shippingAmount] => 9.93
                    [customerNotes] => <br/>Delivery Date: 12/06/2020<br/>Delivery Day: Friday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-06-11
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [40] => stdClass Object
                (
                    [orderId] => 49085705
                    [orderNumber] => ORD0001498
                    [orderKey] => 2092311674947
                    [orderDate] => 2020-05-01T13:28:55.0000000
                    [createDate] => 2020-05-01T14:54:10.6970000
                    [modifyDate] => 2020-05-01T18:54:23.2530000
                    [paymentDate] => 2020-05-01T13:28:55.0000000
                    [shipByDate] => 2020-05-22T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 29075164
                    [customerUsername] => 2957281984579
                    [customerEmail] => traceyslaven234@outlook.com
                    [billTo] => stdClass Object
                        (
                            [name] => Tracey Slaven
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Tracey Slaven
                            [company] => 
                            [street1] => 2/1 150 Wilton Street
                            [street2] => 
                            [street3] => 
                            [city] => Glasgow
                            [state] => Lanarkshire
                            [postalCode] => G20 6DG
                            [country] => GB
                            [phone] => 0141 321 6137
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 79153319
                                    [lineItemKey] => 4568214929475
                                    [sku] => P00004S
                                    [name] => Triple Chocolate Delight Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Triple-Choc-Gateau-1.jpg?v=1588176197
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.94
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6677587
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-01T14:54:10.633
                                    [modifyDate] => 2020-05-01T14:54:10.633
                                )

                        )

                    [orderTotal] => 40.51
                    [amountPaid] => 38.89
                    [taxAmount] => 1.63
                    [shippingAmount] => 9.92
                    [customerNotes] => <br/>Delivery Date: 22/05/2020<br/>Delivery Day: Friday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-21
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [41] => stdClass Object
                (
                    [orderId] => 49085687
                    [orderNumber] => ORD0001501
                    [orderKey] => 2092328747075
                    [orderDate] => 2020-05-01T13:37:17.0000000
                    [createDate] => 2020-05-01T14:54:09.6500000
                    [modifyDate] => 2020-05-01T14:54:14.0900000
                    [paymentDate] => 2020-05-01T13:37:17.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 29075156
                    [customerUsername] => 2957275955267
                    [customerEmail] => simplydevine@aol.com
                    [billTo] => stdClass Object
                        (
                            [name] => June Devine
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Lara Moles
                            [company] => 
                            [street1] => 21 Blackhill Brae
                            [street2] => 
                            [street3] => 
                            [city] => Cowdenbeath
                            [state] => Fife
                            [postalCode] => KY4 8FH
                            [country] => GB
                            [phone] => 01383 510830
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 79153290
                                    [lineItemKey] => 4568249892931
                                    [sku] => P00004S
                                    [name] => Triple Chocolate Delight Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Triple-Choc-Gateau-1.jpg?v=1588176197
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6677587
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-01T14:54:09.587
                                    [modifyDate] => 2020-05-01T14:54:09.587
                                )

                        )

                    [orderTotal] => 41.9
                    [amountPaid] => 41.9
                    [taxAmount] => 2.16
                    [shippingAmount] => 12.95
                    [customerNotes] => <br/>Delivery Date: 23/05/2020<br/>Delivery Day: Saturday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Prem Sat
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_saturday
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-21
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [42] => stdClass Object
                (
                    [orderId] => 49139089
                    [orderNumber] => ORD0001506
                    [orderKey] => 2092588531779
                    [orderDate] => 2020-05-01T15:15:13.0000000
                    [createDate] => 2020-05-01T16:54:18.4300000
                    [modifyDate] => 2020-05-01T20:54:24.3500000
                    [paymentDate] => 2020-05-01T15:15:13.0000000
                    [shipByDate] => 2020-06-12T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 29112359
                    [customerUsername] => 2957479739459
                    [customerEmail] => ameliadrayton@aol.com
                    [billTo] => stdClass Object
                        (
                            [name] => Amelia Flynn
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Amelia Flynn
                            [company] => 
                            [street1] => Bay Trees
                            [street2] => Herne Lane
                            [street3] => 
                            [city] => Littlehampton
                            [state] => West Sussex
                            [postalCode] => BN16 3EE
                            [country] => GB
                            [phone] => 07990 515536
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 79240984
                                    [lineItemKey] => 4568697045059
                                    [sku] => P00018S
                                    [name] => Black Forest Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Black-Forest-Gateau.jpg?v=1588175780
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6676602
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-01T16:54:18.35
                                    [modifyDate] => 2020-05-01T16:54:18.35
                                )

                        )

                    [orderTotal] => 37.56
                    [amountPaid] => 35.95
                    [taxAmount] => 1.63
                    [shippingAmount] => 9.92
                    [customerNotes] => <br/>Delivery Date: 12/06/2020<br/>Delivery Day: Friday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Week day delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-06-11
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [43] => stdClass Object
                (
                    [orderId] => 49139082
                    [orderNumber] => ORD0001508
                    [orderKey] => 2092605112387
                    [orderDate] => 2020-05-01T15:21:40.0000000
                    [createDate] => 2020-05-01T16:54:17.5400000
                    [modifyDate] => 2020-05-01T20:54:26.9370000
                    [paymentDate] => 2020-05-01T15:21:40.0000000
                    [shipByDate] => 2020-06-06T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 29112364
                    [customerUsername] => 2957490683971
                    [customerEmail] => kevinfeaver1@yahoo.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => Kevin Feaver
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Kevin Feaver
                            [company] => 
                            [street1] => Bryn Awelon
                            [street2] => Bottom Road
                            [street3] => 
                            [city] => Wrexham
                            [state] => Wrexham
                            [postalCode] => LL11 4TR
                            [country] => GB
                            [phone] => 07585770671
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 79240974
                                    [lineItemKey] => 4568726175811
                                    [sku] => P00005S
                                    [name] => Triple Chocolate Delight Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Triple-Choc-Gateau-1.jpg?v=1588176197
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 38.5
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6587725
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-01T16:54:17.477
                                    [modifyDate] => 2020-05-01T16:54:17.477
                                )

                        )

                    [orderTotal] => 50.06
                    [amountPaid] => 48.45
                    [taxAmount] => 1.63
                    [shippingAmount] => 9.92
                    [customerNotes] => <br/>Delivery Date: 06/06/2020<br/>Delivery Day: Saturday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Week day delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-06-05
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => 580111
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [44] => stdClass Object
                (
                    [orderId] => 49139065
                    [orderNumber] => ORD0001512
                    [orderKey] => 2092773998659
                    [orderDate] => 2020-05-01T16:42:38.0000000
                    [createDate] => 2020-05-01T16:54:16.0430000
                    [modifyDate] => 2020-05-01T20:55:02.4600000
                    [paymentDate] => 2020-05-01T16:42:38.0000000
                    [shipByDate] => 2020-05-30T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 29112362
                    [customerUsername] => 2957621559363
                    [customerEmail] => iqraahmed808@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Iqra  Ahmed
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Iqra  Ahmed 
                            [company] => 
                            [street1] => 23 Compton Avenue
                            [street2] => 
                            [street3] => 
                            [city] => Luton
                            [state] => Bedfordshire
                            [postalCode] => LU4 9AX
                            [country] => GB
                            [phone] => 07366 708088
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 79240951
                                    [lineItemKey] => 4569023807555
                                    [sku] => P00010S
                                    [name] => Raspberry Fault Line Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Fault-Line-Cake.jpg?v=1588176011
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.94
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6649612
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-01T16:54:15.98
                                    [modifyDate] => 2020-05-01T16:54:15.98
                                )

                        )

                    [orderTotal] => 44.05
                    [amountPaid] => 41.89
                    [taxAmount] => 2.16
                    [shippingAmount] => 12.94
                    [customerNotes] => <br/>Delivery Date: 30/05/2020<br/>Delivery Day: Saturday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Prem Sat
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_saturday
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-29
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [45] => stdClass Object
                (
                    [orderId] => 49284866
                    [orderNumber] => ORD0001522
                    [orderKey] => 2093306871875
                    [orderDate] => 2020-05-02T00:01:52.0000000
                    [createDate] => 2020-05-02T00:54:31.5900000
                    [modifyDate] => 2020-05-02T02:54:35.1070000
                    [paymentDate] => 2020-05-02T00:01:52.0000000
                    [shipByDate] => 2020-05-20T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 29216472
                    [customerUsername] => 2958198243395
                    [customerEmail] => anita_mahabale@yahoo.com
                    [billTo] => stdClass Object
                        (
                            [name] => Anita Mahabale
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Anita Mahabale
                            [company] => 
                            [street1] => 37 Walkwood Avenue
                            [street2] => 
                            [street3] => 
                            [city] => Bournemouth
                            [state] => Hampshire
                            [postalCode] => BH7 7HG
                            [country] => GB
                            [phone] => 7939573419
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 79481472
                                    [lineItemKey] => 4570078543939
                                    [sku] => P00018S
                                    [name] => Black Forest Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Black-Forest-Gateau.jpg?v=1588175780
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6676602
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-02T00:54:31.527
                                    [modifyDate] => 2020-05-02T00:54:31.527
                                )

                        )

                    [orderTotal] => 37.58
                    [amountPaid] => 35.95
                    [taxAmount] => 1.64
                    [shippingAmount] => 9.93
                    [customerNotes] => <br/>Delivery Date: 20/05/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-19
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [46] => stdClass Object
                (
                    [orderId] => 49311916
                    [orderNumber] => ORD0001542
                    [orderKey] => 2093582483523
                    [orderDate] => 2020-05-02T03:03:55.0000000
                    [createDate] => 2020-05-02T04:54:49.1570000
                    [modifyDate] => 2020-05-02T08:54:53.4500000
                    [paymentDate] => 2020-05-02T03:03:55.0000000
                    [shipByDate] => 2020-05-29T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 29234638
                    [customerUsername] => 2958361362499
                    [customerEmail] => Samairb@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => samantha wright
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => samantha wright
                            [company] => 
                            [street1] => 17 Gower Croft
                            [street2] => 
                            [street3] => 
                            [city] => Oldbury
                            [state] => Staffordshire
                            [postalCode] => B69 2GH
                            [country] => GB
                            [phone] => 07525 057221
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 79523227
                                    [lineItemKey] => 4570480869443
                                    [sku] => P00018S
                                    [name] => Black Forest Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Black-Forest-Gateau.jpg?v=1588175780
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6676602
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-02T04:54:49.08
                                    [modifyDate] => 2020-05-02T04:54:49.08
                                )

                        )

                    [orderTotal] => 37.56
                    [amountPaid] => 35.95
                    [taxAmount] => 1.63
                    [shippingAmount] => 9.92
                    [customerNotes] => <br/>Delivery Date: 29/05/2020<br/>Delivery Day: Friday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Week day delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-28
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [47] => stdClass Object
                (
                    [orderId] => 49311912
                    [orderNumber] => ORD0001545
                    [orderKey] => 2093619183683
                    [orderDate] => 2020-05-02T03:27:44.0000000
                    [createDate] => 2020-05-02T04:54:48.3770000
                    [modifyDate] => 2020-05-02T08:54:56.3870000
                    [paymentDate] => 2020-05-02T03:27:44.0000000
                    [shipByDate] => 2020-05-21T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 29234633
                    [customerUsername] => 2957056278595
                    [customerEmail] => bridget.m.hunter@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Bridget Hunter
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Miss Bridget Hunter
                            [company] => 
                            [street1] => 24 Tawd Road
                            [street2] => 
                            [street3] => 
                            [city] => Skelmersdale
                            [state] => Lancashire
                            [postalCode] => WN8 6BP
                            [country] => GB
                            [phone] => +44 7711 044214
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 79523223
                                    [lineItemKey] => 4570534674499
                                    [sku] => P00018S
                                    [name] => Black Forest Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Black-Forest-Gateau.jpg?v=1588175780
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6676602
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-02T04:54:48.3
                                    [modifyDate] => 2020-05-02T04:54:48.3
                                )

                        )

                    [orderTotal] => 37.56
                    [amountPaid] => 35.95
                    [taxAmount] => 1.63
                    [shippingAmount] => 9.92
                    [customerNotes] => <br/>Delivery Date: 21/05/2020<br/>Delivery Day: Thursday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Week day delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-20
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [48] => stdClass Object
                (
                    [orderId] => 49311908
                    [orderNumber] => ORD0001549
                    [orderKey] => 2093660962883
                    [orderDate] => 2020-05-02T03:50:32.0000000
                    [createDate] => 2020-05-02T04:54:47.3470000
                    [modifyDate] => 2020-05-02T08:55:01.8370000
                    [paymentDate] => 2020-05-02T03:50:32.0000000
                    [shipByDate] => 2020-05-23T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 29234640
                    [customerUsername] => 2958399832131
                    [customerEmail] => jasmine.nkl@yahoo.com
                    [billTo] => stdClass Object
                        (
                            [name] => Susan  Lingard
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Susan  Lingard 
                            [company] => 
                            [street1] => 924 Green Lane
                            [street2] => 
                            [street3] => 
                            [city] => Dagenham
                            [state] => Essex
                            [postalCode] => RM8 1BX
                            [country] => GB
                            [phone] => 07910 981181
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 79523219
                                    [lineItemKey] => 4570593460291
                                    [sku] => P00010S
                                    [name] => Raspberry Fault Line Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Fault-Line-Cake.jpg?v=1588176011
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.94
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6649612
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-02T04:54:47.287
                                    [modifyDate] => 2020-05-02T04:54:47.287
                                )

                        )

                    [orderTotal] => 44.05
                    [amountPaid] => 41.89
                    [taxAmount] => 2.16
                    [shippingAmount] => 12.94
                    [customerNotes] => <br/>Delivery Date: 23/05/2020<br/>Delivery Day: Saturday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Prem Sat
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_saturday
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-22
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [49] => stdClass Object
                (
                    [orderId] => 49311899
                    [orderNumber] => ORD0001554
                    [orderKey] => 2093681836099
                    [orderDate] => 2020-05-02T04:03:02.0000000
                    [createDate] => 2020-05-02T04:54:45.9900000
                    [modifyDate] => 2020-05-02T04:54:50.6800000
                    [paymentDate] => 2020-05-02T04:03:02.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 29234619
                    [customerUsername] => 2958419034179
                    [customerEmail] => abigail.usher@hotmail.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => Abigail Usher
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Abigail Usher
                            [company] => 
                            [street1] => 7 Park View
                            [street2] => 
                            [street3] => 
                            [city] => Goole
                            [state] => East Riding Of Yorkshire
                            [postalCode] => DN14 5DF
                            [country] => GB
                            [phone] => 07595 412226
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 79523207
                                    [lineItemKey] => 4570622394435
                                    [sku] => P00019S
                                    [name] => Black Forest Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Black-Forest-Gateau.jpg?v=1588175780
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 35.5
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6816438
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-02T04:54:45.927
                                    [modifyDate] => 2020-05-02T04:54:45.927
                                )

                        )

                    [orderTotal] => 45.45
                    [amountPaid] => 45.45
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 28/05/2020<br/>Delivery Day: Thursday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Week day delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-26
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [50] => stdClass Object
                (
                    [orderId] => 49311885
                    [orderNumber] => ORD0001566
                    [orderKey] => 2093739933763
                    [orderDate] => 2020-05-02T04:53:11.0000000
                    [createDate] => 2020-05-02T04:54:41.9970000
                    [modifyDate] => 2020-05-02T04:54:50.6800000
                    [paymentDate] => 2020-05-02T04:53:11.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 29234625
                    [customerUsername] => 2958474084419
                    [customerEmail] => dalejjamieson@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Dale Jamieson
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Dale Jamieson
                            [company] => 
                            [street1] => 97 Bowyer Way
                            [street2] => 
                            [street3] => 
                            [city] => Morpeth
                            [state] => Northumberland
                            [postalCode] => NE61 2FZ
                            [country] => GB
                            [phone] => 07702 111757
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 79523191
                                    [lineItemKey] => 4570733740099
                                    [sku] => P00009S
                                    [name] => Tiramisu Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Tiramisu-Cake.jpg?v=1588176158
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 35.5
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6816437
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-02T04:54:41.933
                                    [modifyDate] => 2020-05-02T04:54:41.933
                                )

                        )

                    [orderTotal] => 45.45
                    [amountPaid] => 45.45
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 28/05/2020<br/>Delivery Day: Thursday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-26
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [51] => stdClass Object
                (
                    [orderId] => 49338329
                    [orderNumber] => ORD0001571
                    [orderKey] => 2093773062211
                    [orderDate] => 2020-05-02T05:21:53.0000000
                    [createDate] => 2020-05-02T07:03:43.0730000
                    [modifyDate] => 2020-05-02T11:03:54.0330000
                    [paymentDate] => 2020-05-02T05:21:53.0000000
                    [shipByDate] => 2020-05-22T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 29251358
                    [customerUsername] => 2958511210563
                    [customerEmail] => cpa1980@hotmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Carol Paniagua
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Carol Paniagua
                            [company] => 
                            [street1] => 27 Cricketers View
                            [street2] => 
                            [street3] => 
                            [city] => Leeds
                            [state] => Leeds
                            [postalCode] => LS17 8WD
                            [country] => GB
                            [phone] => 07903 701205
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 79564426
                                    [lineItemKey] => 4570797473859
                                    [sku] => P00008S
                                    [name] => Tiramisu Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Tiramisu-Cake.jpg?v=1588176158
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6820300
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-02T07:03:43.01
                                    [modifyDate] => 2020-05-02T07:03:43.01
                                )

                        )

                    [orderTotal] => 37.56
                    [amountPaid] => 35.95
                    [taxAmount] => 1.63
                    [shippingAmount] => 9.92
                    [customerNotes] => <br/>Delivery Date: 22/05/2020<br/>Delivery Day: Friday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-21
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [52] => stdClass Object
                (
                    [orderId] => 49338326
                    [orderNumber] => ORD0001572
                    [orderKey] => 2093775192131
                    [orderDate] => 2020-05-02T05:23:36.0000000
                    [createDate] => 2020-05-02T07:03:42.7970000
                    [modifyDate] => 2020-05-02T11:03:55.4630000
                    [paymentDate] => 2020-05-02T05:23:36.0000000
                    [shipByDate] => 2020-05-20T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 29251365
                    [customerUsername] => 2958512455747
                    [customerEmail] => jorgevegaruiz@yahoo.es
                    [billTo] => stdClass Object
                        (
                            [name] => Jorge Vega Ruiz
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Jorge Vega Ruiz
                            [company] => 
                            [street1] => 44 Stacey Road
                            [street2] => 
                            [street3] => 
                            [city] => Cambridge
                            [state] => Cambridgeshire
                            [postalCode] => CB2 9FG
                            [country] => GB
                            [phone] => 07518 848238
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 79564424
                                    [lineItemKey] => 4570801963075
                                    [sku] => P00008S
                                    [name] => Tiramisu Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Tiramisu-Cake.jpg?v=1588176158
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6820300
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-02T07:03:42.733
                                    [modifyDate] => 2020-05-02T07:03:42.733
                                )

                        )

                    [orderTotal] => 37.56
                    [amountPaid] => 35.95
                    [taxAmount] => 1.63
                    [shippingAmount] => 9.92
                    [customerNotes] => <br/>Delivery Date: 20/05/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-19
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [53] => stdClass Object
                (
                    [orderId] => 49338314
                    [orderNumber] => ORD0001576
                    [orderKey] => 2093804585027
                    [orderDate] => 2020-05-02T05:46:59.0000000
                    [createDate] => 2020-05-02T07:03:41.7000000
                    [modifyDate] => 2020-05-02T07:03:46.0400000
                    [paymentDate] => 2020-05-02T05:46:59.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 29251359
                    [customerUsername] => 2958542471235
                    [customerEmail] => cindyholian@yahoo.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => Cindy Holian
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Cindy Holian
                            [company] => 
                            [street1] => 54 Tuddenham Avenue
                            [street2] => 
                            [street3] => 
                            [city] => Ipswich
                            [state] => Suffolk
                            [postalCode] => IP4 2HF
                            [country] => GB
                            [phone] => 07850 313677
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 79564411
                                    [lineItemKey] => 4570860060739
                                    [sku] => P00001S
                                    [name] => Spring Surprise Cake - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Pinata-Cake-NO-EGGS.jpg?v=1588176645
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 29.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6559585
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-02T07:03:41.657
                                    [modifyDate] => 2020-05-02T07:03:41.657
                                )

                        )

                    [orderTotal] => 39.9
                    [amountPaid] => 39.9
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 27/05/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-25
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [54] => stdClass Object
                (
                    [orderId] => 49338295
                    [orderNumber] => ORD0001580
                    [orderKey] => 2093816545347
                    [orderDate] => 2020-05-02T05:56:09.0000000
                    [createDate] => 2020-05-02T07:03:40.2330000
                    [modifyDate] => 2020-05-02T07:03:46.0400000
                    [paymentDate] => 2020-05-02T05:56:09.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 29251379
                    [customerUsername] => 2958523596867
                    [customerEmail] => urvashi_bilimoria1@hotmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => urvashi bilimoria
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => urvashi bilimoria
                            [company] => 
                            [street1] => 14 Clovelly Road
                            [street2] => 
                            [street3] => 
                            [city] => Leicester
                            [state] => Leicestershire
                            [postalCode] => LE3 8AA
                            [country] => GB
                            [phone] => 07714 323093
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 79564367
                                    [lineItemKey] => 4570884341827
                                    [sku] => P00018S
                                    [name] => Black Forest Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Black-Forest-Gateau.jpg?v=1588175780
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6676602
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-02T07:03:40.187
                                    [modifyDate] => 2020-05-02T07:03:40.187
                                )

                        )

                    [orderTotal] => 35.95
                    [amountPaid] => 35.95
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 28/05/2020<br/>Delivery Day: Thursday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-26
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [55] => stdClass Object
                (
                    [orderId] => 49338285
                    [orderNumber] => ORD0001583
                    [orderKey] => 2093835321411
                    [orderDate] => 2020-05-02T06:08:18.0000000
                    [createDate] => 2020-05-02T07:03:39.4270000
                    [modifyDate] => 2020-05-02T07:03:46.0400000
                    [paymentDate] => 2020-05-02T06:08:18.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 29251367
                    [customerUsername] => 2958569996355
                    [customerEmail] => 906311930@qq.com
                    [billTo] => stdClass Object
                        (
                            [name] => LINGFEI WANG
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => LINGFEI WANG
                            [company] => 
                            [street1] => Asa briggs house 6th Saint John’s Road
                            [street2] => Flat415
                            [street3] => 
                            [city] => LEEDS
                            [state] => 
                            [postalCode] => LS3 1FF
                            [country] => GB
                            [phone] => 07787 052042
                            [residential] => 
                            [addressVerified] => Address validation failed
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 79564351
                                    [lineItemKey] => 4570920648771
                                    [sku] => P00018S
                                    [name] => Black Forest Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Black-Forest-Gateau.jpg?v=1588175780
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6676602
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-02T07:03:39.377
                                    [modifyDate] => 2020-05-02T07:03:39.377
                                )

                        )

                    [orderTotal] => 35.95
                    [amountPaid] => 35.95
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 25/05/2020<br/>Delivery Day: Monday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Week day delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-24
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [56] => stdClass Object
                (
                    [orderId] => 49401749
                    [orderNumber] => ORD0001601
                    [orderKey] => 2094088224835
                    [orderDate] => 2020-05-02T08:43:07.0000000
                    [createDate] => 2020-05-02T09:03:48.9570000
                    [modifyDate] => 2020-05-02T09:03:54.1270000
                    [paymentDate] => 2020-05-02T08:43:07.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 29278096
                    [customerUsername] => 2958839185475
                    [customerEmail] => gw_guowei@msn.com
                    [billTo] => stdClass Object
                        (
                            [name] => WEI GUO
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => WEI GUO
                            [company] => 
                            [street1] => 10 Talbot Meadows
                            [street2] => 
                            [street3] => 
                            [city] => Poole
                            [state] => Dorset
                            [postalCode] => BH12 5DG
                            [country] => GB
                            [phone] => 07858 352555
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 79675381
                                    [lineItemKey] => 4571449393219
                                    [sku] => P00001S
                                    [name] => Spring Surprise Cake - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Pinata-Cake-NO-EGGS.jpg?v=1588176645
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 29.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6559585
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-02T09:03:48.877
                                    [modifyDate] => 2020-05-02T09:03:48.877
                                )

                        )

                    [orderTotal] => 39.9
                    [amountPaid] => 39.9
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 21/05/2020<br/>Delivery Day: Thursday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Week day delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-19
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [57] => stdClass Object
                (
                    [orderId] => 49461560
                    [orderNumber] => ORD0001607
                    [orderKey] => 2094200127555
                    [orderDate] => 2020-05-02T09:48:23.0000000
                    [createDate] => 2020-05-02T11:03:58.8670000
                    [modifyDate] => 2020-05-02T15:04:09.9500000
                    [paymentDate] => 2020-05-02T09:48:23.0000000
                    [shipByDate] => 2020-06-24T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 29346341
                    [customerUsername] => 2958951415875
                    [customerEmail] => 18301896690@163.com
                    [billTo] => stdClass Object
                        (
                            [name] => Jie Huang
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Jie Huang
                            [company] => F,LAT 8
                            [street1] => 52 Baxter Gate
                            [street2] => 
                            [street3] => 
                            [city] => Loughborough
                            [state] => Leicestershire
                            [postalCode] => LE11 1TH
                            [country] => GB
                            [phone] => 07529 151024
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 79784290
                                    [lineItemKey] => 4571680178243
                                    [sku] => P00008S
                                    [name] => Tiramisu Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Tiramisu-Cake.jpg?v=1588176158
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6820300
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-02T11:03:58.79
                                    [modifyDate] => 2020-05-02T11:03:58.79
                                )

                            [1] => stdClass Object
                                (
                                    [orderItemId] => 79784291
                                    [lineItemKey] => 4571680211011
                                    [sku] => P00004S
                                    [name] => Triple Chocolate Delight Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Triple-Choc-Gateau-1.jpg?v=1588176197
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.94
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6677587
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-02T11:03:58.79
                                    [modifyDate] => 2020-05-02T11:03:58.79
                                )

                        )

                    [orderTotal] => 66.51
                    [amountPaid] => 64.9
                    [taxAmount] => 1.63
                    [shippingAmount] => 9.92
                    [customerNotes] => <br/>Delivery Date: 24/06/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-06-23
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [58] => stdClass Object
                (
                    [orderId] => 49553459
                    [orderNumber] => ORD0001628
                    [orderKey] => 2094474985539
                    [orderDate] => 2020-05-02T12:28:29.0000000
                    [createDate] => 2020-05-02T13:04:06.3100000
                    [modifyDate] => 2020-05-02T13:04:11.6100000
                    [paymentDate] => 2020-05-02T12:28:29.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 29385180
                    [customerUsername] => 2959236399171
                    [customerEmail] => hmeakbani@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Mahwish  Akbani
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Mahwish  Akbani 
                            [company] => 
                            [street1] => 16 Thompson Avenue
                            [street2] => 
                            [street3] => 
                            [city] => Manchester
                            [state] => Lancashire
                            [postalCode] => M45 6DU
                            [country] => GB
                            [phone] => 07825 154508
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 79940901
                                    [lineItemKey] => 4572246868035
                                    [sku] => P00004S
                                    [name] => Triple Chocolate Delight Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Triple-Choc-Gateau-1.jpg?v=1588447846
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6677587
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-02T13:04:06.25
                                    [modifyDate] => 2020-05-02T13:04:06.25
                                )

                        )

                    [orderTotal] => 38.9
                    [amountPaid] => 38.9
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 22/05/2020<br/>Delivery Day: Friday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-20
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [59] => stdClass Object
                (
                    [orderId] => 49737483
                    [orderNumber] => ORD0001653
                    [orderKey] => 2094887534659
                    [orderDate] => 2020-05-02T17:13:16.0000000
                    [createDate] => 2020-05-02T19:04:29.9670000
                    [modifyDate] => 2020-05-02T21:04:33.0230000
                    [paymentDate] => 2020-05-02T17:13:16.0000000
                    [shipByDate] => 2020-05-22T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 29560865
                    [customerUsername] => 2959677816899
                    [customerEmail] => mik1s@hotmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Michael Saunders
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Michael Saunders
                            [company] => 
                            [street1] => 6 Meadow Croft Gardens
                            [street2] => 
                            [street3] => 
                            [city] => Nottingham
                            [state] => Nottinghamshire
                            [postalCode] => NG15 6UN
                            [country] => GB
                            [phone] => 07511 110351
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 80236991
                                    [lineItemKey] => 4573060857923
                                    [sku] => P00004S
                                    [name] => Triple Chocolate Delight Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Triple-Choc-Gateau-1.jpg?v=1588447846
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.94
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6677587
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-02T19:04:29.903
                                    [modifyDate] => 2020-05-02T19:04:29.903
                                )

                        )

                    [orderTotal] => 40.53
                    [amountPaid] => 38.89
                    [taxAmount] => 1.64
                    [shippingAmount] => 9.93
                    [customerNotes] => <br/>Delivery Date: 22/05/2020<br/>Delivery Day: Friday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-21
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [60] => stdClass Object
                (
                    [orderId] => 49866666
                    [orderNumber] => ORD0001675
                    [orderKey] => 2095609643075
                    [orderDate] => 2020-05-03T03:12:34.0000000
                    [createDate] => 2020-05-03T05:04:54.6730000
                    [modifyDate] => 2020-05-03T09:04:58.9670000
                    [paymentDate] => 2020-05-03T03:12:34.0000000
                    [shipByDate] => 2020-05-22T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 29647249
                    [customerUsername] => 2960349200451
                    [customerEmail] => jill.jenkins1656@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Gillian Jenkins
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Mavis Arnett
                            [company] => 
                            [street1] => 2 Edna Road
                            [street2] => 
                            [street3] => 
                            [city] => London
                            [state] => Surrey
                            [postalCode] => SW20 8BT
                            [country] => GB
                            [phone] => 07712185785
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 80430976
                                    [lineItemKey] => 4574360338499
                                    [sku] => P00004S
                                    [name] => Triple Chocolate Delight Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Triple-Choc-Gateau-1.jpg?v=1588447846
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.94
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6677587
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-03T05:04:54.61
                                    [modifyDate] => 2020-05-03T05:04:54.61
                                )

                        )

                    [orderTotal] => 40.51
                    [amountPaid] => 38.89
                    [taxAmount] => 1.63
                    [shippingAmount] => 9.92
                    [customerNotes] => <br/>Delivery Date: 22/05/2020<br/>Delivery Day: Friday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-21
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [61] => stdClass Object
                (
                    [orderId] => 49866658
                    [orderNumber] => ORD0001682
                    [orderKey] => 2095706570819
                    [orderDate] => 2020-05-03T04:03:04.0000000
                    [createDate] => 2020-05-03T05:04:52.6300000
                    [modifyDate] => 2020-05-03T09:05:08.2500000
                    [paymentDate] => 2020-05-03T04:03:04.0000000
                    [shipByDate] => 2020-06-09T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 29647251
                    [customerUsername] => 2960400908355
                    [customerEmail] => lyndz456@msn.com
                    [billTo] => stdClass Object
                        (
                            [name] => Lyndsey Champ
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Mrs Julie Church
                            [company] => 
                            [street1] => 35 Adkin Way
                            [street2] => 
                            [street3] => 
                            [city] => Wantage
                            [state] => Berkshire
                            [postalCode] => OX12 9HN
                            [country] => GB
                            [phone] => 07851 400131
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 80430969
                                    [lineItemKey] => 4574506254403
                                    [sku] => P00007S
                                    [name] => Double Chocolate Dream Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Double-Choc-Dream-Cake.jpg?v=1588447527
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 35.5
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6760416
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-03T05:04:52.567
                                    [modifyDate] => 2020-05-03T05:04:52.567
                                )

                        )

                    [orderTotal] => 47.08
                    [amountPaid] => 45.45
                    [taxAmount] => 1.64
                    [shippingAmount] => 9.93
                    [customerNotes] => <br/>Delivery Date: 09/06/2020<br/>Delivery Day: Tuesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Week day delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-06-08
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [62] => stdClass Object
                (
                    [orderId] => 49866638
                    [orderNumber] => ORD0001691
                    [orderKey] => 2095777906755
                    [orderDate] => 2020-05-03T04:38:51.0000000
                    [createDate] => 2020-05-03T05:04:50.1330000
                    [modifyDate] => 2020-05-03T05:04:55.8900000
                    [paymentDate] => 2020-05-03T04:38:51.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 29647245
                    [customerUsername] => 2960442163267
                    [customerEmail] => freyaetaylor@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Freya Taylor
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Freya Taylor
                            [company] => 
                            [street1] => 20 Scott Road
                            [street2] => 
                            [street3] => 
                            [city] => Selby
                            [state] => Yorkshire
                            [postalCode] => YO8 4BL
                            [country] => GB
                            [phone] => 07415354351
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 80430945
                                    [lineItemKey] => 4574609702979
                                    [sku] => P00012S
                                    [name] => Carrot Cake - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Carrot-Cake.jpg?v=1588447499
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6489824
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-03T05:04:50.053
                                    [modifyDate] => 2020-05-03T05:04:50.053
                                )

                        )

                    [orderTotal] => 35.95
                    [amountPaid] => 35.95
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 20/05/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Week day delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-18
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => 580111
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [63] => stdClass Object
                (
                    [orderId] => 49885863
                    [orderNumber] => ORD0001701
                    [orderKey] => 2095896297539
                    [orderDate] => 2020-05-03T05:49:46.0000000
                    [createDate] => 2020-05-03T07:05:01.4730000
                    [modifyDate] => 2020-05-17T16:15:43.8730000
                    [paymentDate] => 2020-05-03T05:49:46.0000000
                    [shipByDate] => 2020-05-23T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 29660759
                    [customerUsername] => 2960525525059
                    [customerEmail] => kg1188@googlemail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Kay Grant
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Finn Wass
                            [company] => The Old Bake House
                            [street1] => The Old Bake House
                            [street2] => 1 Station Road
                            [street3] => 
                            [city] => Corby Glen
                            [state] => Lincolnshire
                            [postalCode] => NG33 4NW
                            [country] => GB
                            [phone] => 07951 632318
                            [residential] => 
                            [addressVerified] => Address not yet validated
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 93436102
                                    [lineItemKey] => 4574812045379
                                    [sku] => P00001S
                                    [name] => Spring Surprise Cake - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Pinata-Cake-NO-EGGS.png?v=1588924495
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 29.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6559585
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-17T16:15:43.873
                                    [modifyDate] => 2020-05-17T16:15:43.873
                                )

                        )

                    [orderTotal] => 42.9
                    [amountPaid] => 42.9
                    [taxAmount] => 2.16
                    [shippingAmount] => 12.95
                    [customerNotes] => <br/>Delivery Date: 23/05/2020<br/>Delivery Day: Saturday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => DPD - Pat Val Prem Sat
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_saturday
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 2020-05-23
                    [holdUntilDate] => 2020-05-22
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [64] => stdClass Object
                (
                    [orderId] => 49885854
                    [orderNumber] => ORD0001705
                    [orderKey] => 2095905964099
                    [orderDate] => 2020-05-03T05:56:23.0000000
                    [createDate] => 2020-05-03T07:05:00.1630000
                    [modifyDate] => 2020-05-03T07:05:04.6970000
                    [paymentDate] => 2020-05-03T05:56:23.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 29660758
                    [customerUsername] => 2960544301123
                    [customerEmail] => charlotte.j.davies@hotmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Charlotte Churcher
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Charlotte Churcher
                            [company] => 
                            [street1] => 90 Leaders Way
                            [street2] => 
                            [street3] => 
                            [city] => Lutterworth
                            [state] => Leicestershire
                            [postalCode] => LE17 4YW
                            [country] => GB
                            [phone] => 07791358042
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 80460913
                                    [lineItemKey] => 4574832558147
                                    [sku] => P00003S
                                    [name] => Candy Stripe Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Pink-Candy-Stripe-Cake.jpg?v=1588447486
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 38.5
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6816436
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-03T07:05:00.1
                                    [modifyDate] => 2020-05-03T07:05:00.1
                                )

                        )

                    [orderTotal] => 51.45
                    [amountPaid] => 51.45
                    [taxAmount] => 2.16
                    [shippingAmount] => 12.95
                    [customerNotes] => <br/>Delivery Date: 23/05/2020<br/>Delivery Day: Saturday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Prem Sat
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_saturday
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-21
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => shopify_draft_order
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [65] => stdClass Object
                (
                    [orderId] => 49923005
                    [orderNumber] => ORD0001721
                    [orderKey] => 2096058499139
                    [orderDate] => 2020-05-03T07:39:49.0000000
                    [createDate] => 2020-05-03T09:05:11.9170000
                    [modifyDate] => 2020-05-03T13:05:27.5230000
                    [paymentDate] => 2020-05-03T07:39:49.0000000
                    [shipByDate] => 2020-05-21T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 29688032
                    [customerUsername] => 2960716169283
                    [customerEmail] => tom.p12@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Thomas Palmer
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Tilly Palmer
                            [company] => 
                            [street1] => 77 Meadway
                            [street2] => 
                            [street3] => 
                            [city] => Harpenden
                            [state] => Herts
                            [postalCode] => AL5 1JH
                            [country] => GB
                            [phone] => +44 0785  049 1647
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 80522905
                                    [lineItemKey] => 4575141199939
                                    [sku] => P00004S
                                    [name] => Triple Chocolate Delight Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Triple-Choc-Gateau-1.jpg?v=1588447846
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.94
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6677587
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-03T09:05:11.84
                                    [modifyDate] => 2020-05-03T09:05:11.84
                                )

                        )

                    [orderTotal] => 40.55
                    [amountPaid] => 38.89
                    [taxAmount] => 1.65
                    [shippingAmount] => 9.94
                    [customerNotes] => <br/>Delivery Date: 21/05/2020<br/>Delivery Day: Thursday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-20
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => shopify_draft_order
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [66] => stdClass Object
                (
                    [orderId] => 49922933
                    [orderNumber] => ORD0001737
                    [orderKey] => 2096197828675
                    [orderDate] => 2020-05-03T09:03:00.0000000
                    [createDate] => 2020-05-03T09:05:06.7030000
                    [modifyDate] => 2020-05-03T09:05:15.8500000
                    [paymentDate] => 2020-05-03T09:03:00.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 29688031
                    [customerUsername] => 2960864084035
                    [customerEmail] => suejohnallen@yahoo.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => Susan Allen
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Susan Allen
                            [company] => 
                            [street1] => 31 Cotton Road
                            [street2] => 
                            [street3] => 
                            [city] => Potters Bar
                            [state] => Middlesex
                            [postalCode] => EN6 5JT
                            [country] => GB
                            [phone] => 07713 862264
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 80522810
                                    [lineItemKey] => 4575415107651
                                    [sku] => P00010S
                                    [name] => Raspberry Fault Line Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Fault-Line-Cake.jpg?v=1588447562
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6649612
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-03T09:05:06.61
                                    [modifyDate] => 2020-05-03T09:05:06.61
                                )

                        )

                    [orderTotal] => 38.9
                    [amountPaid] => 38.9
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 20/05/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-18
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [67] => stdClass Object
                (
                    [orderId] => 49968198
                    [orderNumber] => ORD0001739
                    [orderKey] => 2096207233091
                    [orderDate] => 2020-05-03T09:08:06.0000000
                    [createDate] => 2020-05-03T11:05:23.4930000
                    [modifyDate] => 2020-05-03T15:05:29.6500000
                    [paymentDate] => 2020-05-03T09:08:06.0000000
                    [shipByDate] => 2020-06-02T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 29722004
                    [customerUsername] => 2960881877059
                    [customerEmail] => danieletully@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Daniel Tully
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Daniel Tully
                            [company] => 
                            [street1] => 322 Rangefield Road
                            [street2] => 
                            [street3] => 
                            [city] => Bromley
                            [state] => Kent
                            [postalCode] => BR1 4QY
                            [country] => GB
                            [phone] => 07855061679
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 80594910
                                    [lineItemKey] => 4575450103875
                                    [sku] => P00004S
                                    [name] => Triple Chocolate Delight Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Triple-Choc-Gateau-1.jpg?v=1588447846
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.94
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6677587
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-03T11:05:23.447
                                    [modifyDate] => 2020-05-03T11:05:23.447
                                )

                        )

                    [orderTotal] => 40.53
                    [amountPaid] => 38.89
                    [taxAmount] => 1.64
                    [shippingAmount] => 9.93
                    [customerNotes] => <br/>Delivery Date: 02/06/2020<br/>Delivery Day: Tuesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-06-01
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [68] => stdClass Object
                (
                    [orderId] => 50024087
                    [orderNumber] => ORD0001762
                    [orderKey] => 2096522395715
                    [orderDate] => 2020-05-03T11:41:24.0000000
                    [createDate] => 2020-05-03T13:05:33.2600000
                    [modifyDate] => 2020-05-03T17:05:43.9800000
                    [paymentDate] => 2020-05-03T11:41:24.0000000
                    [shipByDate] => 2020-05-22T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 29762188
                    [customerUsername] => 2961207230531
                    [customerEmail] => Ahbrown24@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Alister Brown
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Alister Brown
                            [company] => 
                            [street1] => 58 Hartford Road
                            [street2] => 
                            [street3] => 
                            [city] => Hook
                            [state] => Hampshire
                            [postalCode] => RG27 8QG
                            [country] => GB
                            [phone] => 07717 740081
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 80685390
                                    [lineItemKey] => 4576204881987
                                    [sku] => P00018S
                                    [name] => Black Forest Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Black-Forest-Gateau.jpg?v=1588447451
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6676602
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-03T13:05:33.167
                                    [modifyDate] => 2020-05-03T13:05:33.167
                                )

                        )

                    [orderTotal] => 37.58
                    [amountPaid] => 35.95
                    [taxAmount] => 1.64
                    [shippingAmount] => 9.93
                    [customerNotes] => <br/>Delivery Date: 22/05/2020<br/>Delivery Day: Friday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-21
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [69] => stdClass Object
                (
                    [orderId] => 50024063
                    [orderNumber] => ORD0001779
                    [orderKey] => 2096651108419
                    [orderDate] => 2020-05-03T12:55:40.0000000
                    [createDate] => 2020-05-03T13:05:28.0200000
                    [modifyDate] => 2020-05-03T13:05:36.1330000
                    [paymentDate] => 2020-05-03T12:55:40.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 29762195
                    [customerUsername] => 2961349771331
                    [customerEmail] => elizabeth.brownlees@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Elizabeth Brownlees
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Elizabeth Brownlees
                            [company] => 
                            [street1] => 1 Russellstown Road
                            [street2] => 
                            [street3] => 
                            [city] => Ballymena
                            [state] => Northern Ireland
                            [postalCode] => BT42 1HP
                            [country] => GB
                            [phone] => 07966 539111
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 80685367
                                    [lineItemKey] => 4576463159363
                                    [sku] => P00012S
                                    [name] => Carrot Cake - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Carrot-Cake.jpg?v=1588447499
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6489824
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-03T13:05:27.943
                                    [modifyDate] => 2020-05-03T13:05:27.943
                                )

                        )

                    [orderTotal] => 35.95
                    [amountPaid] => 35.95
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 05/06/2020<br/>Delivery Day: Friday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-06-03
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [70] => stdClass Object
                (
                    [orderId] => 50024058
                    [orderNumber] => ORD0001781
                    [orderKey] => 2096663461955
                    [orderDate] => 2020-05-03T13:03:17.0000000
                    [createDate] => 2020-05-03T13:05:27.2870000
                    [modifyDate] => 2020-05-03T13:05:36.1330000
                    [paymentDate] => 2020-05-03T13:03:17.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 29762195
                    [customerUsername] => 2961349771331
                    [customerEmail] => elizabeth.brownlees@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Elizabeth Brownlees
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Peter Brownlees
                            [company] => 
                            [street1] => Apartment 1107,
                            [street2] => 100 Greengate
                            [street3] => 
                            [city] => Salford
                            [state] => Lancashire
                            [postalCode] => M3 7NG
                            [country] => GB
                            [phone] => 07966 539111
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 80685360
                                    [lineItemKey] => 4576488751171
                                    [sku] => P00012S
                                    [name] => Carrot Cake - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Carrot-Cake.jpg?v=1588447499
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6489824
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-03T13:05:27.21
                                    [modifyDate] => 2020-05-03T13:05:27.21
                                )

                        )

                    [orderTotal] => 35.95
                    [amountPaid] => 35.95
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 05/06/2020<br/>Delivery Day: Friday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-06-03
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [71] => stdClass Object
                (
                    [orderId] => 50078716
                    [orderNumber] => ORD0001784
                    [orderKey] => 2096690102339
                    [orderDate] => 2020-05-03T13:18:29.0000000
                    [createDate] => 2020-05-03T15:05:42.2030000
                    [modifyDate] => 2020-05-03T19:05:49.9930000
                    [paymentDate] => 2020-05-03T13:18:29.0000000
                    [shipByDate] => 2020-05-20T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 29803060
                    [customerUsername] => 2961391583299
                    [customerEmail] => chris_wood1988@hotmail.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => Christopher Wood
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Christopher Wood
                            [company] => 
                            [street1] => 15 Cedar Drive
                            [street2] => 
                            [street3] => 
                            [city] => Leeds
                            [state] => Leeds
                            [postalCode] => LS14 6US
                            [country] => GB
                            [phone] => 07595 329487
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 80770612
                                    [lineItemKey] => 4576543670339
                                    [sku] => P00001S
                                    [name] => Spring Surprise Cake - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Pinata-Cake-NO-EGGS.jpg?v=1588447805
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 29.94
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6559585
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-03T15:05:42.157
                                    [modifyDate] => 2020-05-03T15:05:42.157
                                )

                        )

                    [orderTotal] => 41.53
                    [amountPaid] => 39.89
                    [taxAmount] => 1.64
                    [shippingAmount] => 9.93
                    [customerNotes] => <br/>Delivery Date: 20/05/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Week day delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-19
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [72] => stdClass Object
                (
                    [orderId] => 50279357
                    [orderNumber] => ORD0001819
                    [orderKey] => 2097508057155
                    [orderDate] => 2020-05-04T00:24:26.0000000
                    [createDate] => 2020-05-04T01:06:02.2070000
                    [modifyDate] => 2020-05-04T03:06:17.6530000
                    [paymentDate] => 2020-05-04T00:24:26.0000000
                    [shipByDate] => 2020-05-30T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 29948540
                    [customerUsername] => 2962316361795
                    [customerEmail] => hendryadams@hotmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Nicola Hendry-Adams
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Margaret Mitchell
                            [company] => 
                            [street1] => 18 Mcgrigor Road
                            [street2] => 
                            [street3] => 
                            [city] => Dunfermline
                            [state] => Fife
                            [postalCode] => KY11 2AF
                            [country] => GB
                            [phone] => +44 0793  996 9901
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 81088248
                                    [lineItemKey] => 4578170765379
                                    [sku] => P00004S
                                    [name] => Triple Chocolate Delight Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Triple-Choc-Gateau-1.jpg?v=1588447846
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.94
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6677587
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-04T01:06:02.147
                                    [modifyDate] => 2020-05-04T01:06:02.147
                                )

                        )

                    [orderTotal] => 44.04
                    [amountPaid] => 41.89
                    [taxAmount] => 2.16
                    [shippingAmount] => 12.93
                    [customerNotes] => <br/>Delivery Date: 30/05/2020<br/>Delivery Day: Saturday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Prem Sat
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_saturday
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-29
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => shopify_draft_order
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [73] => stdClass Object
                (
                    [orderId] => 50281493
                    [orderNumber] => ORD0001824
                    [orderKey] => 2097578639427
                    [orderDate] => 2020-05-04T01:15:55.0000000
                    [createDate] => 2020-05-04T01:22:57.1770000
                    [modifyDate] => 2020-05-04T01:23:02.1500000
                    [paymentDate] => 2020-05-04T01:15:55.0000000
                    [shipByDate] => 2020-05-20T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 29949512
                    [customerUsername] => 2962359418947
                    [customerEmail] => cblackerc@hotmail.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => Claire Blacker
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Ethan Dooley
                            [company] => 
                            [street1] => 14 Lichfield Street
                            [street2] => 
                            [street3] => 
                            [city] => Wigan
                            [state] => Lancashire
                            [postalCode] => WN5 9JY
                            [country] => GB
                            [phone] => 07816 868741
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 81090408
                                    [lineItemKey] => 4578274345027
                                    [sku] => P00006S
                                    [name] => Double Chocolate Dream Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Double-Choc-Dream-Cake.jpg?v=1588447527
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6759340
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-04T01:22:57.097
                                    [modifyDate] => 2020-05-04T01:22:57.097
                                )

                        )

                    [orderTotal] => 37.6
                    [amountPaid] => 35.95
                    [taxAmount] => 1.65
                    [shippingAmount] => 9.94
                    [customerNotes] => <br/>Delivery Date: 20/05/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-19
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [74] => stdClass Object
                (
                    [orderId] => 50292986
                    [orderNumber] => ORD0001826
                    [orderKey] => 2097595613251
                    [orderDate] => 2020-05-04T01:27:33.0000000
                    [createDate] => 2020-05-04T03:23:32.8900000
                    [modifyDate] => 2020-05-04T07:23:41.2900000
                    [paymentDate] => 2020-05-04T01:27:33.0000000
                    [shipByDate] => 2020-05-22T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 29957748
                    [customerUsername] => 2962372821059
                    [customerEmail] => heather_semple@hotmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Heather Semple
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Heather Semple
                            [company] => 
                            [street1] => 198 Glasgow Road
                            [street2] => 
                            [street3] => 
                            [city] => Paisley
                            [state] => Renfrewshire
                            [postalCode] => PA1 3LT
                            [country] => GB
                            [phone] => 07734720808
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 81107938
                                    [lineItemKey] => 4578298855491
                                    [sku] => P00012S
                                    [name] => Carrot Cake - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Carrot-Cake.jpg?v=1588447499
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6489824
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-04T03:23:32.843
                                    [modifyDate] => 2020-05-04T03:23:32.843
                                )

                        )

                    [orderTotal] => 37.56
                    [amountPaid] => 35.95
                    [taxAmount] => 1.63
                    [shippingAmount] => 9.92
                    [customerNotes] => <br/>Delivery Date: 22/05/2020<br/>Delivery Day: Friday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => Week day delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-21
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => 580111
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [75] => stdClass Object
                (
                    [orderId] => 50292952
                    [orderNumber] => ORD0001846
                    [orderKey] => 2097753456707
                    [orderDate] => 2020-05-04T03:14:01.0000000
                    [createDate] => 2020-05-04T03:23:26.4170000
                    [modifyDate] => 2020-05-04T03:23:36.5000000
                    [paymentDate] => 2020-05-04T03:14:01.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 29957751
                    [customerUsername] => 2962421841987
                    [customerEmail] => kfrick102@aol.com
                    [billTo] => stdClass Object
                        (
                            [name] => Karl Frederick
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Karl Frederick
                            [company] => 
                            [street1] => 101 Llangewydd Road
                            [street2] => 
                            [street3] => 
                            [city] => Bridgend
                            [state] => Glamorgan
                            [postalCode] => CF31 4JT
                            [country] => GB
                            [phone] => 07474 676076
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 81107900
                                    [lineItemKey] => 4578534064195
                                    [sku] => P00003S
                                    [name] => Candy Stripe Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Pink-Candy-Stripe-Cake.jpg?v=1588447486
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 38.5
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6816436
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-04T03:23:26.37
                                    [modifyDate] => 2020-05-04T03:23:26.37
                                )

                        )

                    [orderTotal] => 51.45
                    [amountPaid] => 51.45
                    [taxAmount] => 2.16
                    [shippingAmount] => 12.95
                    [customerNotes] => <br/>Delivery Date: 30/05/2020<br/>Delivery Day: Saturday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Prem Sat
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_saturday
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-28
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [76] => stdClass Object
                (
                    [orderId] => 50327349
                    [orderNumber] => ORD0001880
                    [orderKey] => 2097933484099
                    [orderDate] => 2020-05-04T05:18:29.0000000
                    [createDate] => 2020-05-04T05:48:50.2870000
                    [modifyDate] => 2020-05-04T05:48:58.8570000
                    [paymentDate] => 2020-05-04T05:18:29.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 29979905
                    [customerUsername] => 2962618122307
                    [customerEmail] => yzq212@hotmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => ZHIQIAN YU
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => ZHIQIAN YU
                            [company] => 
                            [street1] => Flat 19,
                            [street2] => Benedictine Court
                            [street3] => Priory Place
                            [city] => Coventry
                            [state] => Warwickshire
                            [postalCode] => CV1 5SE
                            [country] => GB
                            [phone] => 07759 673532
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 81155613
                                    [lineItemKey] => 4578829205571
                                    [sku] => P00018S
                                    [name] => Black Forest Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Black-Forest-Gateau.jpg?v=1588447451
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6676602
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-04T05:48:50.237
                                    [modifyDate] => 2020-05-04T05:48:50.237
                                )

                        )

                    [orderTotal] => 35.95
                    [amountPaid] => 35.95
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 20/05/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-18
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [77] => stdClass Object
                (
                    [orderId] => 50383719
                    [orderNumber] => ORD0001891
                    [orderKey] => 2098000166979
                    [orderDate] => 2020-05-04T06:09:20.0000000
                    [createDate] => 2020-05-04T07:41:22.7830000
                    [modifyDate] => 2020-05-04T07:41:27.2130000
                    [paymentDate] => 2020-05-04T06:09:20.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 30014705
                    [customerUsername] => 2962687361091
                    [customerEmail] => rosemarymaddy@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Rosemary Maddy
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Rosemary Maddy
                            [company] => 
                            [street1] => 25 Otter Burn Way
                            [street2] => 
                            [street3] => 
                            [city] => Prudhoe
                            [state] => Northumberland
                            [postalCode] => NE42 6RD
                            [country] => GB
                            [phone] => 01661836775
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 81251321
                                    [lineItemKey] => 4578960867395
                                    [sku] => P00003S
                                    [name] => Candy Stripe Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Pink-Candy-Stripe-Cake.jpg?v=1588447486
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 38.5
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6816436
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-04T07:41:22.733
                                    [modifyDate] => 2020-05-04T07:41:22.733
                                )

                        )

                    [orderTotal] => 48.45
                    [amountPaid] => 48.45
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 20/05/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-18
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [78] => stdClass Object
                (
                    [orderId] => 50383694
                    [orderNumber] => ORD0001899
                    [orderKey] => 2098058821699
                    [orderDate] => 2020-05-04T06:51:00.0000000
                    [createDate] => 2020-05-04T07:41:20.5030000
                    [modifyDate] => 2020-05-15T09:20:44.3470000
                    [paymentDate] => 2020-05-04T06:51:00.0000000
                    [shipByDate] => 2020-05-21T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 30014709
                    [customerUsername] => 2962697224259
                    [customerEmail] => zafar.k87@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Waqas Khan
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Waqas Khan
                            [company] => 
                            [street1] => 33 Elmstead Road
                            [street2] => 
                            [street3] => 
                            [city] => London
                            [state] => Essex
                            [postalCode] => IG3 8AX
                            [country] => GB
                            [phone] => 07813 765853
                            [residential] => 
                            [addressVerified] => Address not yet validated
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 91760848
                                    [lineItemKey] => 4579073097795
                                    [sku] => P00001S
                                    [name] => Spring Surprise Cake - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Pinata-Cake-NO-EGGS.png?v=1588924495
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 29.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6559585
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-15T09:20:44.347
                                    [modifyDate] => 2020-05-15T09:20:44.347
                                )

                        )

                    [orderTotal] => 39.9
                    [amountPaid] => 39.9
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 21/05/2020<br/>Delivery Day: Thursday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 2020-05-21
                    [holdUntilDate] => 2020-05-20
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [79] => stdClass Object
                (
                    [orderId] => 50383683
                    [orderNumber] => ORD0001902
                    [orderKey] => 2098091065411
                    [orderDate] => 2020-05-04T07:13:19.0000000
                    [createDate] => 2020-05-04T07:41:19.0830000
                    [modifyDate] => 2020-05-04T07:41:27.2130000
                    [paymentDate] => 2020-05-04T07:13:19.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 30014689
                    [customerUsername] => 2962787565635
                    [customerEmail] => buss0139@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Carolyn Bussey
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Carolyn Bussey
                            [company] => 
                            [street1] => Apartment 3,
                            [street2] => Cara Court
                            [street3] => 2 Emerald Street
                            [city] => London
                            [state] => Middlesex
                            [postalCode] => WC1N 3QA
                            [country] => GB
                            [phone] => +44 7949 818716
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 81251281
                                    [lineItemKey] => 4579139321923
                                    [sku] => P00010S
                                    [name] => Raspberry Fault Line Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Fault-Line-Cake.jpg?v=1588447562
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6649612
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-04T07:41:19.007
                                    [modifyDate] => 2020-05-04T07:41:19.007
                                )

                        )

                    [orderTotal] => 38.9
                    [amountPaid] => 38.9
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 28/05/2020<br/>Delivery Day: Thursday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Week day delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-26
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [80] => stdClass Object
                (
                    [orderId] => 50466638
                    [orderNumber] => ORD0001908
                    [orderKey] => 2098145787971
                    [orderDate] => 2020-05-04T07:47:36.0000000
                    [createDate] => 2020-05-04T09:41:38.7530000
                    [modifyDate] => 2020-05-04T13:41:43.8930000
                    [paymentDate] => 2020-05-04T07:47:36.0000000
                    [shipByDate] => 2020-05-27T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 30065979
                    [customerUsername] => 2962841305155
                    [customerEmail] => nsssaini@gmsail.com
                    [billTo] => stdClass Object
                        (
                            [name] => neha saini
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => neha saini
                            [company] => 
                            [street1] => Flat 37
                            [street2] => Hayward
                            [street3] => Chatham Place
                            [city] => Reading
                            [state] => Reading
                            [postalCode] => RG1 7AZ
                            [country] => GB
                            [phone] => 07488 526202
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 81368724
                                    [lineItemKey] => 4579252109379
                                    [sku] => P00004S
                                    [name] => Triple Chocolate Delight Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Triple-Choc-Gateau-1.jpg?v=1588447846
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.94
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6677587
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-04T09:41:38.673
                                    [modifyDate] => 2020-05-04T09:41:38.673
                                )

                        )

                    [orderTotal] => 40.51
                    [amountPaid] => 38.89
                    [taxAmount] => 1.63
                    [shippingAmount] => 9.92
                    [customerNotes] => <br/>Delivery Date: 27/05/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-26
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [81] => stdClass Object
                (
                    [orderId] => 50466543
                    [orderNumber] => ORD0001923
                    [orderKey] => 2098281054275
                    [orderDate] => 2020-05-04T09:03:39.0000000
                    [createDate] => 2020-05-04T09:41:33.8070000
                    [modifyDate] => 2020-05-04T09:41:41.0070000
                    [paymentDate] => 2020-05-04T09:03:39.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 30065975
                    [customerUsername] => 2962927091779
                    [customerEmail] => jasmin2688@yahoo.com
                    [billTo] => stdClass Object
                        (
                            [name] => Mario Valencia
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Mario Valencia
                            [company] => 
                            [street1] => 53 Royal Exchange
                            [street2] => 
                            [street3] => 
                            [city] => Newport
                            [state] => Hampshire
                            [postalCode] => PO30 2HN
                            [country] => GB
                            [phone] => 07852 425642
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 81368596
                                    [lineItemKey] => 4579528933443
                                    [sku] => P00006S
                                    [name] => Double Chocolate Dream Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Double-Choc-Dream-Cake.jpg?v=1588447527
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6759340
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-04T09:41:33.743
                                    [modifyDate] => 2020-05-04T09:41:33.743
                                )

                        )

                    [orderTotal] => 35.95
                    [amountPaid] => 35.95
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 21/05/2020<br/>Delivery Day: Thursday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-19
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [82] => stdClass Object
                (
                    [orderId] => 50555873
                    [orderNumber] => ORD0001952
                    [orderKey] => 2098460950595
                    [orderDate] => 2020-05-04T10:29:16.0000000
                    [createDate] => 2020-05-04T11:41:50.8070000
                    [modifyDate] => 2020-05-04T11:41:58.9330000
                    [paymentDate] => 2020-05-04T10:29:16.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 30115458
                    [customerUsername] => 2963227115587
                    [customerEmail] => plnyeung@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Natalie Yeung
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Natalie Yeung
                            [company] => 
                            [street1] => 5 Bush Hill Road
                            [street2] => 
                            [street3] => 
                            [city] => Harrow
                            [state] => Middlesex
                            [postalCode] => HA3 9RG
                            [country] => GB
                            [phone] => 07427 477905
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 81505549
                                    [lineItemKey] => 4579885252675
                                    [sku] => P00018S
                                    [name] => Black Forest Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Black-Forest-Gateau.jpg?v=1588447451
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6676602
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-04T11:41:50.73
                                    [modifyDate] => 2020-05-04T11:41:50.73
                                )

                        )

                    [orderTotal] => 35.95
                    [amountPaid] => 35.95
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 27/05/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-25
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [83] => stdClass Object
                (
                    [orderId] => 50555727
                    [orderNumber] => ORD0001968
                    [orderKey] => 2098559877187
                    [orderDate] => 2020-05-04T11:15:58.0000000
                    [createDate] => 2020-05-04T11:41:45.8470000
                    [modifyDate] => 2020-05-04T11:41:58.9330000
                    [paymentDate] => 2020-05-04T11:15:58.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 30115439
                    [customerUsername] => 2963355893827
                    [customerEmail] => mrandmrsspooner2015@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Chris Spooner
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Chris Spooner
                            [company] => 
                            [street1] => 97 Rivermill
                            [street2] => 
                            [street3] => 
                            [city] => Harlow
                            [state] => Essex
                            [postalCode] => CM20 1NU
                            [country] => GB
                            [phone] => 07764 164968
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 81505324
                                    [lineItemKey] => 4580077142083
                                    [sku] => P00019S
                                    [name] => Black Forest Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Black-Forest-Gateau.jpg?v=1588447451
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 35.5
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6816438
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-04T11:41:45.783
                                    [modifyDate] => 2020-05-04T11:41:45.783
                                )

                        )

                    [orderTotal] => 48.45
                    [amountPaid] => 48.45
                    [taxAmount] => 2.16
                    [shippingAmount] => 12.95
                    [customerNotes] => <br/>Delivery Date: 23/05/2020<br/>Delivery Day: Saturday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Prem Sat
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_saturday
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-21
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [84] => stdClass Object
                (
                    [orderId] => 50555680
                    [orderNumber] => ORD0001973
                    [orderKey] => 2098597036099
                    [orderDate] => 2020-05-04T11:33:25.0000000
                    [createDate] => 2020-05-04T11:41:43.9900000
                    [modifyDate] => 2020-05-04T11:41:58.9330000
                    [paymentDate] => 2020-05-04T11:33:25.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 28633690
                    [customerUsername] => 2954783555651
                    [customerEmail] => alison.park@live.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => Alison Park
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Samuel Dikomite
                            [company] => 
                            [street1] => 27 Stanstead Crescent
                            [street2] => 
                            [street3] => 
                            [city] => Brighton
                            [state] => Sussex
                            [postalCode] => BN2 6TR
                            [country] => GB
                            [phone] => 01323 659587
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 81505253
                                    [lineItemKey] => 4580151951427
                                    [sku] => P00006S
                                    [name] => Double Chocolate Dream Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Double-Choc-Dream-Cake.jpg?v=1588447527
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6759340
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-04T11:41:43.913
                                    [modifyDate] => 2020-05-04T11:41:43.913
                                )

                        )

                    [orderTotal] => 35.95
                    [amountPaid] => 35.95
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 28/05/2020<br/>Delivery Day: Thursday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-26
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [85] => stdClass Object
                (
                    [orderId] => 50555668
                    [orderNumber] => ORD0001974
                    [orderKey] => 2098601328707
                    [orderDate] => 2020-05-04T11:35:44.0000000
                    [createDate] => 2020-05-04T11:41:43.6470000
                    [modifyDate] => 2020-05-04T11:41:58.9330000
                    [paymentDate] => 2020-05-04T11:35:44.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 30115430
                    [customerUsername] => 2963423232067
                    [customerEmail] => apemms@btinternet.com
                    [billTo] => stdClass Object
                        (
                            [name] => Andy Emms
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Andy Emms
                            [company] => 
                            [street1] => 14 Pondfields Rise
                            [street2] => 
                            [street3] => 
                            [city] => Leeds
                            [state] => Yorkshire
                            [postalCode] => LS25 7NY
                            [country] => GB
                            [phone] => 07979 851947
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 81505240
                                    [lineItemKey] => 4580161192003
                                    [sku] => P00006S
                                    [name] => Double Chocolate Dream Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Double-Choc-Dream-Cake.jpg?v=1588447527
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6759340
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-04T11:41:43.6
                                    [modifyDate] => 2020-05-04T11:41:43.6
                                )

                        )

                    [orderTotal] => 35.95
                    [amountPaid] => 35.95
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 22/05/2020<br/>Delivery Day: Friday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Week day delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-20
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [86] => stdClass Object
                (
                    [orderId] => 50659675
                    [orderNumber] => ORD0001978
                    [orderKey] => 2098620301379
                    [orderDate] => 2020-05-04T11:45:03.0000000
                    [createDate] => 2020-05-04T13:42:14.4270000
                    [modifyDate] => 2020-05-04T17:42:22.3800000
                    [paymentDate] => 2020-05-04T11:45:03.0000000
                    [shipByDate] => 2020-05-27T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 30165041
                    [customerUsername] => 2963440238659
                    [customerEmail] => jiangqzgoh@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Quanzhou Jiang
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Quanzhou Jiang
                            [company] => 
                            [street1] => 51 Corsair House
                            [street2] => 5 Starboard Way
                            [street3] => 
                            [city] => London
                            [state] => Essex
                            [postalCode] => E16 2NY
                            [country] => GB
                            [phone] => 07999 753834
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 81654727
                                    [lineItemKey] => 4580200251459
                                    [sku] => P00008S
                                    [name] => Tiramisu Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Tiramisu-Cake.jpg?v=1588447825
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6820300
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-04T13:42:14.377
                                    [modifyDate] => 2020-05-04T13:42:14.377
                                )

                        )

                    [orderTotal] => 37.56
                    [amountPaid] => 35.95
                    [taxAmount] => 1.63
                    [shippingAmount] => 9.92
                    [customerNotes] => <br/>Delivery Date: 27/05/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-26
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [87] => stdClass Object
                (
                    [orderId] => 50659578
                    [orderNumber] => ORD0001997
                    [orderKey] => 2098730434627
                    [orderDate] => 2020-05-04T12:36:43.0000000
                    [createDate] => 2020-05-04T13:42:08.5100000
                    [modifyDate] => 2020-05-04T13:42:17.9900000
                    [paymentDate] => 2020-05-04T12:36:43.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 30165028
                    [customerUsername] => 2963551715395
                    [customerEmail] => jeannie607@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Jean Wright
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Jean Wright
                            [company] => 
                            [street1] => Orchard Cottage
                            [street2] => Mill Road
                            [street3] => 
                            [city] => Diss
                            [state] => Suffolk
                            [postalCode] => IP22 1DU
                            [country] => GB
                            [phone] => 07425 174050
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 81654569
                                    [lineItemKey] => 4580417699907
                                    [sku] => P00009S
                                    [name] => Tiramisu Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Tiramisu-Cake.jpg?v=1588447825
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 35.5
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6816437
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-04T13:42:08.45
                                    [modifyDate] => 2020-05-04T13:42:08.45
                                )

                        )

                    [orderTotal] => 45.45
                    [amountPaid] => 45.45
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 20/05/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-18
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [88] => stdClass Object
                (
                    [orderId] => 50659529
                    [orderNumber] => ORD0002005
                    [orderKey] => 2098818220099
                    [orderDate] => 2020-05-04T13:19:00.0000000
                    [createDate] => 2020-05-04T13:42:05.4070000
                    [modifyDate] => 2020-05-06T03:39:57.0670000
                    [paymentDate] => 2020-05-04T13:19:00.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 30165014
                    [customerUsername] => 2963624329283
                    [customerEmail] => leroytang@yahoo.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => Ka lai jackique Fong
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Amani Fong
                            [company] => 
                            [street1] => Flat 4
                            [street2] => Pottery Gate
                            [street3] => 2 Warwick Road
                            [city] => London
                            [state] => Middlesex
                            [postalCode] => N11 2TU
                            [country] => GB
                            [phone] => 07411 972705
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 81654495
                                    [lineItemKey] => 4580591697987
                                    [sku] => P00003S
                                    [name] => Candy Stripe Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Pink-Candy-Stripe-Cake.jpg?v=1588447486
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 38.5
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6816436
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-04T13:42:05.25
                                    [modifyDate] => 2020-05-04T13:42:05.25
                                )

                        )

                    [orderTotal] => 51.45
                    [amountPaid] => 51.45
                    [taxAmount] => 2.16
                    [shippingAmount] => 12.95
                    [customerNotes] => <br/>Delivery Date: 30/05/2020<br/>Delivery Day: Saturday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Prem Sat
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_saturday
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-28
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [89] => stdClass Object
                (
                    [orderId] => 50659482
                    [orderNumber] => ORD0002012
                    [orderKey] => 2098853183555
                    [orderDate] => 2020-05-04T13:36:52.0000000
                    [createDate] => 2020-05-04T13:42:02.7700000
                    [modifyDate] => 2020-05-04T13:42:17.9900000
                    [paymentDate] => 2020-05-04T13:36:52.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 30165044
                    [customerUsername] => 2963673186371
                    [customerEmail] => samiranaveed@hotmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Samira Naveed
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Samira Naveed
                            [company] => 
                            [street1] => 53 Braemar Avenue
                            [street2] => 
                            [street3] => 
                            [city] => Wembley
                            [state] => Middlesex
                            [postalCode] => HA0 4QW
                            [country] => GB
                            [phone] => 07539 830862
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 81654393
                                    [lineItemKey] => 4580662444099
                                    [sku] => P00007S
                                    [name] => Double Chocolate Dream Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Double-Choc-Dream-Cake.jpg?v=1588447527
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 35.5
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6760416
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-04T13:42:02.707
                                    [modifyDate] => 2020-05-04T13:42:02.707
                                )

                        )

                    [orderTotal] => 45.45
                    [amountPaid] => 45.45
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 27/05/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-25
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => shopify_draft_order
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [90] => stdClass Object
                (
                    [orderId] => 51008363
                    [orderNumber] => ORD0002045
                    [orderKey] => 2099749027907
                    [orderDate] => 2020-05-05T00:56:46.0000000
                    [createDate] => 2020-05-05T01:42:47.4370000
                    [modifyDate] => 2020-05-05T01:42:51.4570000
                    [paymentDate] => 2020-05-05T00:56:46.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 30363117
                    [customerUsername] => 2964679655491
                    [customerEmail] => ruth-burrows@hotmail.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => RUTH BABBAGE
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Claire Babbage
                            [company] => 
                            [street1] => 40 Montpellier Road
                            [street2] => 
                            [street3] => 
                            [city] => Exmouth
                            [state] => Devon
                            [postalCode] => EX8 1JN
                            [country] => GB
                            [phone] => 07534328285
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 82207113
                                    [lineItemKey] => 4582416744515
                                    [sku] => P00001S
                                    [name] => Spring Surprise Cake - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Pinata-Cake-NO-EGGS.jpg?v=1588447805
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 29.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6559585
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-05T01:42:47.39
                                    [modifyDate] => 2020-05-05T01:42:47.39
                                )

                        )

                    [orderTotal] => 39.9
                    [amountPaid] => 39.9
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 20/05/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-18
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [91] => stdClass Object
                (
                    [orderId] => 51008354
                    [orderNumber] => ORD0002049
                    [orderKey] => 2099770032195
                    [orderDate] => 2020-05-05T01:24:54.0000000
                    [createDate] => 2020-05-05T01:42:46.2370000
                    [modifyDate] => 2020-05-05T01:42:51.4570000
                    [paymentDate] => 2020-05-05T01:24:54.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 30363131
                    [customerUsername] => 2964706164803
                    [customerEmail] => djmo_ray@hotmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Vincent  Blake
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Vincent  Blake
                            [company] => 
                            [street1] => 15 Wisteria Way
                            [street2] => 
                            [street3] => 
                            [city] => Northampton
                            [state] => Northamptonshire
                            [postalCode] => NN3 3QB
                            [country] => GB
                            [phone] => 07500 924333
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 82207101
                                    [lineItemKey] => 4582457114691
                                    [sku] => P00019S
                                    [name] => Black Forest Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Black-Forest-Gateau.jpg?v=1588447451
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 35.5
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6816438
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-05T01:42:46.173
                                    [modifyDate] => 2020-05-05T01:42:46.173
                                )

                        )

                    [orderTotal] => 45.45
                    [amountPaid] => 45.45
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 20/05/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Week day delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-18
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [92] => stdClass Object
                (
                    [orderId] => 51035758
                    [orderNumber] => ORD0002058
                    [orderKey] => 2099810762819
                    [orderDate] => 2020-05-05T02:07:25.0000000
                    [createDate] => 2020-05-05T03:24:41.2570000
                    [modifyDate] => 2020-05-05T07:24:46.7400000
                    [paymentDate] => 2020-05-05T02:07:25.0000000
                    [shipByDate] => 2020-05-22T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 30404425
                    [customerUsername] => 2964740440131
                    [customerEmail] => jaynehancock35@yahoo.co.uk
                    [billTo] => stdClass Object
                        (
                            [name] => jayne hancock
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => jayne hancock
                            [company] => 
                            [street1] => 9 Bramley Crescent
                            [street2] => 
                            [street3] => 
                            [city] => Reading
                            [state] => Oxfordshire
                            [postalCode] => RG4 9LU
                            [country] => GB
                            [phone] => 07795 562382
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 82259076
                                    [lineItemKey] => 4582527565891
                                    [sku] => P00016S
                                    [name] => Tropical Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Tropical-Cake.jpg?v=1588447888
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.94
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6552799
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-05T03:24:41.147
                                    [modifyDate] => 2020-05-05T03:24:41.147
                                )

                        )

                    [orderTotal] => 40.51
                    [amountPaid] => 38.89
                    [taxAmount] => 1.63
                    [shippingAmount] => 9.92
                    [customerNotes] => <br/>Delivery Date: 22/05/2020<br/>Delivery Day: Friday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-21
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [93] => stdClass Object
                (
                    [orderId] => 51035755
                    [orderNumber] => ORD0002061
                    [orderKey] => 2099815776323
                    [orderDate] => 2020-05-05T02:12:08.0000000
                    [createDate] => 2020-05-05T03:24:40.3370000
                    [modifyDate] => 2020-05-05T07:24:50.6970000
                    [paymentDate] => 2020-05-05T02:12:08.0000000
                    [shipByDate] => 2020-06-19T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 30404430
                    [customerUsername] => 2953582018627
                    [customerEmail] => nima.gurung06@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Nima Gurung
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Nima Gurung
                            [company] => 
                            [street1] => 21 Jutland Road
                            [street2] => 
                            [street3] => 
                            [city] => London
                            [state] => Kent
                            [postalCode] => SE6 2DQ
                            [country] => GB
                            [phone] => 07736 968339
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 82259073
                                    [lineItemKey] => 4582535790659
                                    [sku] => P00005S
                                    [name] => Triple Chocolate Delight Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Triple-Choc-Gateau-1.jpg?v=1588447846
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 38.5
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6587725
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-05T03:24:40.26
                                    [modifyDate] => 2020-05-05T03:24:40.26
                                )

                        )

                    [orderTotal] => 50.06
                    [amountPaid] => 48.45
                    [taxAmount] => 1.63
                    [shippingAmount] => 9.92
                    [customerNotes] => <br/>Delivery Date: 19/06/2020<br/>Delivery Day: Friday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-06-18
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [94] => stdClass Object
                (
                    [orderId] => 51035752
                    [orderNumber] => ORD0002064
                    [orderKey] => 2099823837251
                    [orderDate] => 2020-05-05T02:19:23.0000000
                    [createDate] => 2020-05-05T03:24:39.4800000
                    [modifyDate] => 2020-05-05T07:24:54.6100000
                    [paymentDate] => 2020-05-05T02:19:23.0000000
                    [shipByDate] => 2020-06-26T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 30404430
                    [customerUsername] => 2953582018627
                    [customerEmail] => nima.gurung06@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Nima Gurung
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Nima Gurung
                            [company] => 
                            [street1] => 21 Jutland Road
                            [street2] => 
                            [street3] => 
                            [city] => London
                            [state] => Kent
                            [postalCode] => SE6 2DQ
                            [country] => GB
                            [phone] => 07736 968339
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 82259070
                                    [lineItemKey] => 4582548766787
                                    [sku] => P00018S
                                    [name] => Black Forest Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Black-Forest-Gateau.jpg?v=1588447451
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 26
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6676602
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-05T03:24:39.383
                                    [modifyDate] => 2020-05-05T03:24:39.383
                                )

                        )

                    [orderTotal] => 37.56
                    [amountPaid] => 35.95
                    [taxAmount] => 1.63
                    [shippingAmount] => 9.92
                    [customerNotes] => <br/>Delivery Date: 26/06/2020<br/>Delivery Day: Friday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-06-25
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => shopify_draft_order
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [95] => stdClass Object
                (
                    [orderId] => 51035750
                    [orderNumber] => ORD0002066
                    [orderKey] => 2099834519619
                    [orderDate] => 2020-05-05T02:33:00.0000000
                    [createDate] => 2020-05-05T03:24:38.5430000
                    [modifyDate] => 2020-05-05T03:24:43.5470000
                    [paymentDate] => 2020-05-05T02:33:00.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 30404432
                    [customerUsername] => 2964766654531
                    [customerEmail] => amaan_rai@hotmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Pindi Uppal
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Pindi Uppal
                            [company] => 
                            [street1] => 20 Monktonhill Road
                            [street2] => 
                            [street3] => 
                            [city] => Troon
                            [state] => Ayrshire
                            [postalCode] => KA10 7EW
                            [country] => GB
                            [phone] => +44 7540 997807
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 82259068
                                    [lineItemKey] => 4582569181251
                                    [sku] => P00005S
                                    [name] => Triple Chocolate Delight Gateau - 8" (8-10 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Triple-Choc-Gateau-1.jpg?v=1588447846
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 38.5
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6587725
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-05T03:24:38.49
                                    [modifyDate] => 2020-05-05T03:24:38.49
                                )

                        )

                    [orderTotal] => 51.45
                    [amountPaid] => 51.45
                    [taxAmount] => 2.16
                    [shippingAmount] => 12.95
                    [customerNotes] => <br/>Delivery Date: 23/05/2020<br/>Delivery Day: Saturday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => DPD - Pat Val Prem Sat
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_saturday
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-21
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [96] => stdClass Object
                (
                    [orderId] => 51035746
                    [orderNumber] => ORD0002070
                    [orderKey] => 2099848511555
                    [orderDate] => 2020-05-05T02:50:30.0000000
                    [createDate] => 2020-05-05T03:24:37.1400000
                    [modifyDate] => 2020-05-05T03:24:43.5470000
                    [paymentDate] => 2020-05-05T02:50:30.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 30404419
                    [customerUsername] => 2964786446403
                    [customerEmail] => inboxixx@hotmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Ariana  Jakovleva
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Ariana  Jakovleva
                            [company] => 
                            [street1] => Flat 29
                            [street2] => Odell House
                            [street3] => 16 Woodberry Down
                            [city] => London
                            [state] => Middlesex
                            [postalCode] => N4 2GB
                            [country] => GB
                            [phone] => 07927 202882
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 82259063
                                    [lineItemKey] => 4582594084931
                                    [sku] => P00016S
                                    [name] => Tropical Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Tropical-Cake.jpg?v=1588447888
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6552799
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-05T03:24:37.077
                                    [modifyDate] => 2020-05-05T03:24:37.077
                                )

                        )

                    [orderTotal] => 38.9
                    [amountPaid] => 38.9
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 27/05/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => shopify_payments
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-25
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [97] => stdClass Object
                (
                    [orderId] => 51035736
                    [orderNumber] => ORD0002075
                    [orderKey] => 2099872301123
                    [orderDate] => 2020-05-05T03:15:28.0000000
                    [createDate] => 2020-05-05T03:24:35.5800000
                    [modifyDate] => 2020-05-05T03:24:43.5470000
                    [paymentDate] => 2020-05-05T03:15:28.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 30404435
                    [customerUsername] => 2964809744451
                    [customerEmail] => sophie.neale@sky.com
                    [billTo] => stdClass Object
                        (
                            [name] => Sophie Neale
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Sophie Neale
                            [company] => 
                            [street1] => 5 Chapel Fields
                            [street2] => 
                            [street3] => 
                            [city] => South Brent
                            [state] => Devon
                            [postalCode] => TQ10 9BS
                            [country] => GB
                            [phone] => 07967 200141
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 82259058
                                    [lineItemKey] => 4582641172547
                                    [sku] => P00001S
                                    [name] => Spring Surprise Cake - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Pinata-Cake-NO-EGGS.jpg?v=1588447805
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 29.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6559585
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-05T03:24:35.517
                                    [modifyDate] => 2020-05-05T03:24:35.517
                                )

                        )

                    [orderTotal] => 39.9
                    [amountPaid] => 39.9
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 20/05/2020<br/>Delivery Day: Wednesday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-18
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [98] => stdClass Object
                (
                    [orderId] => 51035728
                    [orderNumber] => ORD0002078
                    [orderKey] => 2099878461507
                    [orderDate] => 2020-05-05T03:22:12.0000000
                    [createDate] => 2020-05-05T03:24:34.4870000
                    [modifyDate] => 2020-05-06T03:40:34.9170000
                    [paymentDate] => 2020-05-05T03:22:12.0000000
                    [shipByDate] => 
                    [orderStatus] => on_hold
                    [customerId] => 30404416
                    [customerUsername] => 2964816527427
                    [customerEmail] => adam.duckworth@ntlworld.com
                    [billTo] => stdClass Object
                        (
                            [name] => Adam Duckworth
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Adam Duckworth
                            [company] => 
                            [street1] => 81 High Street
                            [street2] => 
                            [street3] => 
                            [city] => Wellingborough
                            [state] => Northamptonshire
                            [postalCode] => NN9 5JN
                            [country] => GB
                            [phone] => 07766 257130
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 82259055
                                    [lineItemKey] => 4582653067331
                                    [sku] => P00001S
                                    [name] => Spring Surprise Cake - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Pinata-Cake-NO-EGGS.jpg?v=1588447805
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 29.95
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6559585
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-05T03:24:34.407
                                    [modifyDate] => 2020-05-05T03:24:34.407
                                )

                        )

                    [orderTotal] => 39.9
                    [amountPaid] => 39.9
                    [taxAmount] => 1.66
                    [shippingAmount] => 9.95
                    [customerNotes] => <br/>Delivery Date: 19/06/2020<br/>Delivery Day: Friday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => Week day delivery
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-06-17
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

            [99] => stdClass Object
                (
                    [orderId] => 51055984
                    [orderNumber] => ORD0002094
                    [orderKey] => 2099903332419
                    [orderDate] => 2020-05-05T03:47:42.0000000
                    [createDate] => 2020-05-05T05:25:40.2100000
                    [modifyDate] => 2020-05-05T09:25:46.0400000
                    [paymentDate] => 2020-05-05T03:47:42.0000000
                    [shipByDate] => 2020-05-22T00:00:00.0000000
                    [orderStatus] => on_hold
                    [customerId] => 30416348
                    [customerUsername] => 2964835074115
                    [customerEmail] => petyag22@gmail.com
                    [billTo] => stdClass Object
                        (
                            [name] => Stoyka Daneva
                            [company] => 
                            [street1] => 
                            [street2] => 
                            [street3] => 
                            [city] => 
                            [state] => 
                            [postalCode] => 
                            [country] => 
                            [phone] => 
                            [residential] => 
                            [addressVerified] => 
                        )

                    [shipTo] => stdClass Object
                        (
                            [name] => Stoyka Daneva
                            [company] => 
                            [street1] => 9 Acacia Grove
                            [street2] => 
                            [street3] => 
                            [city] => New Malden
                            [state] => Surrey
                            [postalCode] => KT3 3BJ
                            [country] => GB
                            [phone] => 07446 649447
                            [residential] => 
                            [addressVerified] => Address validated successfully
                        )

                    [items] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [orderItemId] => 82294954
                                    [lineItemKey] => 4582701334595
                                    [sku] => P00004S
                                    [name] => Triple Chocolate Delight Gateau - 6" (6-8 portions)
                                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Triple-Choc-Gateau-1.jpg?v=1588447846
                                    [weight] => stdClass Object
                                        (
                                            [value] => 352
                                            [units] => ounces
                                            [WeightUnits] => 1
                                        )

                                    [quantity] => 1
                                    [unitPrice] => 28.94
                                    [taxAmount] => 
                                    [shippingAmount] => 
                                    [warehouseLocation] => 
                                    [options] => Array
                                        (
                                        )

                                    [productId] => 6677587
                                    [fulfillmentSku] => 
                                    [adjustment] => 
                                    [upc] => 
                                    [createDate] => 2020-05-05T05:25:40.147
                                    [modifyDate] => 2020-05-05T05:25:40.147
                                )

                        )

                    [orderTotal] => 40.51
                    [amountPaid] => 38.89
                    [taxAmount] => 1.63
                    [shippingAmount] => 9.92
                    [customerNotes] => <br/>Delivery Date: 22/05/2020<br/>Delivery Day: Friday
                    [internalNotes] => 
                    [gift] => 
                    [giftMessage] => 
                    [paymentMethod] => paypal
                    [requestedShippingService] => DPD - Pat Val Premier
                    [carrierCode] => apicode-dpd-local
                    [serviceCode] => dpd_local_parcel_next_day
                    [packageCode] => dpd_local_package
                    [confirmation] => none
                    [shipDate] => 
                    [holdUntilDate] => 2020-05-21
                    [weight] => stdClass Object
                        (
                            [value] => 352.74
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [dimensions] => stdClass Object
                        (
                            [units] => inches
                            [length] => 50
                            [width] => 50
                            [height] => 50
                        )

                    [insuranceOptions] => stdClass Object
                        (
                            [provider] => 
                            [insureShipment] => 
                            [insuredValue] => 0
                        )

                    [internationalOptions] => stdClass Object
                        (
                            [contents] => 
                            [customsItems] => 
                            [nonDelivery] => 
                        )

                    [advancedOptions] => stdClass Object
                        (
                            [warehouseId] => 355188
                            [nonMachinable] => 
                            [saturdayDelivery] => 
                            [containsAlcohol] => 
                            [mergedOrSplit] => 
                            [mergedIds] => Array
                                (
                                )

                            [parentId] => 
                            [storeId] => 241006
                            [customField1] => 
                            [customField2] => 
                            [customField3] => 
                            [source] => web
                            [billToParty] => 
                            [billToAccount] => 
                            [billToPostalCode] => 
                            [billToCountryCode] => 
                            [billToMyOtherAccount] => 
                        )

                    [tagIds] => 
                    [userId] => 
                    [externallyFulfilled] => 
                    [externallyFulfilledBy] => 
                    [labelMessages] => 
                )

        )

    [total] => 924
    [page] => 1
    [pages] => 10
)
