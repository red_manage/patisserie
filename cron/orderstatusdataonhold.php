stdClass Object
(
    [orderId] => 58455408
    [orderNumber] => ORD00019977
    [orderKey] => 2129754423363
    [orderDate] => 2020-05-18T07:26:33.0000000
    [createDate] => 2020-05-18T09:09:29.4470000
    [modifyDate] => 2020-05-19T00:13:44.0270000
    [paymentDate] => 2020-05-18T07:26:33.0000000
    [shipByDate] => 2020-05-20T00:00:00.0000000
    [orderStatus] => awaiting_shipment
    [customerId] => 35294705
    [customerUsername] => 2986743300163
    [customerEmail] => Gl1981@ymail.com
    [billTo] => stdClass Object
        (
            [name] => Gemma Littlewood
            [company] => 
            [street1] => 
            [street2] => 
            [street3] => 
            [city] => 
            [state] => 
            [postalCode] => 
            [country] => 
            [phone] => 
            [residential] => 
            [addressVerified] => 
        )

    [shipTo] => stdClass Object
        (
            [name] => Gemma Littlewood
            [company] => 
            [street1] => 47 Moncktons Avenue
            [street2] => 
            [street3] => 
            [city] => Maidstone
            [state] => Kent
            [postalCode] => ME14 2QF
            [country] => GB
            [phone] => 07855058622
            [residential] => 
            [addressVerified] => Address validated successfully
        )

    [items] => Array
        (
            [0] => stdClass Object
                (
                    [orderItemId] => 93965925
                    [lineItemKey] => 4638752997443
                    [sku] => P00003S
                    [name] => Candy Stripe Gateau - 8" (8-10 portions)
                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/Pink-Candy-Stripe-Cake_1.png?v=1588924068
                    [weight] => stdClass Object
                        (
                            [value] => 352
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [quantity] => 1
                    [unitPrice] => 38.5
                    [taxAmount] => 
                    [shippingAmount] => 
                    [warehouseLocation] => 
                    [options] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [name] => Add-ons
                                    [value] => Inscription (£3.95)
                                )

                            [1] => stdClass Object
                                (
                                    [name] => Inscription
                                    [value] => Happy 70th Birthday Mum
                                )

                            [2] => stdClass Object
                                (
                                    [name] => _io_order_group
                                    [value] => 1589811818574
                                )

                        )

                    [productId] => 6816436
                    [fulfillmentSku] => 
                    [adjustment] => 
                    [upc] => 
                    [createDate] => 2020-05-18T09:09:29.353
                    [modifyDate] => 2020-05-18T09:09:29.353
                )

            [1] => stdClass Object
                (
                    [orderItemId] => 93965926
                    [lineItemKey] => 4638753030211
                    [sku] => P00020S
                    [name] => Inscription
                    [imageUrl] => https://cdn.shopify.com/s/files/1/0272/5848/6851/products/image.jpg?v=1589049201
                    [weight] => stdClass Object
                        (
                            [value] => 352
                            [units] => ounces
                            [WeightUnits] => 1
                        )

                    [quantity] => 1
                    [unitPrice] => 3.95
                    [taxAmount] => 
                    [shippingAmount] => 
                    [warehouseLocation] => 
                    [options] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [name] => _io_parent_order_group
                                    [value] => 1589811818574
                                )

                            [1] => stdClass Object
                                (
                                    [name] => _io_field_name
                                    [value] => Add-ons
                                )

                        )

                    [productId] => 6760417
                    [fulfillmentSku] => 
                    [adjustment] => 
                    [upc] => 
                    [createDate] => 2020-05-18T09:09:29.353
                    [modifyDate] => 2020-05-18T09:09:29.353
                )

        )

    [orderTotal] => 54
    [amountPaid] => 52.39
    [taxAmount] => 1.62
    [shippingAmount] => 9.91
    [customerNotes] => <br/>Delivery Date: 23/05/2020
    [internalNotes] => 
    [gift] => 
    [giftMessage] => 
    [paymentMethod] => paypal
    [requestedShippingService] => Pat Val Premier Delivery
    [carrierCode] => 
    [serviceCode] => 
    [packageCode] => 
    [confirmation] => none
    [shipDate] => 2020-05-20
    [holdUntilDate] => 
    [weight] => stdClass Object
        (
            [value] => 352.78
            [units] => ounces
            [WeightUnits] => 1
        )

    [dimensions] => 
    [insuranceOptions] => stdClass Object
        (
            [provider] => 
            [insureShipment] => 
            [insuredValue] => 0
        )

    [internationalOptions] => stdClass Object
        (
            [contents] => 
            [customsItems] => 
            [nonDelivery] => 
        )

    [advancedOptions] => stdClass Object
        (
            [warehouseId] => 355188
            [nonMachinable] => 
            [saturdayDelivery] => 
            [containsAlcohol] => 
            [mergedOrSplit] => 
            [mergedIds] => Array
                (
                )

            [parentId] => 
            [storeId] => 241006
            [customField1] => 
            [customField2] => 
            [customField3] => 
            [source] => web
            [billToParty] => 
            [billToAccount] => 
            [billToPostalCode] => 
            [billToCountryCode] => 
            [billToMyOtherAccount] => 
        )

    [tagIds] => 
    [userId] => 
    [externallyFulfilled] => 
    [externallyFulfilledBy] => 
    [labelMessages] => 
)
