<?php
include $_SERVER['DOCUMENT_ROOT'] . '/database.php';

// Create connection
$conn = new mysqli($servername, $username, $password, $db);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Deleter query to delete old form data of last before 21 days from current date
$sql = "DELETE FROM shopify_tracktrace WHERE submitdate <= ( CURDATE() - INTERVAL 21 DAY )";
$conn->query($sql);
?>