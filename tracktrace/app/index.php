<?php
include $_SERVER['DOCUMENT_ROOT'] . '/database.php';

// Set variables for our request
$query = array(
  "client_id" => '73cfa5bba993fbe096a41e391472a7da', // Your API key
  "client_secret" => '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', // Your app credentials (secret key)
  "code" => $params['code'] // Grab the access key from the URL
);

// Generate access token URL
$access_token_url = "https://" . $params['shop'] . "/admin/oauth/access_token";

// Configure curl client and execute request
$ch = curl_init();
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_URL, $access_token_url);
curl_setopt($ch, CURLOPT_POST, count($query));
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($query));
$result = curl_exec($ch);
curl_close($ch);

// Store the access token
$result = json_decode($result, true);
$access_token = $result['access_token'];


date_default_timezone_set('Europe/London');

// Create connection
$conn = new mysqli($servername, $username, $password, $db);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Export track trace data in tracktrace.xls file for single store or all store data.
if(isset($_GET['action']) && $_GET['action'] == 'export_list'){
	if($_GET['store']){
		$result = $conn->query('select * from shopify_tracktrace where visited_store = "'.$_GET['store'].'" order by id desc');
	}else{
		$result = $conn->query('select * from shopify_tracktrace order by id desc');
	}

	$columnHeader = '';  
	$columnHeader = "Visited Store"."\t"."Name"."\t"."Email"."\t"."Phone"."\t"."Postcode"."\t"."Date"."\t";  
	$setData = '';  
	if ($result->num_rows > 0) {
		while($user = $result->fetch_assoc()){
	        $value = '"'.$user['visited_store'].'"'."\t".'"'.$user['name'].'"'."\t".'"'.$user['email'].'"'."\t".'"'.$user['phone'].'"'."\t".'"'.$user['postcode'].'"'."\t".'"'.$user['submitdate'].'"'."\t";  
		    $setData .= trim($value) . PHP_EOL;  
		}
	}

	header("Content-type: application/octet-stream");  
	header("Content-Disposition: attachment; filename=tracktrace.xls");  
	header("Pragma: no-cache");  
	header("Expires: 0");  
  	echo ucwords($columnHeader) . "\n" . $setData . "\n"; 
  	exit;
}

?>
<style type="text/css">
	*{
		box-sizing: border-box;
	}
	body{
		padding: 0;
		margin: 0;
		font-family: 'Arial';
		background-color: #f7f7f7;
	}
	.main-box{
	    width: 100%;
    	background: #d9d9d9;
    	font-family: verdana;
    	float: left;
    	height: 100%;
	}
	.main-box .content-box{
		padding: 30px;
		width: 100%;
	}
	.content-box h4{
		font-size: 16px;
		font-weight: bold;
	}
	.main-box .content-box table{
		width: 100%;
		border-collapse: collapse;
		font-size: 14px;
	}
	.main-box .content-box table th{
		background-color: #bfbfbf;
	}
	.main-box .content-box table th,
	.main-box .content-box table td{
		padding: 10px;
	}
	.main-box .content-box table td{
		text-align: center;
	}
	.main-box .content-box tr:nth-child(even) td{
		background-color: #d2cfcf;
	}
	.content-box p {
    text-align: right;
    font-size: 12px;
}
.select_box {
    text-align: right;
}
.select_one {
    float: left;
    width: 50%;
}
.select_two {
    float: right;
    width: 50%;
    display: inline-flex;
    align-items: center;
    padding-left: 44px;
}
.select_two p {
    width: 55%;
    text-align: left;
}
.select_box select#track_visited_store {
    padding: 4px;
    border: 1px solid #ccc;
}
.export_tab {
    text-align: right;
    width: 100%;
    float: right;
    display: inline-block;
}
.export_tab a {
    padding: 10px 20px;
    margin: 13px;
    display: inline-block;
    background: #000;
    width: 87px;
    color: #fff;
    text-decoration: none;
    font-size: 14px;
    border-radius: 5px;
    margin-right: 0px;
}
</style>
<?php

	$curl = curl_init();
	curl_setopt_array($curl, array(
	CURLOPT_URL => "https://de1311fca0a2fb17bb1f52828ae8daeb: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@pat-val-premier.myshopify.com/admin/api/2020-04/locations.json",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 0,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => array(
	"Cookie: __cfduid=db9ea86a18592f2c26102a550468d4f331587629534"
	),
	));

	$response = curl_exec($curl);

	curl_close($curl);
	$html = '<select id="track_visited_store" name="visited_store" class="" required=""><option value="" selected="selected">Select store</option>';
	if($response){
		$results = json_decode($response);
		foreach($results->locations as $location){
			   if($location->id != '34789523523'){
			   	 $html .= '<option value="'.$location->name.'">'.$location->name.'</option>';
			   }
				
		}
	}
	$html .= '</select>';

$result = $conn->query('select * from shopify_tracktrace order by id desc');

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Track Users App</title>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>
<body>
<div class="main-box">
	<div class="content-box">
		<div class="select_one">
		<h4>Track Users</h4>
		</div>
		<div class="select_two">
		<p>Sort by</p>
		<div class="select_box"><?php echo $html; ?></div>
		<div class="export_tab"><a href="javascript:void(0)" id="export_data">Export</a></div>
		</div>
		<table border="1">
			<thead>
				<th>Visited Store</th>
				<th>Name</th>
				<th>Email</th>
				<th>Phone</th>
				<th>Postcode</th>
				<th>Date</th>
			</thead>
			<tbody>
				<?php
					if ($result->num_rows > 0) {
						while($user = $result->fetch_assoc()){ ?>
							<tr>
								<td class="col1"><?php echo $user['visited_store']; ?></td>
								<td><?php echo $user['name']; ?></td>
								<td><?php echo $user['email']; ?></td>
								<td><?php echo $user['phone']; ?></td>
								<td><?php echo $user['postcode']; ?></td>
								<td><?php echo $user['submitdate']; ?></td>
							</tr>
						<?php }
					}
				?>
			</tbody>
		</table>
	</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
	$(function() {    
	    $('body').on('change' ,'#track_visited_store', function() { 
	        $("table td.col1:contains('" + $(this).val() + "')").parent().show();
	        $("table td.col1:not(:contains('" + $(this).val() + "'))").parent().hide();
	    });

	    $('body').on('click','#export_data',function(){
	    	var store = jQuery('#track_visited_store').val();
	    	window.location.href = "?action=export_list&store="+store;
	    });
	});
</script>
</body>
</html>