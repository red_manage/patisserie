<?php 

// Set variables for our request
$shop = "pat-val-premier";
$api_key = "73cfa5bba993fbe096a41e391472a7da";
$scopes = "read_orders,write_orders";
$redirect_uri = "https://matthews42.sg-host.com/tracktrace/app/index.php";

// Build install/approval URL to redirect to
$install_url = "https://" . $shop . ".myshopify.com/admin/oauth/authorize?client_id=" . $api_key . "&scope=" . $scopes . "&redirect_uri=" . urlencode($redirect_uri);

// Redirect
header("Location: " . $install_url);
die();

?>