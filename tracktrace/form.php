<?php
include $_SERVER['DOCUMENT_ROOT'] . '/database.php';

// Create connection
$conn = new mysqli($servername, $username, $password, $db);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Get form values and insert into database 
// Database table shopify_tracktrace
$track_visited_store = $_GET['track_visited_store'];
$track_email = $_GET['track_email'];
$track_name = $_GET['track_name'];
$track_phone = $_GET['track_phone'];
$track_postcode = $_GET['track_postcode'];
$submitdate = date('Y-m-d');
echo 'var success = 0;';
$res = $conn->query('INSERT INTO shopify_tracktrace (`visited_store`, `name`, `email`, `phone`, `postcode`, `submitdate`) VALUES ("'.$track_visited_store.'", "'.$track_name.'", "'.$track_email.'", "'.$track_phone.'", "'.$track_postcode.'", "'.date('Y-m-d').'"  ) ');
if($res){
	echo 'var success = 1;';
}
?>
if(success == 1){
	alert('Form submitted successfully');
	location.reload();
}
