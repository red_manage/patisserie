<?php
$config = parse_ini_file($_SERVER['DOCUMENT_ROOT'] . '/../config.ini', true);
$servername = $config['database']['host'];
$username = $config['database']['username'];
$password = $config['database']['password'];
$db = $config['database']['db'];